#!/usr/bin/python
import h5py
import bisect
import sys

h5filename = "gkwdata.h5"

#a function used to visit the objects in the hdf5 file.
def clip_timedep_dsets(name, object):
    """ discard some of the data from datasets which have the grid1=time attribute. """
    if("grid1" in object.attrs and object.attrs["grid1"] == "time"):
        clip_last_dim(object, clip_index)

def clip_last_dim(object, n):
    """ Clip the data in object, so that the last dimension has n elements."""
    old_shape = object.shape
    newsize = list(object.shape)
    if(earlier_or_later_arg == earlier_option):
        # -> remove the first n values, with respect to the last
        # -> dimension.
        # In order to do this, buffer the data we want to keep.
        tmp = object[..., (n+1):]
        # then resize the dataset
        newsize[-1] -= n+1
        object.resize(newsize)
        # and copy back all the data. probably there is a better syntax...
        object[...,0:newsize[-1]] = tmp
    else:
        # note that the first array dimension as seen from Fortran
        # corresponds here to the *last* dimension (as in C and Java)
        newsize[-1] = n+1
        object.resize(newsize)
    print(object.name+" is clipped from "+str(old_shape)+" to "+str(object.shape))

#a function used to visit the objects in the hdf5 file.
def rm_dsets_at_latest_timestamps(name, object):
    """delete datasets which have a "time" attribute and whose value is
larger than clip_time."""
    if("time" in object.attrs and object.attrs["time"] > clip_time):
        #then discard this dataset, because it was created after the
        #request timestamp.
        print "DELETED: "+name
        del f[name]

def print_usage_and_exit():
    print("""
  Usage:
        %s [--remove-earlier | --remove-later] [ -t <time> | -n <timesteps> ]

  This script acts on %s in the current directory.
  It looks for datasets with the attribute
     grid1 = 'time'
  and clips them according to the value
  given as an argument.
     --remove-earlier  Remove everything before the specified time, including
                       the timestep itself
     --remove-later    Remove everything after the specified time, excluding
                       the timestep itself
        
     -t <time>         the timestamp given in code time units (in general,
                       this is a real number)
     -n <timestep>     the timestamp number (this is an integer), where the
                       starting index is 0

  NOTE: If GKW fails then %s does most likely not contain
  attributes. In that case this script here is useless a priori.

  See also: defer-gkwdatah5-attrs-from-2nd-file.py
"""
    % ( sys.argv[0], h5filename, h5filename))
    exit(1)
        
if __name__ == "__main__":
    if(len(sys.argv) != 4):
        print_usage_and_exit()
    else:
        earlier_option = '--remove-earlier'
        later_option = '--remove-later'
        earlier_or_later_arg = sys.argv[1]
        if(not earlier_or_later_arg in (earlier_option, later_option)):
            print_usage_and_exit()
        clip_earlier = earlier_or_later_arg == earlier_option and True or False
        
        timestamp_option = '-t'
        timestep_option = '-n'
        timetype_arg = sys.argv[2]
        if(not timetype_arg in (timestamp_option, timestep_option)):
            print_usage_and_exit()
        clip_time = timetype_arg == timestamp_option and float(sys.argv[3]) or int(sys.argv[3])
    
    f = h5py.File("gkwdata.h5", "r+")

    try:
        time = f["/grid/time"][0,:]
    except:
        # it is a (1,n) dimensional array
        time = f["/diagnostic/diagnos_growth_freq/time"][0,:]

    #find out how many chunks this is.
    if(timetype_arg == timestamp_option):
        #Find rightmost value less than clip_time
        clip_index = bisect.bisect_left(time, clip_time)
        print(clip_index)
    else:
        clip_index = clip_time

    # clip datasets in those groups:
    f["diagnostic"].visititems(clip_timedep_dsets)
    f["grid"].visititems(clip_timedep_dsets)

    # finally clip the time grid itself
    try:
        clip_last_dim(f["/grid/time"], clip_index)
    except:
        clip_last_dim(f["/diagnostic/diagnos_growth_freq/time"], clip_index)

    # and remove datasets that are created as a whole at later instants
    f.visititems(rm_dsets_at_latest_timestamps)
