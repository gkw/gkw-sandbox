#!/usr/bin/python

# The function below is based on code found on
# http://fusionplasma.co.uk/2013/10/30/Graphing-Fortran-Projects/
# at 2014/10/07.
import re

def build_graph(files,exclude=['hdf5','h5lt','mpi']):
	"""Build a dot graph of the dependency of the modules in the list of files,
	excluding modules named in the list exclude"""

	# Start the graph
	graph = "digraph G {\n"
	deps = {}

	# Pattern for getting the module name.
	p_mod = re.compile("^(?:module|program) ([a-zA-Z0-9_]*)",
	re.IGNORECASE+re.MULTILINE)
	# Pattern for getting the use statements.
	p_use = re.compile("USE ([a-zA-Z0-9_]*),",
	re.IGNORECASE+re.MULTILINE)

	for mod in files:
		with open(mod,'r') as f:
			contents = f.read()

		# Get the true module name
		# Careful! It only gets the first module in the file!
		#if hasattr(re.search(p, contents), 'group'):
		mod_name = re.search(p_mod, contents).group(1)
		#else:
			#print contents

		# Find all the USE statements and get the module
		deps[mod_name] = re.findall(p_use, contents)

	for k in deps:
		# Remove duplicates and unwanted modules
		deps[k] = list(set(deps[k]) - set(exclude))
		for v in deps[k]:
			# Add the edges to the graph
			edge = k + " -> " + v + ";\n"
			graph = graph + edge
 
	# Close the graph and return it
	graph = graph + "}"
	return graph


if __name__ == '__main__':
	from sys import argv
	graph = build_graph(argv[1:-1], [])

	# Print the graph to the standard-output.
	print graph
