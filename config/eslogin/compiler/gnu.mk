## -- Archer HPC UK resource: GNU compiler --

REQUIREDMODULES = PrgEnv-gnu
FC = ftn
FFLAGS_OPT = -O3
FFLAGS_OTHER =
FFLAGS_DOUBLE = -fdefault-real-8 -fdefault-double-8
FFLAGS_OMP = -fopenmp
