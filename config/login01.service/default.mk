## -- Bayreuth Rechenzentrum Cluster using mpiifort intel compiler
# need to load the following modules before compilation:
#    module load intel-cluster-studio-2013
#    module load mpi/intel/4.1.0.030 


OPTFLAGS = on 
DEBUG = off 
PERF=perf

# Uncomment if you need the eigenvalue solver
SLEPC = HAVE_SLEPC
PETSC_DIR=/home/vivaorg/vivaorg_10150057/local_mpiifort
SLEPC_DIR=/home/vivaorg/vivaorg_10150057/local_mpiifort
FFLAGS_INC_SLEPC= -I$(PETSC_DIR)/include
LDFLAGS_SLEPC=-Wl,-rpath,${SLEPC_DIR}/lib -L${SLEPC_DIR}/lib -lslepc -Wl,-rpath,${PETSC_DIR}/lib -L${PETSC_DIR}/lib -lpetsc -lflapack -lfblas \

# Uncomment if you need the HDF5 format - the paths do not yet work for everybody
IO_LIB = HAVE_HDF5
# The compiler flags for HDF5 were taken from $(h5fc -show)
FFLAGS_INC_H5= -I/home/vivaorg/vivaorg_10150057/local_mpiifort/include
LDFLAGS_H5= -L/home/vivaorg/vivaorg_10150057/local_mpiifort/lib /home/vivaorg/vivaorg_10150057/local_mpiifort/lib/libhdf5hl_fortran.a /home/vivaorg/vivaorg_10150057/local_mpiifort/lib/libhdf5_hl.a /home/vivaorg/vivaorg_10150057/local_mpiifort/lib/libhdf5_fortran.a /home/vivaorg/vivaorg_10150057/local_mpiifort/lib/libhdf5.a -L/home/vivaorg/vivaorg_10150057/local_mpiifort/lib -L/home/vivaorg/vivaorg_10150057/local_mpiifort/lib -lsz -lz -lrt -ldl -lm -Wl,-rpath -Wl,/home/vivaorg/vivaorg_10150057/local_mpiifort/lib

FFTLIB = FFT_FFTW3
# Directory where the fft library is located
FFT_LIB=/home/vivaorg/vivaorg_10150057/fftw-3.3.3
FFLAGS_INC_FFTW3= -I$(FFT_LIB)/include
LDFLAGS_FFTW3=-L$(FFT_LIB)/lib -lfftw3

# For performance analysis one can load
#    module load apps/scalasca
# on btrzx3, set
#    FC = scalasca -instrument $(FC)
# and compile as usual.
# This enables scalasca (http://scalasca.org) instrumentation. Default builds
# (without the scalasca wrapper) remain fully optimized
# and without instrumentation.


CC = mpiicc
FC = /cluster/intel/impi/4.1.0.030/bin64/mpiifort
# To compile with the HDF5 lib:
LD = $(FC)

MPI = mpi2

IMPLICIT=umfpack

#SMP = OPENMP
#FFLAGS_OMP = -openmp


REQUIREDMODULES = intel-cluster-studio-2013 \
                   mpi/intel/4.1.0.030

FFLAGS_DOUBLE = -r8
FFLAGS_OPT    = -O3 -no-prec-div
# further interesting optimisation options for the intel compiler:
# -ip -ipo
FFLAGS_DEBUG  = -g -O0 -C -traceback -ftrapuv -check noarg_temp_created,nooutput_conversion
# further interesting debugging options for the intel compiler:
# -warn all -fpe0 -fpe3 -check bounds -WB -check uninit
FFLAGS_OTHER  = -vec-report0

FFLAGS_INC = -I. -I/cluster/intel/impi/4.1.0.030/include64 \
             $(FFLAGS_INC_SLEPC) $(FFLAGS_INC_H5) $(FFLAGS_INC_FFTW3)
LDFLAGS    = $(LDFLAGS_SLEPC) $(LDFLAGS_FFTW3) $(LDFLAGS_H5) \
             -L../../libs/UMFPACK/Lib/ -lumfpack_gkw64_NB -L../../libs/AMD/Lib/ -lamd

