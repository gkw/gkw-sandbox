 GKW - Sandbox Edition
======

This fork of GKW is open for everybody's experiments - if there is need.

have fun

a change, just to test if some issue-related commands in the commit
message have any effect. No luck so far.

I wonder if merging a pull request will fast-forward if there is no conflict.

 LICENSE
=========

(see the LICENSE file)

This file is part of GKW.

GKW is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GKW is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GKW.  If not, see <http://www.gnu.org/licenses/>.
