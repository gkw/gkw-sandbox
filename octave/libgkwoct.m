#!/usr/bin/octave --silent

%% This is a GNU Octave script.

% this is important:
1;



function ret = from_inputdat(pattern,filename)
  %% Use this function like this:
  %%    eval(from_inputdat("vpmax"))
  %%    eval(from_inputdat("mumax"))
  %% Note that this function will grep from input.out, not input.dat!
  %% So logical values are either 'T' or 'F' in this file.
  %% A useful trick is to do it like in this example:
  %%    f = false;
  %%    t = true;
  %%    eval(from_inputdat("spectral_radius"))
  %% Lists of floats can be evaluated like this
  %%  eval([strrep(from_inputdat("parallel_output_timestamps", "input.dat"),'=','=[') , ']'])
  %%
  %% If the optional parameter filename is given,
  %% then given file is read instead of input.out.

  infilename="input.out";
  if(nargin == 2)
      infilename=filename;
  end
  
  commandline = ["grep -w --ignore-case '",pattern,"' ",infilename];
  [status, line_from_inputdat] = system (commandline);
  if(status != 0)
    printf (["problem with ",commandline],"\n");
    exit
  end
  line_from_inputdat = tolower(line_from_inputdat);

  %% remove Fortran comments
  if(index(line_from_inputdat, "!") > 1) 
    line_from_inputdat = line_from_inputdat(1:index(line_from_inputdat, "!")-1);
  end
  
  %% remove a trailing comma
  line_from_inputdat = strrep(line_from_inputdat, ',', '');

  ret = line_from_inputdat;
end


function print_all_formats(basename)
  % if(folder(end) ~= '/')
  %   folder(end+1) = '/';
  % endif

  
  SUFFIX{1} = ".eps";
  PRINTOPTS{1} = "-depsc";
  SUFFIX{2} = ".png";
  PRINTOPTS{2} = "-dpng";

  folder{1} = "eps/";
  folder{2} = "./";

  for i = 1:length(SUFFIX)
    if(~mkdir(folder{i}))
      error(["could not create folder '",folder{i},"'"])
    endif
    print([folder{i},basename,SUFFIX{i}], PRINTOPTS{i})
  endfor
  
endfunction


function hcb = give_imagesc_nancolor(a,colormap_,nancolor)	 
  %% IMAGESC with NaNs assigning a specific color to NaNs
  


  %% find minimum and maximum
  amin=min(a(:));
  amax=max(a(:));
  maxbound = max(abs(amin), abs(amax));
  %% size of colormap
  n = size(colormap_,1);
  %% color step
  %dmap=(amax-amin)/n;
  dmap=(2 * maxbound)/n;

  %% add nan color to colormap
  colormap([nancolor; colormap_]);
  
  %% changing color limits
  %caxis([amin-dmap amax]);
  caxis([-maxbound-dmap maxbound]);
  %% place a colorbar
  hcb = colorbar();
  %%  change Y limit for colorbar to avoid showing NaN color
  %clim(hcb, [amin amax]) %function undefined in octave?

end

function puttext(string, location)
  %% this function allows to write a string on a plot.
  %% instead of coordinates like the usual text(...) functions
  %% it takes a location like in
  %% legend("the foobar function", "location","NorthEast");

  switch(location)
    case 'North'
      t = text(0.5, 0.95, string,
	       'horizontalalignment','center',
	       'units','normalized'
	       );
    case 'NorthEast'
      t = text(0.95, 0.95, string,
	       'horizontalalignment','right',
	       'units','normalized'
	       );
    case 'East'
      t = text(0.95, 0.5, string,
	       'horizontalalignment','right',
	       'units','normalized'
	       );
    case 'SouthEast'
      t = text(0.95, 0.05, string,
	       'horizontalalignment','right',
	       'units','normalized'
	       );
    case 'South'
      t = text(0.5, 0.05, string,
	       'horizontalalignment','center',
	       'units','normalized'
	       );
    case 'SouthWest'
      t = text(0.05, 0.05, string,
	       'horizontalalignment','left',
	       'units','normalized'
	       );
    case 'West'
      t = text(0.05, 0.5, string,
	       'horizontalalignment','left',
	       'units','normalized'
	       );
    case 'NorthWest'
      t = text(0.05, 0.95, string,
	       'horizontalalignment','left',
	       'units','normalized'
	       );
    otherwise
      %South
      t = text(0.5, 0.05, string,
	       'horizontalalignment','center',
	       'units','normalized'
	       );
  endswitch

endfunction

function ret = dlmread_datbin(luname)
  filename = [luname,'.dat'];
  [info, err, msg] = stat(filename);
  if(err ~= -1)
    ret = dlmread(filename);
    return;
  end

  filename = [luname,'.bin'];
  [info, err, msg] = stat(filename);
  if(err ~= -1)
    ret = dlmread(filename);
    return;
  end

  % this option should not exist with newer GKW versions:
  filename = luname
  ret = dlmread(filename);
end
