%!/home/btpp/bt302083/bin/octave --silent

% This is a standalone GNU Octave script.


% You should add
%   addpath("~/gkw/octave");
% to your ~/.octaverc
% or whatever your GKW_HOME folder is, in order to use the
% files where often used functions are defined.
libgkwoct;
libwavelet;

disp("------------- plot_freq_wavelet.m -----------")

graphics_toolkit("gnuplot")
set(0, 'defaultfigurevisible', 'off');

%%%%%% SETTINGS


set(0, 'defaultlinelinewidth', 6);
%set(0, 'defaultaxesygrid','on');

%%%%%%%%% DEFINITIONS



%%%%%%%%% CREATE A COLORMAP - THERE ARE BETTER WAYS, BUT AT LEAST SOMETHING...
%http://cresspahl.blogspot.de/2012/03/expanded-control-of-octaves-colormap.html
%It expects 4 input parameters: N is the number of intermediate
%\\points that your colormap should have. The other three are matrices
%that contain the transitions for each channel.
% Such a matrix has the following form:
%% M = [ 0, b1;
%%       x1,b2;
%%       ...
%%       x_n, b_(n+1);
%%       1, b_(n+1);
%%     ];
function mymap = colormapRGBmatrices( N, rm, gm, bm)
  x = linspace(0,1, N);
  rv = interp1( rm(:,1), rm(:,2), x);
  gv = interp1( gm(:,1), gm(:,2), x);
  mv = interp1( bm(:,1), bm(:,2), x);
  mymap = [ rv', gv', mv'];
  %exclude invalid values that could appear
  mymap( isnan(mymap) ) = 0;
  mymap( (mymap>1) ) = 1;
  mymap( (mymap<0) ) = 0;
end
% create a colormap with a sharp white domain around the center
whitedelta = 0.03;
MR=[0,1; 
    0.5,1;
    0.5+whitedelta,0.7; %this is the important extra point
    1,0];
MB=[0,0; 
    0.5-whitedelta,0.7; %this is the important extra point
    0.5,1;
    1,1];
MG=[0,0;
    0.5-whitedelta,0.7; %this is the important extra point
    0.5,1;
    0.5+whitedelta,0.7; %this is the important extra point
    1,0];
ncolors = 500;
hot2cold = colormapRGBmatrices(ncolors,MR,MG,MB);


%%%%%%%% LOAD DATA
try
  % load with new bindings
  pkg load hdf5oct
  s.diagnostic.diagnos_freq_wavelet.phi_time_wavelet = h5read("gkwdata.h5","/diagnostic/diagnos_freq_wavelet/phi_time_wavelet");
catch
  try
    % load with old bindings
    h5filename = 'gkwdata.h5'
    s = load('-hdf5', h5filename, 'diagnostic', 'grid', 'geom');
  catch
    % load ascii data
    s.diagnostic.diagnos_freq_wavelet.phi_time_wavelet = dlmread_datbin('phi_time_wavelet');
  end
end

eval(from_inputdat("dtim"));

f_nyquist = 1/(2*dtim)

%%%%%%%%%%%%%%

data = s.diagnostic.diagnos_freq_wavelet.phi_time_wavelet;

% NOTE the very last value of the data sequence output by the
% diagnos_freq_wavelet diagnostic is the total average! Therefore
% consider all except the last element for now.

levels = make_wavelet_scales_sequence(length(data)-1);

wavelet_data = rearrange_wavelet_sequence(data(1:end-1), levels)

freq_axis = get_scale_axis(f_nyquist, levels, wavelet_data)

time_axis = get_horiz_axis(dtim, levels, wavelet_data)


wavelet_plotmatrix = make_wavelet_plotmatrix(levels, wavelet_data, data(end));

nancolor = [0.7 0.95 0.7]; % light green
nancolor = [0.9 0.9 0.9]; % grey
%cmap = [cold2(end:-1:1,:) ; hot2 ];
%cmap = diverging_map(0:0.002:1, [0.230, 0.299, 0.754], [0.706, 0.016, 0.150])
cmap = diverging_map(0:0.002:1, [0, 0, 1], [1, 0, 0]);

imagesc(time_axis, freq_axis, wavelet_plotmatrix)
give_imagesc_nancolor(wavelet_plotmatrix, cmap, nancolor);
xlabel ("time [{t_{ref}}]");
%ylabel ("frequency [{1/t_{ref}}]");
ylabel ("frequency index i, corresp. to {f_{Ny}/2^i}");
colorbar("EastOutside");
print_all_formats("time_wavelet") 

imagesc(time_axis, freq_axis(4:end,:), wavelet_plotmatrix(4:end,:));
give_imagesc_nancolor(wavelet_plotmatrix(4:end,:), cmap, nancolor);
xlabel ("time [{t_{ref}}]");
%ylabel ("frequency [{1/t_{ref}}]");
ylabel ("frequency index i, corresp. to {f_{Ny}/2^i}");
colorbar("EastOutside");
print_all_formats("wavelet_slowfiltered")

## disp("%%%%%%%%%%%%% plot test data")
## try
##   data = s.diagnostic.diagnos_freq_wavelet.test_wavelet_trafo;
##   wavelet_data = {};
##   log2(length(data))
##   for i = 1:log2(length(data))
##     wavelet_data{i} = data(end/2+1:end);
##     data = data(1:end/2);
##   endfor
##   wavelet_data
##   wavelet_plotmatrix =nan;
##   for l = 1:length(wavelet_data)
##     for i = 1:length(wavelet_data{l})
##       % the dataelement {l}{i} is appended 2**(l-1) times to the column l.
##       cellwidth = 2**(l-1);
##       % start index of this cell:
##       m = (i-1)*cellwidth+1;
##       wavelet_plotmatrix(l,m:m+cellwidth-1) = wavelet_data{l}(i);
##     end
##   end
##   %% and put also the cell with the total average value
##   wavelet_plotmatrix(end+1,:) = data(1);

##   wavelet_plotmatrix % debug output

##   give_imagesc_nancolor(wavelet_plotmatrix, copper(), nancolor)
##   xlabel ("time");
##   ylabel ("frequency");
##   colorbar("EastOutside");
##   print_all_formats("wavelet_test")


##   mean(s.diagnostic.diagnos_freq_wavelet.test_wavelet_orig)
## catch
##   disp("test data is not available")
## end_try_catch
