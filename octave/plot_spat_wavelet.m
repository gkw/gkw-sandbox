#!/home/btpp/bt302083/bin/octave --silent

# This is a GNU Octave script.

# If this script won't run, it may be because 

# is not found. In this case put the line
# addpath("~/da/utils");
# with the folder that contains it to the file ~/.octaverc .
libgkwoct;
libwavelet;

disp("------------- plot_spat_wavelet.m -----------")

graphics_toolkit("gnuplot")
set(0, 'defaultfigurevisible', 'off');

###### SETTINGS

set(0, 'defaultlinelinewidth', 6);
%set(0, 'defaultaxesygrid','on');

%%%%%%%% LOAD DATA

eval(from_inputdat("ntime"));

try
  % load with new bindings
  pkg load hdf5oct
  s.diagnostic.diagnos_spat_wavelet.phi_time_wavelet = h5read("gkwdata.h5","/diagnostic/diagnos_spat_wavelet/phi_spat_wavelet");
catch
  try
    h5filename = 'gkwdata.h5'
    s = load('-hdf5', h5filename, 'diagnostic', 'grid', 'geom');
  catch
    s.diagnostic.diagnos_spat_wavelet.phi_spat_wavelet = reshape(dlmread_datbin('phi_spat_wavelet.dat'),ntime);
    s.grid.time = dlmread_datbin('time');
  end
end

eval(from_inputdat("nx"));
n_x_grid = nx
s.diagnostic.diagnos_grid.lxn
dx = s.diagnostic.diagnos_grid.lxn/n_x_grid

k_nyquist = 1/(2*dx)

%%%%%%%%%%%%%%

data = s.diagnostic.diagnos_spat_wavelet.phi_spat_wavelet';

maxval = max(max(data))
minval = min(min(data))

levels = make_wavelet_scales_sequence(size(data,1)-1);

nancolor = [0.9 0.9 0.9]; % grey
%cmap = [cold2(end:-1:1,:) ; hot2 ];
%cmap = diverging_map(0:0.002:1, [0.230, 0.299, 0.754], [0.706, 0.016, 0.150])
cmap = diverging_map(0:0.002:1, [0, 0, 1], [1, 0, 0]);

ibegin = 1
for i = ibegin:size(data,2)
  disp(["--- ",num2str(i)," ---"])

  wavelet_data = rearrange_wavelet_sequence(data(1:end-1,i), levels);

  if(i == ibegin)
    k_axis = get_scale_axis(k_nyquist, levels, wavelet_data);

    x_axis = get_horiz_axis(dx, levels, wavelet_data);
  end

  wavelet_plotmatrix = make_wavelet_plotmatrix(levels, wavelet_data, data(end,i));

  imagesc(x_axis, k_axis, wavelet_plotmatrix)
  give_imagesc_nancolor(wavelet_plotmatrix, cmap, nancolor);
  caxis([minval maxval]);
  xlabel ("radial coord. ");
  ylabel ("scale index i, corresp. to {k_{Ny}/2^i}");
  puttext(["{t_N = ",num2str(s.grid.time(i), "%6f"), "}"], "SouthEast")
  colorbar("EastOutside");
  print_all_formats(["x_wavelet",num2str(i, "%06d")])
end
