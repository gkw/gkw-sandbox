
1;

function levels = make_wavelet_scales_sequence(data_length)
  levels = [1];
  while(length(levels) < data_length)
    levels = [levels levels];
    levels(end+1) = levels(end) + 1;
  end
  % not all of these indices are probably necessesary, if the run does
  % not happen to have exactly the right number of timesteps.
  levels = levels(1:data_length);
end


function wavelet_data = rearrange_wavelet_sequence(data, levels)
  % the sequence in 'levels' is how the data comes out of the freq_wavelet diagnostic of GKW.
  % Using this sequence, the data is rearranged.
  wavelet_data = {};
  for i = 1:length(data)
    wavelet_data{levels(i)}(end+1) = data(i);
  end
  %wavelet_data % debug output
end


function scale_axis = get_scale_axis(f_nyquist, levels, wavelet_data)
  nf = max(levels);
  nt = length(wavelet_data{1})
  for i = 1:nt
    scale_axis(:,i) = f_nyquist./2.**(0:(max(levels)-1));
    % as the above is not yet ideal (because the imagesc cannot deal
    % with nonlinearly spaced axis values, it seems), take the simple
    % index numbers:
    scale_axis(:,i) = (0:(max(levels)-1));
  end
  scale_axis(end+1,:) = max(levels);
  %scale_axis(end,:) = 0; % does not work :-( imagesc does not take it
end


function horiz_axis = get_horiz_axis(delta, levels, wavelet_data)
  nf = max(levels);
  for i = 1:nf
    horiz_axis(i,:) = (1:length(wavelet_data{1})) * 2*delta;
  end
end

function wavelet_plotmatrix = make_wavelet_plotmatrix(levels, wavelet_data, totavg)
  %%%% in order to plot it, create now a rectangular matrix from the ragged wavelet data structure.
  %% create the matrix
  wavelet_plotmatrix(1:max(levels), 1:length(wavelet_data{1})) = nan;
  %% fill it
  for l = 1:length(wavelet_data)
    for i = 1:length(wavelet_data{l})
      % the dataelement {l}{i} is appended 2**(l-1) times to the column l.
      cellwidth = 2**(l-1);
      % start index of this cell:
      m = (i-1)*cellwidth+1;
      wavelet_plotmatrix(l,m:m+cellwidth-1) = wavelet_data{l}(i);
    end
  end
  %% and put also the cell with the total average value
  totavg
  wavelet_plotmatrix(end+1,:) = totavg;
end

