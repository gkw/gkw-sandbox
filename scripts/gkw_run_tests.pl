#!/bin/sh
#  -*-Perl-*-  (for Emacs)    vim:set filetype=perl:  (for vim)
#============================================================================#
# Run the right perl version:
if [ -x /usr/local/bin/perl ]; then
    perl=/usr/local/bin/perl
elif [ -x /usr/bin/perl ]; then
    perl=/usr/bin/perl
else
    perl=`which perl | sed 's/.*aliased to *//'`
fi

exec $perl -x -S $0 "$@"     # -x: start from the following line
#============================================================================#
#! /Good_Path/perl -w
# line 17
#
# Name:   gkw_run_tests.pl
# Original Author: aps (A.P.Snodin@warwick.ac.uk)
# SVN:    $Id: gkw_run_tests 1245 2009-11-21 03:56:53Z phsgbq $
# Description:
#
#       Script to run test problems for the gkw code.
# 
#----------------------------------------------------------------------------#

use strict;
use File::Compare;
use Getopt::Long;
use POSIX;
use Sys::Hostname;
use File::Copy;
use File::Path qw(mkpath);

$SIG{'INT'} = 'INT_handler';

my $help;
my $barrier =
    "+----+------------------------------------------------------------\n";
my $FORCE_NONE = 3;
my $FORCE_SERIAL = 1;
my $FORCE_PARALLEL = 2;
my $force_serialparallel = 0;
my $force_serialruns_w_mpirun = 0;
    
my $mpiruncmd      = "mpirun";        # the command to run mpi executables
my $hn              = hostname;
chomp($hn);
my $cwd = getcwd;
chomp($cwd);

my @testdirs;
my %cap;

# information about the tests
my @failed_tests_running;    # somewhere to put the output
my @failed_tests_output;
my $nfailed_running  = 0;
my $nfailed_output   = 0;
my $ntests_completed = 0;
my $ntests_failed    = 0;

my (%opts);                  # variables written by GetOption
(my $cmdname = $0) =~ s{.*/}{};

# How do we determine if the code ran succesfully?
my $successful_run_line =
  "Run successfully completed";    # find this in the output

# get input from various switches
Getopt::Long::config("bundling");
GetOptions(
    \%opts,
    qw( -h     --help
        -l     --list
        -s     --serial
        -p     --parallel
        -m     --mpirun
               --clean
        -d=s   --code_home=s
        -e=s   --executable=s
        -i=s   --inputfile=s
      )
) or $help = 1, die "$!\n";

# get and check the GKW_HOME environment variable
my $code_home = ($opts{'d'} || $opts{'code_home'} || "$ENV{GKW_HOME}" || "0");
if ("$code_home" eq "0")
{
    die
      "You need to set the environment variable GKW_HOME to where the GKW code is located.\n";
}


# the standard directory to look for testcases
my $default_testdir = "$code_home/tests/standard";
my @testcases = ("$default_testdir");

# where to look for executables (relative to the GKW_HOME directory for now)
my $execdir = "$code_home/run";


# ARGV holds now the command line arguments which were not parsed.
# Any remaining command line arguments should be
#  * input files or
#  * directories
if (@ARGV) {
    @ARGV = map { s{^(?!/)}{$cwd/}; $_ } @ARGV;
    for my $d (@ARGV) {
	#print "$d\n";
    }
    @testcases = @ARGV ;
    chomp(@testcases);
}

if ($opts{'h'} || $opts{'help'}) { $help = 1; usage(); }
if ($opts{'s'} || $opts{'serial'})
{
    $force_serialparallel = $force_serialparallel | $FORCE_SERIAL;
}
if ($opts{'p'} || $opts{'parallel'})
{
    $force_serialparallel = $force_serialparallel | $FORCE_PARALLEL;
}
if($force_serialparallel == 0)
{
    $force_serialparallel = $FORCE_NONE;
}

if ($opts{'m'} || $opts{'mpirun'})
{
    $force_serialruns_w_mpirun = 1
}

# if the executable is specified as a command line argument, read it.
my $exe = ($opts{'e'} || $opts{'executable'} || "0");
if ("$exe" eq "0")
{
    # default: find all the executables in the GKW run directory
    # and sort
    my @executables = find_gkw_testing_executables("$execdir");
    $exe = $executables[0] || "_none_";
}
print "Tests are performed with the executable \n\n";
print " * $exe\n";
print "\n";

# if the executable is specified as a command line argument, read it.
my $input_filename = ($opts{'i'} || $opts{'inputfile'} || "0");
if ("$input_filename" eq "0")
{
    # default: the input filename for GKW
    $input_filename = "input.dat";
}

# Try to parse the capabilities of the executable.
# At the moment this is not used.
#get_executable_capabilities($default_testdir, $exe);


# check if the h5diff command is available
print "Check if h5diff is available...";
my $h5diff_avail = (system("h5diff --version") == 0);
if(!$h5diff_avail)
{
    print "-> differences in HDF5 files will not be checked.\n";
}else{
    print "-> differences in HDF5 files can be checked.\n";
}

# do the tests
perform_tests();

die "\n\n" . " DONE " . "\n";

#=============================================================================

sub usage
{
    print STDERR << "EOF";

Usage:  $cmdname [options] [dir1 [dir2 [..]]]

Run GKW testcases or show information about them.

If one or more directories are given, they will be searched
recursively for testcases.
If no directories are given, then all testcases below the folder
    $default_testdir
will be run.

In addition, it is checked if the source code, consisting of all
Fortran files (filenames match *[Ff]90) below the code_home,
contains non-ascii characters or tabulators. If it does, a message is
displayed as testcase no. -1

For now, the script looks for any executable files gkw*.x in the
directory
    $execdir
The default is to test the executable with the most recent
modification time.  You can set a previously compiled executable to be
the one to test by simply doing "touch <your-old-gkw-executable> ".

Options:
  -h,  --help              Display this usage message
  -l,  --list              List the test input files, then exit
  -d,  --code_home=<dir>   Use <dir>, instead of the directory specified
                           in the environment variable \$GKW_HOME
  -e,  --executable=<file> Use <file> as the executable, instead of looking for
                           the most recent GKW executable
  -i,  --inputfile=<name>  Use <name> as name for input
                           files, instead of 'input.dat'
  -s,  --serial            Consider only the serial tests
  -p,  --parallel          Consider only the parallel tests
  -m,  --mpirun            Do the serial runs with $mpiruncmd, too.
       --clean             Clean up the test directories, then exit

Mandatory arguments to long options are also mandatory
for any corresponding short options.
       
Examples:
  # run all tests in the default folder:
  $cmdname
  # run all tests in the given folders:
  $cmdname extra/
  # run tests using the executables and
  # test files found in /tmp/gkw:
  $cmdname -d /tmp/gkw

  # run all parallel nonlinear testcases of the
  # standard test set:
  $cmdname --parallel \$(grep -ril 'non_linear[^fF]*[tT]' \$GKW_HOME/tests/standard | sed -e 's\|[^/]*\$\|\|' | uniq)
  # run all serial testcases which produce the 'kxspec' file
  $cmdname --serial \$(find \$GKW_HOME/tests/standard -name 'kxspec*' | sed -e 's\|[0-9]*/[^/]*\$\|\|' | uniq)
    
  # list all serial testcases below the current directory:
  $cmdname --serial -l .

  # test Arne's RMHD solver
  $cmdname -e ~/linar/linar -i 'linar_input.dat' ~/linar/test

Testcases:
  A testcase is a folder which contains
     * the file $input_filename
     * all further necessary files, like restart files. These files should not
       have the suffixes .txt or .info .
     * a subfolder reference/ with all output files the test run is to compare
       against. When you construct a new testcase, you first generate the
       reference files by running with an old (trusted) excutable, then copy
       them to the reference folder.
       Note that binary files cause difficulties in comparison, you should
       consider testing them just via the md5 checksum.
     * From a trusted run, one can generate md5 checksums with the command
         md5sum onefiletocheck.dat otherfiletocheck > checksums.md5
       If this file containing the checksums is put to
       reference/checksums.md5, then the script will compare the md5 sums
       of the testruns with them.
     * one or several (empty) run folders with numbers as names. This causes
       test runs with the respective number of MPI processes.
       Example: the subfolders 1/ and 8/ exist. Thus, this testcase is once run
       serially and once with 8 MPI processes.
  Note that files with the suffixes *.txt or *.info like COMMENTS.TXT are not
  copied to the run folders.

Report bugs to http://www.code.google.com/p/gkw
EOF
    exit;
}

#------------------------------------------------------------------------------
sub perform_tests
{

    # get the right mpirun command (or mpiexec) to use later
    get_mpiruncmd();

    print "\n\n";

    if ($opts{'l'} || $opts{'list'}) {
	print "Numbers in brackets denote the number of MPI processes.\n\n";
    }

    print "$barrier";
    printf "| %2d | Check if source code is ASCII and does not contain tabulators:\n", -1;
    print "+----+\n";
    if(check_code_is_ascii_notabs()) {
	print "\n OK - only ASCII letters, no tabs.\n";
    } else {
	print "\n BAD - there are non-ASCII letters or tabs!\n";
    }

    # loop over the given testcase directories.
    foreach my $file (@testcases) {
	if ( -d $file) {
	    #print "I will now look for testcases in $file\n";
	    
	    # $file might be either a folder containing a testcase
	    # or a folder which contains subfolder containing several testcases
	    # (hence a testcase set)

	    visit_folder($file);
	    
	} elsif ( -f $file ) {
	    print "$barrier";
	    print "$file is a file, not a folder. Give me folders!\n\n";
	}
    }

    print_summary();

    # exit with nonzero exit code if there were failed runs or tests
    exit ($ntests_failed > 0);
}

sub visit_folder
{
    my $tstdir = shift;
    #print "** VISIT $tstdir\n";

    # check if it contains an input file
    if ( is_a_testcase($tstdir) ) {
	# This means the folder $tstdir contains one testcase.

	if ($opts{'l'} || $opts{'list'}) {
	    # We only list it.
	    my @nprocs_list = get_procs($tstdir);
	    my $nprocs = join(',', @nprocs_list);
	    chomp($nprocs);
	    print " $tstdir  \($nprocs\)\n";
	} else {
	    # Execute the requested runs.
	    perform_one_test($tstdir);
	}
	
    } else {
	# check if the subfolders contain testcases
	opendir(my $DIR, $tstdir)
          || die "get_test_directories: can't open $tstdir to find tests\n";
        # get a list of subfolders, excluding such which begin with a dot.
	my @subfolders = grep { /^[^\.]/ && -d "$tstdir/$_" } readdir($DIR);
        closedir $DIR;

	 # sort lexically
	@subfolders = sort @subfolders;

	# look into every subfolder
	foreach my $subfolder (@subfolders) {
	    #print " I am visiting $tstdir/$subfolder\n";
	    if ( -d "$tstdir/$subfolder" ) {
		# this might be either a folder containing a testcase
		# or a folder which contains subfolder containing several testcases
		# (hence a testcase set)
		visit_folder("$tstdir/$subfolder");
	    }
	}
    }
}

sub is_a_testcase
{
    my $tstdir = shift;
    return ( -e "$tstdir/$input_filename" && -d "$tstdir/reference" );
}

#--------------------------------------------------------------------------------

sub perform_one_test
{
    my $dir = shift;
    
    print "$barrier";

    chdir($dir) || die "cannot cd to $dir \n";
    printf "| %2d | In folder %s:\n", $ntests_completed, $dir;
    print "+----+\n";
    my @procs           = get_procs($dir);
    my @reference_files = get_reference_files($dir);

    # perform one simulation run for each requested parallelisation
    # scheme.
    RUN: foreach my $nprocs (@procs)
    {
	# delete all preexisting files
	clean_dir("$dir/$nprocs");
	
	if ($opts{'clean'}) {
	    next RUN;
	}
	my $code_output = "$successful_run_line";

	# go into the run folder
	chdir("$dir/$nprocs");

	# copy necessary files of this testcase into the run folder.
	opendir(my $DIR, $dir);
	# version 1: just copy all files
	#my @files_to_copy = grep { -f "$dir/$_" } readdir($DIR);
	# version 2: copy all files, except those whose name
	# contains the given strings. This allows to put a COMMENTS.txt next to
	# the file necessary for the testcase.
	my @files_to_copy = grep { !/txt|info/ && -f "$dir/$_" } readdir($DIR);
	closedir $DIR;
	foreach my $file_to_copy (@files_to_copy)
	{
	    copy ("$dir/$file_to_copy","$dir/$nprocs/$file_to_copy");
	}

	##########################################################
	# Run the simulation
	$code_output = run_code($nprocs);
	##########################################################

	chdir("$dir");
	
	if (check_run($code_output))
	{
	    print "The run terminated successfully.\n";
	    print "The avail. reference files are:\n";
	    print "@reference_files\n";

	    if (perform_checks($nprocs,@reference_files))
	    {
		print "Test with $nprocs processes failed.\n\n";
		$failed_tests_output[$nfailed_output] = "$dir $nprocs";
		$nfailed_output = $nfailed_output + 1;
		$ntests_failed  = $ntests_failed  + 1;
	    }
	    else
	    {
		print "Test with $nprocs processes successful.\n\n";
	    }
	}
	else
	{
	    print "#  RUN FAILED! (check output below)\n";
	    print "\n\n$code_output\n\n";
	    print "#  RUN FAILED! (check the output above)\n";
	    
	    $failed_tests_running[$nfailed_running] = "$dir $nprocs";
	    $nfailed_running = $nfailed_running + 1;
	    $ntests_failed   = $ntests_failed   + 1;
	}
			
    }
    # Successful or not, this test is completed
    $ntests_completed++;

}

#------------------------------------------------------------------------------
# clean up the local run directory
# (uses gkw_clean_run for now)
#

sub clean_dir
{
    my $dir = shift;
    my $cleanstat = 1;
    my $cleanout  = `rm -f $dir/*`;
    #or like that?
    #unlink glob "$dir/*";
    return
}

#------------------------------------------------------------------------------
# run the code and pipe both stdout and stderr to the file called 'out'.
#
sub run_code
{

    my $procs   = shift;
    my $output  = "hello";
    my $runstat = 0;
    my $runcmd;

    # set and print run command
    if ($procs == 1 && $force_serialruns_w_mpirun == 0)
    {
        $runcmd = "$exe 2>&1";
    }
    elsif ($procs == 1 && $force_serialruns_w_mpirun == 1)
    {
        $runcmd = "$mpiruncmd -np 1 $exe 2>&1";
    }
    elsif ($procs > 1)
    {
        $runcmd = "$mpiruncmd -np $procs $exe 2>&1";
    }

    print "#  running ... (with $runcmd)\n";

    # run the test, putting the std output into the variable $output.
    # schedule the run with low priority to allow backups processes and working.
    $output = `(nice -n +15 $runcmd | tee out)`;

    return ($output);

}

#------------------------------------------------------------------------------
# check the run exited successfully
#
sub check_run
{
    my $output  = shift;
    return ($output =~ /$successful_run_line/);
}

#------------------------------------------------------------------------------
# perform checks on the various reference files
#
sub perform_checks
{
    my $failed           = 0;
    my $nprocs           = shift;
    my @reference_files  = @_;
    my $nreference_files = @reference_files;
    print "performing checks...\n";
    if ($nreference_files eq 0)
    {
        print "(no files to check)\n";
    }
    foreach my $file (@reference_files)
    {
	if($file =~ /\.h5/ && $h5diff_avail)
	{
	    print "comparison $nprocs/$file <-> reference/$file : ";
	    
	    my $h5difference = `h5diff --relative=1.0e-8 $nprocs/$file reference/$file`;
	    if($?)
	    {
		print "OK - no large differences in the HDF5 datasets";
	    }
	    else
	    {
		print "BAD!\n\n";
		print "$h5difference";
		# h5diff reports the actual differences with the
		# option --report .
		$failed = 1;
	    }
	}
	elsif($file !~ /\.md5/)
	{
	    print "comparison $nprocs/$file <-> reference/$file : ";
	    if (compare("$nprocs/$file", "reference/$file") == 0)
	    {
		print "OK - no difference\n";
	    }
	    else
	    {
		print "BAD!\n\n";
		#  print "diff output/$file reference/$file\n\n";
		my $difference = `diff $nprocs/$file reference/$file`;
		print "$difference\n\n";
		$failed = 1;
	    }
	}
    }
    # check md5 checksums
    my $checksumfile = "reference/checksums.md5";
    if( -e $checksumfile)
    {
	chdir($nprocs);
	my $checkmsg = `md5sum --check ../$checksumfile 2>&1`;
	print "MD5 checksums:\n";
	print $checkmsg;
	if ( $? != 0 )
	{
	    $failed = 1;
	}
    }
    
    return $failed;
}

#------------------------------------------------------------------------------
# obtain the reference files for this test
#
sub get_reference_files
{
#FIXME
    my $dir = shift;
    my @files;
    my @to_test;

    if (!opendir(DIR, "$dir/reference"))
    {
        print "No reference files for this run\n";
        return 1;
    }
    else
    {
        @to_test = readdir(DIR);
        closedir(DIR);
    }

    foreach my $file (@to_test)
    {
        if (-f "$dir/reference/$file")
        {
            push(@files, "$file");
        }
    }

    return @files;
}

#------------------------------------------------------------------------------
# obtain the number of processors for this test
#
sub get_procs
{
    # HOW IT IS DONE AT THE MOMENT:
    # return a list of subfolders whose names are made up of numbers only.

    my $dir        = shift;
    my @nprocs;

    opendir(DIR, $dir)
      || die
      "get_procs: can't open $dir\n";
      #
      while (my $file = readdir(DIR))
      {
        if ($file =~ m/^[0-9]+$/ )
        {
            #print " found $file\n";
            push(@nprocs, "$file");
        }
    }
    closedir DIR;

    # # ALTERNATIVE:
    # # parse the line of the input file which contains
    # # the keyword 'nmp'
    # my $nprocs_label        = "nmp";           # no. of procs string in input file


    # my $procs_line = `grep nmp $dir/$input_filename || echo "1"`;

    # $procs_line =~ s/.*\!{2,}.*$//g;    # remove lines with 2 or more "!"
    # $procs_line =~ s/.*nmp\=//g;        # remove everything before "="
    # chomp($procs_line);

    # @nprocs = split(/,/, $procs_line);

    if ($force_serialparallel == $FORCE_SERIAL)
    {
	# remove all elements from the array @nprocs which are different than "1"
	@nprocs = grep { $_ == "1" } @nprocs;
    }

    if ($force_serialparallel == $FORCE_PARALLEL)
    {
	# remove all elements from the array @nprocs which are equal to "1"
	@nprocs = grep { $_ != "1" } @nprocs;
    }

    # return an ARRAY of strings, each element being a number of processors
    return @nprocs;
}


#---------------------------------------------------------------------------
# print a summary of the tests
#
sub print_summary
{

    print "\n------------------ SUMMARY -----------------------------\n";
    print "\n";
    #print "# input files: $ntests\n"; # FIXME this is not counted at the moment
    print "# failures: $ntests_failed\n";
    print "   (running): $nfailed_running\n";
    print "  (checking): $nfailed_output\n";
    print "\n";
    if ($ntests_failed > 0)
    {

        if ($nfailed_running > 0)
        {
            print "failed to run:\n";
            print "\n";
            foreach my $testfile (@failed_tests_running)
            {
                print "* $testfile\n";
            }
	    print "\n";
        }
        if ($nfailed_output > 0)
        {
            print "failed tests:\n";
            print "\n";
            foreach my $testfile (@failed_tests_output)
            {
                print "- $testfile\n";
            }
        }
    }
    else
    {
        print "All tests successful\n";
    }

}

#---------------------------------------------------------------------------------
# find the GKW executable to test; $GKW_HOME/run
#
sub find_gkw_testing_executables
{

    my $dir = shift;
    my @testing_executables;

    #print "looking for testable executables...\n\n";

    opendir(DIR, $dir)
      || die
      "find_testing_executables: can't open $dir to find executables\n";

    # Look for gkw*.x in the directory and see if it is executable;
    # if so, add it to the list of executables.
    while (my $file = readdir(DIR))
    {
        if ($file =~ /gkw.*\.x/ && -x "$dir/$file")
        {

            #            print " found $file\n";
            push(@testing_executables, "$dir/$file");
        }
    }
    closedir DIR;

    # sort the executables from new to old in modification time
    @testing_executables = sort { -M $a <=> -M $b } @testing_executables;

    return @testing_executables;
}

#---------------------------------------------------------------------------------
# get the right mpirun command
#
sub get_mpiruncmd
{

    # set mpiruncmd via environment variable if present
    if (exists $ENV{GKW_MPIRUNCMD})
    {
        print "Setting mpiruncmd via \$GKW_MPIRUNCMD:\n";
        $mpiruncmd = "$ENV{GKW_MPIRUNCMD}";
	print "mpiruncmd = $mpiruncmd";
        return 0;
    } else {
        print "** you may need to set the environment variable\n";
        print "** GKW_MPIRUNCMD to the correct run command\n";
        print "** using default: $mpiruncmd";
        #die "done";
    }
}

#-----------------------------------------------------------------------------
#
# This is a 'special testcase' : The external command checks if the GKW source
# code contains only ASCII letters without tabulators, and complains otherwise.
#
sub check_code_is_ascii_notabs
{
    # print `find $code_home -regextype posix-basic -name '[^\#]*[Ff]90'`;
    # print '-----------------';
    # check everything below the code_home folder:
    my $extcmd_output = `find $code_home -name '[a-zA-Z0-9_]*[Ff]90' -exec grep --with-filename --perl-regexp --line-number "[\x80-\xFF\t]" '{}' \\;`;
    print $extcmd_output;
    return ($extcmd_output eq "");
}

#-----------------------------------------------------------------------------
sub get_executable_capabilities
{

    my ($clean_directory, $executable) = @_;
    chdir($clean_directory) || die "cannot cd to $clean_directory\n";
    if (-d "$input_filename") { unlink "$input_filename"; }
    if ($executable eq "_none_")
    {
        print "ERROR: NO executables found!\n";
        print "(maybe you need to compile the code)\n";
        die;
    }
    my $basic_output = `$executable`;
    #print "$basic_output\n";

    #GKW_SPECIFIC:
    if ("$basic_output" =~ m/GKW/)
    {
        print "The executable\n" . " $executable\n" . "appears to be fine.\n";

        #        return 1;
    }
    else
    {
        print "ERROR: The executable "
          . "$executable"
          . " is either broken or is\n";
        print "not GKW. Please check and try again!\n";
        #print "output:\n";
        #print "$basic_output\n";
        die;
    }

    # rather than print the basic output, the information should be put into
    # variables.
    my $capvar;
    my $capval;
    ##while my $line ($basic_output) {
    ##	chomp($line);
    ##    if ( "$line" =~ m/\:/ ) {
    ##        ($capvar, $capval) = split(/:/, $line);
    ##        print "capvar: $capvar - capval: $capval\n";
    ##    }
    ##}
    open(EXOUT, "$executable|");
    my @lines = <EXOUT>;
    close(EXOUT);
    foreach my $line (@lines)
    {
        $line =~ s/\s//g;
        if ("$line" =~ m/\:/)
        {
            ($capvar, $capval) = split(/:/, $line);
            #print "##$line\n";
            chomp($capvar);
            chomp($capval);
            print "$capvar\=$capval\n";
            $cap{$capvar} = $capval;
        }
    }
    #GKW_SPECIFIC:
    my @neededparams = ("MPI", "OPENMP", "MPIRUNCMD", "FFTLIB", "REVISION",
        "REAL_PRECISION", "Eigenvaluesolver");
    foreach my $param (@neededparams)
    {
        die "Missing $param\n" unless defined $cap{$param};
        $cap{$param} = simplify_val($param,$cap{$param});
          print "$param \=$cap{$param}\n";
    }

    #%cap = simplify_hash(%cap);


}

#-----------------------------------------------------------------------------

sub simplify_val
{
    my $param = shift;
    my $val   = shift;

    $val =~ tr/a-z/A-Z/;

    if ( $param eq "MPI" || $param eq "OPENMP" )
    {
        my @acceptable_values = ( "MPI2", "TRUE", "FALSE", "0", "OFF", "ON",
                                   "YES",   "NO", "1", "T", "F", "Y", "N" );
#        die "Value for $param ($val) is bad, acceptable values are"
#            . "@acceptable_values" unless ( grep ($val,@acceptable_values) eq "" );
        $val =~ s/MPI2/1/;
        $val =~ s/TRUE/1/;
        $val =~ s/FALSE/0/;
        $val =~ s/OFF/0/;
        $val =~ s/YES/1/;
        $val =~ s/ON/1/;
        $val =~ s/NO/0/;
        $val =~ s/T/1/;
        $val =~ s/Y/1/;
        $val =~ s/F/0/;
        $val =~ s/N/0/;
    }

    if ( $param eq "FFTLIB" )
    {
        $val =~ s/OFF/0/;
        $val =~ s/NONE/0/;
    }

    if ( $param eq "REAL_PRECISION" )
    {
        $val =~ s/\,.*$//;
        $val =~ s/8/DOUBLE/;
        $val =~ s/4/SINGLE/;
    }

    if ( $param eq "Eigenvaluesolver" )
    {
        $val =~ s/T/1/;
        $val =~ s/N/0/;
    }

    return $val;
}


sub INT_handler
{
    print "Don't interrupt me!\n";
    die "done\n";
}

#-----------------------------------------------------------------------------
