#!/bin/sh
# Run the right perl version:
if [ -x /usr/local/bin/perl ]; then
  perl=/usr/local/bin/perl
elif [ -x /usr/bin/perl ]; then
  perl=/usr/bin/perl
else
  perl=`which perl| sed 's/.*aliased to *//'`
fi

exec $perl -x -S $0 "$@"     # -x: start from the following line
#============================================================================#
#!perl -w
#
# Name: gkwrun.pl
# Description:
#
#  Simple script to run the GKW code, primarily for use when testing the code.
#  This may be merged into the test script itself.
#
#  Typically requires an executable, input file and run directory.
#  Existing input options are:
#    * A run command for MPI
#    * Number of MPI processes
#    * Option to keep existing data
#  Some possibly required options are:
#    * Misc input files (e.g. geom files)
#    * Tidy on success / keep files on fail / debug ?
#    * executable information ?
#
#----------------------------------------------------------------------------#
use strict;

use File::Copy;
use File::Path qw(mkpath rmtree);
use Cwd;
use Cwd 'abs_path';
my $cwd = getcwd(); chomp($cwd);

my $default_run_directory ="$cwd";
my $default_executable ="./gkw.x";
my $default_mpiruncmd ="mpirun";
my $default_input_file = "input.dat";
my $input_file_name = "input.dat";
my $keep_old_data = 0;
my $tidy_up = 0;
my $info    = 0;
my $debug   = 0;
my $stdout;

use Getopt::Long;
Getopt::Long::config("bundling");
my %opts;
GetOptions(\%opts,
           qw( -x=s --executable=s
               -i=s --input-file=s
                    --geom-files=s
                    --run-directory=s
                    --mpiruncmd=s
               -n=s --num-mpi-procs=s
                    --keep-old-data
                    --tidy
                    --debug
                    --executable-info)
          ) or die "ABORT: unknown option\n";

my $executable    = ($opts{'x'} || $opts{'executable'} || $default_executable);
my $input_file    = ($opts{'i'} || $opts{'input-file'} || $default_input_file);
my $run_directory = ($opts{'run-directory'} || $default_run_directory);
my $mpiruncmd     = ($opts{'mpiruncmd'} || $default_mpiruncmd);
my $mpiprocs      = ($opts{'n'} || $opts{'num-mpi-procs'} || "1");

if ($opts{'keep-old-data'})    { $keep_old_data = 1; }
if ($opts{'tidy'})             { $tidy_up       = 1; }
if ($opts{'executable-info'})  { $info          = 1; }
if ($opts{'debug'})            { $debug         = 1; }

# The executable must be specified, unless the default is in the current
# directory.
if ( -e "$cwd/$executable" ) {
  $executable = abs_path($executable);
} else {
  die "ABORT: executable not found\n" unless ( -e "$executable" );
}

# If not using the default input file (which is the one in the directory
# we change to), get the full path to the requested one.
if ( "$input_file" eq "$default_input_file" ) {
} else {
  if ( -e "$cwd/$input_file" ) {
    $input_file = abs_path($input_file);
  }
}

# Make new directory if necessary, otherwise we cannot get the full path.
mkpath ($run_directory) unless ( -d "$run_directory" );

# get the absolute path for the run directory
$run_directory = abs_path($run_directory);

chdir($run_directory) || die "ABORT: cannot cd to $run_directory \n";

clean_run_directory() unless ($keep_old_data);
get_input_file();
$stdout=run_code();

# clean up
if ($tidy_up) { clean_run_directory(); }

# print result
print "$stdout\n";

#-----------------------------------------------------------------------------


## Clean all files in the present directory, other than those in the exception
## list.
sub clean_run_directory {

  my $wd = getcwd();

  opendir (DIR,$wd) || die "ABORT: cannot read contents of $wd\n";
  my @files = readdir (DIR);
  closedir DIR;

  foreach my $f (@files) {
    if ( -f $f ) { unless ( $f =~ m/^input.dat/ ) { unlink "$f"; } }
  }

}

## Get the input file
##
sub get_input_file {
  if ($debug) {
    print "Input file:         $input_file\n";
    print "Default input file: $default_input_file\n";
  }
  # check the required file exists
  if ( ! -e $input_file ) {
    die "ABORT: input not found!\n" unless ($info);
  }

  # If we want to use a file that is not the default, remove any existing file
  # that would otherwise be used.
  if ( not "$default_input_file" eq "$input_file" ) {
    if ($debug) { print "Attempting to make symlink\n"; }
    # copy the file if not a symlink; otherwise just remove it
    if ( not -l $default_input_file) { copy ("$default_input_file","$default_input_file".".old"); }
    unlink "$default_input_file";
    symlink "$input_file","$default_input_file" unless ($info);
  }

}

## Run the code
##
sub run_code {

  my $output;

  if ("$mpiruncmd" eq "none") {
    $output = `$executable 2>&1`;
  } else {
    $output = `$mpiruncmd -np $mpiprocs $executable 2>&1`;
  }

  return $output;

}
