#!/bin/sh
# build a file for sending to cpc
# run this from the top level gkw/ directory, directly above src/

# TODO: add sample input files (and appropriate outputs?)

# Should also include the following files:
# doc/gkw_and_chease.tex (or perhaps better the pdf)
# chease/doc/RunChease.tex (or perhaps better the pdf) + necessary input files???  
# doc/inputs + some more that are not yet there
# doc/input.dat.sample (This is a vital part of the documentation!!!)
# doc/input.dat.minimum
# scripts/gkw_construct-parallel*
# scripts/gkw_clean_run

mkdir -p tmp/gkw/src

# copy the source files to tmp dir
for prefix in global \
        external/specfun \
        external/marsaglia \
        fft \
        mpicomms \
        constants \
        mpidatatypes \
        mpiinterface \
        general \
        control \
        grid \
        dist \
        io \
        components \
        geom \
        matdat \
        velocitygrid \
        collisionop \
        mode \
        functions \
        rotation \
        normalise \
        non_linear_terms \
        tearingmodes \
        diagnostic \
        exp_integration \
        init \
        linear_terms \
        linart
do
  cp src/$prefix.*90 tmp/gkw/src/
done

# copy the POSIX makefile and strip out the external directory
cp src/makefile tmp/gkw/src/
perl -p -i -n -e 's/external\///g' tmp/gkw/src/makefile

# create a top-level makefile
cat << EOF > tmp/gkw/makefile
# GKW top level makefile

default:
	(cd src; \$(MAKE))
clean:
	(cd src; \$(MAKE) clean)
EOF

# copy the README file
cp README tmp/gkw/

# copy some samples 
mkdir -p tmp/gkw/samples

mkdir -p tmp/gkw/samples/general
cp ./doc/input.dat.sample tmp/gkw/samples/general/input.dat

mkdir -p tmp/gkw/samples/minimal
cp ./doc/input.dat.minimum tmp/gkw/samples/minimal/input.dat
#+ some more that are not yet there
mkdir -p tmp/gkw/samples/STD
cp ./doc/input/STD tmp/gkw/samples/STD/input.dat

mkdir -p tmp/gkw/samples/STD_kinetic
cp ./doc/input/STD_kinetic tmp/gkw/samples/STD_kinetic/input.dat

mkdir -p tmp/gkw/samples/cyclone
cp ./doc/input/cyclone tmp/gkw/samples/cyclone/input.dat

#mkdir -p tmp/gkw/samples/geometry_example
#cp ../chease/example/input/gkw.example tmp/gkw/samples/geometry_example/input.dat
#cp ../chease/example/output/hamada.example.dat tmp/gkw/samples/geometry_example/hamada.dat

mkdir -p tmp/gkw/samples/simple_itg
cp ./doc/input/simple_itg tmp/gkw/samples/simple_itg/input.dat

mkdir -p tmp/gkw/samples/simple_example/output
cp ./doc/input/simple_example tmp/gkw/samples/simple_example/input.dat
cp ./doc/input/output/* tmp/gkw/samples/simple_example/output/

mkdir -p tmp/gkw/bin
cp ./scripts/gkw_clean_run tmp/gkw/bin/gkw_clean_run
cp ./scripts/gkw_construct-parallel-output.sh tmp/gkw/bin/gkw_construct-parallel-output.sh

# build the gziped tarball
cd tmp
tar -cvzf gkw-cpc.tar.gz gkw/
cd ..
mv tmp/gkw-cpc.tar.gz ./

# clean up
rm -fr tmp/gkw
