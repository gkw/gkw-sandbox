!-----------------------------------------------------------------------------
! SVN:$Id$
!> 
!> This module reads the input file, and checks for errors
!> It then calls the initialisation routines
!> from all other modules in the correct order.
!>
!-----------------------------------------------------------------------------
module init
  
  implicit none

  private
  ! public subroutines
  public :: initialize, deallocate_runtime_arrays, finalize

contains

! ----------------------------------------------------------------------------
! SVN:$Id$
!> This subroutine should do the initalization of all 
!> the quantities
!-----------------------------------------------------------------------------

subroutine initialize(check_input)

  use global, only : r_tiny
  use general,         only : gkw_exit
  use control,         only : imod_init, spectral_radius, flux_tube
  use control,         only : read_file, auto_restart, testing 
  use control,         only : method, control_initt
  use control,         only : io_format, io_legacy, io_testdata
  use grid,            only : setup_grid, lx, psil, psih, n_x_grid 
  use mode,            only : mode_init, mode_box_recon, mode_check_params
  use mode,            only : kgrid, krbal, kgrid_nonspec_rad
  use normalise,       only : normalise_init
  use diagnostic,      only : diagnostic_allocate, diagnostic_read_last_data
  use diagnostic,      only : diagnostic_init
  use restart,         only : read_restart_file, abort_if_old_data_exists
  use restart,         only : restarted
  use dist,            only : dist_init
  use components,      only : components_input_species, components_profiles
  use components,      only : components_beta_cal, rhostar
  use geom,            only : geom_init_grids, parallelize_geom
  use geom,            only : geom_output, geom_check_params, geom_type
  use geom,            only : shift_end_grid
  use exp_integration, only : init_explicit
  use eiv_integration, only : init_eigenvalue
  use linear_terms,    only : calc_linear_terms
  use non_linear_terms,only : nonlinear_init 
  use rotation,        only : rotation_init, need_fft, parallelize_rotation
  use velocitygrid,    only : velgrid_init
  use tearingmodes,    only : tearingmodes_init
  use collisionop,     only : mom_conservation,ene_conservation
  use gyro_average,    only : gyro_average_init, gyro_average_allocate
  use gyro_average,    only : blending_order
  use krook,           only : krook_init, nlkrook 
  use mpiinterface,    only : set_comm_fdisi_io, processor_number
  use io,              only : init_io

  
  ! for input checking, call this routine with check_input=.true.
  logical, optional, intent(in) :: check_input
  
  integer :: ix

  ! Read, broadcast and check all the namelist items; initialize anything
  ! necessary before allocation.
  call read_params

  ! initialize the IO interfaces
  call init_io(processor_number, io_format, io_testdata, &
     & read_file .or. auto_restart, &
     & io_legacy)

  ! anything else in control
  call control_initt

  ! setup_grid reads the sizes of the grids and calculates the local
  ! grid sizes on each processor from this input. Additional communicators
  ! are set up for a mpi cartesian topology if necessary. 
  call setup_grid

  ! Exit here if the code is compiled only to check the input
  if (present(check_input)) then
    if (check_input) then
      call gkw_exit('Input file read sucessfully, input.out written')
    end if
  end if

  ! setup communicator to read / write the restart file (after setup_grid) 
  call set_comm_fdisi_io()

  ! before calling the geometry, or dist_init routines, set lx for the global runs 
  ! FJC: this should go in spcgeneral_check_params
  if (.not. flux_tube) then 
    if (rhostar < r_tiny) call gkw_exit('Zero rhostar in global run')
    lx =  (psih - psil) / rhostar 
  endif 

  ! This routine also sets up and allocates everything required in
  ! dist 
  call dist_init(mom_conservation,ene_conservation,blending_order)

  ! Allocate all the most fundamental arrays, necessary for the computations
  call allocate_core

  ! need to reread the species into the now allocated arrays
  call components_input_species

  ! Output all the namelist items and output some other metadata
  call write_params

  ! calculate the proper value of beta and beta_prime 
  ! (called here in case the species beta_prime is required by miller geom)
  call components_beta_cal 

  ! Read the mode information
  ! this should be after allocate
  call mode_init

  ! Initialize the grids
  call geom_init_grids

  ! normalize the first for the global case
  ! FJC: suggest rhostar be moved to geom, and these lines to geom
  if (.not.flux_tube) then 
    do ix = 1, n_x_grid
      shift_end_grid(ix) = shift_end_grid(ix) / rhostar
    end do 
  endif 

  !Since chease read can modify q and shat, some checks need repeating
  !APS: ideally this hack should be removed.
  if(geom_type=='chease') then 
    call geom_check_params(2)
    call mode_check_params(2)
  endif

  ! if 2D array of modes is used one must call kgrid 
  ! Must be called before krbal. For a single mode one 
  ! must still call this routine since it normalises 
  ! the wave vectors used in the code 
  if (spectral_radius) then 
    call kgrid
  else 
    call kgrid_nonspec_rad
  endif

  ! Further allocate arrays
  call allocate_more

  ! Write geom quantities to file before parallelize_geom!
  ! Note these cannot be written with other write_run_params.
  if (.not. testing) call geom_output()
  if (.not. testing) call mode_box_recon

  !Must occur after components_init, and after geom_init_grids
  !Uses global tensors, so must be before parallelize_geom
  call rotation_init

  !Warning DO NOT change the order of this and the above routines
  call parallelize_geom
  call parallelize_rotation

  ! for the global case recalculate the profiles 
  call components_profiles 

  ! calculate the proper value of beta and beta_prime 
  ! (called again in case the chease inputs are required)
  call components_beta_cal

  ! calculate the parameters along the field line 
  call krbal

  !Allocates and initialises the velocity grid (uniform or not)
  !For vptrap case must be called after parallelize_geom
  call velgrid_init

  ! Allocate the arrays of diagnostic
  call diagnostic_allocate()

  ! set up the grids for the distribution function
  !call dist_grid_setup <! done by dist

  ! Initialise the array containing the Maxwellian
  call init_fmaxwl

  ! Initialise the gyro_average module
  if (.not.spectral_radius) then
    call gyro_average_allocate
    call gyro_average_init
  end if

  if (.not. testing) then   
    ! read from file (restart) if requested 
    if (read_file .or. auto_restart) then
      call read_restart_file()
    end if

    if (restarted) then
      call diagnostic_read_last_data
    else
      call abort_if_old_data_exists
    endif
  end if

  !The sizes of the real space box and location of the modes are calculated
  !This may be needed even if non_linear = false
  !This must be called after kgrid
  if (need_fft) call nonlinear_init
    
  ! calculate the linear terms, setup the matrix 
  call calc_linear_terms
    
  ! if krook operator is required do the initialization (before init_explicit)
  if (nlkrook) call krook_init 
    
  ! Initialize the parameters for the explicit/implicit time step 
  select case(method) 
  case('EIV')
    call init_eigenvalue
  case('EXP') 
    ! initialise the explict time integration and persistent communication
    ! needs only control, dist, mpicomms, and grid (could go higher up)
    call init_explicit        
  case('IMP') 
    ! currently imp_int called from linart contains the initialisation.
    ! If the initialisation is split off, it should be called here
  case default 
    call gkw_exit('init: unknown integration scheme (method)')
  end select
      
  !The following case is for the study of tearing modes.  The magnetic island
  !is initialised as a perturbation in the parallel vector potential.
  call tearingmodes_init

  ! and initialise the distribution (but should not change the fields)
  ! in the cases where the g2f corretion is applied
  ! uses the linear terms / gyro_average matrices since it calls
  ! the fields solver, and in the cases also with parallel_x
  ! also uses the persistent_comm_init setup inside init_explicit
  call init_fdisi 

  ! initialize normalise
  call normalise_init()
   
  ! if starting with 1 mode do imod_init
  if (imod_init > 0) call init_imod()
    
  ! set up the diagnostics (must be after nonlinear init)
  ! done last to reduce incidence of opening files then aborting
  if (method /= 'EIV') then
    call diagnostic_init
  end if
        
end subroutine initialize


!-----------------------------------------------------------------------------
!> Read the parameters from the input file. Call the corresponding routines
!> that do the parameter checking.
!-----------------------------------------------------------------------------
subroutine read_params()
  use version,         only : gkw_info, output_header
  use mpiinterface,    only : root_processor
  use control,         only : control_bcast_nml, control_read_nml
  use control,         only : control_check_params
  use grid,            only : grid_read_nml, number_of_species
  use grid,            only : grid_bcast_nml, grid_check_params
  use diagnostic,      only : diagnostic_read_nml
  use diagnostic,      only : diagnostic_bcast_nml, diagnostic_check_params
  use rotation,        only : rotation_read_nml
  use rotation,        only : rotation_bcast_nml, rotation_check_params
  use geom,            only : geom_read_nml
  use geom,            only : geom_bcast_nml, geom_check_params
  use mode,            only : mode_read_nml
  use mode,            only : mode_bcast_nml, mode_check_params
  use components,      only : components_read_nml_spcg
  use components,      only : components_bcast_nml_spcg
  use components,      only : components_check_params_spcg
  use components,      only : components_read_nml_spec  
  use components,      only : components_bcast_nml_spec
  use components,      only : components_check_params_spec
  use components,      only : n_spec
  use collisionop,     only : collisionop_read_nml
  use collisionop,     only : collisionop_bcast_nml, collisionop_check_params
  use linear_terms,    only : linear_terms_read_nml, linear_terms_bcast_nml
  use linear_terms,    only : linear_terms_check_params
  use krook,           only : krook_read_nml, krook_bcast_nml
  use gyro_average,    only : gyro_average_read_nml, gyro_average_bcast_nml 
  use source_time,     only : source_time_read_nml, source_time_bcast_nml
  use source_time,     only : source_time_check_params
  use eiv_integration, only : eiv_integration_read_nml
  use eiv_integration, only : eiv_integration_bcast_nml
  use eiv_integration, only : eiv_integration_check_params
  use rho_par_switch,  only : rho_par_switch_read_nml
  use rho_par_switch,  only : rho_par_switch_bcast_nml 

  integer, parameter :: file_unit = 91
  
  integer :: io_stat, i
  logical :: input_exists

  ! exit for now when no input file exists
  inquire(FILE='input.dat',EXIST=input_exists)
  if (.not. input_exists) call gkw_info()

  ! Read and check the various namelists
  ! Set io_stat to zero; only root_processor will obtain a different value.
  io_stat = 0
  
  ! The order here matters for the checks, and should follow the
  ! dependency hierarcy

  open(file_unit,file='input.dat',FORM='formatted',STATUS='old', &
         POSITION='rewind', ACTION='read')
  ! control
  if (root_processor) then
    rewind(file_unit)
    call control_read_nml(file_unit, io_stat, .false.)
  end if
  call namelist_error_check('control',io_stat)
  call control_bcast_nml
  call control_check_params

  ! grid
  if (root_processor) then
    rewind(file_unit)
    call grid_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('gridsize',io_stat)
  call grid_bcast_nml
  call grid_check_params

  ! geom
  if (root_processor) then
    rewind(file_unit)
    call geom_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('geom',io_stat)
  call geom_bcast_nml
  call geom_check_params(1) !may be called again later

  ! mode
  if (root_processor) then
    rewind(file_unit)
    call mode_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('mode',io_stat)
  call mode_bcast_nml
  call mode_check_params(1) !may be called again later

  ! components spcgeneral
  if (root_processor) then
    rewind(file_unit)
    call components_read_nml_spcg(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('spcgeneral',io_stat)
  call components_bcast_nml_spcg
  call components_check_params_spcg
  
  ! components species
  ! *** The species can be read for checking, but not initialised till after
  ! *** allocate.
  do i=1,n_spec
    ! Rewind the file when the first species namelist is read.
    if (root_processor) then
      if (i == 1) rewind(file_unit)
      call components_read_nml_spec(file_unit, io_stat)
    end if
    call namelist_error_check('species',io_stat,i)
    call components_bcast_nml_spec
    call components_check_params_spec
  end do

  ! rotation
  if (root_processor) then
    rewind(file_unit)
    call rotation_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('rotation',io_stat)
  call rotation_bcast_nml
  call rotation_check_params

  ! eiv_integration (optional)
  if (root_processor) then
    rewind(file_unit)
    call eiv_integration_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('eiv_integration',io_stat)
  call eiv_integration_bcast_nml
  call eiv_integration_check_params

  ! linear terms (optional)
  if (root_processor) then
    rewind(file_unit)
    call linear_terms_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('linear_terms',io_stat)
  call linear_terms_bcast_nml
  call linear_terms_check_params

  ! collisions (optional)
  if (root_processor) then
    rewind(file_unit)
    call collisionop_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('collisions',io_stat)
  call collisionop_bcast_nml
  call collisionop_check_params

  ! krook (optional)
  if (root_processor) then
    rewind(file_unit)
    call krook_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('krook',io_stat)
  call krook_bcast_nml

  ! gyro_average (optional / nonspectral only)
  if (root_processor) then
    rewind(file_unit)
    call gyro_average_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('gyroaverage',io_stat)
  call gyro_average_bcast_nml

  ! source time (optional)
  if (root_processor) then
    rewind(file_unit)
    call source_time_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('source_time',io_stat)
  call source_time_bcast_nml
  call source_time_check_params

  ! source time (optional)
  if (root_processor) then
    rewind(file_unit)
    call rho_par_switch_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('finite_rho_parallel',io_stat)
  call rho_par_switch_bcast_nml

  ! diagnostic (optional)
  if (root_processor) then
    rewind(file_unit)
    call diagnostic_read_nml(file_unit,io_stat, .false.)
  end if
  call namelist_error_check('diagnostic',io_stat)
  call diagnostic_bcast_nml
  call diagnostic_check_params
  
  close(file_unit)
  
  ! The parameters which were read from input.dat into namelists are metadata.
  ! We want to attach them also to the output data, be it a simple ascii file
  ! or HDF5 attributes or other.
  ! Therefore the IO interfaces must be initialized before those metadata can
  ! be output. The IO interfaces can be initialized only *after* the input.dat
  ! has been read, because they must know the desired format and whether the
  ! run is a restarted one or not.

end subroutine read_params


!-----------------------------------------------------------------------------
!> Write the parameters in the desired output
!> format, e.g. to the file input.out .
!-----------------------------------------------------------------------------
subroutine write_params()
  use mpiinterface, only : root_processor, mpibarrier
  use version,         only : output_header
  use control,         only : control_write_nml
  use grid,            only : grid_write_nml
  use diagnostic,      only : diagnostic_write_nml
  use rotation,        only : rotation_write_nml
  use geom,            only : geom_write_nml
  use mode,            only : mode_write_nml
  use components,      only : components_write_nml_spcg
  use components,      only : n_spec, components_write_nml_spec
  use collisionop,     only : collisionop_write_nml
  use linear_terms,    only : linear_terms_write_nml
  use krook,           only : krook_write_nml
  use gyro_average,    only : gyro_average_write_nml
  use source_time,     only : source_time_write_nml
  use eiv_integration, only : eiv_integration_write_nml
  use rho_par_switch,  only : rho_par_switch_write_nml
  
  integer, parameter :: out_file_unit = 92
  integer :: io_stat, i
  
  if (root_processor) then
    open(UNIT=out_file_unit, FILE='input.out', FORM='formatted', &
       & STATUS='replace', POSITION='rewind')

    ! write the header for the input.out file
    call output_header(out_file_unit)

    call control_write_nml(out_file_unit,io_stat)
    call grid_write_nml(out_file_unit,io_stat)
    call diagnostic_write_nml(out_file_unit,io_stat)
    call geom_write_nml(out_file_unit,io_stat)
    call mode_write_nml(out_file_unit,io_stat)
    call components_write_nml_spcg(out_file_unit,io_stat)
    do i=1,n_spec
      call components_write_nml_spec(out_file_unit, i, io_stat)
    end do
    call rotation_write_nml(out_file_unit,io_stat)
    call eiv_integration_write_nml(out_file_unit,io_stat)
    call linear_terms_write_nml(out_file_unit,io_stat)
    call collisionop_write_nml(out_file_unit,io_stat)
    call krook_write_nml(out_file_unit,io_stat)
    call gyro_average_write_nml(out_file_unit,io_stat)
    call source_time_write_nml(out_file_unit,io_stat)
    call rho_par_switch_write_nml(out_file_unit,io_stat)
    close(out_file_unit)
  end if
  ! (formerly: don't want to use input.out till it is written) Now,
  ! input.out is not to be read in anymore, but input.dat is read
  ! twice.
  call mpibarrier()

  
end subroutine write_params

! !-----------------------------------------------------------------------------
! !> processor the namelist given in read_namelist_from_input
! !> FJC: This routine wrapper should be removed, it does not add anything
! !> except confusion.The readwrite namelist routines should be called directly 
! !> with no interface since the read/write argument has not need to be optional.
! !> If necessary, they can be renamed to module_readwrite_namelist
! !-----------------------------------------------------------------------------
! subroutine read_nml(read_namelist_from_input,file_unit,io_stat,i_spec)
  
!   use control,       only : control_read_nml
!   use grid,          only : grid_read_nml
!   use diagnostic,    only : diagnostic_read_nml
!   use rotation,      only : rotation_read_nml
!   use geom,          only : geom_read_nml
!   use mode,          only : mode_read_nml
!   use components,    only : components_read_nml_spcg
!   use components,    only : components_read_nml_spec
!   use collisionop,   only : collisionop_read_nml
  
!   interface
!     subroutine read_namelist_from_input(i_lun,i_stat,l_write)
!       integer, intent(in)  :: i_lun
!       integer, intent(out) :: i_stat
!       logical, optional, intent(in)  :: l_write
!     end subroutine read_namelist_from_input
!   end interface

!   integer, intent(in)           :: file_unit
!   integer, intent(out)          :: io_stat
!   integer, optional, intent(in) :: i_spec

!   logical, parameter :: l_write = .false.

!   if (present(i_spec)) then
!     ! The input file may contain several 'species' namelists.
!     ! In order to read them consecutively, the file has to stay open while they
!     ! are read.
!     if (i_spec == 1) then
!       ! Open the file anew when the first species namelist is read.
! #error      open(file_unit,file='input.dat',FORM='formatted',STATUS='old')
!     end if
!   else
!     ! By rewinding the file pointer before any other namelist is read,
!     ! we allow for an arbitrary order of the namelists.
!     open(file_unit,file='input.dat',FORM='formatted',STATUS='old', &
!        & POSITION='rewind')
!   end if

!   ! call the read_input routine in the appropriate module
!   call read_namelist_from_input(file_unit,io_stat,l_write)
  
!   ! Close the file, unless we have more species to read
!   if (present(i_spec)) then
!     if (i_spec == n_spec) close(file_unit)
!   else
!     close(file_unit)
!   end if
    
! end subroutine read_nml


!-----------------------------------------------------------------------------
!> Check io_stat for a read then abort if any non-zero values are found.
!-----------------------------------------------------------------------------
subroutine namelist_error_check(list_name,io_stat,ispc)

  use mpiinterface, only : root_processor, mpibcast
  use general,      only : gkw_exit

  character (len=*), intent(in) :: list_name
  integer, intent(in)           :: io_stat
  integer, optional, intent(in) :: ispc

  integer :: my_io_stat

  my_io_stat = 0

  ! root_processor first reports any error
  if (root_processor) then

    if (io_stat > 0) then
      write(*,*) '* Error in namelist '//list_name//'.'
      if (present(ispc)) write(*,*) '* for species number: ',ispc
    end if
    
    if (io_stat < 0) then
      if (list_name == 'diagnostic'      .or. list_name == 'linear_terms' .or.  &
       &  list_name == 'krook'           .or. list_name == 'gyroaverage'  .or.  &
       &  list_name == 'source_time'     .or. list_name == 'rotation'     .or.  &
       &  list_name == 'eiv_integration' .or. list_name == 'finite_rho_parallel' ) then
        write(*,*) '* Optional namelist '//list_name//' MISSING.'
      else
        write(*,*) '* Namelist '//list_name//' MISSING.'
        if (present(ispc)) write(*,*) '* for species number: ',ispc
      end if
    end if
    
    ! For optional namelists, ignore missing status
    my_io_stat = io_stat    
    select case(list_name)
      case('diagnostic','linear_terms','rotation','krook','gyroaverage',  &
         & 'source_time','collisions','eiv_integration','finite_rho_parallel')
        if (io_stat < 0) my_io_stat = 0
      case default; my_io_stat = io_stat
    end select
    
  endif

  ! broadcast io_stat from root_processor
  call mpibcast(my_io_stat,1)

  ! abort on non-zero io_stat
  if (my_io_stat /= 0) then
    call gkw_exit('namelist_error_check: problem with '//list_name)
  end if

end subroutine namelist_error_check

!-----------------------------------------------------------------------------
!> Subroutine that initialises the distribution function with a 
!> choice of intial conditions, and the maxwellian
!> should not change the fields !
!> Note: fdisi contains g, but since the A|| perturbation is intially
!> zero, initialising f is the same as g (except for tearingmodes)
!> GLOBAL: Needs a 1/dgrid for all the fdisi init options
!> and some tgrids also
!-----------------------------------------------------------------------------
subroutine init_fdisi
  
  use marsaglia,      only : ran_kiss
  use control,        only : spectral_radius, fullf_wo_Fm
  use restart,        only : restarted
  use grid,           only : nmod, nx, ns, nsp, nmu, nvpar, n_x_grid, lx
  use grid,           only : n_vpar_grid, n_mu_grid, n_s_grid
  use grid,           only : ls, lmu, lvpar, lsp, lrx, gx, number_of_species
  use dist,           only : fdisi, ifdis, nelem_conserve, fmaxwl, n_conserve 
  use fields,         only : get_averaged_apar, calculate_fields 
  use fields,         only : field_solve_nonspec_wrap
  use index_function, only : indx
  use geom,           only : bn, sgr, kthnorm, dxgr
  use mode,           only : mode_box, ixzero, iyzero, ikxspace 
  use mode,           only : kxrh, krho
  use components,     only : quench_modes, rhostar 
  use components,     only : signz, de, amp_init, amp_init_imag, finit, kwid_ini
  use components,     only : vthrat, isl_ls, isl_mode, tmp, tearingmode, lfinit_radial_dirichlet
  use velocitygrid,   only : vpgr, mugr, intvp, intmu
  use constants,      only : ci1, pi 
  use mpiinterface,   only : root_processor
  use general,        only : gkw_abort
        
  ! integers for the loop over all grid points 
  integer :: imod, ix ,i, j, k, is, line, i_s, j_mu, k_vpar, i_sp, i_x, idx
 
  ! Dummy variables 
  real    :: random1, random2, vperp, apar_ga, random3, random4, a
  complex :: amp_ini
  integer :: idum, ikxspace_local, imodisland 
  integer :: iplusmode,iminusmode, en, p, modnum
  real    :: balloonpos, dumbuf, dum 

  ! initialise
  k = -999 ; is = -999

  !Initialise the complex initial amplitude
  amp_ini=cmplx(amp_init,amp_init_imag)  

  !Initialise collisions conservation fields to zero 
  !(probably not needed)
  !(could this field be real rather than complex?)
  if (nelem_conserve > 0) then 
    fdisi(n_conserve+1:n_conserve+nelem_conserve) = (0.E0,0.E0)
  end if
    
  ! There may be ways to initialise the distribution which first read
  ! a restart file and then modify it.  For those, it is checked
  ! whether this run is indeed a restarted one.
  if ((finit == 'restart+quenchZF' &
     & ) .and. .not. restarted) then
    call gkw_abort("The initial condition 'restart+quenchZF' can only be used when restarting a run.")
  end if

  if (restarted) then
    ! These initial conditions differs from the ordinary set below:
    ! They assumes that the run is restarted and fdisi is already filled.
    ! FIXME Therefore, fdisi contains g and this is not equal to f as for a
    ! fresh run. This is not correctly done at the moment.
    select case(finit)
    case('restart+quench_ky')
      ! With this initial condition the zonal flow eigenmode is set to zero.

      ! zero mode
      do idx = 1, size(quench_modes)
        if(1 <= quench_modes(idx) .and. quench_modes(idx) <= nmod) then
          do ix = 1, nx; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar;
            do is = 1, nsp
              fdisi(indx(ifdis,quench_modes(idx),ix,i,j,k,is)) = 0.
            end do
          end do; end do; end do; end do;
        end if
      end do

    case default
      ! do not modify the distribution if none of the above options
      ! is specified.
      continue
    end select
  else
    ! The following initial conditions are applicable for new runs,
    ! i.e. not restarted ones.
    
    select case(finit)
    case('noise','gnoise') ! grid scale uniform (or gaussian) noise

      do imod = 1, nmod; do i_s = 1, n_s_grid; do j_mu = 1, n_mu_grid 
        do k_vpar = 1, n_vpar_grid; do i_sp = 1, number_of_species
          do i_x = 1, n_x_grid

            ! always generate 2 numbers on interval [0 1]
            random1 = ran_kiss() ; random2 = ran_kiss()

            ! apply box-muller transformation to make gaussian
            if (finit=='gnoise') then 
              do while (random1 < 1.e-8 .or. random1 > 1.-1.e-8)
                random1 = ran_kiss()
              end do
              random3 = sqrt(-2.*log(random1))*cos(2*pi*random2)
              random4 = sqrt(-2.*log(random1))*sin(2*pi*random2)
              random1 = random3; random2=random4
            else ! shift onto [-1 1]
              random1 = 1.-2.*random1
              random2 = 1.-2.*random2
            end if

            ! get the local index
            i= ls(i_s); j= lmu(j_mu); k=lvpar(k_vpar)
            is= lsp(i_sp); ix=lrx(i_x)

            ! only initialise if we are on the local proc
            if (i >= 1 .and. i <= ns .and. j >= 1 .and. j <= nmu &
               & .and. k >= 1 .and. k <= nvpar .and. is >= 1    &
               & .and. is <= nsp .and. ix >= 1 .and. ix <= nx) then

              ! No initialisation of the zonal flow 
              if (imod.eq.iyzero) then
                fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
              else 
                fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini *   &
                   &            (random1 + random2*ci1)
              end if

            end if

          end do
        end do; end do
      end do; end do; end do 

    case('hybrid') ! gaussian noise shaped towards something physical

      ! modal peak in ky maxwellian
      a=sqrt(2.)*kwid_ini/kthnorm

      do imod = 1, nmod; do i_s = 1, n_s_grid; do j_mu = 1, n_mu_grid
        do k_vpar = 1, n_vpar_grid; do i_sp = 1, number_of_species
          do i_x = 1, n_x_grid

            ! always generate 2 numbers on interval [0 1]
            random1 = ran_kiss() ; random2 = ran_kiss()

            ! apply box-muller transformation to make gaussian
            do while (random1 < 1.e-8 .or. random1 > 1.-1.e-8)
              random1 = ran_kiss()
            end do
            random3 = sqrt(-2.*log(random1))*cos(2*pi*random2)
            random4 = sqrt(-2.*log(random1))*sin(2*pi*random2)
            random1 = random3; random2=random4

            ! get the local index
            i= ls(i_s); j= lmu(j_mu); k=lvpar(k_vpar)
            is= lsp(i_sp); ix=lrx(i_x)

            ! only initialise if we are on the local proc
            if (i >= 1 .and. i <= ns .and. j >= 1 .and. j <= nmu &
               & .and. k >= 1 .and. k <= nvpar .and. is >= 1    &
               & .and. is <= nsp .and. ix >= 1 .and. ix <= nx) then

              ! No initialisation of the zonal flow 
              if (imod.eq.iyzero) then
                fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
              else 
                fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini *        &                                              
                                ! random imaginary number, uniform on [-1,1]
                   (random1 + random2*ci1) *                            &
                                !ballooning, gaussian centered on kx=0
                   cos(2*pi*sgr(i))*exp(-kxrh(ix)*kxrh(ix)/(20.*a*a))*  &
                                ! maxwellian in velocity space
                   fmaxwl(ix,i,j,k,is) *                                &
                                ! maxwellian in ky, with a modal peak at sqrt(2)*a
                   exp(-krho(imod)*krho(imod)/(2*a*a)) *                &
                   krho(imod)*krho(imod)/(a*a*a)
              end if

            end if

          end do
        end do; end do
      end do; end do; end do

    case('cosine6')

      a=sqrt(2.)*kwid_ini/kthnorm

      ! Maxwellian in velocity space
      ! Ballooning and gaussian centered about kx = 0
      ! Like hybrid but without the random element or ky dependence - for MTM
      ! Like cosine3 and 4, but with better kx dependence
      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx
          ! No initialisation of the zonal flow 
          if ((imod.eq.1).and.mode_box.and.(nmod.ne.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)
          ! or radial streamers
          elseif ((ix == ixzero).and.mode_box) then          
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)
          else
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini                      &
               & * (cos(2*pi*sgr(i)) + 1.0)*exp(-kxrh(ix)*kxrh(ix)/(20.*a*a))  &
               & * exp(-(vpgr(i,j,k,is)**2+2.E0*bn(ix,i)*mugr(j)))
          endif
        end do; end do
      end do; end do; end do; end do

    case('cosine5')

      a=sqrt(2.)*kwid_ini/kthnorm

      ! Maxwellian in velocity space
      ! Ballooning and gaussian centered about kx = 0, excluding ixzero modes
      ! Like hybrid but without the random element or ky dependence - for MTM
      ! Like cosine3 and 4, but with better kx dependence
      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx
          ! No initialisation of the zonal flow 
          if ((imod.eq.1).and.mode_box.and.(nmod.ne.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)
          else
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini                      &
               & * (cos(2*pi*sgr(i)) + 1.0)*exp(-kxrh(ix)*kxrh(ix)/(20.*a*a))  &
               & * exp(-(vpgr(i,j,k,is)**2+2.E0*bn(ix,i)*mugr(j)))
          endif
        end do; end do
      end do; end do; end do; end do

    case('cosine4')
      ! This makes sense particularly with the spectral method: The
      ! initial condition is bell-shaped in velocity space and has in
      ! position space a cosine-shaped hill at the center of the
      ! field line, i.e. at the central radial wavenumber k_psi=0
      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx
          ! No initialisation of the zonal flow 
          if ((imod.eq.1).and.mode_box.and.(nmod.ne.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)
          elseif(ix == ixzero) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini              &
               & * (cos(2*pi*sgr(i)) + 1.0)                             &
               & * exp(-(vpgr(i,j,k,is)**2+2.E0*bn(ix,i)*mugr(j)))
          else
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
          endif
        end do; end do
      end do; end do; end do; end do

    case('cosine3')
      ! resembles the option cosine2 but is bell-shaped in velocity space:
      ! the distribution function becomes smaller towards the borders
      ! of the velocity space

      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx 

          ! No initialisation of the zonal flow 
          if ((imod.eq.1).and.mode_box.and.(nmod.ne.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
          else 
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini              &
               & * (cos(2*pi*sgr(i)) + 1.0)                            &
               & * exp(-(vpgr(i,j,k,is)**2+2.E0*bn(ix,i)*mugr(j)))
          endif

        end do; end do
      end do; end do; end do; end do

    case('cosine2') !default 

      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx

          ! No initialisation of the zonal flow
          if ((imod.eq.1).and.mode_box.and.(nmod.ne.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)
          else
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini*(cos(2*pi*sgr(i))+1.0) 
          endif

        end do; end do
      end do; end do; end do; end do

    case('cosine') !deprecated - see issue 31.

      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx 

          ! No initialisation of the zonal flow 
          if (imod.eq.iyzero) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
          else 
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini * cos(2*pi*sgr(i)) 
          endif

        end do; end do
      end do; end do; end do; end do

    case('sine')  

      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu;  do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx 

          ! No initialisation of the zonal flow 
          if ((imod.eq.1).and.mode_box.and.(nmod.ne.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
          else 
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini*de(ix,is)    &
               &      *(sin(2*pi*sgr(i))+1.0) 
          endif

        end do; end do
      end do; end do; end do; end do

    case('gam')

      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx

          ! Initialisation of the mode (m,n)=(1,0)
          if ((imod.eq.1).and.mode_box.and.(nmod.ne.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini*de(ix,is)    &
               &      *(sin(sgr(i)))
          else
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = 0.E0
          endif

        end do; end do
      end do; end do; end do; end do

    case('gauss')

      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx 

          i_x = gx(ix) + n_x_grid/2.
          if (i_x .gt. n_x_grid ) i_x = i_x - n_x_grid

          ! No initialisation of the zonal flow 
          if (imod.eq.iyzero) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
          else if(spectral_radius) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini *            & 
               & exp(-(real(imod)/real(2*nmod))**2)*         &
               & exp(-(real(i_x)/real(2*n_x_grid))**2)
          else
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini *            & 
               & exp(-(real(imod)/real(2*nmod))**2)*         &
               & exp(-(real(i_x)**2/(2.*dxgr)**2))
          endif

        end do; end do 
      end do; end do; end do; end do

    case('zero')

      do imod = 1, nmod; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        do is = 1, nsp; do ix = 1, nx 

          ! No initialisation of the dist function
          ! For use in conjunction with the tearing mode the
          ! presence of an island is a sufficient perturbation 
          ! Note the g2f correction is applied below !!
          fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  

        end do; end do
      end do; end do; end do; end do

    case('zonal') ! For rosenbluth hinton test

      do imod = 1, nmod; do i = 1, ns;  do j = 1, nmu 
        do k = 1, nvpar; do is = 1, nsp 

          idum = 0
          do ix = 1, nx 
            ! all non zonal components are zero 
            if (iyzero /= imod) then 
              fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)
            endif
            if (.not. spectral_radius) then
              fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_init *         &
                 & fmaxwl(ix,i,j,k,is) * (sin(2*pi*gx(ix)/n_x_grid))
            else
              ! find the central mode 
              if (ixzero == ix) then
                idum = ix 
              endif
            end if
          end do

          if (spectral_radius) then
            if (idum.eq.0) call gkw_abort('No kx = 0 mode found in init zonal')
            ix = idum 
            if ((ix.le.1).or.(ix.ge.nx))                               &
               &            call gkw_abort('No finite kx mode found in init') 
            fdisi(indx(ifdis,imod,1,i,j,k,is)) = -ci1 * amp_init *     &
               & fmaxwl(1,i,j,k,is)/2.
            fdisi(indx(ifdis,imod,nx,i,j,k,is)) = ci1 *amp_init *      &
               & fmaxwl(nx,i,j,k,is)/2.
          end if

        end do; end do
      end do; end do; end do

    case('sgauss') !For single mode runs only

      do is = 1, nsp; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        vperp = sqrt(2.E0*bn(1,i)*mugr(j))
        fdisi(indx(ifdis,1,1,i,j,k,is)) = amp_ini*exp(-((vperp-2.0)/0.5)**2) &
           &                        * exp(-((vpgr(i,j,k,is)+2.E0)/0.5)**2)
      end do; end do; end do; end do

    case('sgauss2')

      ! positive part
      do is = 1, nsp; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        vperp = sqrt(2.E0*bn(1,i)*mugr(j))
        fdisi(indx(ifdis,1,1,i,j,k,is)) = amp_ini*                     &
           & exp(-((vpgr(i,j,k,is)+2.E0)/0.5)**2)*                     &
           & exp(-((vperp-2.0)/0.5)**2)
      end do; end do; end do; end do

      ! negative part
      do is = 1, nsp; do i = 1, ns; do j = 1, nmu; do k = 1, nvpar
        vperp = sqrt(2.E0*bn(1,i)*mugr(j))
        fdisi(indx(ifdis,1,1,i,j,k,is)) = fdisi(indx(ifdis,1,1,i,j,k,is)) &
           & -amp_ini * exp(-((vpgr(i,j,k,is)-2.E0)/0.5)**2)*            &
           & exp(-((vperp-2.0)/0.5)**2)
      end do; end do; end do; end do

    case('helical')
      !A helical density perturbation resonant with the magnetic island
      !to study GAM dynamnics in the presence of a magnetic island.
      !ikxspace_local = 1
      if (mode_box) then
        ikxspace_local = ikxspace
      else !if not mode_box ikxspace has no meaning in the rest of the code
        ikxspace_local = 1
        !ixzero set in mode.
      end if

      if (nmod.eq.1) then
        imodisland=1
        en = 1
      else if (nmod.gt.1) then       
        imodisland=isl_mode
        en = imodisland-1
      end if
      if(imodisland.gt.nmod) then
        call gkw_abort('Tearing mode: The poloidal mode the island is &
           &  defined as must be less than nmod')
      end if

      if(root_processor) then
        write(*,*)'Helical density perturbation chosen to be resonant &
           & with magnetic island'
      end if

      do is = 1,nsp; do j = 1,nmu; do k = 1,nvpar; do i=1,ns
        balloonpos = ikxspace_local*en*sgr(i)
        if (abs(sgr(i)).lt.1e-10) then
          dumbuf = pi*fmaxwl(ixzero,i,j,k,is)*exp(-(balloonpos/isl_ls)**2)
          fdisi(indx(ifdis,imodisland,ixzero,i,j,k,is)) = dumbuf*ci1
        else
          dumbuf = sin(pi*balloonpos)*exp(-(balloonpos/isl_ls)**2)     &
             & *fmaxwl(ixzero,i,j,k,is)/balloonpos
          fdisi(indx(ifdis,imodisland,ixzero,i,j,k,is)) = dumbuf*ci1
        end if

        !number of non-zero positive frequency radial modes
        modnum = (nx-1)/2

        if (nx.gt.1) then
          do p = 1,modnum
            iplusmode = ixzero+p
            iminusmode = ixzero-p
            balloonpos = (ikxspace_local*en*sgr(i)+1.E0*p)
            dumbuf = fmaxwl(iminusmode,i,j,k,is)*sin(pi*balloonpos)*   &
               &  exp(-(balloonpos/isl_ls)**2)/balloonpos
            fdisi(indx(ifdis,imodisland,iminusmode,i,j,k,is)) = dumbuf*ci1
            balloonpos = (ikxspace_local*en*sgr(i)-1.E0*p)
            dumbuf = fmaxwl(iplusmode,i,j,k,is)*sin(pi*balloonpos)*    &
               &  exp(-(balloonpos/isl_ls)**2)/balloonpos
            fdisi(indx(ifdis,imodisland,iplusmode,i,j,k,is)) = dumbuf*ci1
          end do
        end if

      end do; end do; end do; end do

    case('kxzero') !A single mode with kx=0, useful for testing

      if (ixzero.eq.0) call gkw_abort("No kx = 0 mode")
      fdisi=(0.E0,0.E0)
      do i = 1, ns; do j = 1, nmu; do k = 1, nvpar; do is = 1, nsp 
        fdisi(indx(ifdis,3,ixzero,i,j,k,is))=amp_ini
      end do; end do; end do; end do

    case('kyzero') !A single mode with ky=0, useful for testing

      fdisi=(0.E0,0.E0)
      do i = 1, ns; do j = 1, nmu; do k = 1, nvpar; do is = 1, nsp 
        fdisi(indx(ifdis,1,ixzero+3,i,j,k,is))=amp_ini
      end do; end do; end do; end do

    case('line') !A line of gaussians (in real space)

      fdisi=(0.E0,0.E0)
      do i = 1, ns; do j = 1, nmu; do k = 1, nvpar; do is = 1, nsp 
        do ix = 1, nx; do imod = 1, nmod
          i_x = gx(ix)

          a = 0.0
          if (spectral_radius) a = kxrh(ix)

          do line=-49,50 !100 Gaussians shifted by 0.01
            idx=indx(ifdis,imod,ix,i,j,k,is)
            fdisi(idx)=fdisi(idx) + amp_init * (                       &
               & exp(-(real(i_x-ixzero)/real(2*n_x_grid))**2) +       &
               &              exp(-(real(imod)/(2*nmod)**2)) )        &
               * exp(ci1*a*line*0.01*lx) 
          end do
        end do; end do
      end do; end do; end do; end do

    case('ecurrent') !Initialisation of the electron distribution function
      !so that there is a radial current profile so as to induce a tearing mode.
      !Velocity space is a shifted Maxwellian.
      fdisi=(0.E0,0.E0)
      a=0.5  
      do imod = 1,nmod; do ix = 1,nx; do i = 1, ns; do j = 1, nmu
        do k = 1, nvpar; do is = 1, nsp 
          !Parallel current in electrons to trigger a tearing mode.  
          !Perturbation is uniform toroidally and uniform along s,
          !with a gaussian shape radially.
          if ((signz(is).lt.0).and.(imod.eq.1)) then
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = -amp_ini             &
               &  *vthrat(is) *vpgr(i,j,k,is) *fmaxwl(ix,i,j,k,is)   &
               &  *sqrt(1.E0*pi/a) *exp(-pi*pi*kxrh(ix)**2)
          else
            fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini              &
               &      *de(ix,is)*(cos(2*pi*sgr(i))+1.0) 
          end if
        end do; end do
      end do; end do; end do; end do

    case default
      call gkw_abort('Unknown initilization switch finit: '//trim(finit))

    end select
  end if

  ! Multiply the initial distribution with some factor, that will make it zero
  ! near the axis and the plasma edge.
  if (lfinit_radial_dirichlet) then
    do imod = 1,nmod; do ix = 1,nx; do i = 1, ns; do j = 1, nmu
      do k = 1, nvpar; do is = 1, nsp
        i_x = gx(ix)
        ! \note The product (\tanh(+x) + 1)(\tanh(-x) + 1) \leq 1, thus there
        !       is no factor 1/2 needed in front.
        fdisi(indx(ifdis,imod,ix,i,j,k,is)) = fdisi(indx(ifdis,imod,ix,i,j,k,is)) &
             &      *(tanh(0.1/tan(pi*i_x/n_x_grid)) + 1.E0)*(tanh(-0.1/tan(pi*i_x/n_x_grid)) + 1.E0)
      end do; end do
    end do; end do; end do; end do
  end if

  ! Calculate the fields
  if (spectral_radius) then
    call calculate_fields(fdisi(1))
  else
    call field_solve_nonspec_wrap(fdisi(1),0.)
  endif

  ! In electromagnetic cases where the initial apar field is not zero
  ! (e.g. tearingmodes)
  ! correct the initialised distribution initialised by init_fdis
  ! to correspond to f, not g

  if (tearingmode.and..not.restarted) then
    do imod = 1, nmod; do ix = 1, nx; do i = 1, ns; do is = 1, nsp
      do j = 1, nmu; do k = 1, nvpar

        ! FIXME apar_ga is real, but function is complex valued?
        apar_ga = get_averaged_apar(imod,ix,i,j,is,fdisi)
        
        fdisi(indx(ifdis,imod,ix,i,j,k,is)) =           &
            & fdisi(indx(ifdis,imod,ix,i,j,k,is))+      &
            & 2.E0*signz(is)*vthrat(is)*vpgr(i,j,k,is)  &
            & *fmaxwl(ix,i,j,k,is)/tmp(ix,is)*apar_ga

      end do; end do
    end do; end do; end do; end do

  end if
  
  ! Add the Maxwell in the case of a run without a background 
  ! distribution 
  if (fullf_wo_Fm) then 
  
    imod = iyzero 
    if (imod == 0) call gkw_abort('No zero mode to store FM (fullf_wo_Fm = .true.)')
    
    do ix = 1, nx; do i = 1, ns; 
    
      ! The total charge is subtracted 
      !WARNING needs to be parallelized 
      dum = 0.E0 
      do is = 1, nsp; do j = 1, nmu; do k = 1, nvpar 
        dum = dum + signz(is)*bn(ix,i)*intvp(i,j,k,is)*intmu(j)*fmaxwl(ix,i,j,k,is) 
      end do; end do; end do; 

      do is = 1, nsp; do j = 1, nmu; do k= 1, nvpar         
        fdisi(indx(ifdis,imod,ix,i,j,k,is)) = fdisi(indx(ifdis,imod,ix,i,j,k,is)) + & 
             (fmaxwl(ix,i,j,k,is) - dum) / rhostar              
      end do; end do; end do 
      
    end do; end do 
    
  endif 

end subroutine init_fdisi

!-----------------------------------------------------------------------------
!> Initialise the array containing the Maxwellian
!> (or slowing down distribution)
!-----------------------------------------------------------------------------
subroutine init_fmaxwl

  use grid,         only : nx, ns, nsp, nmu, nvpar
  use dist,         only : fmaxwl, falpha
  use dist,         only : f_EP, df_EPdv, df_EPdv_sinhc
  use dist,         only : ghost_points_s, ghost_points_vpar
  use dist,         only : ghost_points_mu
  use geom,         only : bn
  use components,   only : de, types, pbg 
  use components,   only : dgrid, tmp, tgrid
  use components,   only : energetic_particles
  use components,   only : vpar_mean
  use rotation,     only : cfen
  use constants,    only : pi
  use global,       only : r_tiny
  use velocitygrid, only : vpgr, mugr, intmu, intvp
  use functions,    only : mod_sinh_gkw
  use control,      only : order_of_the_scheme  

  ! integers for the loop over all grid points 
  integer :: ix ,i, j, k, is
  integer :: is_extra, ivpar_extra 

  ! Dummy variables 
  logical :: alpha_dist
  logical :: energetic_dist
  real    :: norm, vn, par

  !A bit of a hack so that the extended (ghost points)
  !Maxwellian is initialised even when not parallelised.
  is_extra=2
  ivpar_extra=2
  if(order_of_the_scheme.eq.'second_order')then
    is_extra=1
    ivpar_extra=1
  endif

  ! calculate the normalized maxwellian w**3 / n_grid Fm
  do i = 1-is_extra, ns+is_extra
    do j = 1-ghost_points_mu, nmu+ghost_points_mu
      do k = 1-ivpar_extra, nvpar+ivpar_extra
        do ix = 1, nx; do is = 1, nsp
          if (types(is)=='EP') then
            fmaxwl(ix,i,j,k,is) = 0.E0
          else
            fmaxwl(ix,i,j,k,is) = exp(-(vpgr(i,j,k,is)**2+             &
                     &  2.E0*bn(ix,i)*mugr(j))/(tmp(ix,is)/tgrid(is))) &
                     &  / (sqrt(tmp(ix,is)*pi/tgrid(is))**3)  
          end if
          if (dgrid(is) > r_tiny) then
            fmaxwl(ix,i,j,k,is) = de(ix,is)*fmaxwl(ix,i,j,k,is) / dgrid(is)
          end if
          !Centrifugal parallel density variation,  cfen=0 if vcor=0
          fmaxwl(ix,i,j,k,is)=fmaxwl(ix,i,j,k,is)*exp(-cfen(i,is))
        end do; end do 
      end do
    end do
  end do

  ! the non Maxwellian distributions
 
  alpha_dist     = .false.
  energetic_dist = .false.
  par            = -1.
  do is = 1, nsp 
    if (types(is).eq.'alpha') then 
      alpha_dist = .true.
      par        = pbg(1,is)
    endif
    if ((types(is).eq.'EP').and.(energetic_particles)) then
      energetic_dist = .true.
    end if
  end do
  !Slowing-down distribution for alpha particles
  if (alpha_dist) then 
    ! WARNING this part works only for the vpar_max = 3. 
    ! specify as temperature E_alpha / (9 T_ref) 
    ! the parameter is pbg = 9 E_c / E_alpha 
    do i = 1, ns; do j = 1, nmu ; do k = 1, nvpar 
      vn = sqrt(vpgr(i,j,k,is)**2+2.E0*bn(ix,i)*mugr(j)) 
      if (vn.lt.3) then 
        falpha(i,j,k) = 3.E0 / (4.*pi*log(1.E0 + 27.E0*par**(-1.5))* & 
                      & (par**1.5 + vn**3))
      endif
    end do; end do; end do
  end if
  !Double shifted Maxwellian for NBI-like distribution
  if (energetic_dist) then
    do i = 1, ns; do j = 1,nmu; do k = 1, nvpar; do ix = 1, nx; do is = 1, nsp
      if (types(is) == 'EP') then
        f_EP(ix,i,j,k,is) = exp(-2.E0*bn(ix,i)*mugr(j)/                      &
            & (tmp(ix,is)/tgrid(is)))*                                       &
            & (                                                              &
            & exp(-((vpgr(i,j,k,is)-vpar_mean)**2)/(tmp(ix,is)/tgrid(is)))   &
            & +                                                              &
            & exp(-((vpgr(i,j,k,is)+vpar_mean)**2)/(tmp(ix,is)/tgrid(is)))   &
            & )*                                                             &
            & 0.5E0/(sqrt(tmp(ix,is)*pi/tgrid(is))**3)
        df_EPdv(ix,i,j,k,is) = - 1.E0/(tmp(ix,is)) *                         &
            & exp(-2.E0*bn(ix,i)*mugr(j)/                                    &
            & (tmp(ix,is)/tgrid(is)))*                                       &
            & ( (vpgr(i,j,k,is)-vpar_mean) *                                 &
            & exp(-((vpgr(i,j,k,is)-vpar_mean)**2)/(tmp(ix,is)/tgrid(is)))   &
            & + (vpgr(i,j,k,is)+vpar_mean) *                                 &
            & exp(-((vpgr(i,j,k,is)+vpar_mean)**2)/(tmp(ix,is)/tgrid(is)))   &
            & )*                                                             &
            & 0.5E0/(sqrt(tmp(ix,is)*pi/tgrid(is))**3)
        df_EPdv_sinhc(ix,i,j,k,is) = exp(-(vpgr(i,j,k,is)**2 +               &
            &  2.E0*bn(ix,i)*mugr(j))/(tmp(ix,is)/tgrid(is)))                &
            &  / (sqrt(tmp(ix,is)*pi/tgrid(is))**3) *                        &
            & exp(-vpar_mean**2/(tmp(ix,is)/tgrid(is))) *                    &
            & 4.E0*vpar_mean/(tmp(ix,is)/tgrid(is))*                         &
            & mod_sinh_gkw(2.E0*vpar_mean*vpgr(i,j,k,is)/(tmp(ix,is)))*      &
            & 0.5E0/(sqrt(tmp(ix,is)*pi/tgrid(is))**3)
      else
        f_EP(ix,i,j,k,is)          = 0.E0
        df_EPdv(ix,i,j,k,is)       = 0.E0
        df_EPdv_sinhc(ix,i,j,k,is) = 0.E0
      end if
      if (dgrid(is) > r_tiny) then 
        f_EP(ix,i,j,k,is) = de(ix,is)*f_EP(ix,i,j,k,is) / dgrid(is) 
        df_EPdv(ix,i,j,k,is) = de(ix,is)*df_EPdv(ix,i,j,k,is) / dgrid(is) 
        df_EPdv_sinhc(ix,i,j,k,is) = de(ix,is)*df_EPdv_sinhc(ix,i,j,k,is)    &
                                   & / dgrid(is) 
      end if
    end do; end do; end do; end do; end do
  end if
  ! Renormalize the Maxwell to ensure the correct density
  ! Switched off at the moment since this does not work 
  ! when parallizing over the magnetic moment
  do i = 1, ns; do is =1,nsp ; do ix = 1, nx  
     norm = 0.E0
     do j = 1, nmu ; do k = 1, nvpar 
       norm = norm + bn(ix,i)*intmu(j)*intvp(i,j,k,is)*fmaxwl(ix,i,j,k,is)
     end do; end do
  end do; end do; end do 


end subroutine init_fmaxwl
  
!-----------------------------------------------------------------------------
!> Experimental subroutine used to initialise the code in unusual ways
!> Please explain here what I do, and add imod_init to input.dat.sample
!> I could be merged into init_fdisi. My purpose is to initialise only
!> certain modes when a run is restarted. Presently I am called after
!> all the other initialisation routines. Ideally I would be together
!> With the init_fdisi routines, so that I could chose one of the
!> options there. However, the restart file is not read till *after*
!> init_fdisi is called. My various switches have not been worked out
!-----------------------------------------------------------------------------

subroutine init_imod

  use global,          only : ind_persist
  use grid,            only : nmod,ns,nx,nvpar,nmu,nsp
  use dist,            only : fdisi, ifdis
  use index_function,  only : indx
  use marsaglia,       only : ran_kiss
  use control,         only : imod_init,amp_imod,mode_persist,amp_zon
  use mode,            only : mode_box,ixzero
  use constants,       only : ci1
  use components,      only : amp_init,de
  use exp_integration, only : persistent_mode

  real    :: random1, random2
  integer :: imod, ix, is, i, j, k, iiii
  complex :: amp_ini

  ! for a persistent mode with ikxspace=1, allocate space for a copy of the
  ! intial condition and the index array for copying it into fdisi
  if (mode_persist) then
    allocate(ind_persist(ns*nx*nvpar*nmu*nsp))
    allocate(persistent_mode(ns*nx*nvpar*nmu*nsp))
  end if
  
  amp_ini=amp_init*(1.E0,0.E0)
  iiii=0
  do imod=1,nmod; do i=1,ns; do j=1,nmu; do k=1,nvpar; do is=1,nsp; do ix=1,nx

    if ((imod == 1) .and. mode_box .and. (nmod /= 1)) then
      ! Initialisation of the zonal flow 
      if (ix == ixzero) then
        fdisi(indx(ifdis,imod,ix,i,j,k,is)) = (0.E0,0.E0)  
      else if(ix < ixzero) then
        random1=ran_kiss() ; random2=ran_kiss()
        fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_zon*amp_ini*de(ix,is)*   &
             &                       ((1-2*random1) + (1-2*random2)*ci1)
      else
        fdisi(indx(ifdis,imod,ix,i,j,k,is)) =                              &
             &       conjg(fdisi(indx(ifdis,imod,2*ixzero-ix,i,j,k,is)))
      end if
    else if (imod == imod_init) then
      ! Initialisation of 1 mode
      fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_imod*                      &
           &                          fdisi(indx(ifdis,imod,ix,i,j,k,is))      
      if (mode_persist) then
        ! set up the persistent mode
        iiii=iiii+1
        ind_persist(iiii)=indx(ifdis,imod,ix,i,j,k,is)
        persistent_mode(iiii) = fdisi(ind_persist(iiii))
      end if
    else
      random1 = ran_kiss() ; random2 = ran_kiss() 
      fdisi(indx(ifdis,imod,ix,i,j,k,is)) = amp_ini *                      &
               &   de(ix,is) * ((1-2*random1) + (1-2*random2)*ci1)
    endif

  end do ; end do ; end do  ; end do ; end do ; end do

end subroutine init_imod

!--------------------------------------------------------------------
!> This routine calls the routines that allocate the most fundamental arrays in
!> several modules.
!--------------------------------------------------------------------
subroutine allocate_core

  use matdat,           only : matdat_allocate 
  use components,       only : components_allocate 
  use geom,             only : geom_allocate 
  use mode,             only : mode_allocate
  use rotation,         only : rotation_allocate
  use fields,           only : fields_allocate

  ! Allocate the arrays of matdat.f90 
  call matdat_allocate
    
  ! Allocate the arrays of fields.F90 
  call fields_allocate

  ! Allocate the arrays of components.f90 
  call components_allocate

  ! Allocate the arrays of geom.f90 
  call geom_allocate

  ! Allocate the arrays of mode.f90  
  call mode_allocate  

  ! Allocate the arrays in rotation.f90
  call rotation_allocate
 
end subroutine allocate_core

!--------------------------------------------------------------------
!> This routine calls the routines that allocate other arrays.
!> (These arrays may have sizes which had to be computed before. And
!> may that computation may need an array allocated in the
!> routine allocate_core(). This is why a second round of allocation can
!> make sense.)
!--------------------------------------------------------------------
subroutine allocate_more
  use non_linear_terms, only : nonlinear_allocate

  ! Allocate the arrays of non_linear_terms.f90 Note 
  ! that these arrays are also used for the recon-
  ! struction of linear modes if mode_box is true 
  call nonlinear_allocate 

end subroutine allocate_more

!-----------------------------------------------------------------------------
!> This routine calls the various routines that deallocate the arrays. 
!> Many of the arrays are, at present, not deallocated at the end of the 
!> run. Of course, this is not strictly necessary.
!-----------------------------------------------------------------------------
subroutine deallocate_runtime_arrays

  use exp_integration, only : exp_integration_deallocate
  use fields,          only : fields_deallocate

  call exp_integration_deallocate
  call fields_deallocate

end subroutine deallocate_runtime_arrays

subroutine finalize()
  use components,      only : components_deallocate
  use diagnostic, only : diagnostic_finalize
  use io, only    : finalize_io

  call components_deallocate

  ! clean up any diagnostics and close files
  call diagnostic_finalize()
  
  ! close the IO interfaces
  call finalize_io()

end subroutine finalize

end module init
