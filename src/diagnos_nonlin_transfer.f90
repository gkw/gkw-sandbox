!------------------------------------------------------------------------------
! SVN:$Id$
!>
!> This diagnostic calculates the energy transfer by nonlinear triad
!> interactions.
!>
!> 2 arrays are output:
!>
!> 'nonlin_transfer'
!>
!> 'nonlin_self_interaction'
!------------------------------------------------------------------------------
module diagnos_nonlin_transfer
  use control, only : lnonlin_transfer

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: output

  public :: lnonlin_transfer, nonlin_transfer_interval
  
  integer, save :: lun_nonlin_transfer, lun_nl_self_int

  integer, save :: nonlin_transfer_interval
  
contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    lnonlin_transfer =  .false.
    nonlin_transfer_interval = 1
  end subroutine set_default_nml_values


  !--------------------------------------------------------------------
  !> Broadcast all namelist items of this diagnostic to all processes.
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast
    
    call mpibcast(lnonlin_transfer,1)
    call mpibcast(nonlin_transfer_interval,1)
    
  end subroutine bcast

  !--------------------------------------------------------------------
  !> Check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use general, only : gkw_warn
    use control, only : spectral_radius, non_linear
    if (lnonlin_transfer) then
      if (.not.non_linear) then
        call gkw_warn('nonlinear chart diagnostic can only be used for &
           & nonlinear runs')
        lnonlin_transfer = .false.
      endif
      
      if (.not.spectral_radius) then
        call gkw_warn('nonlinear chart diagnostic is not yet tested for the &
           & nonspectral case')
      endif

      if (nonlin_transfer_interval < 1) then
        call gkw_warn('nonlin_transfer_interval: only values >= 0 make sense. &
           & Value is set to 1.')
        nonlin_transfer_interval = 1
      endif
    endif

  end subroutine check

  !--------------------------------------------------------------------
  !> Initialize the diagnostic. This is the place to open logical
  !> units.
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use io, only : open_real_lu, binary_fmt, xy_fmt
    use io, only : attach_metadata
    use io, only : description_key, comments_key, phys_unit_key, not_avail
    use diagnos_generic, only : attach_metadata_grid
    use non_linear_terms, only : nonlin_transfer, nl_self_int
    use non_linear_terms, only : nonlin_transfer_collect
    
    if (.not.lnonlin_transfer) return
    
    nonlin_transfer_collect = .false.

    if(root_processor) then
      ! open logical units
      call open_real_lu('nonlin_transfer', &
         & 'diagnostic/diagnos_nonlin_transfer', &
         & shape(nonlin_transfer), &
         & binary_fmt, lun_nonlin_transfer)
      call attach_metadata_grid(lun_nonlin_transfer, 'time', 'krho', 'krho', &
         & 'krho_extended', binary_fmt)
      call attach_metadata(lun_nonlin_transfer, phys_unit_key, not_avail, &
         & binary_fmt)
      call attach_metadata(lun_nonlin_transfer, description_key, &
         & 'Entropy transfer rate transfered by the nonlinear interaction &
         & of two binormal &
         & modes (2. and 3. dimension of this dataset) to a third mode &
         & (4. dimension)', &
         & binary_fmt)
      call attach_metadata(lun_nonlin_transfer, comments_key, &
         & 'The sum of all contribution at one timestep should sum up to &
         & zero.', binary_fmt)


      call open_real_lu('nonlin_self_interaction', &
         & 'diagnostic/diagnos_nonlin_transfer', &
         & shape(nl_self_int), &
         & binary_fmt, lun_nl_self_int)
      call attach_metadata_grid(lun_nl_self_int, 'time', 'nmodes', binary_fmt)
      call attach_metadata(lun_nl_self_int, phys_unit_key, not_avail, binary_fmt)
      call attach_metadata(lun_nl_self_int, description_key, &
         & 'The entropy transfer rate due to nonlinear action of each &
         & binormal mode with itself.', binary_fmt)
      call attach_metadata(lun_nl_self_int, comments_key, not_avail, binary_fmt)

    end if

  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use general, only : gkw_abort
    use grid, only : nmod
    use non_linear_terms, only : nonlin_transfer, mphiw3, nl_self_int
    use mode, only : nmodes
    integer :: ierr
    if (.not.lnonlin_transfer) return


  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !> Clean up, deallocate, close everything.
  !--------------------------------------------------------------------
  subroutine finalize()
    use non_linear_terms, only : nonlin_transfer, nl_self_int
    use io, only : close_lu, binary_fmt
    use mpiinterface, only : root_processor

    if (.not.lnonlin_transfer) return
    
    ! deallocate all arrays of this diagnostic
    if(allocated(nonlin_transfer)) deallocate(nonlin_transfer)
    if(allocated(nl_self_int)) deallocate(nl_self_int)

    ! be nice and close all logical units
    if(root_processor) then
      call close_lu(lun_nonlin_transfer, binary_fmt)
      call close_lu(lun_nl_self_int, binary_fmt)
    end if
  end subroutine finalize

  !--------------------------------------------------------------------
  !> The routine calc_largestep() is to be called repeatedly, after every
  !> large timestep, and here the diagnostic should calculate its
  !> quantities.
  !> 
  !> Splitting calculation and output is to be encouraged, particularly when
  !>  - the output is higher dimensional, and requires MPI or MPI-IO
  !>  - same data is output in different dimensional slices
  !--------------------------------------------------------------------
  subroutine calc_largestep()
    use non_linear_terms, only : add_non_linear_terms
    use non_linear_terms, only : nonlin_transfer_reset, nonlin_transfer_collect
    use dist, only : fdisi, nsolc
    use control, only : ntotstep

    if (.not.lnonlin_transfer) return

    if (modulo(ntotstep,nonlin_transfer_interval) /= 0) return
    
    call nonlin_transfer_reset()
    nonlin_transfer_collect = .true.
    call add_non_linear_terms(fdisi(1:nsolc))
    nonlin_transfer_collect = .false.

  end subroutine calc_largestep

  !--------------------------------------------------------------------
  !> this routine writes the content of the buffer nonlin_transfer
  !> to a file
  !--------------------------------------------------------------------
  subroutine output()
    use non_linear_terms, only : nonlin_transfer, mphiw3, nl_self_int
    use grid,             only : nmod
    use control,          only : naverage
    use general,          only : gkw_abort, int2char
    use io,               only : append_chunk, xy_fmt, binary_fmt
    use mpiinterface,     only : root_processor, mpireduce_sum
    real :: buf(nmod,nmod,mphiw3)

    if (.not.lnonlin_transfer) return

    call calc_largestep

    call mpireduce_sum(nonlin_transfer, buf, nmod, nmod, mphiw3)
    nonlin_transfer = buf

    if (root_processor) then

      call append_chunk(lun_nonlin_transfer, nonlin_transfer, &
         & xy_fmt, binary_fmt)

      call append_chunk(lun_nl_self_int, nl_self_int, xy_fmt, binary_fmt)
    endif

  end subroutine output

end module diagnos_nonlin_transfer
