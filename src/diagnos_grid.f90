!------------------------------------------------------------------------------
! SVN:$Id$
!> Add module description here
!------------------------------------------------------------------------------
module diagnos_grid

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output
  public :: read_last_data
  public :: output

  integer, save :: lun_time
  
contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()

  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast
  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use control, only : io_legacy
    use io, only : open_real_lu, ascii_fmt

    if(root_processor) then
      if(.not. io_legacy) then
        call open_real_lu('time', 'grid', &
           & (/ 1 /), ascii_fmt, lun_time)
      end if
    end if
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()

  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()
    use io, only : ascii_fmt, close_lu
    use mpiinterface, only : root_processor
    if(root_processor) then
      call close_lu(lun_time, ascii_fmt)
    end if
  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()
    use mode, only : mode_label
    use mpiinterface, only : root_processor
    use io, only : output_array, xy_fmt, ascii_fmt, attach_metadata
    use io, only : phys_unit_key, description_key, comments_key, not_avail
    use diagnos_generic, only : attach_metadata_grid

    ! write the model_label array to a file. It gives information about
    ! which modes are coupled over the boundary conditions.
    if(root_processor) then
      call output_array('mode_label', 'diagnostic/diagnos_grid', &
         & mode_label*1.0, 'F', &
         & '(4096(F5.0,1x))', ascii_fmt)
      call attach_metadata('mode_label', 'diagnostic/diagnos_grid', &
         & phys_unit_key, not_avail, &
         & ascii_fmt)
      call attach_metadata('mode_label', 'diagnostic/diagnos_grid', &
         & description_key, not_avail, &
         & ascii_fmt)
      call attach_metadata('mode_label', 'diagnostic/diagnos_grid', &
         & comments_key, not_avail, &
         & ascii_fmt)

      call write_xykxky_grids
      call write_kx_connections
      call write_box_parameters
    end if
    call write_velspace_grids



  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine write_kx_connections
    use mpiinterface, only : root_processor
    use grid, only : nmod, nx
    use mode, only : ixplus, ixminus, kxrh, ikxspace, mode_box
    use geom, only : q, eps, shat
    use io, only : get_free_file_unit, clean0r
    integer :: lun
    integer :: imod, ix
    ! Write the kx connections to file
    if (.not.root_processor) return
    
    if (mode_box) then
      call get_free_file_unit(lun)
      open(lun,FILE="kx_connect.dat", FORM='formatted', POSITION='asis')
22    format('shat: ',f6.2,2x,'q: ',f6.2,2x,'eps: ',f6.2,2x,'ikxspace: ', i4)
23    format(i6,f10.6,i6,i6,i6)
      write(lun,22) shat, q, eps, ikxspace
      write(lun,*) 'imod, kxrh(ix), ix, ixplus(imod,ix), ixminus(imod,ix)'
      do imod = 1, nmod
        do ix = 1, nx
          write(lun,23) imod, clean0r(kxrh(ix)), ix, &
             & ixplus(imod,ix), ixminus(imod,ix)
        end do
      end do
      close(lun)
    end if
  end subroutine write_kx_connections

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine read_last_data()
    use control,      only : non_linear, nlapar, icomplete, io_legacy
    use control,      only : last_largestep_time
    use control,      only : time, naverage, dtim
    use mode,         only : mode_box
    use io,           only : lu_exists, ascii_fmt, close_lu
    use io,           only : read_last_chunk, open_real_lu
    use mpiinterface, only : root_processor, mpibcast
    use general,      only : gkw_warn
    integer :: lun, nlast
    real, dimension(1) :: last_chunk

    last_largestep_time = 0.0

    if(.not. io_legacy) then
      if(root_processor) then
        if(.not. lu_exists('time', 'grid', ascii_fmt)) then
          write(*,*) 'file or dataset grid/time not found. Do not read last value.'
          ! Otherwise the time value set in the restart
          ! module is kept.
        else
          call open_real_lu('time', 'grid', &
           & (/ 1 /), ascii_fmt, lun)
          call read_last_chunk(lun, '(3(es13.5))', last_chunk(1:1), &
             & nlast, ascii_fmt)
          call close_lu(lun, ascii_fmt)

          time = last_chunk(1)

          if(icomplete /= nlast) then
            call gkw_warn('The logical unit "time" '// &
               & 'is shorter or longer than expected.');
            write (*,*) "icomplete = ", icomplete
            write (*,*) "number of chunks in logical unit = ", nlast
          end if
        end if
      end if
      
      call mpibcast(time,1)
    end if
    
  end subroutine read_last_data


  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !> 
  !---------------------------------------------------------------------------
  subroutine write_xykxky_grids
    use io, only : output_array, xy_fmt, ascii_fmt, attach_metadata
    use io, only : phys_unit_key, description_key, comments_key, not_avail
    use mode,             only : lyn, lxn, kxrh, krho, mode_box
    use grid,             only : n_s_grid, n_x_grid, nmod
    use geom,             only : q, eps, kthnorm, xgr
    use control,          only : spectral_radius, flux_tube, non_linear
    use non_linear_terms, only : mphiw3
    use mpiinterface,     only : root_processor
    use diagnos_generic, only :  mphit, mrad_G

    integer :: i,j,ix,imod

    if(.not. root_processor) return

    ! phi diagnostic related outputs
    modebox : if (mode_box) then
      
      call output_array('xgr', &
         & 'grid', xgr, &
         & 'F', xy_fmt, ascii_fmt)
      call attach_metadata('xgr', &
         & 'grid', phys_unit_key, &
         & 'dimensionless', &
         & ascii_fmt)
      call attach_metadata('xgr', &
         & 'grid', description_key, &
         & 'radial coordinate grid', ascii_fmt)
      call attach_metadata('xgr', &
         & 'grid', comments_key, &
         & 'In GKW the radial coordinate is epsilon, the inverse aspect ratio. &
         & This quantity appears &
         & also as eps in geom. Some people call the radial coord. the &
         & flux tube label.', ascii_fmt)

      ! The xphi file is not useful for global runs ?
      call output_array('xphi', 'grid', reshape( &
         & (/ ((real(j-1)*lxn/real(mrad_G), j = 1, mrad_G), i = 1, mphit) /), &
         & (/ mrad_G, mphit /)), &
         & 'F', xy_fmt, ascii_fmt)
      call attach_metadata('xphi', 'grid', phys_unit_key, '\rho_{ref}', &
         & ascii_fmt)
      call attach_metadata('xphi', 'grid', description_key, &
         & 'radial coordinate grid', ascii_fmt)
      call attach_metadata('xphi', 'grid', comments_key, &
         & 'The motivation for the xphi,yphi outputs was that when you do &
         & contourf(xphi,yphi,some_2d_data) &
         & You get an output in which the units of both x and y directions &
         & are the same, i.e. in metres normalized to rho_ref. ', &
         & ascii_fmt)

      call output_array('yphi', 'grid', reshape( &
         & (/ ((real(i-1)*lyn/real(mphit), j = 1, mrad_G), i = 1, mphit) /), &
         & (/ mrad_G, mphit /)), &
         & 'F', xy_fmt, ascii_fmt)
      call attach_metadata('yphi', 'grid', phys_unit_key, not_avail, ascii_fmt)
      call attach_metadata('yphi', 'grid', description_key, not_avail, ascii_fmt)
      call attach_metadata('yphi', 'grid', comments_key, &
         ' Actually the grid yphi is not the real space coordinate zeta but &
         & its projection onto theta, i.e. 2 pi/krho_theta .', &
         & ascii_fmt)
    end if modebox

    if (spectral_radius) then
      call output_array('kxrh', 'grid', reshape( &
         & (/ (kxrh, imod = 1, nmod) /), &
         & (/ n_x_grid, nmod /)), &
         & 'F', xy_fmt, ascii_fmt)
      call attach_metadata('kxrh', 'grid', phys_unit_key, not_avail, ascii_fmt)
      call attach_metadata('kxrh', 'grid', description_key, not_avail, ascii_fmt)
      call attach_metadata('kxrh', 'grid', comments_key, not_avail, ascii_fmt)
    end if

    if(flux_tube) then
      call output_array('krho', 'grid', reshape (&
         & (/ ((krho(imod)*kthnorm, ix = 1,n_x_grid), imod = 1,nmod) /), &
         & (/ n_x_grid, nmod /)), &
         & 'F', xy_fmt, ascii_fmt)

      if(non_linear .and. nmod > 1) then
        ! in order to compute the nonlinear term, a finer spatial
        ! grid/a larger k-space grid is used. Output the wavevectors
        ! of that grid:
        ! FIXME this is a bit sloppy
        call output_array('krho_extended', 'grid', reshape (&
           & (/ (krho(2)*kthnorm* imod, imod = 0,mphiw3) /), &
           & (/ mphiw3 /)), &
           & 'F', xy_fmt, ascii_fmt)
      end if
    end if

    call output_array('kzeta', 'grid', reshape (&
       & (/ ((krho(imod), ix = 1,n_x_grid), imod = 1,nmod) /), &
       & (/ n_x_grid, nmod /)), &
       & 'F', xy_fmt, ascii_fmt)


  end subroutine write_xykxky_grids

  !-------------------------------------------------------------------------
  !> Output velocity grids at s point xy_slice_ipar
  !>
  !-------------------------------------------------------------------------
  subroutine write_velspace_grids
    use global,         only : int2char_zeros
    use dist,           only : ifdis
    use index_function, only : indx
    use grid,           only : nmu,nvpar, ls, lsp, number_of_species 
    use grid,           only : nmod, proc_subset
    use velocitygrid,   only : intmu,intvp, vpgr, mugr
    use mode,           only : ixzero
    use diagnos_generic, only : velocity_slice_output, xy_slice_ipar
    use io, only : ascii_fmt
    use control, only : io_legacy
    use global, only : dotdat

    integer :: i, j, isl, ix
    real, allocatable, dimension(:,:) :: local_vpar_mu

    ! set ix here to ixzero 
    ix = ixzero 

    ! allocate arrays to contain the full slice and local slice
    allocate(local_vpar_mu(nvpar,nmu))

    !USE xy_slice_ipar as for xy-slices
    !local s
    isl=ls(xy_slice_ipar)

    !should be safe, since the output routine also returns these.
    if(.not.proc_subset(0,xy_slice_ipar,0,0,0)) return 

    !------------------------------------------
    ! copy the local array into the buffer
    do j=1,nmu
      do i=1,nvpar
        local_vpar_mu(i,j) = intvp(isl,1,i,1)
      end do
    end do

    call velocity_slice_output('diagnostic/diagnos_grid', &
       & local_vpar_mu,dotdat('intvp',io_legacy),ix,xy_slice_ipar,1, ascii_fmt)
    
    !------------------------------------------
    ! copy the local array into the buffer
    do j=1,nmu
      do i=1,nvpar
        ! bn should not feature here: the velocity grid is normalised to Bref
        ! The velocity of a particle is directly the value of the grid
        !  local_vpar_mu(i,j)=sqrt(2.*bn(ix,isl)*mugr(j))
        local_vpar_mu(i,j)=sqrt(2.*mugr(j))
      end do
    end do

    ! former filename: distr2.dat
    call velocity_slice_output('grid', &
       & local_vpar_mu,dotdat('vperp', io_legacy),ix,xy_slice_ipar,1, ascii_fmt)

    !------------------------------------------
    ! copy the local array into the buffer
    do j=1,nmu
      do i=1,nvpar
        local_vpar_mu(i,j)=intmu(j)
      end do
    end do

    call velocity_slice_output('diagnostic/diagnos_grid', &
       & local_vpar_mu,dotdat('intmu',io_legacy),ix,xy_slice_ipar,1, ascii_fmt)
    
    !------------------------------------------
    ! copy the local array into the buffer
    do j=1,nmu
      do i=1,nvpar
        local_vpar_mu(i,j) = vpgr(isl,1,i,1)
      end do
    end do

    !Reorder array to do this
    !local_vpar_mu(:,:)=vpgr(isl,:,:)

    !former filename: distr1.dat
    call velocity_slice_output('grid', &
       & local_vpar_mu, dotdat('vpgr',io_legacy),ix,xy_slice_ipar,1, ascii_fmt)
    
  end subroutine write_velspace_grids

  !-------------------------------------------------------------------------
  !>
  !-------------------------------------------------------------------------
  subroutine write_box_parameters
    use io,               only : get_free_file_unit
    use io,               only : write_run_parameter
    use mode,             only : lyn, lxn, mode_box
    use non_linear_terms, only : mphi, mphiw3
    use grid,             only : n_s_grid, nx, nmod
    use geom,             only : q, eps
    use control,          only : spectral_radius, testing
    use global,           only : lverbose
    use mpiinterface,     only : root_processor
    use diagnos_generic,  only : any_3d_output, mphit, mphiw3t, mrad_G, mrad_l
    use io, only : output_array, xy_fmt, ascii_fmt, attach_metadata
    use io, only : phys_unit_key, description_key, comments_key, not_avail
    use diagnos_generic, only : attach_metadata_grid

    integer :: file_unit
    
    if (.not.root_processor) return

    if(any_3d_output .and. .not. testing) then
      ! The following old IO code is an exception - a diagnostic should
      ! not deal with the lower level IO but use the io wrapper module
      ! instead.
      call get_free_file_unit(file_unit)
      open(file_unit, FILE='3DOutputParam.dat', FORM='formatted', &
         & POSITION='asis')
      write(file_unit,*) 'BoxSize:', lxn
      write(file_unit,*) 'BoxSize:', lyn
      write(file_unit,*) 'mrad:', mrad_G
      write(file_unit,*) 'mphi:', mphi
      write(file_unit,*) 'ns:', n_s_grid
      write(file_unit,*) 'q:', q
      write(file_unit,*) 'eps:', eps
      write(file_unit,*) 'mphit:', mphit
      if (lverbose) write(*,*) '* Geom parameters written to 3DOutputParam.dat'
      close(file_unit)
    end if

    call write_run_parameter('box3D_params', 'lxn', lxn)
    call output_array('lxn','diagnostic/diagnos_grid', (/ lxn /), 'F', &
       & xy_fmt, ascii_fmt)
    call attach_metadata('lxn', 'diagnostic/diagnos_grid', &
       & phys_unit_key, not_avail, &
       & ascii_fmt)
    call attach_metadata('lxn', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)
    call attach_metadata('lxn', 'diagnostic/diagnos_grid', &
       & comments_key, not_avail, &
       & ascii_fmt)
    
    call write_run_parameter('box3D_params', 'lyn', lyn)
    call output_array('lyn','diagnostic/diagnos_grid', (/ lyn /), 'F', &
       & xy_fmt, ascii_fmt)
    call attach_metadata('lyn', 'diagnostic/diagnos_grid', &
       & phys_unit_key, not_avail, &
       & ascii_fmt)
    call attach_metadata('lyn', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)
    call attach_metadata('lyn', 'diagnostic/diagnos_grid', &
       & comments_key, not_avail, &
       & ascii_fmt)
    
    call write_run_parameter('box3D_params', 'mrad_G', mrad_G)
    call output_array('mrad_G','diagnostic/diagnos_grid', (/ mrad_G*1.0 /), &
       & 'F', xy_fmt, ascii_fmt)
    call attach_metadata('mrad_G', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)

    call write_run_parameter('box3D_params', 'mrad_l', mrad_l)
    call output_array('mrad_G','diagnostic/diagnos_grid', (/ mrad_l*1.0 /), &
       & 'F', xy_fmt, ascii_fmt)
    call attach_metadata('mrad_G', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)
    
    call write_run_parameter('box3D_params', 'mphi', mphi)
    call output_array('mphi','diagnostic/diagnos_grid', (/ mphi*1.0 /), &
       & 'F', xy_fmt, ascii_fmt)
    call attach_metadata('mphi', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)

    call write_run_parameter('box3D_params', 'mphiw3', mphiw3)
    call output_array('mphi','diagnostic/diagnos_grid', (/ mphiw3*1.0 /), &
       & 'F', xy_fmt, ascii_fmt)
    call attach_metadata('mphi', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)

    call write_run_parameter('box3D_params', 'mphit', mphit)
    call output_array('mphit','diagnostic/diagnos_grid', (/ mphit*1.0 /), &
       & 'F', xy_fmt, ascii_fmt)
    call attach_metadata('mphit', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)

    call write_run_parameter('box3D_params', 'mphiw3t', mphiw3t)
    call output_array('mphit','diagnostic/diagnos_grid', (/ mphiw3t*1.0 /), &
       & 'F', xy_fmt, ascii_fmt)
    call attach_metadata('mphit', 'diagnostic/diagnos_grid', &
       & description_key, not_avail, &
       & ascii_fmt)

  end subroutine write_box_parameters


  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output()
    use control, only : time, io_legacy
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use mpiinterface, only : root_processor
    
    if(.not. root_processor) return

    if(.not. io_legacy) then
      call append_chunk(lun_time, (/ time /), '(1(es13.5))', ascii_fmt)
    end if

  end subroutine output


end module diagnos_grid
