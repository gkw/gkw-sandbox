! $Id$
!----------------------------------------------------------------------------
!! Doxygen frontpage
!> \mainpage
!! 
!! GKW is a gyro-kinetic simulation code for the study of turbulence 
!! in magnetised plasmas developed from the original linear code LINART.
!!
!! This is the Doxygen generated <b>reference dictionary</b> for GKW.
!! The quickest way to lookup a variable or routine is to use the <b>search
!! box above.</b>
!! 
!! The project source homepage, is at <b>http://bitbucket.org/gkw</b>
!! where you can:  
!!  - Find the full latex documentation and the source
!!  - Contribute to the issue tracker
!!  - Review and comment on changes to the source
!!  - Find instructions for downloading a copy of GKW.
!!
!!<hr> 
!!   Copyright (C) 2003, 2004, 2005
!!     A.G. Peeters, D. Strintzi
!! 
!!   Copyright (C) 2007, 2008, 2009, 2010
!!     A.G. Peeters, Y. Camenen, F.J. Casson, W.A. Hornsby, A.P. Snodin,
!!     D. Strintzi, G. Szepesi
!!
!!  Copyright (C) 2012, 2013, 2014, 2015
!!    A.G. Peeters, Y. Camenen, F.J. Casson, W.A. Hornsby, A.P. Snodin,
!!    D. Strintzi, G. Szepesi, R. Buchholz, S. Grosshauser, P. Manas, 
!!    P. Migliano, M. Siccinio, T. Sung,  D. Zarzoso 
!!<hr> 
!! 
!! A detailed description of the code, how to build it, and how run it
!! can be found in the associated paper [1].
!! 
!! If you use GKW (or some results obtained from it) in any publication, we
!! politely request that you cite paper [1] below, in which the code is
!! comprehensively described. If you wish, you could also cite the original
!! LINART paper [2] and/or the first paper in which GKW was used [3].
!! 
!!  - [1] A.G. Peeters Y. Camenen, F.J. Casson, W.A. Hornsby, A.P. Snodin,
!!      D. Strintzi, and G. Szepesi,
!!      Computer Physics Communications, <b>180</b>, 2650 (2009)
!!      http://dx.doi.org/10.1016/j.cpc.2009.07.001
!! 
!!  - [2] A.G. Peeters, D. Strintzi, Phys. Plasmas, <b>11</b>, 3748 (2004)
!!      http://dx.doi.org/10.1063/1.1762876
!! 
!!  - [3] A.G. Peeters, C. Angioni, D. Strintzi,
!!      Phys. Rev. Lett. <b>98</b>, 265003 (2007) 
!!      http://dx.doi.org/10.1103/PhysRevLett.98.265003
!! 
!! GKW is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!! 
!! GKW is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License (http://www.gnu.org/licenses/gpl-3.0.txt)
!! for more details.
!< End Doxygen frontpage
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! SVN:$Id$
!> Main Program.
!> Top level for GKW
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
program linart
!----------------------------------------------------------------------------
! Normalization: all length scales are normalized
! to the major radius. All velocities are normalized
! the the thermal velocity of the ions. 
! NOTE vth = sqrt(2 T / m )
!--------------------------------------------------------------------

  use mpiinterface,    only : mpiinit, mpiwtime, root_processor
  use mpiinterface,    only : mpibarrier, mpibcast, mpifinalize
  use control,         only : ntime, max_seconds,method, max_sec, &
                            & stop_me, ndump_ts, itime, max_t_fdis_write
  use control,         only : last_smallstep_time, last_largestep_time, time
  use diagnostic,      only : diagnostic_naverage, diagnostic_initial_output, &
                            & diagnostic_final_output
  use restart,         only : write_restart_file
  use exp_integration, only : explicit_integration, init_explicit
  use init,            only : initialize, finalize, deallocate_runtime_arrays
  use imp_integration, only : imp_int
  use eiv_integration, only : eigenvalue_integration
  use general,         only : gkw_exit
  use perform,         only : perfout, perf_measure

  implicit none

  integer :: i, idump=0
  !> time at begin of the run
  double precision :: t_begin
  !> time at the end of the run
  double precision :: t_end
  double precision :: t_tus, t_1, t_predict
  double precision :: t_begin_main, t_end_main
  double precision :: t_diag_b, t_diag, t_step_b, t_step
  real    :: t_1r
  logical :: exstop, exdump

  ! initialize mpi
  call mpiinit()

  ! get the start time of the run 
  t_begin = mpiwtime()

  ! call the initialization
  call initialize

  ! get the time at the start of the main loops
  t_begin_main = mpiwtime()

  ! check if there is anything to output at the beginning of the run
  call diagnostic_initial_output()

  last_largestep_time = time
  last_smallstep_time = time ! setting heredoes not matter with
                             ! current diagnostics

  ! loop over large time steps
  large_time_steps : do i = 1, ntime

    ! store loop value in control
    itime = i

    t_step_b = mpiwtime() 

    select case(method) 
    case('EXP')
      ! do the time integration naverage times 
      call explicit_integration(i)
    case('IMP') 
      call imp_int
    case('EIV')
      call eigenvalue_integration
      exit large_time_steps
    case default 
      call gkw_exit('Unknown method of integration')
    end select   

    t_step = mpiwtime() - t_step_b

    ! calculate and output diagnostics
    t_diag_b = mpiwtime()
    call diagnostic_naverage()
    t_diag = mpiwtime() - t_diag_b

    ! Warn if the diagnostics are slow.
    ! This can happen if there are filesystem problems
    if (t_diag/t_step > 0.1) then
      if (root_processor) then
        write(*,*)
        write(*,*) 'WARNING: Diagnostics are slow:'
        write(*,'(A,es13.5)') ' Diagnostics time (last call):     ', t_diag  
        write(*,'(A,es13.5)') ' Non-diagnostics time (last step): ', t_step
        write(*,*)
      end if 
    end if
    
    ! check for external stop criterium
    exstop = .false.
    if (root_processor) inquire(file='gkw.stop',exist = exstop)
    call mpibcast(exstop,1) ! broadcast from root
    if (exstop) then
      call mpibarrier()
      if (root_processor) then
        write(*,*)'External stop '
        open (9, FILE = 'gkw.stop')
        close (9, STATUS='delete')              
      end if
      exit large_time_steps
    end if 
    
    !Predict total runtime after first iteration
    if (i == 1 .and. root_processor) then
      
      !Time for one iteration in seconds
      t_1 = mpiwtime()-t_begin_main
      t_1r = t_1
      !Total predicted runtime in seconds
      t_predict=t_1*ntime

      if (t_predict > 60.) then
        write(*,*)
        write(*,*) 'Iteration 1 completed successfully.'
        write(*,*) 'Predicted runtime: ', nint(t_predict/60.), ' minutes'
        write(*,*)
      end if
      
      if (t_predict > max_seconds .and. max_seconds > 0.) then
        write(*,*) 'WARNING: Run likely to terminate from max_sec input'
        write(*,*) 'Stop will occur after ', nint((max_sec-300-t_1)/60.), 'minutes' 
        write(*,*) 'Iterations expected: ', nint((max_sec-300-t_1)/t_1), 'of ',  &
            &       ntime,' requested'
        write(*,*)
      end if

      if(perf_measure) then
        !add a perfloop close here
        call perfout(i)
      end if
      
    end if !time predictor
    
    call mpibcast(t_1r,1)

    ! check if maximum time has been exceeded 
    if (max_seconds > 0.) then 
      t_tus = mpiwtime()
      ! the standard is to stop 5 minutes and 1 iteration before walltime
      if (t_tus-t_begin > max_seconds - 300 - t_1r - max_t_fdis_write) then
        if (root_processor) then 
          write(*,*) 'max_sec: stop' 
        end if
        exit large_time_steps
      end if 
    end if 

    ! check if the code needs to stop (convergence, timestep, ...)
    if (stop_me) then 
      if (root_processor) then 
        write(*,*) 'Internal: stop' 
      endif 
      exit large_time_steps 
    endif
    
    ! check for dump criterion
    ! If the code hit a stop condition above, the last dump file 
    ! is not overwritten, because FDS will be written on exit anyway
    idump = idump + 1
    if (root_processor) inquire(file='gkw.dump',exist = exdump) 
    call mpibcast(exdump,1) ! broadcast from root
    if ((exdump.or.(idump.eq.ndump_ts)).and. i.ne.ntime) then
      idump=0
      call mpibarrier()
      if (root_processor.and.exdump) then
        write(*,*)'External dump '
        open (9, FILE = 'gkw.dump')
        close (9, STATUS='delete')              
      end if

      call write_restart_file(.false.)
    end if

    last_largestep_time = time

  end do large_time_steps

  ! get the main loop end time
  t_end_main = mpiwtime()

  ! deallocate arrays used only in the main time loop
  call deallocate_runtime_arrays

  if (method /= 'EIV') then
    ! write restart file first just in case other diagnostics fail
    call write_restart_file(.true.)
  
    ! check if there is anything to output at the end of the run
    call diagnostic_final_output()
  end if

  ! determine the CPU time used 
  t_end = mpiwtime()

  if (root_processor) then
    ! Please do not remove the success message from the following line
    ! unless the test script is modified accordingly.
    write(*,*)'Run successfully completed, Run time :',t_end-t_begin 
    write(*,*)'                    (main loop time):',t_end_main-t_begin_main  
  end if

  if (root_processor .and. perf_measure) call perfout()

  ! finalize in general
  call finalize()

  ! finalize mpi 
  call mpifinalize()

  ! exit status zero indicates successful run
  stop 0

end program linart
