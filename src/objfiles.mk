##############################################################################
### GKW object files.
### If you add a new source file, please ensure you set svn:keywords :
###   svn propset svn:keywords "Author Date Id Revision" newfile.f90
### and add the svn id tag in the header. This is used to number executables.
###
### SVN:$Id$
##############################################################################

F90OBJ =  init.o perform.o linart.o index_function.o constants.o mpicomms.o  \
          specfun.o marsaglia.o components.o fft.o switches.o   \
          exp_integration.o geom.o matdat.o control.o general.o grid.o io.o  \
          functions.o dist.o mode.o linear_terms.o non_linear_terms.o        \
          imp_integration.o normalise.o mpiinterface.o mpidatatypes.o        \
          collisionop.o global.o velocitygrid.o rotation.o tearingmodes.o    \
          diagnostic.o \
          diagnos_generic.o diagnos_growth_freq.o \
          diagnos_rad.o diagnos_eng.o diagnos_energetics.o \
          diagnos_corr.o \
          diagnos_velspace.o diagnos_fluxes.o diagnos_fluxes_vspace.o \
          diagnos_energy.o \
          diagnos_fields.o diagnos_moments.o diagnos_mode_struct.o \
          diagnos_grid.o \
          diagnos_f.o diagnos_stresses.o diagnos_zfshear.o \
          diagnos_nonlin_transfer.o diagnos_freq_wavelet.o \
          diagnos_spat_wavelet.o \
          ompinterface.o gauss.o gyro_average.o fields.o \
          structures.o restart.o mpighosts.o \
          matconv.o krook.o source_time.o eiv_integration.o rho_par_switch.o \
          version.o io_ascii.o io_binary.o io_hdf5.o neoequil.o

## objects required for the main program
OBJLIST = $(F90OBJ) $(UMF_INTERFACE)

## objects required for the input checking executable
CHECKOBJLIST = input_check.o $(filter-out linart.o,$(OBJLIST))
