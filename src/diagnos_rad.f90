!------------------------------------------------------------------------------
! SVN: $Id$
!> Outputs radial profiles for non-spectral runs.
!> A mixture of fields, moments, energetics, and background profiles.
!> Could be logically re-organised into other relevant modules.
!------------------------------------------------------------------------------
module diagnos_rad

  implicit none

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, output

  private

  !> the luns for the radial profile files
  integer, parameter :: maxsp = 16
  integer, save :: i_delta_n(maxsp), i_delta_v(maxsp)
  integer, save :: i_delta_t(maxsp), i_fields, i_prof_back
  !> luns for turbulence and zonal mode intensity
  integer, save :: i_f_int, i_zf_int
  !> luns for turbulence and zonal mode entropy
  integer, save :: i_f_entropy, i_zf_entropy
  !> luns for  turbulence and zonal mode entropy fields
  integer, save :: i_f_entropy_fields, i_zf_entropy_fields

  !> zonal mode entropy flux ExB particles and fields real part
  integer, save :: i_zf_entropy_flux_re, i_zf_entropy_flux_fields_re
  !> zonal mode entropy flux ExB particles and fields imag part
  integer, save :: i_zf_entropy_flux_im, i_zf_entropy_flux_fields_im
  !> perturbations entropy flux ExB 
  integer, save :: i_f_entropy_flux
  !> zonal mode entropy flux drift particles and fields 
  integer, save :: i_zf_entropy_flux_drift, i_zf_entropy_flux_fields_drift
  !> perturbations entropy flux drift particles and fields 
  integer, save :: i_f_entropy_flux_drift, i_f_entropy_flux_fields_drift

  !> Help arrays
  real, save, allocatable  :: dum_G(:)            !< array global in x
  real, save, allocatable  :: dum(:), dum_buf(:)  !< array local in x

  !> Diagnostic switches
  logical, save, public :: lrad_tint = .false.  ! turbulence intensity
  logical, save, public :: lrad_moment = .false.
  logical, save, public :: lrad_field = .false.
  logical, save, public :: lrad_entropy = .false.
  !> radial profiles
  logical, save, public :: lradial_entropy = .false.

  !> logical unit numbers for output files (in legacy format/order/grouping)
  integer, save :: i_radial_entropy = -1, i_entropy_flux  = -1

  !> Entropy results
  real, allocatable :: rad_entr(:,:)

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides.
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    use control, only : flux_tube
    lrad_field =(.not. flux_tube)
    lrad_moment = .false.
    lrad_tint = .false.
    lrad_entropy = .false.
    lradial_entropy= .false.
  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(lradial_entropy, 1)
    call mpibcast(lrad_field, 1)
    call mpibcast(lrad_moment, 1)
    call mpibcast(lrad_tint, 1)
    call mpibcast(lrad_entropy, 1)


  end subroutine bcast

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine check()
    use control, only : nlapar, nlbpar, spectral_radius
    use general, only : gkw_warn

    ! This is only makes the switching behaviour complicated and is therefore
    ! removed:
    !if(lrad_field.or.lrad_moment.or.lrad_tint.or.lrad_entropy)then
    !  lradial_profile = .true.
    !end if

    if (lradial_entropy) then
      if (spectral_radius) then
        call gkw_warn('No radial entropy flux in the spectral version')
        lradial_entropy = .false.
      endif
      if (nlapar) then
        call gkw_warn('No radial entropy flux in EM runs')
        lradial_entropy = .false.
      endif
      if (nlbpar) then
        call gkw_warn('No radial entropy flux in EM runs')
        lradial_entropy = .false.
      endif
    endif

    if (spectral_radius .and. (lrad_field.or.lrad_moment.or. &
      & lrad_tint.or.lrad_entropy))then
      call gkw_warn('The radial profiles have no physical meaning running spectral.')
    endif

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use diagnos_generic, only : lradial_profile
    use grid, only : n_x_grid, nx
    use general, only : gkw_abort
    integer :: ierr

    if (lradial_entropy) then
      allocate(rad_entr(n_x_grid,2),stat = ierr)
      if (ierr /= 0) call gkw_abort('Cannot allocate rad_entr in diagnostics')
    endif

    if (lradial_profile) then
      ! allocate help array
      ierr = 0
      allocate(dum_G(n_x_grid), stat = ierr)
      if (ierr /= 0) call gkw_abort('unable to allocate dum_G in diagnos_rad')
      allocate(dum(nx), stat = ierr)
      if (ierr /= 0) call gkw_abort('unable to allocate dum in diagnos_rad')
      allocate(dum_buf(nx), stat = ierr)
      if (ierr /= 0) call gkw_abort('unable to allocate dum_buf in diagnos_rad')
    end if

  end subroutine allocate_mem


  !---------------------------------------------------------------------------
  !> This subroutine opens the necessary files
  !---------------------------------------------------------------------------
  subroutine init
    use mpiinterface, only : root_processor
    use io,           only : open_real_lu, ascii_fmt
    use grid,         only : n_x_grid, nx, nsp, proc_subset, gsp
    use general,      only : gkw_abort
    use global,       only : int2char_zeros, dotdat
    use control,      only : io_legacy
    integer :: is
    character (len=18) ::  filename

    if (nsp > maxsp) call gkw_abort('increase maxsp in diagnos_rad')

    if (root_processor) then
      if (lradial_entropy) then 
        call open_real_lu(dotdat('entropy_rad',io_legacy), &
           & 'diagnostic/diagnos_rad', (/ n_x_grid /), &
           & ascii_fmt, i_radial_entropy)

        call open_real_lu(dotdat('entropy_flr',io_legacy), &
           & 'diagnostic/diagnos_rad', (/ n_x_grid /), &
           & ascii_fmt, i_entropy_flux)
      endif
    end if

    if (lrad_moment) then
      ! open one file per species, on relevant processors
      ! FIXME This probably does not work with the current serial HDF5 IO!!
      do is = 1, nsp
        if (proc_subset(1,1,1,1,gsp(is))) then
          filename="delta_n"//trim(int2char_zeros(gsp(is),2))
          call open_real_lu(dotdat(trim(filename),io_legacy), &
             & 'diagnostic/diagnos_rad', (/ n_x_grid /), &
             & ascii_fmt, i_delta_n(is))

          filename="delta_v"//trim(int2char_zeros(gsp(is),2))
          call open_real_lu(dotdat(trim(filename),io_legacy), &
             & 'diagnostic/diagnos_rad', (/ n_x_grid /), &
             & ascii_fmt, i_delta_v(is))

          filename="delta_t"//trim(int2char_zeros(gsp(is),2))
          call open_real_lu(dotdat(trim(filename),io_legacy), &
             & 'diagnostic/diagnos_rad', (/ n_x_grid /), &
             & ascii_fmt, i_delta_t(is))
        end if
      end do
    end if

    if (root_processor) then
      if (lrad_tint)then
        call open_real_lu(dotdat('f_int',io_legacy), &
           & 'diagnostic/diagnos_rad', (/ n_x_grid /), &
           & ascii_fmt, i_f_int)
        call open_real_lu(dotdat('zf_int',io_legacy), &
           & 'diagnostic/diagnos_rad', (/ n_x_grid /), &
           & ascii_fmt, i_zf_int)
      endif
      if (lrad_field)then
        call open_real_lu('fields_rad', 'diagnostic/diagnos_rad', (/ n_x_grid /), &
           & ascii_fmt, i_fields)
      endif
      if (lrad_entropy)then
        call open_real_lu(dotdat('f_entropy_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_f_entropy)
        call open_real_lu(dotdat('zf_entropy_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy)
        call open_real_lu(dotdat('f_entropy_fields_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_f_entropy_fields)
        call open_real_lu(dotdat('zf_entropy_fields_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy_fields)
        call open_real_lu(dotdat('zf_entropy_flux_re_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy_flux_re)
        call open_real_lu(dotdat('zf_entropy_flux_im_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy_flux_im)
        call open_real_lu(dotdat('zf_entropy_flux_fields_re_radial_profile', &
           & io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy_flux_fields_re)
        call open_real_lu(dotdat('zf_entropy_flux_fields_im_radial_profile', &
           & io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy_flux_fields_im)
        call open_real_lu(dotdat('f_entropy_flux_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_f_entropy_flux)
        call open_real_lu(dotdat('zf_entropy_flux_drift_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy_flux_drift)
        call open_real_lu(dotdat('zf_entropy_flux_fields_drift_radial_profile', &
           & io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_zf_entropy_flux_fields_drift)
        call open_real_lu(dotdat('f_entropy_flux_drift_radial_profile',io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_f_entropy_flux_drift)
        call open_real_lu(dotdat('f_entropy_flux_fields_drift_radial_profile', &
           & io_legacy), &
           & 'diagnostic/diagnos_rad', &
           & (/ n_x_grid /), &
           & ascii_fmt, i_f_entropy_flux_fields_drift)
      endif
    endif
  end subroutine init

  !--------------------------------------------------------------------
  !> This routine is called at the beginning of each run (after
  !> restarts, too).
  !--------------------------------------------------------------------
  subroutine initial_output()
    use diagnos_generic, only : lradial_profile

    if(lradial_profile) then
      call output_background_profiles
    end if

  end subroutine initial_output

  !------------------------------------------------------------------------------
  !> write the background profiles on file 
  !------------------------------------------------------------------------------
  subroutine output_background_profiles 
    use io,           only : output_array, xy_fmt, ascii_fmt
    use geom,         only : xgr, qx, shatx
    use grid,         only : nsp, nx, number_of_species, n_x_grid
    use mpiinterface, only : root_processor, gather_array 
    use components,   only : de, tmp, fp, tp, vp, iadia, tm_drive, ecurx
    use components,   only : n_spec
    use mpicomms,     only : COMM_X_NE, COMM_SP_NE
    use general,      only : gkw_abort
    use control,      only : io_legacy
    use global,       only : dotdat
    use io, only : attach_metadata
    use io, only : phys_unit_key, description_key, comments_key, not_avail
    use diagnos_generic, only : attach_metadata_grid

    integer :: ix, ierr
    real, allocatable  :: den_G(:,:), temp_G(:,:), rlt_G(:,:), rln_G(:,:)
    real, allocatable  :: ecurx_G(:), uprim_G(:,:)

    ! allocate help array
    ierr = 0
    allocate(den_G(n_x_grid,n_spec), stat = ierr)
    if (ierr /= 0) call gkw_abort('unable to allocate den_G in diagnos_rad')
    allocate(temp_G(n_x_grid,n_spec), stat = ierr)
    if (ierr /= 0) call gkw_abort('unable to allocate temp_G in diagnos_rad')
    allocate(rlt_G(n_x_grid,n_spec), stat = ierr)
    if (ierr /= 0) call gkw_abort('unable to allocate rlt_G in diagnos_rad')
    allocate(rln_G(n_x_grid,n_spec), stat = ierr)
    if (ierr /= 0) call gkw_abort('unable to allocate rln_G in diagnos_rad')
    allocate(uprim_G(n_x_grid,n_spec), stat = ierr)
    if (ierr /= 0) call gkw_abort('unable to allocate uprim_G in diagnos_rad')
    allocate(ecurx_G(n_x_grid), stat = ierr)
    if (ierr /= 0) call gkw_abort('unable to allocate ecurx_G in diagnos_rad')

    ! gather the density/.. information onto the root processor
    call gather_array(den_G(  1:n_x_grid, 1:number_of_species), n_x_grid,      &
       & number_of_species, de( 1:nx, 1:nsp), nx, nsp, COMM_X_NE, COMM_SP_NE)
    call gather_array(temp_G( 1:n_x_grid, 1:number_of_species), n_x_grid,      &
       & number_of_species, tmp(1:nx, 1:nsp), nx, nsp, COMM_X_NE, COMM_SP_NE)
    call gather_array(rln_G(  1:n_x_grid, 1:number_of_species), n_x_grid,      &
       & number_of_species, fp( 1:nx, 1:nsp), nx, nsp, COMM_X_NE, COMM_SP_NE)
    call gather_array(rlt_G(  1:n_x_grid, 1:number_of_species), n_x_grid,      &
       & number_of_species, tp( 1:nx, 1:nsp), nx, nsp, COMM_X_NE, COMM_SP_NE)
    call gather_array(uprim_G(1:n_x_grid, 1:number_of_species), n_x_grid,      &
       & number_of_species, vp( 1:nx, 1:nsp), nx, nsp, COMM_X_NE, COMM_SP_NE)

    ! If there is an adiabatic species, these information also has to be collected.
    if (0 /= iadia) then
      call gather_array(den_G(  1:n_x_grid, n_spec), n_x_grid,&
         & de( 1:nx, nsp+iadia), nx, COMM_X_NE)
      call gather_array(temp_G( 1:n_x_grid, n_spec), n_x_grid,&
         & tmp(1:nx, nsp+iadia), nx, COMM_X_NE)
      call gather_array(rln_G(  1:n_x_grid, n_spec), n_x_grid,&
         & fp( 1:nx, nsp+iadia), nx, COMM_X_NE)
      call gather_array(rlt_G(  1:n_x_grid, n_spec), n_x_grid,&
         & tp( 1:nx, nsp+iadia), nx, COMM_X_NE)
      call gather_array(uprim_G(1:n_x_grid, n_spec), n_x_grid,&
         & vp( 1:nx, nsp+iadia), nx, COMM_X_NE)
    end if

    if(tm_drive) then
      call gather_array(ecurx_G(1:n_x_grid), n_x_grid,  &
         & ecurx(1:nx),nx,COMM_X_NE)
      do ix=1, n_x_grid
        !Currently a dirty hack as this wont work with multiple ion species
        uprim_G(ix,2) = uprim_G(ix,2)+ecurx_G(ix)
      enddo
    endif

    if(root_processor) then
      !> Format for the file 'prof_back'.  This file has 3 (x, q,
      !> shat) + 5 * n_spec columns, where n_spec is the total number
      !> of species, adiabatic and kinetic.  The columns for the
      !> species are respectively (n, T_s, R/L_n, R/L_T, u'), where
      !> first all the densities are given, then the temperature, etc.

      call output_array(dotdat('prof_back', io_legacy) , &
         & 'diagnostic/diagnos_rad', reshape(&
         & (/ (xgr(ix), qx(ix), shatx(ix), den_G(ix,:), temp_G(ix,:), &
         &  rln_G(ix,:), rlt_G(ix,:), uprim_G(ix,:), ix = 1, n_x_grid) /), &
         & (/ 3+5*n_spec, n_x_grid /)), &
         & 'F', xy_fmt, ascii_fmt)
      
      if(.not.io_legacy) then

        ! the q profile appears also in geom, but the traditional
        ! format of geom cannot be read by scripts so easily...
        call output_array('qx', &
           & 'diagnostic/diagnos_rad/background_profiles', qx, &
           & 'F', xy_fmt, ascii_fmt)
        call attach_metadata_grid('qx', &
           & 'diagnostic/diagnos_rad/background_profiles', 'xgr', ascii_fmt)
        call attach_metadata('qx', &
           & 'diagnostic/diagnos_rad/background_profiles', phys_unit_key, &
           & not_avail, &
           & ascii_fmt)
        call attach_metadata('qx', &
           & 'diagnostic/diagnos_rad/background_profiles', description_key, &
           & 'radial profile of the safety factor q' , ascii_fmt)
        call attach_metadata('qx', &
           & 'diagnostic/diagnos_rad/background_profiles', comments_key, &
           & not_avail, ascii_fmt)

        ! the magnetic shear profile appears also in geom
        call output_array('shatx', &
           & 'diagnostic/diagnos_rad/background_profiles', shatx, &
           & 'F', xy_fmt, ascii_fmt)
        call attach_metadata_grid('shat', &
           & 'diagnostic/diagnos_rad/background_profiles', 'xgr', ascii_fmt)
        call attach_metadata('shatx', &
           & 'diagnostic/diagnos_rad/background_profiles', phys_unit_key, &
           & not_avail, &
           & ascii_fmt)
        call attach_metadata('shatx', &
           & 'diagnostic/diagnos_rad/background_profiles', description_key, &
           & 'magnetic shear profile', ascii_fmt)
        call attach_metadata('shatx', &
           & 'diagnostic/diagnos_rad/background_profiles', comments_key, &
           & 'This quantity appears as shat in geom', ascii_fmt)

        call output_array('background_dens', &
           & 'diagnostic/diagnos_rad/background_profiles', den_G, &
           & 'F', xy_fmt, ascii_fmt)
        call attach_metadata_grid('background_dens', &
           & 'diagnostic/diagnos_rad/background_profiles', 'xgr', 'species', &
           & ascii_fmt)
        call attach_metadata('background_dens', &
           & 'diagnostic/diagnos_rad/background_profiles', phys_unit_key, &
           & not_avail, &
           & ascii_fmt)
        call attach_metadata('background_dens', &
           & 'diagnostic/diagnos_rad/background_profiles', description_key, &
           & 'background radial profile of the density', ascii_fmt)
        call attach_metadata('background_dens', &
           & 'diagnostic/diagnos_rad/background_profiles', comments_key, &
           & not_avail, ascii_fmt)

        call output_array('background_temp', &
           & 'diagnostic/diagnos_rad/background_profiles', temp_G, &
           & 'F', xy_fmt, ascii_fmt)
        call attach_metadata_grid('background_temp', &
           & 'diagnostic/diagnos_rad/background_profiles', 'xgr', 'species', &
           & ascii_fmt)
        call attach_metadata('background_temp', &
           & 'diagnostic/diagnos_rad/background_profiles', phys_unit_key, &
           & not_avail, &
           & ascii_fmt)
        call attach_metadata('background_temp', &
           & 'diagnostic/diagnos_rad/background_profiles', description_key, &
           & 'background radial profile of the temperature', ascii_fmt)
        call attach_metadata('background_temp', &
           & 'diagnostic/diagnos_rad/background_profiles', comments_key, &
           & not_avail, ascii_fmt)

        call output_array('background_rln', &
           & 'diagnostic/diagnos_rad/background_profiles', rln_G, &
           & 'F', xy_fmt, ascii_fmt)
        call attach_metadata_grid('background_rln', &
           & 'diagnostic/diagnos_rad/background_profiles', 'xgr', 'species', &
           & ascii_fmt)
        call attach_metadata('background_rln', &
           & 'diagnostic/diagnos_rad/background_profiles', phys_unit_key, &
           & not_avail, &
           & ascii_fmt)
        call attach_metadata('background_rln', &
           & 'diagnostic/diagnos_rad/background_profiles', description_key, &
           & 'background radial profile of the radial density gradient', &
           & ascii_fmt)
        call attach_metadata('background_rln', &
           & 'diagnostic/diagnos_rad/background_profiles', comments_key, &
           & not_avail, ascii_fmt)

        call output_array('background_rlt', &
           & 'diagnostic/diagnos_rad/background_profiles', rlt_G, &
           & 'F', xy_fmt, ascii_fmt)
        call attach_metadata_grid('background_rlt', &
           & 'diagnostic/diagnos_rad/background_profiles', 'xgr', 'species',  &
           & ascii_fmt)
        call attach_metadata('background_rlt', &
           & 'diagnostic/diagnos_rad/background_profiles', phys_unit_key, &
           & not_avail, &
           & ascii_fmt)
        call attach_metadata('background_rlt', &
           & 'diagnostic/diagnos_rad/background_profiles', description_key, &
           & 'background radial profile of the radial temperature gradient', &
           & ascii_fmt)
        call attach_metadata('background_rlt', &
           & 'diagnostic/diagnos_rad/background_profiles', comments_key, &
           & not_avail, ascii_fmt)

        call output_array('background_uprim', &
           & 'diagnostic/diagnos_rad/background_profiles', uprim_G, &
           & 'F', xy_fmt, ascii_fmt)
        call attach_metadata_grid('background_uprim', &
           & 'diagnostic/diagnos_rad/background_profiles', 'xgr', 'species', &
           & ascii_fmt)
        call attach_metadata('background_uprim', &
           & 'diagnostic/diagnos_rad/background_profiles', phys_unit_key, &
           & not_avail, &
           & ascii_fmt)
        call attach_metadata('background_uprim', &
           & 'diagnostic/diagnos_rad/background_profiles', description_key, &
           & 'background radial profile of the negative toroidal rotation &
           & gradient', &
           & ascii_fmt)
        ! FIXME fix this or warn properly
        call attach_metadata('background_uprim', &
           & 'diagnostic/diagnos_rad/background_profiles', comments_key, &
           & 'Currently a dirty hack as this wont work with multiple ion &
           & species!', ascii_fmt)

      end if

    endif

    ! deallocate the help arrays
    deallocate(den_G)
    deallocate(temp_G)
    deallocate(rln_G)
    deallocate(rlt_G)
    deallocate(uprim_G)
    deallocate(ecurx_G)

  end subroutine output_background_profiles

  !------------------------------------------------------------------------------
  ! Subroutine that writes the radial profiles
  ! Not optimized 
  !------------------------------------------------------------------------------
  subroutine output_radial_profile(lrad_tint,lrad_moment,lrad_field,lrad_entropy) 

    use control,        only : nlapar
    use geom,           only : ints
    use grid,           only : nx, n_x_grid, ns, nmod, parallel_s, nvpar, nmu 
    use grid,           only : nsp, parallel_vpar, parallel_mu, gsp
    use grid,           only : proc_subset, gx, lx
    use dist,           only : iphi, iapar, fdisi, ifdis, fmaxwl
    use index_function, only : indx
    use mpiinterface,   only : root_processor, gather_array, mpiallreduce_sum
    use mpicomms,       only : COMM_X_NE, COMM_S_NE, COMM_VPAR_NE_MU_NE
    use components,     only : rhostar, de, dgrid
    use general,        only : gkw_abort
    use geom,           only : bn, efun
    use velocitygrid,   only : intvp, intmu, vpgr, mugr 
    use mode,           only : iyzero, krho
    use fields,         only : get_averaged_phi
    use matdat,         only : get_f_from_g
    use components,     only : signz, tmp
    use constants,      only : ci1, pi
    use io,             only : append_chunk, xy_fmt, ascii_fmt
    use source_time,    only : source_wave_number, source_time_ampl, modulation
    use linear_terms,   only : drift
    use rotation,       only : coriolis, cf_drift

    ! loop integers
    integer :: ix, i, imod, j, k, is, imod_dum
    logical :: lrad_tint,lrad_moment,lrad_field,lrad_entropy

    ! real for non-global rhostar
    real :: dum_rhostar
    
    ! dummy variables
    real :: drift_x, drift_y, drift_z

    ! real for the normalized velocity space integral of the Maxwellian
    ! the integral is calculated at each grid points (species, radial, field)
    !real :: intfm, intfm1, intfm2, intfm_sum

    ! matrix element and amplitude of the source
    !complex :: mat_elem 

    ! check if a zero mode exists 
    if (iyzero == 0) return 

    ! check on rhostar
    if (rhostar == 0) then
      dum_rhostar = 1.
    else
      dum_rhostar = rhostar
    endif

    ! THIS IS A MESS !
    if(lrad_field)then
      call gather_reduce_write_field(iphi,'phi')
      if (nlapar) call gather_reduce_write_field(iapar,'apa')
    endif

    if(lrad_moment)then
      ! The density, toroidal velocity, and temperature perturbations
      imod = iyzero 
      do is = 1, nsp 

        ! calculate the density profile
        dum(:) = 0. 
        do ix = 1, nx; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar 
            dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*  &
               & dum_rhostar*fdisi(indx(ifdis,imod,ix,i,j,k,is))
          end do; end do;
        end do; end do; 

        ! sum for global s 
        if (parallel_s) then 
          call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
          dum(:) = dum_buf(:)
        endif

        if ((parallel_vpar).or.(parallel_mu)) then 
          call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
          dum(:) = dum_buf(:)
        endif

        ! then gather the array 
        call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

        ! write on file 
        if (proc_subset(1,1,1,1,gsp(is))) then 
          call append_chunk(i_delta_n(is), dum_G, xy_fmt, ascii_fmt)
        endif

        ! calculate the toroidal velocity profile
        dum(:) = 0. 
        do ix = 1, nx; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar 
            dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)* &
               & dum_rhostar*fdisi(indx(ifdis,imod,ix,i,j,k,is))          &
               & * vpgr(i,j,k,is)
          end do; end do;
        end do; end do; 

        ! sum for global s 
        if (parallel_s) then 
          call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
          dum(:) = dum_buf(:)
        endif
        if ((parallel_vpar).or.(parallel_mu)) then 
          call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
          dum(:) = dum_buf(:)
        endif

        ! then gather the array 
        call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

        ! write on file 
        if (proc_subset(1,1,1,1,gsp(is))) then 
          call append_chunk(i_delta_v(is), dum_G, xy_fmt, ascii_fmt)
        endif

        ! calculate the temperature profile
        dum(:) = 0. 
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar 
            dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)* &
               & dum_rhostar*fdisi(indx(ifdis,imod,ix,i,j,k,is)) *        &
               & (vpgr(i,j,k,is)**2 + 2.E0*mugr(j)*bn(ix,i)) * 2.E0 * &
               & dgrid(is)/de(ix,is)/3.E0
          end do; end do; 
        end do; end do; 

        ! sum for global s 
        if (parallel_s) then 
          call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
          dum(:) = dum_buf(:)
        endif
        if ((parallel_vpar).or.(parallel_mu)) then 
          call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
          dum(:) = dum_buf(:)
        endif

        ! then gather the array 
        call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

        ! write on file from single processor containing this species
        if (proc_subset(1,1,1,1,gsp(is))) then 
          call append_chunk(i_delta_t(is), dum_G, xy_fmt, ascii_fmt)
        endif

      end do; ! loop over local species
    end if

    ! turbulence intensity
    if(lrad_tint)then

      ! phi^2
      dum(:) = 0. 
      do imod=1, nmod
        do ix = 1, nx ; do i = 1, ns 
          if (imod .ne. iyzero) dum(ix) = dum(ix) + & 
             & ints(i)*abs(fdisi(indx(iphi,imod,ix,i)))**2
        end do; end do; 
      end do

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        call append_chunk(i_f_int, dum_G, xy_fmt, ascii_fmt)
      endif

      ! the zonal mode
      imod = iyzero
      dum(:) = 0.
      do ix = 1, nx ; do i = 1, ns 
        dum(ix) = dum(ix) + & 
           & ints(i)*abs(fdisi(indx(iphi,imod,ix,i)))**2
      end do; end do; 

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        call append_chunk(i_zf_int, dum_G, xy_fmt, ascii_fmt)
      endif
    endif

    ! entropy
    if (lrad_entropy) then

      ! entropy particles in the perturbations
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                   & 2.E0*abs(get_f_from_g(imod,ix,i,j,k,is,fdisi))**2/(2.E0*fmaxwl(ix,i,j,k,is)) )
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        call append_chunk(i_f_entropy, dum_G, xy_fmt, ascii_fmt)
      endif

      ! entropy particles in the zonal mode
      imod = iyzero
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
               & abs(get_f_from_g(imod,ix,i,j,k,is,fdisi))**2/(2.E0*fmaxwl(ix,i,j,k,is)) )
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        call append_chunk(i_zf_entropy, dum_G, xy_fmt, ascii_fmt)
      endif

      ! entropy fileds in the perturbations
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                   & 2.E0*(signz(is)**2*fmaxwl(ix,i,j,k,is))/(2.E0*tmp(ix,is)**2)* &
                   & (abs(fdisi(indx(iphi,imod,ix,i)))**2- &
                   & abs(get_averaged_phi(imod,ix,i,j,is,fdisi))**2))
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        call append_chunk(i_f_entropy_fields, dum_G, xy_fmt, ascii_fmt)
      endif

      ! entropy fields in the zonal mode
      imod = iyzero
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
               & (signz(is)**2*fmaxwl(ix,i,j,k,is))/(2.E0*tmp(ix,is)**2)* &
               & (abs(fdisi(indx(iphi,imod,ix,i)))**2- &
               & abs(get_averaged_phi(imod,ix,i,j,is,fdisi))**2))
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        call append_chunk(i_zf_entropy_fields, dum_G, xy_fmt, ascii_fmt)
      endif

      ! entropy flux ExB particles zonal mode real part
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*2.E0*real( &
                      & (1.0/fmaxwl(ix,i,j,k,is))* &
                      & get_f_from_g(iyzero,ix,i,j,k,is,fdisi)*get_f_from_g(imod,ix,i,j,k,is,fdisi)* &
                      & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_zf_entropy_flux_re,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_zf_entropy_flux_re, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux ExB particles zonal mode imag part
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*2.E0*aimag( &
                      & (1.0/fmaxwl(ix,i,j,k,is))* &
                      & get_f_from_g(iyzero,ix,i,j,k,is,fdisi)*get_f_from_g(imod,ix,i,j,k,is,fdisi)* &
                      & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_zf_entropy_flux_im,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_zf_entropy_flux_im, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux ExB fileds zonal mode real part
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*2.E0*real( &
                      & (signz(is)/tmp(ix,is))* &
                      & get_averaged_phi(iyzero,ix,i,j,is,fdisi)*get_f_from_g(imod,ix,i,j,k,is,fdisi)* &
                      & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_zf_entropy_flux_fields_re,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_zf_entropy_flux_fields_re, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux ExB fileds zonal mode imag part
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*2.E0*aimag( &
                      & (signz(is)/tmp(ix,is))* &
                      & get_averaged_phi(iyzero,ix,i,j,is,fdisi)*get_f_from_g(imod,ix,i,j,k,is,fdisi)* &
                      & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_zf_entropy_flux_fields_im,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_zf_entropy_flux_fields_im, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux ExB perturbations
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=-nmod, nmod
              do imod_dum=-nmod, nmod
                if (imod .ne. 0 .AND. imod_dum .ne. 0) then
                  if (imod .ne. iyzero .AND. imod_dum .ne. iyzero .AND. imod .ne. -iyzero .AND. imod_dum .ne. -iyzero) then
                    if (imod-imod_dum <= nmod .AND. imod-imod_dum >= -nmod .AND. imod .ne. imod_dum) then
                      if (imod > 0 .AND. imod_dum > 0 .AND. imod-imod_dum > 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (get_f_from_g(imod_dum,ix,i,j,k,is,fdisi)/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*get_averaged_phi(imod_dum,ix,i,j,is,fdisi) )* &
                                & get_f_from_g(imod-imod_dum,ix,i,j,k,is,fdisi)* &
                                & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
                      else if (imod > 0 .AND. imod_dum < 0 .AND. imod-imod_dum > 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (conjg(get_f_from_g(-imod_dum,ix,i,j,k,is,fdisi))/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*conjg(get_averaged_phi(-imod_dum,ix,i,j,is,fdisi)) )* &
                                & get_f_from_g(imod-imod_dum,ix,i,j,k,is,fdisi)* &
                                & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
                      else if (imod > 0 .AND. imod_dum > 0 .AND. imod-imod_dum < 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (get_f_from_g(imod_dum,ix,i,j,k,is,fdisi)/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*get_averaged_phi(imod_dum,ix,i,j,is,fdisi) )* &
                                & conjg(get_f_from_g(-imod+imod_dum,ix,i,j,k,is,fdisi))* &
                                & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
                      else if (imod > 0 .AND. imod_dum < 0 .AND. imod-imod_dum < 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (conjg(get_f_from_g(-imod_dum,ix,i,j,k,is,fdisi))/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*conjg(get_averaged_phi(-imod_dum,ix,i,j,is,fdisi)) )* &
                                & conjg(get_f_from_g(-imod+imod_dum,ix,i,j,k,is,fdisi))* &
                                & conjg(ci1*krho(imod)*efun(ix,i,2,1)*get_averaged_phi(imod,ix,i,j,is,fdisi)))
                      else if (imod < 0 .AND. imod_dum > 0 .AND. imod-imod_dum > 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (get_f_from_g(imod_dum,ix,i,j,k,is,fdisi)/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*get_averaged_phi(imod_dum,ix,i,j,is,fdisi) )* &
                                & get_f_from_g(imod-imod_dum,ix,i,j,k,is,fdisi)* &
                                & conjg(-ci1*krho(-imod)*efun(ix,i,2,1)*conjg(get_averaged_phi(-imod,ix,i,j,is,fdisi))))
                      else if (imod < 0 .AND. imod_dum < 0 .AND. imod-imod_dum > 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (conjg(get_f_from_g(-imod_dum,ix,i,j,k,is,fdisi))/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*conjg(get_averaged_phi(-imod_dum,ix,i,j,is,fdisi)) )* &
                                & get_f_from_g(imod-imod_dum,ix,i,j,k,is,fdisi)* &
                                & conjg(-ci1*krho(-imod)*efun(ix,i,2,1)*conjg(get_averaged_phi(-imod,ix,i,j,is,fdisi))))
                      else if (imod < 0 .AND. imod_dum > 0 .AND. imod-imod_dum < 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (get_f_from_g(imod_dum,ix,i,j,k,is,fdisi)/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*get_averaged_phi(imod_dum,ix,i,j,is,fdisi) )* &
                                & conjg(get_f_from_g(-imod+imod_dum,ix,i,j,k,is,fdisi))* &
                                & conjg(-ci1*krho(-imod)*efun(ix,i,2,1)*conjg(get_averaged_phi(-imod,ix,i,j,is,fdisi))))
                      else if (imod < 0 .AND. imod_dum < 0 .AND. imod-imod_dum < 0) then
                        dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                                & (conjg(get_f_from_g(-imod_dum,ix,i,j,k,is,fdisi))/(2.E0*fmaxwl(ix,i,j,k,is)) + &
                                & signz(is)/tmp(ix,is)*conjg(get_averaged_phi(-imod_dum,ix,i,j,is,fdisi)) )* &
                                & conjg(get_f_from_g(-imod+imod_dum,ix,i,j,k,is,fdisi))* &
                                & conjg(-ci1*krho(-imod)*efun(ix,i,2,1)*conjg(get_averaged_phi(-imod,ix,i,j,is,fdisi))))
                      endif
                    endif
                  endif
                endif
              end do;
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_f_entropy_flux,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_f_entropy_flux, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux drift particles zonal mode
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            call drift(ix,i,j,k,is,drift_x,drift_y,drift_z,coriolis,cf_drift,.true.)
            dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                    & abs(get_f_from_g(iyzero,ix,i,j,k,is,fdisi))**2/(2.E0*fmaxwl(ix,i,j,k,is))* &
                    & drift_x)
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_zf_entropy_flux_drift,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_zf_entropy_flux_drift, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux drift fields zonal mode
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            call drift(ix,i,j,k,is,drift_x,drift_y,drift_z,coriolis,cf_drift,.true.)
            dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                    & (signz(is)/tmp(ix,is))**2*fmaxwl(ix,i,j,k,is)* & 
                    & abs(get_averaged_phi(iyzero,ix,i,j,is,fdisi))**2/2.E0* &
                    & drift_x)
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_zf_entropy_flux_fields_drift,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_zf_entropy_flux_fields_drift, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux drift particles perturbations
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                call drift(ix,i,j,k,is,drift_x,drift_y,drift_z,coriolis,cf_drift,.true.)
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                        & 2.E0*abs(get_f_from_g(imod,ix,i,j,k,is,fdisi))**2/(2.E0*fmaxwl(ix,i,j,k,is))* &
                        & drift_x)
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_f_entropy_flux_drift,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_f_entropy_flux_drift, dum_G, xy_fmt, ascii_fmt)
      endif


      ! entropy flux drift fields perturbations
      dum(:) = 0.
      do is = 1, nsp
        do ix = 1, nx ; do i = 1, ns 
          do j = 1, nmu; do k = 1, nvpar
            do imod=1, nmod
              if (imod .ne. iyzero) then
                call drift(ix,i,j,k,is,drift_x,drift_y,drift_z,coriolis,cf_drift,.true.)
                dum(ix) = dum(ix) + ints(i)*intvp(i,j,k,is)*intmu(j)*bn(ix,i)*( &
                        & 2.E0*(signz(is)/tmp(ix,is))**2*fmaxwl(ix,i,j,k,is)* & 
                        & abs(get_averaged_phi(imod,ix,i,j,is,fdisi))**2/2.E0* &
                        & drift_x)
              endif
            end do;
          end do; end do; 
        end do; end do;
      end do;

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif
      if ((parallel_vpar).or.(parallel_mu)) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_VPAR_NE_MU_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      ! write on file 
      if (root_processor) then 
        !oldio: write(i_f_entropy_flux_fields_drift,101)(dum_G(ix),ix = 1, n_x_grid)
        call append_chunk(i_f_entropy_flux_fields_drift, dum_G, xy_fmt, ascii_fmt)
      endif

    endif

  end subroutine output_radial_profile


  subroutine gather_reduce_write_field(id,name)
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use mpicomms, only : COMM_X_NE, COMM_S_NE
    use dist, only : fdisi
    use index_function, only : indx
    use grid, only : nx, n_x_grid, ns, nmod, parallel_s
    use geom, only : ints
    use mpiinterface, only : root_processor, gather_array, mpiallreduce_sum

    integer :: id
    character(len=3) :: name
    integer :: imod, ix, i

    ! loop over binormal modes 
    do imod = 1, nmod 

      dum(:) = 0. 
      do ix = 1, nx 
        do i = 1, ns 
          dum(ix) = dum(ix) + ints(i)*abs(fdisi(indx(id,imod,ix,i))) 
        end do
      end do

      ! sum for global s 
      if (parallel_s) then 
        call mpiallreduce_sum(dum,dum_buf,nx,COMM_S_NE)
        dum(:) = dum_buf(:)
      endif

      ! then gather the array 
      call gather_array(dum_G(1:n_x_grid), n_x_grid, dum(1:nx),nx,COMM_X_NE)

      if (root_processor) then 
        call append_chunk(i_fields, dum_G, xy_fmt, ascii_fmt)
      endif

    end do ! binormal

  end subroutine gather_reduce_write_field

  !--------------------------------------------------------------------------
  !> Close the files and deallocates the arrays
  !--------------------------------------------------------------------------
  subroutine finalize

    ! as this diagnostic opens many logical units, depending on
    ! various switches, it is easier to rely on the io module to close
    ! them.

    
    if(allocated(rad_entr)) deallocate(rad_entr)

    ! deallocate the help arrays 
    if(allocated(dum_G)) deallocate(dum_G)
    if(allocated(dum)) deallocate(dum) 
    if(allocated(dum_buf)) deallocate(dum_buf)

  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output()
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use mpiinterface, only : root_processor
    use diagnos_generic, only : lwrite_output1, lradial_profile
    use non_linear_terms, only : entropy_radial

    if (lwrite_output1) then
      ! calculate the entropy flux 
      if (lradial_entropy) call entropy_radial(rad_entr)

      ! The radial profiles of phi / delta_n / delta_v / delta_t + background 
      if (lradial_profile) call output_radial_profile(lrad_tint,lrad_moment, &
         & lrad_field,lrad_entropy)

      if(root_processor) then
        if (lradial_entropy) then 
          call append_chunk(i_radial_entropy, &
             & rad_entr(:,1), xy_fmt, ascii_fmt)

          call append_chunk(i_entropy_flux, &
             & rad_entr(:,2), xy_fmt, ascii_fmt)
        endif
      end if
    end if

  end subroutine output


end module diagnos_rad 
