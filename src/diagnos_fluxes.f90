!------------------------------------------------------------------------------
! SVN:$Id$
!> Calculates the fluxes of particles, energy and  toroidal angular momentum.
!> Calculates both the anomalous as well as the neo-classical fluxes. 
!> The anomalous flux is furthermore split into contributions due to the 
!> ExB velocity and the magnetic flutter and magnetig compression.
!> Most of the work is done by subroutine calc_fluxes, which also documents
!> the calculation and the noremalisation.  Note that the fluxes are also
!> (for the moment) independently recalculated in diagnos_fluxes_vspace
!------------------------------------------------------------------------------
module diagnos_fluxes

  implicit none

  private

  logical, save, public :: lcalc_fluxes

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, final_output
  public :: output, screen_output

  public :: pflux_es, eflux_es, vflux_es

  ! logicals to control the fluxes output; default to T
  logical, save, public :: lpflux, leflux , lvflux
  logical, save, public :: lfluxes_spectra
  logical, save, public :: lfluxes_em_spectra

  !> A switch to toggle an experimental redefinition of fluxes.
  !> Former comment: "(misuses bpar fluxes arrays)". CHECK if this is still
  !> the case?
  logical, parameter :: lrenorm = .false.
  
  !> logical unit numbers of various output files (new format/order/grouping)
  integer, save :: i_pflux = -1, i_eflux = -1, i_vflux = -1

  !> logical unit numbers for output files (in legacy format/order/grouping)
  integer, save :: i_fluxes_legacy = -1, i_fluxes_em = -1, i_neoclass = -1
  integer, save :: i_neoclass_old = -1
  integer, save :: i_efluxmag = -1, i_pfluxmag = -1
  integer, save :: i_efluxmag_em = -1, i_pfluxmag_em = -1
  integer, save :: i_fluxes_bpar = -1
  integer, save :: i_efluxspec = -1, i_pfluxspec = -1, i_vfluxspec = -1
  integer, save :: i_efluxxspec = -1, i_pfluxxspec = -1, i_vfluxxspec = -1
  integer, save :: i_pflux_rad_es = -1, i_vflux_rad_es = -1, i_eflux_rad_es = -1 
  integer, save :: i_efluxspec_em = -1, i_pfluxspec_em = -1, i_vfluxspec_em = -1
  integer, save :: i_efluxxspec_em = -1, i_pfluxxspec_em = -1, i_vfluxxspec_em = -1

  integer, save :: i_deltaprime =-1
  integer, save :: i_torque =-1


  !> number of slots in fluxes write array
  integer, save :: nfluxes
  
  !> the particle flux per mode : pflux(nmod,nx,number_of_species) due to the 
  !> ExB motion 
  real, save, allocatable, dimension(:,:,:) :: pflux_es

  !> the energy flux per mode : eflux(nmod,nx,number_of_species) due to the 
  !> ExB motion 
  real, save, allocatable, dimension(:,:,:) :: eflux_es

  !> the toroidal angular momentum flux per mode : vflux(nmod,nx,number_of_species)
  !> due to the ExB motion 
  real, save, allocatable, dimension(:,:,:) :: vflux_es

  !> the particle flux per mode pflux(nmod,nx,number_of_species) due to the
  !> flutter of the field  
  real, save, allocatable, dimension(:,:,:) :: pflux_em

  !> the energy flux per mode : eflux(nmod,nx,number_of_species) due to the 
  !> flutter of the field 
  real, save, allocatable, dimension(:,:,:) :: eflux_em

  !> the toroidal angular momentum flux per mode : vflux(nmod,nx,number_of_species)
  !> due to the flutter of the field  
  real, save, allocatable, dimension(:,:,:) :: vflux_em

  !> arrays for island torque and deltap diagnostics
  real, save, allocatable, dimension(:,:) :: deltap_isl
  real, save, allocatable, dimension(:,:) :: torque_isl
   
  !> the particle flux per mode pflux(nmod,nx,number_of_species) due to the
  !> compression of the field  
  real, save, allocatable, dimension(:,:,:) :: pflux_bpar

  !> the energy flux per mode : eflux(nmod,nx,number_of_species) due to the 
  !> compression of the field 
  real, save, allocatable, dimension(:,:,:) :: eflux_bpar

  !> the toroidal angular momentum flux per mode : vflux(nmod,nx,number_of_species)
  !> due to the compression of the field  
  real, save, allocatable, dimension(:,:,:) :: vflux_bpar
  
  !> total particle flux per species : pflux_tot(number_of_species)
  !> electrostatic, electromagnetic, compression
  real, save, allocatable, dimension(:) :: pflux_tot_es
  real, save, allocatable, dimension(:) :: pflux_tot_em
  real, save, allocatable, dimension(:) :: pflux_tot_bpar
  
  !> the total energy flux per species : eflux_tot(number_of_species) 
  !> electrostatic, electromagnetic and compression contributions 
  real, save, allocatable, dimension(:) :: eflux_tot_es
  real, save, allocatable, dimension(:) :: eflux_tot_em
  real, save, allocatable, dimension(:) :: eflux_tot_bpar
  
  !> total toroidal angular momentum flux per species : vflux_tot(number_of_species)
  !> electrostatic, electromagnetic and compression contributions 
  real, save, allocatable, dimension(:) :: vflux_tot_es
  real, save, allocatable, dimension(:) :: vflux_tot_em
  real, save, allocatable, dimension(:) :: vflux_tot_bpar
  
  !> array for writing lines of the fluxes file
  real, save, allocatable, dimension(:) :: flux_tot_es
  real, save, allocatable, dimension(:) :: flux_tot_em
  real, save, allocatable, dimension(:) :: flux_tot_bpar
 
  !> The neoclassical particle flux.  pflux_nc(number_of_species)
  real, save, allocatable, dimension(:) :: pflux_nc
  
  !> The neoclassical energy flux. eflux_nc(number_of_species)
  real, save, allocatable, dimension(:) :: eflux_nc

  !> The neoclassical momentum flux. vflux_nc(number_of_species)
  real, save, allocatable, dimension(:) :: vflux_nc
  
  !> The neoclassical particle flux.  pflux_nc_old(number_of_species)
  real, save, allocatable, dimension(:) :: pflux_nc_old
  
  !> The neoclassical energy flux. eflux_nc_old(number_of_species)
  real, save, allocatable, dimension(:) :: eflux_nc_old
  !> The neoclassical potential energy flux. eflux_nc(number_of_species)
  real, save, allocatable, dimension(:) :: eflux2_nc_old

  !> The neoclassical momentum flux. vflux_nc_old(number_of_species)
  real, save, allocatable, dimension(:) :: vflux_nc_old
  
  !> The buffer for communication. fluxbuf(nmod,nx,number_of_species)
  real, save, allocatable, dimension(:,:,:) :: fluxbuf(:,:,:)
  
  !> The buffer for communication. fluxncbuf(number_of_species)
  real, save, allocatable, dimension(:) :: fluxncbuf

  !> arrays for islands diagnostics
  real, save, allocatable, dimension(:) :: deltaprime
  real, save, allocatable, dimension(:) :: torque

  !> spectral particle flux per species : pflux_spec(nmod,number_of_species)
  !> electrostatic and electro-magnetic contributions 
  real, save, allocatable, dimension(:,:) :: pflux_spec
  real, save, allocatable, dimension(:,:) :: pflux_xspec !<(nx,number_of_species)
  real, save, allocatable, dimension(:,:) :: pflux_em_spec
  real, allocatable, dimension(:,:) :: pflux_em_xspec !<(nx,number_of_species)

  !> spectral energy flux per species : eflux_spec(nmod,number_of_species)
  !> electrostatic and electromagnetic contributions 
  real, save, allocatable, dimension(:,:) :: eflux_spec
  real, save, allocatable, dimension(:,:) :: eflux_xspec !<(nx,number_of_species)
  real, save, allocatable, dimension(:,:) :: eflux_em_spec
  real, save, allocatable, dimension(:,:) :: eflux_em_xspec !<(nx,number_of_species)

  !> the spectral toroidal angular momentum per species : 
  !> vflux_spec(nmod,number_of_species)
  !> electrostatic and electromagnetic contributions 
  real, save, allocatable, dimension(:,:) :: vflux_spec
  real, save, allocatable, dimension(:,:) :: vflux_xspec !<(nx,number_of_species)
  real, save, allocatable, dimension(:,:) :: vflux_em_spec
  real, save, allocatable, dimension(:,:) :: vflux_em_xspec !<(nx,number_of_species)

  !> Private FFT arrays.
  complex, save, allocatable :: a(:,:), b(:,:)
  real, save, allocatable :: ar(:,:), br(:,:), cr(:,:)

  !> complex slice in xy for mpi reduction
  complex, save, allocatable, dimension(:,:) :: cslice_xy
  
  !> complex slice in xy for mpi reduction
  complex, save, allocatable, dimension(:,:) :: c_xy

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
  lpflux = .true.
  leflux = .true.
  lvflux = .true.
  
  lfluxes_spectra = .true.
  lfluxes_em_spectra = .false.
  lcalc_fluxes = .true.
  end subroutine set_default_nml_values


  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast
    
    call mpibcast(lcalc_fluxes,1)
    call mpibcast(lpflux,1)
    call mpibcast(leflux,1)
    call mpibcast(lvflux,1)
    call mpibcast(lfluxes_spectra,1)
    call mpibcast(lfluxes_em_spectra,1)

  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use control, only : nlapar
    use general, only : gkw_warn
    
    ! Typically we want all the fluxes to be output
    nfluxes=0
    if (lpflux) nfluxes = nfluxes + 1
    if (leflux) nfluxes = nfluxes + 1
    if (lvflux) nfluxes = nfluxes + 1

    ! output of the EM fluxes spectra only if EM run
    if ((.not. nlapar).and.lfluxes_em_spectra) then
      call gkw_warn('Output of the EM fluxes spectra not possible with nlapar=.false.')
      lfluxes_em_spectra = .false.
    end if

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use grid, only : number_of_species, n_x_grid, nmod
    use io, only : open_real_lu, ascii_fmt
    use global, only : int2char_zeros
    use control, only : io_legacy
    use control, only : nlapar, neoclassics, nlbpar
    use mpiinterface, only : root_processor
    use diagnos_generic, only : lradial_profile
    use components, only : tearingmode, tm_drive
    use global, only : dotdat
    character(len=25), dimension(number_of_species*nfluxes) :: labels
    integer :: is, iflux
    
    if(root_processor) then
      ! open the file to write the fluxes
      if(io_legacy) then
        do is = 1, number_of_species
          iflux = 1
          if(lpflux) then
            labels(iflux+(is-1)*nfluxes) = 'pflux_species'// &
               & trim(adjustl(int2char_zeros(is,2)))
            iflux = iflux + 1
          end if
          if(leflux) then
            labels(iflux+(is-1)*nfluxes) = 'eflux_species'// &
               & trim(adjustl(int2char_zeros(is,2)))
            iflux = iflux + 1
          end if
          if(lvflux) then
            labels(iflux+(is-1)*nfluxes) = 'vflux_species'// &
               & trim(adjustl(int2char_zeros(is,2)))
            iflux = iflux + 1
          end if
        end do
        call open_real_lu(dotdat('fluxes',io_legacy), &
           & 'diagnostic/diagnos_fluxes', &
           & (/ number_of_species*nfluxes /), &
           & ascii_fmt, i_fluxes_legacy, labels)
      else
        if(lpflux) then
          call open_real_lu('pflux', 'diagnostic/diagnos_fluxes', &
             & (/ number_of_species /), ascii_fmt, i_pflux)
        end if
        if(leflux) then
          call open_real_lu('eflux', 'diagnostic/diagnos_fluxes', &
             & (/ number_of_species /), ascii_fmt, i_eflux)
        end if
        if(lvflux) then
          call open_real_lu('vflux', 'diagnostic/diagnos_fluxes', &
             & (/ number_of_species /), ascii_fmt, i_vflux)
        end if
      end if

      if(nlapar) then
        call open_real_lu(dotdat('fluxes_em', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ number_of_species*nfluxes /), &
           & ascii_fmt, i_fluxes_em)
      end if

      if(nlbpar.or.lrenorm) then
        call open_real_lu(dotdat('fluxes_bpar', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ number_of_species*nfluxes /), &
           & ascii_fmt, i_fluxes_bpar)
      end if

      if (neoclassics) then
        call open_real_lu(dotdat('fluxes_nc_old', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ 4*number_of_species /), &
           & ascii_fmt, i_neoclass_old)
        call open_real_lu(dotdat('fluxes_nc', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ 3*number_of_species /), &
           & ascii_fmt, i_neoclass)
      end if

      if (lradial_profile) then 
        call open_real_lu('pflux_rad_es', 'diagnostic/diagnos_fluxes', &
           & (/ n_x_grid*number_of_species /), &
           & ascii_fmt, i_pflux_rad_es)

        call open_real_lu('vflux_rad_es', 'diagnostic/diagnos_fluxes', &
           & (/ n_x_grid*number_of_species /), &
           & ascii_fmt, i_vflux_rad_es)

        call open_real_lu('eflux_rad_es', 'diagnostic/diagnos_fluxes', &
           &(/ n_x_grid*number_of_species /), &
           & ascii_fmt, i_eflux_rad_es)
      endif

      if (lfluxes_spectra) then
        call open_real_lu(dotdat('eflux_sup', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species /), &
           & ascii_fmt, i_efluxmag)
        call open_real_lu(dotdat('pflux_sup', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species /), &
           & ascii_fmt, i_pfluxmag)
        if (nmod > 1) then
          call open_real_lu(dotdat('eflux_spectra', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species /), &
             & ascii_fmt, i_efluxspec)
          call open_real_lu(dotdat('pflux_spectra', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species /), &
             & ascii_fmt, i_pfluxspec)
          call open_real_lu(dotdat('vflux_spectra', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species /), &
             & ascii_fmt, i_vfluxspec)
        end if
        if (n_x_grid > 1) then
          call open_real_lu(dotdat('eflux_xspec', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ n_x_grid*number_of_species/), &
             & ascii_fmt, i_efluxxspec)
          call open_real_lu(dotdat('pflux_xspec', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ n_x_grid*number_of_species/), &
             & ascii_fmt, i_pfluxxspec)
          call open_real_lu(dotdat('vflux_xspec', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ n_x_grid*number_of_species/), &
             & ascii_fmt, i_vfluxxspec)
        end if
      end if

      if (lfluxes_em_spectra) then
        call open_real_lu(dotdat('eflux_em_sup', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species /), &
           & ascii_fmt, i_efluxmag_em)
        call open_real_lu(dotdat('pflux_em_sup', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species /), &
           & ascii_fmt, i_pfluxmag_em)
        if (nmod > 1) then
          call open_real_lu(dotdat('eflux_em_spectra', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species/), &
             & ascii_fmt, i_efluxspec_em)
          call open_real_lu(dotdat('pflux_em_spectra', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species/), &
             & ascii_fmt, i_pfluxspec_em)
          call open_real_lu(dotdat('vflux_em_spectra', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ nmod*number_of_species/), &
             & ascii_fmt, i_vfluxspec_em)
        end if
        if (n_x_grid > 1) then
          call open_real_lu(dotdat('eflux_em_xspec', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ n_x_grid*number_of_species /), &
             & ascii_fmt, i_efluxxspec_em)
          call open_real_lu(dotdat('pflux_em_xspec', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ n_x_grid*number_of_species /), &
             & ascii_fmt, i_pfluxxspec_em)
          call open_real_lu(dotdat('vflux_em_xspec', io_legacy), &
             & 'diagnostic/diagnos_fluxes', (/ n_x_grid*number_of_species /), &
             & ascii_fmt, i_vfluxxspec_em)
        end if
      end if


      if(tearingmode.or.tm_drive) then
        call open_real_lu(dotdat('deltaprime', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ number_of_species /), &
           & ascii_fmt, i_deltaprime)
        call open_real_lu(dotdat('torque', io_legacy), &
           & 'diagnostic/diagnos_fluxes', (/ number_of_species /), &
           & ascii_fmt, i_torque)
      end if

    end if

  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use grid, only : nmod,nx,number_of_species,n_s_grid,n_x_grid
    use general, only : gkw_abort
    use components, only : tearingmode, tm_drive
    use diagnos_generic, only : mphit, mphiw3t, mrad_l
    use non_linear_terms, only : mphiw3
    use diagnos_generic, only : any_3d_output, xy_fluxes
    integer :: ierr

    ierr=0

    !Private FFT arrays for diagnostics only.
    allocate(a(mphiw3t,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate a in diagnostic')
    endif

    allocate(b(mphiw3t,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate b in diagnostic')
    endif

    allocate(ar(mphit,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate ar in diagnostic')
    endif

    allocate(br(mphit,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate br in diagnostic')
    endif

    allocate(cr(mphit,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate cr in diagnostic')
    endif

    !Why is this needed for any_3d_output?
    if(any_3d_output .or. XY_fluxes) then
      allocate(cslice_xy(mphiw3,mrad_l),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: cslice_xy')
      allocate(c_xy(mphiw3t,mrad_l),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: c_xy')
    end if

    allocate(pflux_es(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_es')
    allocate(eflux_es(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_es')
    allocate(vflux_es(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_es')
    allocate(pflux_em(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_em')
    allocate(eflux_em(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_em')
    allocate(vflux_em(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_em')
    allocate(pflux_bpar(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_bpar')
    allocate(eflux_bpar(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_bpar') 
    allocate(vflux_bpar(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_bpar') 
    allocate(fluxbuf(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: fluxbuf') 
    allocate(pflux_tot_es(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_tot_es')
    allocate(pflux_tot_em(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_tot_em')
    allocate(pflux_tot_bpar(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_tot_bpar')
    allocate(eflux_tot_es(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_tot_es')
    allocate(eflux_tot_em(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_tot_em')
    allocate(eflux_tot_bpar(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_tot_bpar')
    allocate(vflux_tot_es(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_tot_es')
    allocate(vflux_tot_em(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_tot_em')
    allocate(vflux_tot_bpar(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_tot_bpar')
    allocate(pflux_nc(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_nc')
    allocate(eflux_nc(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_nc')
    allocate(vflux_nc(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_nc')
    allocate(pflux_nc_old(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pflux_nc_old')
    allocate(eflux_nc_old(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux_nc_old')
    allocate(eflux2_nc_old(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: eflux2_nc_old')
    allocate(vflux_nc_old(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vflux_nc_old')
    allocate(fluxncbuf(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: fluxncbuf')
    allocate(flux_tot_es(nfluxes*number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: flux_tot_es')
    allocate(flux_tot_em(nfluxes*number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: flux_tot_em')
    allocate(flux_tot_bpar(nfluxes*number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: flux_tot_bpar')

    ! Arrays for spectral fluxes
    if (lfluxes_spectra) then
      allocate(pflux_spec(nmod,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: pflux_spec')
      allocate(eflux_spec(nmod,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: eflux_spec')
      allocate(vflux_spec(nmod,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: vflux_spec')
      allocate(pflux_xspec(n_x_grid,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: pflux_xspec')
      allocate(eflux_xspec(n_x_grid,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: eflux_xspec')
      allocate(vflux_xspec(n_x_grid,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: vflux_xspec')
    end if

    if (lfluxes_em_spectra) then  
      allocate(pflux_em_spec(nmod,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: pflux_em_spec')
      allocate(eflux_em_spec(nmod,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: eflux_em_spec')
      allocate(vflux_em_spec(nmod,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: vflux_em_spec')
      allocate(pflux_em_xspec(nx,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: pflux_em_xspec')
      allocate(eflux_em_xspec(nx,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: eflux_em_xspec')
      allocate(vflux_em_xspec(nx,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: vflux_em_xspec')
    end if

    if (tearingmode.or.tm_drive) then
      allocate(deltaprime(number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: deltaprime')
      allocate(torque(number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: torque')
      allocate(deltap_isl(n_x_grid,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: deltap_isl')
      allocate(torque_isl(n_x_grid,number_of_species),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: torque_isl')
    end if
    
  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()

  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()

  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine final_output(number)
    use diagnos_generic, only : xy_estep
    integer, intent(in) :: number

    !call output_regular(.false.)

    if (.not.xy_estep) then
      call output_xy(number)
    end if

  end subroutine final_output

  !--------------------------------------------------------------------------
  !>
  !> This routine calculates the fluxes of particles, energy and  
  !> toroidal angular momentum
  !>
  !>
  !> Normalisation is such that the flux in the gradient of psi 
  !> (R_ref \nabla \psi, because psi is normalised) is 
  !>
  !> Gamma (R_r nabla psi)    = n_s rho^*2 v_thref pflux 
  !> Q_s (R_r nabla psi)      = n_s rho^*2 v_thref T_s eflux 
  !> Pi_phi(R_r nabla psi) = n_s rho^*2 v_thref m_s R_ref v_ths vflux 
  !>
  !> So to get the real flux one still needs to multiply with the 
  !> density and or temperature of the particular species 
  !>
  !> The normalization, however, has the advantage that 
  !>
  !> D       = rho^*2 Ln vthref pflux 
  !> chi     = rho^*2 Lt vthref eflux 
  !> chi_phi = rho^*2 Lu vthref v_R vflux 
  !>
  !> where the L refers to the gradient length. Note that the momentum 
  !> flux must be multiplied with the relative velocity 
  !>
  !> The routine calculates both the anomalous as well as the neo-
  !> classical fluxes. The anomalous flux is furthermore split in 
  !> the contributions due to the ExB velocity and the magnetic 
  !> flutter. 
  !--------------------------------------------------------------------------
  subroutine calc_fluxes(magnitude)
    use control,        only : nlapar, neoclassics, ncqtol, fluxtol, stop_me
    use control,        only : spectral_radius, flux_tube, ifluxtol
    use control,        only : nlphi, nlbpar
    use control,        only : nan_stop
    use grid,           only : nx, ns, nmu, nvpar, nsp, n_x_grid 
    use grid,           only : nmod, number_of_species, gsp, gx
    use dist,           only : fdisi, fdis_tmp, fmaxwl, i_mom, i_ene
    use dist,           only : iphi, iapar, ibpar
    use geom,           only : ints, bn, efun, signB, bt_frac, Rfun 
    use geom,           only : jfun, metric 
    use mode,           only : krho, kxrh, ixzero, iyzero
    use components,     only : tmp, vthrat, signz, mas
    use components,     only : tearingmode, rhostar, tm_drive
    use rotation,       only : vcor
    use velocitygrid,   only : intmu, intvp, mugr, vpgr
    use constants,      only : pi, ci1
    use matdat,         only : matn,jjn,nmatn, matn_e, jjn_e, matn_v, jjn_v
    use matdat,         only : get_f_from_g 
    use index_function, only : indx 
    use collisionop,    only : mom_conservation,ene_conservation, conservation
    use fields,         only : get_averaged_phi, get_averaged_apar
    use fields,         only : get_averaged_bpar 
    use linear_terms,   only : drift,lpoisson,lvpgrphi,lneorotsource
    use rotation,       only : cf_trap, cf_drift, cfen, vcor, coriolis
    use general,        only : gkw_is_nan
    use rho_par_switch, only : lflux_rhostar
    use mpiinterface,   only : mpiallreduce_sum, root_processor
    use mpiinterface,   only : number_of_processors
    use io,             only : append_chunk, xy_fmt, ascii_fmt
    use diagnos_generic, only : dfieldds

    !> The argument is a switch for calculating flux suprema assuming 90
    !> degree phase angle when this result is used to normalise the
    !> actual fluxes.
    !> It extracts the phase angle (e.g. for quasilinear comparisions) .
    !> In future, may also want to compute the total flux as complex number
    !> so that average phase can be computed AFTER all the averages.
    logical, intent(in) :: magnitude

    ! integers for the loop over all grid points 
    integer :: imod, ix, i, j, k, is 

    ! the gyro-averaged fields 
    complex :: phi_ga, apar_ga, bpar_ga 
    ! and their deviation to s
    complex, allocatable :: dphi_gads(:), dapar_gads(:), dbpar_gads(:)
    ! toggles the rhostar correction in the momentum flux
    ! TODO make input parameter

    ! Dummy variables 
    complex :: dum, dumes1, dumes2, dumem1, dumem2, dumbpar1, dumbpar2
    complex :: dumes_rs, dumem_rs, dumbpar_rs
    complex :: dumem1_cos
    complex :: fdis, aparallel
    real    :: omega_times_time
    ! real phi2, apa2, bpa2
    real drift_x, drift_y, drift_z, dumnc, dumbs
    integer l, imomidx

    ! The global species (ix) index is in isglb (ixg)
    integer isglb, ixg

    ! index for flux output array
    integer :: iflux

    integer, parameter :: arr_size=6
    real    :: std_ncq, mean_ncq, std_flux, mean_flux, velshift
    real, dimension(arr_size), save :: ncq_array=(/6,5,4,3,2,1/)
    real, dimension(arr_size), save :: flux_array=(/6,5,4,3,2,1/)
    integer, save :: incq=0, influx=0

    if(.not. magnitude .and..not. lcalc_fluxes) return
    if(magnitude .and..not. (lcalc_fluxes .and. lfluxes_spectra))  return

    ! Initialize the fluxes to zero 
    pflux_es     = 0. ; eflux_es = 0.     ; vflux_es     = 0.
    pflux_em     = 0. ; eflux_em = 0.     ; vflux_em     = 0.
    pflux_bpar   = 0. ; eflux_bpar = 0.   ; vflux_bpar   = 0.
    pflux_nc     = 0. ; eflux_nc = 0.     ; vflux_nc     = 0.
    pflux_nc_old = 0. ; eflux_nc_old = 0. ; vflux_nc_old = 0.
    eflux2_nc_old = 0.


    ! rho* correction requires derivative of the fields 
    if (lflux_rhostar) then
      if (nlphi) allocate(dphi_gads(ns))
      if (nlapar) allocate(dapar_gads(ns))
      if (nlbpar) allocate(dbpar_gads(ns))
    endif

    if (tm_drive.or.tearingmode) then
      deltap_isl=0.
      torque_isl=0.
    end if

    !  Calculate the particle flux
    nmod1: do imod = 1, nmod 
      nx1:  do ix = 1, nx 
        nsp1:    do is = 1, nsp

          ! the actual (global) species index - blanks are left elsewhere
          isglb = gsp(is)
          ! the actual (global) x index - blanks are left elsewhere
          ixg = gx(ix)

          !Neoclassical fluxes old version
          ! check if this is the 0,0 mode for which the neoclassical
          ! fluxes are calculated 
          ! FJC_NON_SPECTRAL: ix /= ixzero has no meaning
          if ( neoclassics .and. (ix .eq. ixzero) .and.(imod .eq. iyzero)) then
            ns1: do i = 1, ns 
              nmu1: do j = 1, nmu 
                nvpar1: do k = 1, nvpar 

                  if(lneorotsource) then
                    call drift(ix,i,j,k,is,drift_x,drift_y,drift_z,.false.,cf_drift,.true.)       
                  else
                    call drift(ix,i,j,k,is,drift_x,drift_y,drift_z,coriolis,cf_drift,.true.)
                  endif
                  ! Distribution function without A|| correction 
                  fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)
                  if (intvp(i,j,k,is).eq.0.) fdis = 0.

                  ! The potential 
                  phi_ga  = get_averaged_phi(imod,ix,i,j,is,fdisi) 

                  !Following is a correction for the Landau term
                  if(lpoisson.and.lvpgrphi) then
                    fdis = fdis + signz(is)*fmaxwl(ix,i,j,k,is)*phi_ga/tmp(ix,is)
                  end if
                  velshift = signB*bt_frac(ix,i)*rfun(ix,i)*vpgr(i,j,k,is)
                  if(.not.lneorotsource) then
                    velshift = velshift + vcor*jfun(ix,i)
                  endif

                  ! common factors 
                  dumnc = drift_x * (bn(ix,i)*intmu(j)*intvp(i,j,k,is)/signz(is))*  &
                     & fdis*ints(i)
                  dumbs = (bn(ix,i)**2*intmu(j)*intvp(i,j,k,is)*ints(i))*     &
                     & fdis*vpgr(i,j,k,is)*signz(is)
                  dum = (vpgr(i,j,k,is)**2 +  2.E0*mugr(j)*bn(ix,i))            

                  !Correction for when the centrifugal drift is included.
                  if(cf_drift.or.cf_trap) then
                    dum = dum + cfen(i,is)
                  endif
                  !Further correction if a self-consistent potential is included.
                  if(lpoisson) then
                    dum = dum + signz(is)*phi_ga/tmp(ix,is)
                  end if

                  ! the fluxes
                  pflux_nc_old(isglb) = pflux_nc_old(isglb) + dumnc  
                  eflux_nc_old(isglb) = eflux_nc_old(isglb) + dumnc*dum  
                  vflux_nc_old(isglb) = vflux_nc_old(isglb) + dumnc * velshift
                  eflux2_nc_old(isglb) = eflux2_nc_old(isglb) + dumbs

                end do nvpar1
              end do nmu1
            end do ns1
            !write(*,*) 'Neoclassical particle flux (old version)', pflux_nc(isglb)
            !write(*,*) 'Neoclassical heat flux (old version)', eflux_nc(isglb)
            !write(*,*) 'Neoclassical momentum flux (old version)', vflux_nc(isglb)

          end if

          ! New neoclassical diagnostics (momentum conserving term)
          ! FJC_NON_SPECTRAL: ix /= ixzero has no meaning
          neo: if (neoclassics .and. (ix .eq. ixzero).and.(imod .eq. iyzero) .and. &
             & conservation) then
            dumnc = 0.
            ns2: do i = 1, ns 
              nmu2: do j = 1, nmu 
                nvpar2: do k = 1, nvpar
                  dumnc = 0.E0
                  dum = vpgr(i,j,k,is)**2 +  2.E0*mugr(j)*bn(ix,i) 

                  if(mom_conservation) then
                    imomidx = indx(i_mom,imod,ix,i,is) 
                    dumnc = fdisi(imomidx) * fmaxwl(ix,i,j,k,is) * vpgr(i,j,k,is)
                  end if

                  if(ene_conservation) then
                    imomidx = indx(i_ene,imod,ix,i,is)
                    dumnc = dumnc + fdisi(imomidx) * fmaxwl(ix,i,j,k,is) * (dum-3.0E0/2.0E0)    
                  end if

                  velshift = signB*bt_frac(ix,i)*rfun(ix,i)*vpgr(i,j,k,is)
                  if(.not.lneorotsource) then
                    velshift = velshift + vcor*jfun(ix,i)
                  endif
                  dumnc = - dumnc*velshift*4*pi*efun(ix,i,1,2)*bn(ix,i)/signz(is)*&
                     & sqrt(mas(is)*tmp(ix,is))*intmu(j)* intvp(i,j,k,is)*ints(i)

                  !Correction for when the centrifugal drift is included.
                  if(cf_drift.or.cf_trap) then
                    dum = dum + cfen(i,is) 
                  endif
                  if(lpoisson) then
                    dum = dum + signz(is)*phi_ga/tmp(ix,is)
                  end if

                  pflux_nc(isglb) = pflux_nc(isglb) + dumnc  
                  eflux_nc(isglb) = eflux_nc(isglb) + dumnc * (dum-5.0E0/2.0E0)
                  vflux_nc(isglb) = vflux_nc(isglb) + dumnc * 0.5E0*velshift
                end do nvpar2
              end do nmu2
            end do ns2

            !write(*,*) 'Neoclassical particle flux (momcon)', pflux_nc(isglb)
            !write(*,*) 'Neoclassical heat flux (momcon)', eflux_nc(isglb)
            !write(*,*) 'Neoclassical momentum flux (momcon)', vflux_nc(isglb)
          end if neo

          ! need to communicate the derivatives before doing this
          ! this is done in exp integration
          ! FJC_NON_SPECTRAL: ix /= ixzero has no meaning
          neo2: if (neoclassics .and. (ix .eq. ixzero).and.(imod .eq. iyzero)) then
            dumnc = 0.
            nmatm: do l = 1, nmatn
              if (jjn(l) .eq. 0) then 
                write(*,*) l, nmatn
              end if
              !if (jjn(l).gt.nf) then
              !  write(*,*) 'ghost cell'
              !end if
              ! the minus is part of the factors
              ! fdis_tmp here contains f, not g (see exp_int)
              dumnc = - matn(l) * fdis_tmp(jjn(l)) 
              pflux_nc(isglb) = pflux_nc(isglb) + dumnc 
              dumnc = - matn_e(l) * fdis_tmp(jjn_e(l)) 
              eflux_nc(isglb) = eflux_nc(isglb) + dumnc
              dumnc = - matn_v(l) * fdis_tmp(jjn_v(l)) 
              vflux_nc(isglb) = vflux_nc(isglb) + dumnc  
            end do nmatm !loops over species!!, nmu, nvpar, and ns

            !write(*,*) 'Neoclassical particle flux (differential part)', pflux_nc(isglb)
            !write(*,*) 'Neoclassical heat flux (differential part)', eflux_nc(isglb)
            !write(*,*) 'Neoclassical momentum flux (differential part)', vflux_nc(isglb)
          end if neo2

          ! Integral over the velocity space 
          nmu3: do j = 1, nmu
            ! rho* correction requires derivative of the fields 
            if (lflux_rhostar ) then
              if (nlphi)  call dfieldds( iphi,imod,ix,j,is,dphi_gads)
              if (nlapar) call dfieldds( iapar,imod,ix,j,is,dapar_gads)
              if (nlbpar) call dfieldds( ibpar,imod,ix,j,is,dbpar_gads)
            endif
            nvpar3: do k = 1, nvpar

              ! phi2 = 0.
              ! apa2 = 0. 
              ! bpa2 = 0.

              ! Do the average over the flux surface
              ns3: do i = 1, ns

                ! the gyro-averaged fields 
                phi_ga  = get_averaged_phi(imod,ix,i,j,is,fdisi) 
                apar_ga = get_averaged_apar(imod,ix,i,j,is,fdisi)
                bpar_ga = get_averaged_bpar(imod,ix,i,j,is,fdisi) 

                ! fdis is the distribution without A_par contribution  
                fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)

                ! If assuming 90 degree phase angle in fluxes,
                ! take the magnitudes of the complex numbers  
                if (magnitude) then
                  phi_ga  = abs(phi_ga)
                  apar_ga = abs(apar_ga) 
                  bpar_ga = abs(bpar_ga)
                  fdis    = -ci1*abs(fdis)
                end if


                ! in the implicit scheme fdis can be NaN for intvp = 0 
                if (intvp(i,j,k,is).eq.0) fdis = 0.
                ! the 1 in the last index of efun indicates that this
                ! is the radial component of the respective fluxes
                dum  = 2.E0*ints(i)*(efun(ix,i,1,1)*kxrh(ix) +       &
                   & efun(ix,i,2,1)*krho(imod))*bn(ix,i)*fdis

                dumes1 = dum*conjg(phi_ga)*intvp(i,j,k,is)
                dumes2 = dum*conjg(phi_ga)*bn(ix,i)*intvp(i,j,k,is)
                dumem1 = -2.E0*vthrat(is)*vpgr(i,j,k,is)*dum*              &
                   & conjg(apar_ga)*intvp(i,j,k,is)
                dumem2 = -2.E0*vthrat(is)*vpgr(i,j,k,is)*dum*              &
                   & conjg(apar_ga)*bn(ix,i)*intvp(i,j,k,is)
                dumbpar1 = 2.E0*mugr(j)*tmp(ix,is)*dum*                 &
                   & conjg(bpar_ga)*intvp(i,j,k,is)/signz(is)
                dumbpar2 = 2.E0*mugr(j)*tmp(ix,is)*dum*                 &
                   & conjg(bpar_ga)*bn(ix,i)*intvp(i,j,k,is)/signz(is)

                if (lflux_rhostar) then
                  if (nlphi) then
                    dumes_rs = 2.E0*ints(i)*efun(ix,i,3,1)*bn(ix,i) * fdis &
                       * conjg(dphi_gads(i))*intvp(i,j,k,is)
                    !the fluxes requires the real part of the rho*
                    !correction. below the imaginary part of dumes[12]
                    !is used. therefore I multiply with ci1
                    dumes1 = dumes1 + dumes_rs * rhostar * ci1
                    dumes2 = dumes2 + dumes_rs*bn(ix,i)*rhostar * ci1
                  endif
                  if( nlapar) then
                    dumem_rs = -2.E0*ints(i)*vthrat(is)*vpgr(i,j,k,is) * &
                       efun(ix,i,1,3)*bn(ix,i) * fdis  *        &
                       conjg(dapar_gads(i))*intvp(i,j,k,is)
                    dumem1 = dumem1 + rhostar * dumem_rs * ci1
                    dumem2 = dumem2 + dumem_rs * bn(ix,i) * rhostar * ci1
                  endif
                  if (nlbpar) then
                    dumbpar_rs = 2.E0*ints(i)*mugr(j)* tmp(ix,is)*      &
                       efun(ix,i,1,3) * fdis  *                 &
                       conjg(dbpar_gads(i))*intvp(i,j,k,is)/signz(is)
                    dumbpar1 = dumbpar1 + rhostar * dumbpar_rs * ci1
                    dumbpar2 = dumbpar2 + dumbpar_rs*bn(ix,i)*rhostar * ci1
                  endif
                endif

                ! SRG phi2, apa2 and bpa2 are unused?
                ! phi2 = phi2 + ints(i)*abs(phi_ga)**2
                ! apa2 = apa2 + ints(i)*abs(apar_ga)**2
                ! bpa2 = bpa2 + ints(i)*abs(bpar_ga)**2

                pflux_es(imod,ixg,isglb)=pflux_es(imod,ixg,isglb) +         &
                   & aimag(dumes1)*intmu(j) 
                eflux_es(imod,ixg,isglb) = eflux_es(imod,ixg,isglb) +       &
                   & intmu(j)*(vpgr(i,j,k,is)**2*aimag(dumes1)+                &
                   & 2.E0*mugr(j)*aimag(dumes2))
                vflux_es(imod,ixg,isglb)=vflux_es(imod,ixg,isglb) +         &
                   & aimag(dumes1)*intmu(j)*vpgr(i,j,k,is)*Rfun(ix,i)*bt_frac(ix,i)*signB 

                pflux_em(imod,ixg,isglb)=pflux_em(imod,ixg,isglb)+          &
                   & aimag(dumem1)*intmu(j) 
                eflux_em(imod,ixg,isglb) = eflux_em(imod,ixg,isglb) +       &
                   & intmu(j)*(vpgr(i,j,k,is)**2*aimag(dumem1) +               &
                   & 2.E0*mugr(j)*aimag(dumem2))
                vflux_em(imod,ixg,isglb)=vflux_em(imod,ixg,isglb)+          &
                   & aimag(dumem1)*intmu(j)*vpgr(i,j,k,is)*Rfun(ix,i)*bt_frac(ix,i)*signB 

                if (.not.lrenorm) then
                  pflux_bpar(imod,ixg,isglb)=pflux_bpar(imod,ixg,isglb)+          &
                     & aimag(dumbpar1)*intmu(j) 
                  eflux_bpar(imod,ixg,isglb) = eflux_bpar(imod,ixg,isglb) +       &
                     & intmu(j)*(vpgr(i,j,k,is)**2*aimag(dumbpar1) +               &
                     & 2.E0*mugr(j)*aimag(dumbpar2))
                  vflux_bpar(imod,ixg,isglb)=vflux_bpar(imod,ixg,isglb)+          &
                     & aimag(dumbpar1)*intmu(j)*vpgr(i,j,k,is)*Rfun(ix,i)*bt_frac(ix,i)*signB  
                else
                  ! temporary misuse of the bpar flux for an experiment
                  ! (rescaling of the es flux)
                  dumes1 = dum*conjg(phi_ga)*intvp(i,j,k,is)/sqrt(metric(ix,i,1,1))
                  dumes2 = dum*conjg(phi_ga)*bn(ix,i)*intvp(i,j,k,is)/sqrt(metric(ix,i,1,1))

                  pflux_bpar(imod,ixg,isglb)=pflux_bpar(imod,ixg,isglb) +         &
                     & aimag(dumes1)*intmu(j) 
                  eflux_bpar(imod,ixg,isglb) = eflux_bpar(imod,ixg,isglb) +       &
                     & intmu(j)*(vpgr(i,j,k,is)**2*aimag(dumes1)+                &
                     & 2.E0*mugr(j)*aimag(dumes2))
                  vflux_bpar(imod,ixg,isglb)=vflux_bpar(imod,ixg,isglb) +         &
                     & aimag(dumes1)*intmu(j)*vpgr(i,j,k,is)*Rfun(ix,i)*bt_frac(ix,i)*signB
                endif

                if (nlapar .and. (tearingmode.or.tm_drive) &
                   .and. (imod.eq.2)) then

                  !IMPORTANT!! This is the leading term, having confused the
                  !periodicidy angle of the island xi with the 
                  !binormal coordinate of GKW, may be incorrect for large islands 
                  aparallel             = fdisi(indx(iapar,imod,ix,i))
                  omega_times_time      = ATAN2(-aimag(aparallel),real(aparallel))
                  dumem1_cos            = signz(is)*vthrat(is)* &
                     vpgr(i,j,k,is)*fdis* &
                     EXP(CMPLX(0,omega_times_time))*ints(i)
                  deltap_isl(ixg,isglb) = deltap_isl(ixg,isglb)+ &
                     real(dumem1_cos)*intmu(j)* &
                     intvp(i,j,k,is)*bn(ix,i)
                  torque_isl(ixg,isglb) = torque_isl(ixg,isglb)- &
                     aimag(dumem1_cos)*intmu(j)* &
                     intvp(i,j,k,is)*bn(ix,i)
                end if

              end do ns3
            end do nvpar3
          end do nmu3
        end do nsp1
      end do nx1
    end do nmod1

    !deallocate temp derivation of the fields
    if (lflux_rhostar)  then
      if(allocated(dphi_gads)) deallocate(dphi_gads)
      if(allocated(dapar_gads)) deallocate(dapar_gads)
      if(allocated(dbpar_gads)) deallocate(dbpar_gads)
    endif

    ! only when run on more than one processor (saves two copies)
    if (number_of_processors.gt.1) then 

      ! The 3D arrays ([k]y, k[x], species)
      ! are MPI reduced over s and velocity and "gathered" over x and species
      call fluxes_parallel_sum_gather(pflux_es)
      call fluxes_parallel_sum_gather(pflux_em)
      call fluxes_parallel_sum_gather(pflux_bpar)
      call fluxes_parallel_sum_gather(eflux_es)
      call fluxes_parallel_sum_gather(eflux_em)
      call fluxes_parallel_sum_gather(eflux_bpar)
      call fluxes_parallel_sum_gather(vflux_es)
      call fluxes_parallel_sum_gather(vflux_em)
      call fluxes_parallel_sum_gather(vflux_bpar)
      if (tearingmode.or.tm_drive) call fluxes_parallel_sum_gather_x_sp(deltap_isl)
      if (tearingmode.or.tm_drive) call fluxes_parallel_sum_gather_x_sp(torque_isl) 

      ! 1D arrays (species only)
      call mpiallreduce_sum(pflux_nc,fluxncbuf,number_of_species)
      pflux_nc(:) = fluxncbuf(:)

      call mpiallreduce_sum(eflux_nc,fluxncbuf,number_of_species)
      eflux_nc(:) = fluxncbuf(:)

      call mpiallreduce_sum(vflux_nc,fluxncbuf,number_of_species)
      vflux_nc(:) = fluxncbuf(:)

      call mpiallreduce_sum(pflux_nc_old,fluxncbuf,number_of_species)
      pflux_nc_old(:) = fluxncbuf(:)

      call mpiallreduce_sum(eflux_nc_old,fluxncbuf,number_of_species)
      eflux_nc_old(:) = fluxncbuf(:)

      call mpiallreduce_sum(eflux2_nc_old,fluxncbuf,number_of_species)
      eflux2_nc_old(:) = fluxncbuf(:)

      call mpiallreduce_sum(vflux_nc_old,fluxncbuf,number_of_species)
      vflux_nc_old(:) = fluxncbuf(:)

    end if

    if ((ncqtol > 0.).and.neoclassics) then
      ! fill in the array storing the nc flux history
      incq = mod(incq+1,arr_size)
      ncq_array(incq+1) = eflux_nc(1)

      ! standard deviation of the nc flux history
      std_ncq = 0.
      mean_ncq = sum(ncq_array)/arr_size
      do i=1,arr_size
        std_ncq = std_ncq + (ncq_array(i)-mean_ncq)**2
      end do
      std_ncq = sqrt(std_ncq/arr_size)

      ! do a check for convergence on the growth rate
      !APS: TODO only implemented for single (or max) growth rate for now
      stop_me = (std_ncq < ncqtol) .or. stop_me
      if (stop_me .and. root_processor) write(*,*) 'Neoclassical Convergence reached: stop'
    end if

    ! sum the total flux - make seperate routine? 
    species: do is = 1, number_of_species 
      pflux_tot_es(is) = 0. 
      eflux_tot_es(is) = 0. 
      vflux_tot_es(is) = 0. 
      pflux_tot_em(is) = 0. 
      eflux_tot_em(is) = 0. 
      vflux_tot_em(is) = 0. 
      pflux_tot_bpar(is) = 0. 
      eflux_tot_bpar(is) = 0. 
      vflux_tot_bpar(is) = 0. 

      if (tearingmode.or.tm_drive) then
        deltaprime(is)=0.
        torque(is)=0.
      end if

      do imod = 1, nmod

        if(lfluxes_spectra) then
          pflux_spec(imod,is) = 0. 
          eflux_spec(imod,is) = 0. 
          vflux_spec(imod,is) = 0. 
        end if
        if(lfluxes_em_spectra) then
          pflux_em_spec(imod,is) = 0. 
          eflux_em_spec(imod,is) = 0. 
          vflux_em_spec(imod,is) = 0. 
        end if

        do ix = 1, n_x_grid

          if(lfluxes_spectra) then
            pflux_spec(imod,is) = pflux_spec(imod,is) + pflux_es(imod,ix,is)
            eflux_spec(imod,is) = eflux_spec(imod,is) + eflux_es(imod,ix,is)
            vflux_spec(imod,is) = vflux_spec(imod,is) + vflux_es(imod,ix,is)
          end if
          if(lfluxes_em_spectra) then
            pflux_em_spec(imod,is) = pflux_em_spec(imod,is) + pflux_em(imod,ix,is)
            eflux_em_spec(imod,is) = eflux_em_spec(imod,is) + eflux_em(imod,ix,is)
            vflux_em_spec(imod,is) = vflux_em_spec(imod,is) + vflux_em(imod,ix,is)
          end if

          pflux_tot_es(is) = pflux_tot_es(is) + pflux_es(imod,ix,is)
          eflux_tot_es(is) = eflux_tot_es(is) + eflux_es(imod,ix,is)
          vflux_tot_es(is) = vflux_tot_es(is) + vflux_es(imod,ix,is)

          pflux_tot_em(is) = pflux_tot_em(is) + pflux_em(imod,ix,is)
          eflux_tot_em(is) = eflux_tot_em(is) + eflux_em(imod,ix,is) 
          vflux_tot_em(is) = vflux_tot_em(is) + vflux_em(imod,ix,is) 

          pflux_tot_bpar(is) = pflux_tot_bpar(is) + pflux_bpar(imod,ix,is)
          eflux_tot_bpar(is) = eflux_tot_bpar(is) + eflux_bpar(imod,ix,is) 
          vflux_tot_bpar(is) = vflux_tot_bpar(is) + vflux_bpar(imod,ix,is) 

          if ((tearingmode.or.tm_drive) .and. nlapar &
             .and. (imod.eq.2)) then
            deltaprime(is) = deltaprime(is)+ deltap_isl(ix,is) 
            torque(is) = torque(is)+ torque_isl(ix,is)
          end if
        end do !n_x_grid

        ! for the non-spectral case (but still flux tube) pflux_tot (as well) 
        ! as the spectrum must be divided by n_x_grid
        if (.not.spectral_radius .and. flux_tube) then 

          if(lfluxes_spectra) then
            pflux_spec(imod,is) = pflux_spec(imod,is) / n_x_grid 
            eflux_spec(imod,is) = eflux_spec(imod,is) / n_x_grid
            vflux_spec(imod,is) = vflux_spec(imod,is) / n_x_grid
          end if
          if(lfluxes_em_spectra) then
            pflux_em_spec(imod,is) = pflux_em_spec(imod,is) / n_x_grid 
            eflux_em_spec(imod,is) = eflux_em_spec(imod,is) / n_x_grid
            vflux_em_spec(imod,is) = vflux_em_spec(imod,is) / n_x_grid
          end if

        endif

      end do !nmod

      ! modify the total fluxes for the non-spectral case 
      if (.not.spectral_radius) then 
        pflux_tot_es(is) = pflux_tot_es(is) / n_x_grid
        eflux_tot_es(is) = eflux_tot_es(is) / n_x_grid
        vflux_tot_es(is) = vflux_tot_es(is) / n_x_grid

        pflux_tot_em(is) = pflux_tot_em(is) / n_x_grid
        eflux_tot_em(is) = eflux_tot_em(is) / n_x_grid
        vflux_tot_em(is) = vflux_tot_em(is) / n_x_grid

        pflux_tot_bpar(is) = pflux_tot_bpar(is) / n_x_grid
        eflux_tot_bpar(is) = eflux_tot_bpar(is) / n_x_grid
        vflux_tot_bpar(is) = vflux_tot_bpar(is) / n_x_grid

        if (tearingmode.or.tm_drive) then
          deltaprime(is) = deltaprime(is) / n_x_grid  
          torque(is) = torque(is) / n_x_grid
        end if

      endif

      !kx_spectra
      ! FJC_NON_SPECTRAL need FFT
      if (lfluxes_spectra) then
        do ix = 1, n_x_grid
          pflux_xspec(ix,is) = 0. 
          eflux_xspec(ix,is) = 0. 
          vflux_xspec(ix,is) = 0.
          do imod = 1, nmod 
            pflux_xspec(ix,is) = pflux_xspec(ix,is) + pflux_es(imod,ix,is)
            eflux_xspec(ix,is) = eflux_xspec(ix,is) + eflux_es(imod,ix,is)
            vflux_xspec(ix,is) = vflux_xspec(ix,is) + vflux_es(imod,ix,is)
          end do !nmod
        end do !nx
      end if !lfluxes_spectra

      if (lfluxes_em_spectra) then
        do ix = 1, n_x_grid
          pflux_em_xspec(ix,is) = 0. 
          eflux_em_xspec(ix,is) = 0. 
          vflux_em_xspec(ix,is) = 0.
          do imod = 1, nmod 
            pflux_em_xspec(ix,is) = pflux_em_xspec(ix,is) + pflux_em(imod,ix,is)
            eflux_em_xspec(ix,is) = eflux_em_xspec(ix,is) + eflux_em(imod,ix,is)
            vflux_em_xspec(ix,is) = vflux_em_xspec(ix,is) + vflux_em(imod,ix,is)
          end do !nmod
        end do !nx
      end if !lfluxes_em_spectra

      ! order the required fluxes for output
      iflux = 1
      if (lpflux) then
        flux_tot_es(iflux + (is-1)*nfluxes) = pflux_tot_es(is)
        flux_tot_em(iflux + (is-1)*nfluxes) = pflux_tot_em(is)
        flux_tot_bpar(iflux + (is-1)*nfluxes) = pflux_tot_bpar(is)
        iflux = iflux + 1
      end if
      if (leflux) then
        flux_tot_es(iflux + (is-1)*nfluxes) = eflux_tot_es(is)
        flux_tot_em(iflux + (is-1)*nfluxes) = eflux_tot_em(is)
        flux_tot_bpar(iflux + (is-1)*nfluxes) = eflux_tot_bpar(is)
        iflux = iflux + 1
      end if
      if (lvflux) then
        flux_tot_es(iflux + (is-1)*nfluxes) = vflux_tot_es(is)
        flux_tot_em(iflux + (is-1)*nfluxes) = vflux_tot_em(is)        
        flux_tot_bpar(iflux + (is-1)*nfluxes) = vflux_tot_bpar(is)
        iflux = iflux + 1
      end if
    end do species

    ! if only fluxes suprema, output and return
    if (magnitude) then
      if (root_processor) then
        if(lfluxes_spectra) then
          call append_chunk(i_efluxmag, &
             ! & (/ ((eflux_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(eflux_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_pfluxmag, &
             ! & (/ ((pflux_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(pflux_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
        end if

        if (lfluxes_em_spectra) then
          call append_chunk(i_efluxmag_em, &
             ! & (/ ((eflux_em_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(eflux_em_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_pfluxmag_em, &
             ! & (/ ((pflux_em_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(pflux_em_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
        end if
      end if
      return
    end if

    if (fluxtol > 0.) then
      ! fill in the array storing the flux history
      influx = mod(influx+1,arr_size)
      flux_array(influx+1) = flux_tot_es(min(ifluxtol,size(flux_tot_es)))
      !    = sum(eflux_tot_es(:)) + &
      !  & + sum(pflux_tot_es(:)) + &
      !  & + sum(vflux_tot_es(:)) 

      ! standard deviation of the flux history
      std_flux = 0.
      mean_flux = sum(flux_array)/arr_size
      do i=1,arr_size
        std_flux = std_flux + (flux_array(i)-mean_flux)**2
      end do
      std_flux = sqrt(std_flux/arr_size)

      ! do a check for flux convergence
      if (.not. stop_me) then
        stop_me = (std_flux/mean_flux < fluxtol) .or. stop_me
        if (stop_me .and. root_processor) write(*,*) 'Fluxes convergence reached: stop'
      end if
    end if

    ! attempt to stop the code if things are going badly wrong
    if (gkw_is_nan(eflux_tot_es(1))) then
      if (root_processor) write(*,*) ' ******* Hit a NaN in fluxes: stop *******'
      if (root_processor) write(*,*)
      nan_stop = .true.
      stop_me = .true.
    end if

  contains

    !--------------------------------------------------------------------------
    !> This internal routine completes the MPI reduction of fluxes arrays over 
    !> s+velocity space and "gathers" over the species/radial grid (if required)
    !> Called by ALL procs with flux_array(nmod,n_x_grid,number_of_species)
    !--------------------------------------------------------------------------
    subroutine fluxes_parallel_sum_gather(flux_array)
      !use grid, only : parallel_x, ixpb, ixpe
      !integer :: imysp
      real, intent(inout) :: flux_array(nmod,n_x_grid,number_of_species)

      ! reduce zero-padded arrays (in species and x) over COMM_WORLD
      call mpiallreduce_sum(flux_array, fluxbuf, nmod, n_x_grid, number_of_species)
      flux_array(:,:,:) = fluxbuf(:,:,:)

      ! (the x grid [and species] directions are zero padded, but alternative
      ! reduce only over velocity, and parallel directions may be faster)    
      ! call mpiallreduce_sum(flux_array,fluxbuf,nmod,n_x_grid,number_of_species, &
      ! & COMM=COMM_X_EQ)
      ! if (parallel_x) then  !gather the array in x
      ! do imysp = 1, number_of_species
      ! call gather_array(flux_array(:,:,imysp),nmod,n_x_grid,  &
      ! & fluxbuf(:,ixpb:ixpe,imysp),nmod,nx,        &
      ! & COMM_DUMMY,COMM_X_NE,ALLGATHER=.true.)
      ! end do
      ! else
      ! flux_array(:,:,:) = fluxbuf(:,:,:) 
      ! end if

    end subroutine fluxes_parallel_sum_gather
    !--------------------------------------------------------------------------
    !> This internal routine completes the MPI reduction of fluxes arrays over 
    !> s+velocity space and "gathers" over the species/radial grid (if required)
    !> Called by ALL procs with flux_array(n_x_grid,number_of_species)
    !--------------------------------------------------------------------------
    subroutine fluxes_parallel_sum_gather_x_sp(flux_array)
      real, intent(inout) :: flux_array(n_x_grid,number_of_species)

      ! reduce zero-padded arrays (in species and x) over COMM_WORLD
      call mpiallreduce_sum(flux_array, fluxbuf(1,:,:), n_x_grid, number_of_species)
      flux_array(:,:) = fluxbuf(1,:,:)

    end subroutine fluxes_parallel_sum_gather_x_sp
    

  end subroutine calc_fluxes

  !-------------------------------------------------
  ! Calculate the 2D fluxes vd*f s-averaged
  ! Please document me better !
  !-------------------------------------------------
  subroutine flmag_xy_output(direction, file_count)
    use dist,             only : fdis => fdisi
    use velocitygrid,     only : intvp,intmu,vpgr,mugr
    use grid,             only : nmod,nx,nmu,nvpar,nsp,ns, gsp
    use grid,             only : proc_subset
    use geom,             only : bn,ints,dfun
    use components,       only : tmp,signz  
    use global,           only : int2char_zeros
    use matdat,           only : get_f_from_g
    use mpiinterface,     only : mpiallreduce_sum
    use mpicomms,         only : COMM_SP_EQ_X_EQ
    use general,          only : gkw_abort
    use diagnos_generic,  only : xy_output_array

    character(len=3), intent(in) :: direction
    integer,  intent(in) :: file_count
    character(len=5) :: prefix
    complex :: fdisi,dens
    integer :: j,k,imod,ix,is,i,idir 
    real :: vdrift
    character (len=6), save :: file_count_suffix
    file_count_suffix=trim(int2char_zeros(file_count,6))

    select case(direction)
    case('rad') ! radial
      idir = 1
      prefix='flmgr'

    case('pol') ! poloidal
      idir = 2
      prefix='flmgp'

    case default
      call gkw_abort('flmag_xy_output unknown direction')    
    end select

    ! loop over the local species
    do is=1,nsp    

      ! start with zero slice (important if looping over 2 or more species)
      cslice_xy(:,:) = (0.,0.) ; c_xy = (0.,0.)

      ! loop over the slice points for this species
      do imod=1,nmod ; do ix=1,nx

        ! integrate over the velocity space for this point
        dens=(0.E0,0.E0)

        do j=1,nmu ; do k=1,nvpar
          ! N.B. this is probably slow!
          do i=1,ns

            fdisi = get_f_from_g(imod,ix,i,j,k,is,fdis)
            if (intvp(i,j,k,is).eq.0) fdisi = 0.   

            ! WARNING: Should call the drift function of linear terms here instead 
            ! call drift

            vdrift = (vpgr(i,j,k,is)**2+(mugr(j)*bn(ix,i)))* &
               &   dfun(ix,i,idir)*tmp(ix,is)/signz(is)
            dens = dens + vdrift*bn(ix,i)*intvp(i,j,k,is)*intmu(j)*fdisi*ints(i)

          end do
        end do; end do 
        ! store the density in a temporary slice for reduction later
        c_xy(imod,ix) = dens

      end do; end do

      !Sum the slice over other processors if necessary
      call mpiallreduce_sum(c_xy(1:nmod,1:nx),cslice_xy(1:nmod,1:nx),nmod,nx, &
         & COMM_SP_EQ_X_EQ)

      ! Select only one x slice to write
      if (proc_subset(0,1,1,1,gsp(is))) then
        ! generate a luname based on the global species
        call xy_output_array('diagnostic/diagnos_fluxes', &
           & cslice_xy(1:nmod,1:nx), &
           & prefix//trim(int2char_zeros(gsp(is),2)),'_'//file_count_suffix)
      end if

    end do
  end subroutine flmag_xy_output


  !-----------------------------------------------------------------------------
  !> Calculation of fluxes in real space for 2D output
  !> This in independent from the spectral flux calculation as it operates 
  !> in real space.  This is necessary because the spectral fluxes calculation 
  !> loses the phase (and therefore spatial) information when it multiplies the
  !> complex conjugates. For the flux surface average, the two methods agree
  !> i.e. the average of the XY_flux exactly matches the total flux calculation

  !> This is a generalised routine for any direction and any field and moment
  !> and for a single slice and for the flux surface average
  !> The input arguments are used to select which flux should be calculated:
  !>   direc  selects between radial and poloidal directions 
  !>   fluxf  selects between phi, apa or bpar.
  !>   moment selects between particle, energy, velocity flux
  !>   fs_av  selects between flux surface average or single slice 
  !>
  !> The way this routine is called repeatedly is a bit inefficient, since
  !> the FFTs are repeated unnecessarily.  Note however, the diagnostics timings 
  !> will warn the user is if they become a significant fraction of runtime
  !-----------------------------------------------------------------------------
  subroutine xy_flux_calculation(fdis,direc,fluxf,moment,fs_av,file_count)

    use dist,             only : nsolc
    use velocitygrid,     only : intvp,intmu,vpgr,mugr
    use grid,             only : nmod,nx,nmu,nvpar,ls, nsp,ns,gsp,ls
    use grid,             only : proc_subset
    use control,          only : time
    use geom,             only : bn, efun, ints
    use mode,             only : krho, kxrh
    use components,       only : tmp,vthrat,signz, tearingmode, isl_mode
    use io,               only : output_array
    use io,               only : xy_fmt, binary_fmt
    use tearingmodes,     only : omega_rot
    use non_linear_terms, only : jind 
    use constants,        only : ci1
    use matdat,           only : get_f_from_g 
    use fields,           only : get_averaged_phi, get_averaged_apar 
    use fields,           only : get_averaged_bpar
    use mpiinterface,     only : gather_array, mpiallreduce_sum
    use mpicomms,         only : COMM_VPAR_NE_MU_NE, COMM_SP_EQ_X_EQ 
    use mpicomms,         only : COMM_X_NE, COMM_DUMMY
    use general,          only : genfilename, gkw_abort
    use diagnos_generic,  only : xy_slice_ipar, lisl_follow
    use diagnos_generic,  only : four2real_2D
    use diagnos_generic,  only : mphit, mrad_G, mrad_l

    complex, dimension(nsolc), intent(in) :: fdis
    integer, intent(in) :: file_count
    integer :: i,j,k,imod,ix,is
    integer :: ib, ie, ipar_write
    character (len=15) :: luname
    character (len=3), intent(in) :: direc     !< poloidal or radial
    character (len=3), intent(in) :: fluxf     !< which field
    character,         intent(in) :: moment    !< which moment
    logical,           intent(in) :: fs_av     !< if flux surface average
    character (len=6) :: prefix
    real    :: vsqr, tens1, tens2
    complex :: fdisi
    complex :: phi_ga, apar_ga, bpar_ga   

    real :: rdum(mphit,mrad_G)

    ! Set for flux surface average, if not, single s point
    ! Select the points on the field line needed and processors
    if (.not. fs_av) then      
      ! local s index
      ipar_write = xy_slice_ipar
      ib = ls(xy_slice_ipar) 
      ie = ls(xy_slice_ipar)

      ! Return if the point xy_slice_ipar is not on the local processor.
      if (.not. proc_subset(0,xy_slice_ipar,0,0,0)) return
    else
      ib = 1
      ie = ns    
      ipar_write = 1    
    end if

    !generate a luname based on the global species; use the file counter
    select case(direc)

    case('rad')

      !Choses the tensor components 
      !tens1 = efun(ipar,1,1)
      !tens2 = efun(ipar,2,1)
      !Changes the name according to the field
      select case(fluxf)
      case('phi')
        prefix = 'FFlesr'
      case('apa')
        prefix = 'FFlemr'
      case('bpa')
        prefix = 'FFlbpr'
      case default            

        call gkw_abort('Wrong field in xy fluxes calculation')

      end select

    case('pol')

      !Choses the tensor components
      !tens1 = efun(ipar,1,2) 
      !tens2 = 0.E0
      select case(fluxf)
      case('phi')
        prefix = 'FFlesp'
      case('apa')
        prefix = 'FFlemp'   
      case('bpa')
        prefix = 'FFlbpp'
      case default            

        call gkw_abort('Wrong field in xy fluxes calculation')

      end select

    case default

      call gkw_abort('Wrong direction choice in xy fluxes calculation')

    end select

    ! Overwrite the first letter of the flux based on moment
    select case(moment)
    case('p')  ! Particle (PFlesr, PFlesp, PFlemr, PFlemp)
      prefix(1:1) = 'P'           
    case('e')  ! Energy   (EFlesr, EFlesp, EFlemr, EFlemp)     
      prefix(1:1) = 'E'
    case('v')  ! Momentum (VFlesr, VFlesp, VFlemr, VFlemp)
      prefix(1:1) = 'V'      
    case default              
      call gkw_abort('Wrong moment in xy fluxes calculation')

    end select

    ! Overwrite the third letter of the flux for fs_av, gives lunames:
    ! PFAesr, PFAesp, PFAemr, PFAemp
    ! EFAesr, EFAesp, EFAemr, EFAemp 
    ! VFAesr, VFAesp, VFAemr, VFAemp
    if (fs_av) prefix(3:3) = 'A'    

    ! loop over the local species
    species : do is=1,nsp

      ! start with zero slice (important if looping over 2 or more species)
      cslice_xy(:,:) = (0.,0.)
      !Dummy arrays taken from non-linear-terms
      a = (0.,0.)
      ar = (0.,0.)
      b = (0.,0.) 
      br = (0.,0.)
      cr = (0.,0.)

      spoints: do i=ib,ie
        do k=1,nvpar;  do j=1,nmu
          a = (0.,0.)
          b = (0.,0.)
          ar= (0.,0.)
          br= (0.,0.)

          ! loop over the slice points for this species
          do imod=1,nmod ; do ix=1,nx

            select case(direc)
            case('rad')
              tens1 = efun(ix,i,1,1)
              tens2 = efun(ix,i,2,1)
            case('pol')
              tens1 = efun(ix,i,1,2)
              tens2 = 0.
            end select

            ! fdis is the distribution without A_par contribution  
            fdisi = get_f_from_g(imod,ix,i,j,k,is,fdis) 
            if (intvp(i,j,k,is).eq.0) fdisi = 0. 
            !Just the distribution function
            b(imod,jind(ix))=fdisi

            select case(fluxf)
            case('phi')
              ! here is a factor of two less than the spectral calculation
              ! this is because only the spectral calculation needs
              ! the complex conjugate conversion
              ! |f|^2 = (1/2) |f||f*|
              phi_ga = get_averaged_phi(imod,ix,i,j,is,fdis)
              a(imod,jind(ix))=ci1*(tens1*kxrh(ix) +           &
                 & tens2*krho(imod))*bn(ix,i)*phi_ga
            case('apa')
              apar_ga = get_averaged_apar(imod,ix,i,j,is,fdis) 
              a(imod,jind(ix))=-2.E0*ci1*(tens1*kxrh(ix) +          &
                 & tens2*krho(imod))*  &
                 & bn(ix,i)*vthrat(is)*vpgr(i,j,k,is)*apar_ga  
            case('bpa')
              bpar_ga = get_averaged_bpar(imod,ix,i,j,is,fdis) 
              a(imod,jind(ix))=2.E0*ci1*(tens1*kxrh(ix) +           &
                 & tens2*krho(imod))*  &
                 & bn(ix,i)*tmp(ix,is)*mugr(j)*bpar_ga/signz(is)
            case default
              call gkw_abort('Wrong field in xy fluxes calculation')  
            end select

            if(tearingmode.and.lisl_follow)then
              b(imod,jind(ix)) = b(imod,jind(ix)) * &
                 & exp(cmplx(0.0, (imod-1)*omega_rot*time/(isl_mode-1)))
              a(imod,jind(ix)) = a(imod,jind(ix)) * &
                 & exp(cmplx(0.0, (imod-1)*omega_rot*time/(isl_mode-1)))
            end if

          end do; end do

          call four2real_2D(ar,a,1)
          call four2real_2D(br,b,1)

          do imod = 1, mrad_l ; do ix = 1, mphit 
            ! Choose the moment to calculate
            select case(moment)
            case('p')
              vsqr = 1.0

            case('e')             
              vsqr = vpgr(i,j,k,is)**2+(2.0*mugr(j)*bn(1,i))

            case('v')
              !! FIXME: Still needs RBt/B factors to agree quantatively with total
              vsqr = vpgr(i,j,k,is)

            case default
              call gkw_abort('Wrong moment in xy fluxes calculation')

            end select

            cr(ix,imod) = cr(ix,imod)+ar(ix,imod)*br(ix,imod)*  &
               & vsqr*intvp(i,j,k,is)*intmu(j)
          end do; end do

        end do; end do ! velocity
      end do spoints

      ! sum the slice over other processors if necessary; put result into ar
      if (fs_av) then
        ar = 0.0
        call mpiallreduce_sum(cr,ar,mphit,mrad_l,COMM_SP_EQ_X_EQ) 
        ar = ar*ints(1)     
      else
        ar = 0.0
        call mpiallreduce_sum(cr,ar,mphit,mrad_l,COMM_VPAR_NE_MU_NE)
      end if

      ! gather global array in x (allgather)
      call gather_array(rdum(1:mphit,1:mrad_G),mphit,mrad_G,  &
         & ar(1:mphit,1:mrad_l),mphit,mrad_l,  &
         & COMM_DUMMY,COMM_X_NE,ALLGATHER=.true.)

      !Now write with 1 processor only
      if (proc_subset(1,ipar_write,1,1,gsp(is))) then
        ! generate a luname based on the global species; use file_count
        call genfilename(prefix,luname,gsp(is),file_count)
        ! NOTE with the rewrite of the io, both the binary
        ! and the ascii output produced by the following line are in 'F'
        ! order. Before, the ascii output used to be in 'C' order.
        ! NOTE by using luname, logical units of both formats are numbered
        ! according to the file_count.
        ! NOTE by using luname(1:10), all data is appended to a single logical
        ! unit and the value of file_count is ignored.

        !call output_array(luname(1:10), 'diagnostic/diagnos_fluxes', &
        call output_array(luname, 'diagnostic/diagnos_fluxes', &
           & rdum, 'F', xy_fmt, binary_fmt)

        ! call output_array(luname, 'diagnostic/diagnos_fluxes', &
        !    & rdum, 'C', xy_fmt, binary_fmt)
      end if

    end do species

  end subroutine xy_flux_calculation


  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !> Small wrapper to output 2d fluxes arrays as binary or ascii outputs
  !> Assumes certain prefix length (6 characters)
  !> Could probably be merged with ky_output_array 
  !> (no x gather is required for these arrays, which are global in x)
  !> For nonspectral, does yet not FFT back to kx
  !> Call from root processor only
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine output_k2_fluxes(array,prefix,file_count)

    use io,           only : output_array, xy_fmt, binary_fmt
    use general,      only : genfilename
    use grid,         only : number_of_species, nmod, n_x_grid
    use mpiinterface, only : root_processor

    character(len=6), intent(in) :: prefix
    integer, intent(in) :: file_count
    character (len=15) :: luname
    real, intent(in) :: array(nmod,n_x_grid,number_of_species)
    integer :: is

    ! use only one processor for output
    if (.not. root_processor) return

    do is = 1, number_of_species
      ! generate a luname based on the global species
      call genfilename(prefix,luname,is,file_count)
      ! ignore file count:
      !call output_array(luname(1:10), 'diagnostic/diagnos_fluxes', &
      ! use file count:
      call output_array(luname, 'diagnostic/diagnos_fluxes', &
         & array(:,:,is), 'F', xy_fmt, binary_fmt)
      ! call output_array(luname, 'diagnostic/diagnos_fluxes', &
      !    & array(:,:,is), 'C', xy_fmt, binary_fmt)

    end do
  end subroutine output_k2_fluxes

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output(magnitude,file_count)
    use diagnos_generic, only : xy_estep, lwrite_output1
    logical, intent(in) :: magnitude
    integer, intent(in) :: file_count
    if(lwrite_output1) then
      call output_regular(magnitude)

      if (xy_estep) then
        call output_xy(file_count)
      end if
    end if
  end subroutine output

  

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output_regular(magnitude)
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use control, only : io_legacy
    use control, only : nlapar, neoclassics, nlbpar
    use diagnos_generic, only : lradial_profile
    use mode, only : mode_box
    use grid, only : n_x_grid, nmod, number_of_species
    use components, only : tearingmode, tm_drive
    use diagnos_generic, only : lwrite_output1
    logical, intent(in) :: magnitude
    integer :: is
    integer :: iflux
    
    if (lwrite_output1 .and. magnitude) then
      ! Recompute fluxes suprema.
      ! WARNING: do use any results computed
      ! in subroutine fluxes after this call.
      call calc_fluxes(magnitude)
    else

      call calc_fluxes(magnitude)

      !
      ! write the fluxes
      !

      ! for non-fluxtube cases write the complete radial profile to file   
      if (lradial_profile) then    
        call append_chunk(i_pflux_rad_es, &
           !& (/ ((sum(pflux_es(:,ix,is)),ix = 1, n_x_grid), &
           !& is = 1, number_of_species) /), &
           & reshape(sum(pflux_es,1),(/n_x_grid * number_of_species/)), &
           & xy_fmt, ascii_fmt)
        call append_chunk(i_vflux_rad_es, &
           ! & (/ ((sum(vflux_es(:,ix,is)),ix = 1, n_x_grid), &
           ! & is = 1, number_of_species) /), &
           & reshape(sum(vflux_es,1),(/n_x_grid * number_of_species/)), &
           & xy_fmt, ascii_fmt)

        call append_chunk(i_eflux_rad_es, &
           ! & (/ ((sum(eflux_es(:,ix,is)),ix = 1, n_x_grid), &
           ! & is = 1, number_of_species) /), &
           & reshape(sum(eflux_es,1),(/n_x_grid * number_of_species/)), &
           & xy_fmt, ascii_fmt)
      endif

      if(io_legacy) then
        call append_chunk(i_fluxes_legacy, flux_tot_es, xy_fmt, ascii_fmt)
      else
        ! this indexing business would be trivial if the different fluxes
        ! were not all stored alternating in flux_tot_es but in three
        ! separate arrays.
        iflux = 1
        if(lpflux) then
          call append_chunk(i_pflux, &
             & flux_tot_es(iflux:nfluxes*number_of_species:nfluxes), &
             & xy_fmt, ascii_fmt)
          iflux = iflux + 1
        end if
        if(leflux) then
          call append_chunk(i_eflux, &
             & flux_tot_es(iflux:nfluxes*number_of_species:nfluxes), &
             & xy_fmt, ascii_fmt)
          iflux = iflux + 1
        end if
        if(lpflux) then
          call append_chunk(i_pflux, &
             & flux_tot_es(iflux:nfluxes*number_of_species:nfluxes), &
             & xy_fmt, ascii_fmt)
        end if
      end if

      if (nlapar) then
        call append_chunk(i_fluxes_em, flux_tot_em, xy_fmt, ascii_fmt)
      end if

      ! deltaprime and torque
      if (tearingmode.or.tm_drive) then
        call append_chunk(i_deltaprime, deltaprime, xy_fmt, ascii_fmt)
        call append_chunk(i_torque, torque, xy_fmt, ascii_fmt)
      end if

      ! fluxes_bpar.dat
      if (nlbpar.or.lrenorm) then
        call append_chunk(i_fluxes_bpar, flux_tot_bpar, xy_fmt, ascii_fmt)
      end if

      if (neoclassics) then
        call append_chunk(i_neoclass_old, &
           & (/ (pflux_nc_old(is), eflux_nc_old(is), vflux_nc_old(is), &
           & eflux2_nc_old(is), is = 1, number_of_species) /), xy_fmt, ascii_fmt)
        call append_chunk(i_neoclass, &
           & (/ (pflux_nc(is), eflux_nc(is), vflux_nc(is), &
           & is = 1, number_of_species) /), xy_fmt, ascii_fmt)
      end if

      ! fluxes spectra
      fluxes_spectra : if(lfluxes_spectra) then
        !Write the fluxes spectra
        !Must have 1024 > nmod * number_of_species
        !Possibility of a fortran column limit?

        if (nmod > 1) then
          call append_chunk(i_efluxspec, &
             ! & (/((eflux_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(eflux_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_pfluxspec, &
             ! & (/((pflux_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(pflux_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_vfluxspec, &
             ! & (/((vflux_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(vflux_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
        end if
        if (n_x_grid > 1) then 
          call append_chunk(i_efluxxspec, &
             ! & (/ ((eflux_xspec(ix,is),ix = 1,n_x_grid), &
             ! & is = 1, number_of_species) /), &
             & reshape(eflux_xspec,(/n_x_grid * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_pfluxxspec, &
             ! & (/ ((pflux_xspec(ix,is),ix = 1,n_x_grid), &
             ! & is = 1, number_of_species) /), &
             & reshape(pflux_xspec,(/n_x_grid * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_vfluxxspec, &
             ! & (/ ((vflux_xspec(ix,is),ix = 1,n_x_grid), &
             ! & is = 1, number_of_species) /), &
             & reshape(vflux_xspec,(/n_x_grid * number_of_species/)), &
             & xy_fmt, ascii_fmt)
        end if

      end if fluxes_spectra

      ! EM fluxes spectra
      fluxes_em_spectra : if(lfluxes_em_spectra) then

        !Write the EM fluxes spectra
        !Must have 1024 > nmod * number_of_species
        !Possibility of a fortran column limit?
        if (nmod > 1) then
          call append_chunk(i_efluxspec_em, &
             ! & (/ ((eflux_em_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(eflux_em_spec,(/nmod * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_pfluxspec_em, &
             ! & (/ ((pflux_em_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(pflux_em_spec,(/nmod * number_of_species/)), &
             xy_fmt, ascii_fmt)
          call append_chunk(i_vfluxspec_em, &
             ! & (/ ((vflux_em_spec(imod,is),imod = 1,nmod), &
             ! & is = 1, number_of_species) /), &
             & reshape(vflux_em_spec,(/nmod * number_of_species/)), &
             xy_fmt, ascii_fmt)
        end if
        if (n_x_grid > 1) then 
          call append_chunk(i_efluxxspec_em, &
             ! & (/ ((eflux_em_xspec(ix,is),ix = 1,n_x_grid), &
             ! & is = 1, number_of_species) /), &
             & reshape(eflux_em_xspec,(/n_x_grid * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_pfluxxspec_em, &
             ! & (/ ((pflux_em_xspec(ix,is),ix = 1,n_x_grid), &
             ! & is = 1, number_of_species) /), &
             & reshape(pflux_em_xspec,(/n_x_grid * number_of_species/)), &
             & xy_fmt, ascii_fmt)
          call append_chunk(i_vfluxxspec_em, &
             ! & (/ ((vflux_em_xspec(ix,is),ix = 1,n_x_grid), &
             ! & is = 1, number_of_species) /), &
             & reshape(vflux_em_xspec,(/n_x_grid * number_of_species/)), &
             & xy_fmt, ascii_fmt)
        end if

      end if fluxes_em_spectra
    end if

  end subroutine output_regular


  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output_xy(file_count)
    use mpiinterface, only : root_processor
    use mode, only : mode_box
    use dist, only : fdisi, nsolc
    use diagnos_generic, only : lwrite_output1, lphi_diagnostics
    use diagnos_generic, only : xy_fluxes, xy_fluxes_fsa
    use diagnos_generic, only : xy_fluxes_p, xy_fluxes_v
    use diagnos_generic, only : xy_fluxes_em, xy_fluxes_bpar
    use diagnos_generic, only : xy_fluxes_k, xy_fluxes_bi
    integer, intent(in) :: file_count
    integer :: ifl
    logical :: eflux_write, pflux_write, vflux_write, lfsa_dum

    if (.not.lwrite_output1) return

    if (mode_box .and. lphi_diagnostics) then

      ! loop over two types of output
      do ifl = 1, 2

        select case(ifl)
        case(1)  ! single point xy_slice_ipar
          lfsa_dum = .false.
          eflux_write = xy_fluxes
          pflux_write = xy_fluxes_p
          vflux_write = xy_fluxes_v
        case(2)  ! flux surface average
          lfsa_dum = xy_fluxes_fsa
          eflux_write = xy_fluxes_fsa
          pflux_write = xy_fluxes_p .and. xy_fluxes_fsa
          vflux_write = xy_fluxes_v .and. xy_fluxes_fsa
        end select

        if (eflux_write) then
          call XY_flux_calculation(fdisi(1:nsolc),'rad','phi','e',lfsa_dum,file_count)
          call flmag_xy_output('rad',file_count)

          if (XY_fluxes_em) then
            call XY_flux_calculation(fdisi(1:nsolc),'rad','apa','e',lfsa_dum,file_count)
          end if
          if (XY_fluxes_bpar) then    
            call XY_flux_calculation(fdisi(1:nsolc),'rad','bpa','e',lfsa_dum,file_count)
          end if
        end if

        if (XY_fluxes_bi .and. eflux_write) then

          call XY_flux_calculation(fdisi(1:nsolc),'pol','phi','e',lfsa_dum,file_count)

          call flmag_xy_output('pol',file_count)

          if (XY_fluxes_em) then
            call XY_flux_calculation(fdisi(1:nsolc),'pol','apa','e',lfsa_dum,file_count)
          end if
          if (XY_fluxes_bpar) then    
            call XY_flux_calculation(fdisi(1:nsolc),'pol','bpa','e',lfsa_dum,file_count)
          end if
        end if

        if (pflux_write) then
          call XY_flux_calculation(fdisi(1:nsolc),'rad','phi','p',lfsa_dum,file_count)

          if (XY_fluxes_em) then
            call XY_flux_calculation(fdisi(1:nsolc),'rad','apa','p',lfsa_dum,file_count)
          end if
          if (XY_fluxes_bpar) then    
            call XY_flux_calculation(fdisi(1:nsolc),'rad','bpa','p',lfsa_dum,file_count)
          end if
        end if

        if (XY_fluxes_bi .and. pflux_write) then
          call XY_flux_calculation(fdisi(1:nsolc),'pol','phi','p',lfsa_dum,file_count)

          if (XY_fluxes_em) then
            call XY_flux_calculation(fdisi(1:nsolc),'pol','apa','p',lfsa_dum,file_count)
          end if
          if (XY_fluxes_bpar) then    
            call XY_flux_calculation(fdisi(1:nsolc),'pol','bpa','p',lfsa_dum,file_count)
          end if
        end if

        if (vflux_write) then
          call XY_flux_calculation(fdisi(1:nsolc),'rad','phi','v',lfsa_dum,file_count)

          if (XY_fluxes_em) then
            call XY_flux_calculation(fdisi(1:nsolc),'rad','apa','v',lfsa_dum,file_count)
          end if
          if (XY_fluxes_bpar) then    
            call XY_flux_calculation(fdisi(1:nsolc),'rad','bpa','v',lfsa_dum,file_count)
          end if
        end if

        if (XY_fluxes_bi .and. vflux_write) then
          call XY_flux_calculation(fdisi(1:nsolc),'pol','phi','v',lfsa_dum,file_count)

          if (XY_fluxes_em) then
            call XY_flux_calculation(fdisi(1:nsolc),'pol','apa','v',lfsa_dum,file_count)
          end if
          if (XY_fluxes_bpar) then    
            call XY_flux_calculation(fdisi(1:nsolc),'pol','bpa','v',lfsa_dum,file_count)
          end if
        end if

      end do

      ! 2D spectral outputs (these are all flux surface averages)
      if (xy_fluxes_k .and. root_processor) then

        if (xy_fluxes_P) call output_k2_fluxes(pflux_es,'PFlesk',file_count)

        call output_k2_fluxes(eflux_es,'EFlesk',file_count) 

        if (xy_fluxes_v) call output_k2_fluxes(vflux_es,'VFlesk',file_count)        

        if (xy_fluxes_p .and. xy_fluxes_em) call output_k2_fluxes(pflux_em,'PFlemk',file_count) 

        if (xy_fluxes_em) call output_k2_fluxes(eflux_em,'EFlemk',file_count) 

        if (xy_fluxes_v .and. xy_fluxes_em) call output_k2_fluxes(vflux_em,'VFlemk',file_count)

        if (xy_fluxes_p .and. xy_fluxes_bpar) call output_k2_fluxes(pflux_bpar,'PFlbpk',file_count) 

        if (xy_fluxes_bpar) call output_k2_fluxes(eflux_bpar,'EFlbpk',file_count) 

        if (xy_fluxes_v .and. xy_fluxes_bpar) call output_k2_fluxes(vflux_bpar,'VFlbpk',file_count)

      end if
    end if
  end subroutine output_xy

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine screen_output()
    use mpiinterface, only : root_processor
    use control,   only : non_linear, neoclassics
    use grid,      only : number_of_species, nmod, nx
    use mode,      only : mode_box

    integer :: is, imod, ix

    ! Only root process shall write to terminal
    if(.not.root_processor) return

    if(non_linear) then
      write(*,*)
      do is = 1, number_of_species
        write(*,*)'Species ',is
        write(*,*)'Particle flux : ',pflux_tot_es(is)
        write(*,*)'Energy flux   : ',eflux_tot_es(is)
        write(*,*)'Momentum flux : ',vflux_tot_es(is)
        write(*,*)
      end do

    else
      if(mode_box) then
        do is = 1, number_of_species
          write(*,10)is 
          write(*,20)pflux_tot_es(is)
          write(*,30)eflux_tot_es(is)
          write(*,40)vflux_tot_es(is)
        end do
10      format('Species  : ',I4)
20      format('The total particle flux   :',es13.5)
30      format('The total energy flux     :',es13.5)
40      format('The total momentum flux   :',es13.5)

        if(neoclassics)then
          do is = 1, number_of_species
            write(*,10)is
            write(*,34)pflux_nc(is)
            write(*,35)eflux_nc(is)
            write(*,36)vflux_nc(is)
          end do
34        format('The total neo particle flux   :',es13.5)
35        format('The total neo energy flux     :',es13.5)
36        format('The total neo momentum flux   :',es13.5)
        end if
      else
        mod : do imod = 1, nmod 
          x : do ix = 1, nx 
            species : do is = 1, number_of_species  
              !Note this output block generates zeros for nlapar = false
              !Since the electromagnetic fluxes are zero
              write(*,1)imod,ix,is
1             format('Toroidal mode ',I3,' Radial mode ',I3,' Species ',I2) 
              write(*,2)pflux_es(imod,ix,is), pflux_em(imod,ix,is), pflux_bpar(imod,ix,is) 
2             format('The particle flux  (ES/EM/BPAR) : ',3(es13.5,1X)) 
              write(*,3)eflux_es(imod,ix,is), eflux_em(imod,ix,is), eflux_bpar(imod,ix,is) 
3             format('The energy flux    (ES/EM/BPAR) : ',3(es13.5,1X)) 
              write(*,4)vflux_es(imod,ix,is), vflux_em(imod,ix,is),vflux_bpar(imod,ix,is)
4             format('The momentum flux  (ES/EM/BPAR) : ',3(es13.5,1X))
              write(*,*)
            end do species
          end do x
        end do mod
      end if
    end if

  end subroutine screen_output

end module diagnos_fluxes
