! SVN: $Id$
!-----------------------------------------------------------------------------
!> This module contains a routine to re-normalise the solution in linear runs
!> so that it remains numerically tractable. 
!> 
!> The normalisation factors are public so that the growth rates can be 
!> calculated in diagnos_growth_freq. 
!> 
!> To use the per toroidal mode option,(1) the switch must be set in the appropriate
!> input namelist; (2) the mode spacing must be set to 1 (via ikxspace) when
!> running a with box of modes (mode_box =T).
!> This module also has the effect of normalisation linear fluxes to |phi|^2
!> which is useful for quasilinear studies.
!>
!> For nonlinear runs, saturation occurs and normalisation is not needed.
!> But for special NL studies it is possible to normalise per toroidal mode. 
!-----------------------------------------------------------------------------
module normalise

  implicit none

  private

  public :: normalise_fdisi, normalise_with_fdisi, normalise_init

  !> single normalisation factor
  real, save, public :: fnorm0d
  !> total accumulated single normalisation factor
  real, save, public :: normfactor

  !> normalisation factor for each toroidal mode
  real, save, public, allocatable :: fnorm1d(:)

  !> ready to use switch
  logical, save :: initialised = .false.

  !> temporary array for reduction
  real, save, allocatable :: fnorm1d_tmp(:)

  ! arrays for index
  integer, save, allocatable :: findx(:,:), lindx(:,:)

contains


!-------------------------------------------------------------------------------
!> Setup everything necessary to use the other routines in the module.
!> - Allocate the necessary arrays (should be done at allocate)
!> - Create an index array to reference the fields for each mode. For a
!>   single normalisation factor this is not necessary as the fields can be
!>   accessed as one block.
!> - Create an index array to reference each mode of fdisi to be normalised.
!>   With a single factor, all fdisi is normalised with that factor.
!------------------------------------------------------------------------------

subroutine normalise_init()

  use control,        only : nlphi, nlapar, nlbpar, normalized
  use control,        only : normalize_per_toroidal_mode
  use grid,           only : nx, nmod, ns, nsp, nmu, nvpar, parallel_s
  use dist,           only : ifdis, iphi, iapar, ibpar, number_of_fields
  use index_function, only : indx
  use general,        only : gkw_abort

  integer, allocatable :: field_id(:)
  integer :: ierr, idx, ipar, is, jv, kt, ix, imod, k, nfields

  ! Per toroidal mode normalisation
  if (normalized .and. normalize_per_toroidal_mode) then

    ! allocate an array for the normalisation factors for all toroidal
    allocate(fnorm1d(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('normalise_init :: fnorm1d')
    ! need a temporary array for sum reduction when spatially parallelised
    if (parallel_s) then
      allocate(fnorm1d_tmp(nmod),stat=ierr)
    if (ierr /= 0) call gkw_abort('normalise_init :: fnorm1d_tmp')
    end if
    ! allocate the index array for normalising
    allocate(findx(nmod,nx*ns*nsp*nmu*nvpar),stat=ierr)
    if (ierr /= 0) call gkw_abort('normalise_init :: findx')

    ! fill the index array
    idx = 0
    do is=1,nsp ; do kt=1,nvpar ; do jv=1,nmu ; do ipar=1,ns ; do ix=1,nx
      idx = idx + 1
      do imod = 1, nmod
        findx(imod,idx) = indx(ifdis,imod,ix,ipar,jv,kt,is)
      end do
    end do ; end do ; end do ; end do ; end do

    ! Construct the array for quick look up of the fields when calculating the
    ! normalisation factor.

    allocate(field_id(number_of_fields),stat=ierr)
    if (ierr /= 0) call gkw_abort('normalise_init :: field_id')
    nfields = 0
    if (nlphi) then
      nfields = nfields + 1
      field_id(nfields) = iphi
    end if
    if (nlapar) then
      nfields = nfields + 1
      field_id(nfields) = iapar
    end if
    if (nlbpar) then
      nfields = nfields + 1
      field_id(nfields) = ibpar
    end if

    ! allocate the array
    allocate(lindx(nmod,number_of_fields*ns*nx))
    if (ierr /= 0) call gkw_abort('normalise_init :: lindx')
    ! fill the array
    idx = 0
    lindx(:,:) = 0
    do k = 1, number_of_fields ;  do is = 1, ns ;  do ix = 1, nx
      ! N.B. use nmod for inner loop, both here and elsewhere in the module
      idx = idx + 1
      do imod = 1, nmod
        lindx(imod,idx) = indx(field_id(k),imod,ix,is)
      end do
    end do ; end do ; end do

    ! deallocate the field_id
    if (allocated(field_id)) deallocate(field_id)

    ! initialise the factors
    fnorm1d(:) = 1.

  else if (normalized) then

    ! just intialise the normalisation factor if there is only 1 single factor
    fnorm0d = 1.
    normfactor = 1.

  else

    ! just initialise the normalisation factor.
    ! Note that fnorm0d will loose its sense in this case and does not
    ! stay equal to 1.0
    fnorm0d = 1.
    normfactor = 1.

  end if

  ! done with the intialisation
  initialised = .true.

end subroutine normalise_init


!-------------------------------------------------------------------------------
!> Normalise the input fdis by a factor calculated from the fields. Calculate
!> the factor in this routine, then call normalise_with_fdisi() to do the
!> normalisation.
!------------------------------------------------------------------------------

subroutine normalise_fdisi(fdis,nsolc)

  use mpicomms,     only : COMM_S_NE_X_NE
  use mpiinterface, only : mpiallreduce_sum 
  use global,       only : r_tiny
  use grid,         only : parallel_s, parallel_x, nmod, nx, ns
  use grid,         only : n_procs_s, n_procs_x
  use dist,         only : nphi, number_of_fields, nregular_fields_end
  use general,      only : gkw_abort
  use geom,         only : ints
  use control,      only : normalized, normalize_per_toroidal_mode

  integer, intent(in) :: nsolc
  complex, intent(inout) :: fdis(nsolc)

  real :: fnorm0d_tmp
  integer :: i, j

  ! check to see if the module is initialised
  if (.not. initialised) call gkw_abort('normalise_fdisi: not initialised!')

  ! decide what normalisation to perform

  if (normalize_per_toroidal_mode) then ! must be normalized as well

    ! calculate the normalisation factors
    fnorm1d(:) = 0.
    do j = 1, number_of_fields*nx*ns
      do i = 1, nmod
        fnorm1d(i) = fnorm1d(i) + abs(fdis(lindx(i,j)))**2
      end do
    end do

    ! check if the value is too small;
    where (fnorm1d < r_tiny) fnorm1d = 1./(1.*n_procs_s*n_procs_x)

    ! reduce sum over space and take sqrt()
    if (parallel_s .or. parallel_x) then
      ! sum over other processors responsible for other parts of fields
      call mpiallreduce_sum(fnorm1d,fnorm1d_tmp,nmod,COMM_S_NE_X_NE)
      fnorm1d = fnorm1d_tmp
    end if

    ! make the norm factor  { |phi|^2 + |A|^2 } 
    fnorm1d = fnorm1d*ints(1)
    fnorm1d = sqrt(fnorm1d)

  else
    ! 1 normalisation factor !------------------------------------in a linear run

    ! Calculate fnorm0d even if normalized == false
    ! because it is needed to compute growth rates.

    ! first sum of local contribution
    fnorm0d = sum(abs(fdis(nphi:nregular_fields_end))**2)

    ! reduce sum over processors
    if (parallel_s .or. parallel_x) then
      ! sum over other processors responsible for other parts of s
      call mpiallreduce_sum(fnorm0d,fnorm0d_tmp,1,COMM_S_NE_X_NE)
      fnorm0d = fnorm0d_tmp
    end if
    
    ! make the norm factor  { |phi|^2 + |A|^2 } 
    fnorm0d = fnorm0d*ints(1)

    ! check if the value is too small
    if (fnorm0d < r_tiny) then
      ! use 1.0 instead of small values
      fnorm0d = 1.
    else
      fnorm0d = sqrt(fnorm0d)
    endif

  end if

  ! perform the actual normalisation
  if (normalized) call normalise_with_fdisi(fdis,nsolc)

end subroutine normalise_fdisi

!****************************************************************************
!> Normalise the input fdis by a factor calculated from the fields. This
!> routine is where the normalisation actually takes place.
!----------------------------------------------------------------------------

subroutine normalise_with_fdisi(fdis,nsolc)

  use control, only : normalized, normalize_per_toroidal_mode,imod_init
  use grid,    only : nx, nmod, nsp, nmu, ns, nvpar

  integer, intent(in) :: nsolc
  complex, intent(inout) :: fdis(nsolc)

  real :: fnorm0d1
  integer :: i, j

  if (normalized .and. normalize_per_toroidal_mode) then

    do i = 1, nmod
      if (i /= imod_init) then
        do j = 1, ns*nsp*nmu*nvpar*nx
          fdis(findx(i,j)) = fdis(findx(i,j)) / fnorm1d(i)
        end do
      end if
    end do

  else 
 
    if (normalized) then
      ! perform on the whole array when a single value is used
      fnorm0d1 = 1. / fnorm0d
      fdis = fdis * fnorm0d1
      ! accumulate the normalisation factors.
      ! This can be handy for diagnostics.
      ! Eventually, normfactor can become infinite and therefore
      ! looses its sense then!
      normfactor = normfactor * fnorm0d
    else
      ! do nothing if not normalised in any way
    end if

  endif 

end subroutine normalise_with_fdisi


end module normalise
