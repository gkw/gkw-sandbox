!------------------------------------------------------------------------------
! SVN:$Id$
!> Output moments (density, flow, temperature perturbations) in 3D and in
!> 2D perpendicular slices (spectral and real).
!------------------------------------------------------------------------------
module diagnos_moments

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, final_output
  public :: output

  !> For output of moments spectra
  real, save, allocatable, dimension(:,:) :: moment_spec

  !> logical unit numbers
  integer, save :: i_denspec = -1, i_enespec = -1 

  !> complex slice in xy for mpi reduction
  complex, save, allocatable, dimension(:,:) :: cslice_xy
  !> complex slice in xy for mpi reduction
  complex, save, allocatable, dimension(:,:) :: c_xy
  !> A buffer for communication. buf(nmod,nx,number_of_species)
  real, save, allocatable, dimension(:,:,:) :: buf(:,:,:)
  
  !> (testing) buffer for 3D outputs
  real, save, allocatable :: out3dbuf(:,:,:)

  !> Private FFT arrays.
  complex, save, allocatable :: a(:,:)
  real, save, allocatable :: ar(:,:)

  !> handle for MPI subarray datatype, used to output complete 3d fields
  integer, save :: ntype

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    
  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast
  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use io, only : open_real_lu, ascii_fmt
    use grid, only : nmod, number_of_species
    use mpidatatypes, only : create_subarray_datatype
    use global, only : id_mod, id_x, id_s
    use mpiinterface, only : MPIREAL_X
    use control, only : spectral_radius
    use grid, only : proc_subset
    use control, only : io_legacy
    use diagnos_generic, only : mphit, mrad_l
    
    if (proc_subset(0,0,1,1,0)) then
      ! create MPI-IO subarray datatype for 3d real space field
      if (spectral_radius) then
        call create_subarray_datatype(MPIREAL_X,ntype, &
           & id_mod,id_x,id_s,FFTX=mrad_l,FFTY=mphit)
      else
        call create_subarray_datatype(MPIREAL_X,ntype, &
           & id_mod,id_x,id_s,FFTY=mphit)
      end if
    end if

    if(root_processor) then
      if(io_legacy) then
        call open_real_lu('den_spectra.dat', 'diagnostic/diagnos_moments', &
           & (/ nmod*number_of_species /), &
           & ascii_fmt, i_denspec)
        call open_real_lu('ene_spectra.dat', 'diagnostic/diagnos_moments', &
           & (/ nmod*number_of_species /), &
           & ascii_fmt, i_enespec)
      else
        call open_real_lu('den_spectra', 'diagnostic/diagnos_moments', &
           & (/ nmod, number_of_species /), &
           & ascii_fmt, i_denspec)
        call open_real_lu('ene_spectra', 'diagnostic/diagnos_moments', &
           & (/ nmod, number_of_species /), &
           & ascii_fmt, i_enespec)
      end if
    end if
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use general, only : gkw_abort
    use grid, only : nmod,nx,number_of_species,n_x_grid,ns
    use diagnos_generic, only : compute_fft_sizes
    use non_linear_terms, only : mphiw3
    use diagnos_generic, only : den3d, ene3d, xy_dens, xy_temp
    use diagnos_generic, only : xy_current, xy_current2
    use diagnos_generic, only : mphiw3t, mphit, mrad_l
    
    integer :: ierr

    allocate(moment_spec(nmod,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: moment_spec')
    allocate(buf(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: fluxbuf')

    !Private FFT arrays for diagnostics only.
    allocate(a(mphiw3t,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate a in diagnostic')
    endif
    allocate(ar(mphit,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate ar in diagnostic')
    endif
    
    if(den3d.or.ene3d.or.xy_dens.or.xy_temp.or. &
       & xy_current.or.xy_current2) then
      allocate(cslice_xy(mphiw3,mrad_l),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: cslice_xy')
      allocate(c_xy(mphiw3t,mrad_l),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: c_xy')
    end if

    ! Arrays used when the whole three dimensional outputs selected.
    if (den3d .or. ene3d) then
      ! array for 3d output
      allocate(out3dbuf(mphit,mrad_l,ns),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: out3dbuf')
    end if
    
  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()

    if(allocated(moment_spec)) deallocate(moment_spec)

  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()

  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine final_output(number)
    use diagnos_generic, only : xy_estep
    use dist, only : fdisi, nsolc
    integer, intent(in), optional :: number

    if(.not.xy_estep) then
      ! moments spectra
      !if (lfluxes_spectra) then ! legacy switch
        call output_momentspec_ky(fdisi(1:nsolc),'den',i_denspec)
        call output_momentspec_ky(fdisi(1:nsolc),'ene',i_enespec)
      !end if
    end if

  end subroutine final_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output(file_count)
    use dist, only : fdisi, nsolc
    use mode, only : mode_box
    use diagnos_generic, only : lwrite_output1, xy_estep, lphi_diagnostics
    use diagnos_generic, only : den3d, ene3d, xy_dens, xy_temp
    use diagnos_generic, only : xy_current, xy_current2
    use diagnos_generic, only : xy_slice_ipar
    integer, intent(in) :: file_count
    

    if (.not.lwrite_output1) return
    
    if(xy_estep) then
      ! moments spectra
      !if (lfluxes_spectra) then
        call output_momentspec_ky(fdisi(1:nsolc),'den',i_denspec)
        call output_momentspec_ky(fdisi(1:nsolc),'ene',i_enespec)
      !end if
  
      if (.not. (mode_box .and. lphi_diagnostics)) return

      ! Outputs 3D files for the whole flux tube.
      ! WARNING, data volume can be huge.
      if(den3d) call output_moment_3d(fdisi(1:nsolc),'den',file_count)
      if(ene3d) call output_moment_3d(fdisi(1:nsolc),'ene',file_count)

      if (xy_dens) call output_moment_xy('den',xy_slice_ipar,file_count)
      if (xy_temp) call output_moment_xy('ene',xy_slice_ipar,file_count) 
      if (xy_current) call output_moment_xy('pac',xy_slice_ipar,file_count) 
      if (xy_current2) call output_moment_xy('p2c',xy_slice_ipar,file_count)
     
    end if

  end subroutine output

  !---------------------------------------------------------------------------
  !> Generic routine to write a 2D slice of a distribution function moment
  !> over all velocity space for each species individually at global s point
  !> ig.
  !---------------------------------------------------------------------------
  subroutine output_moment_xy(moment, ig, file_count)
    !FJC future: subroutine output_moment_xy(moment,ig,isg,file_count)
    use dist,            only : fdis => fdisi
    use velocitygrid,    only : intvp,intmu,vpgr,mugr
    use grid,            only : nmod,nx,nmu,nvpar,nsp, gsp, ls
    use grid,            only : proc_subset
    use geom,            only : bn
    use global,          only : int2char_zeros
    use general,         only : gkw_abort
    use matdat,          only : get_f_from_g 
    use mpiinterface,    only : mpiallreduce_sum
    use mpicomms,        only : COMM_VPAR_NE_MU_NE
    use diagnos_generic, only : xy_spec
    use diagnos_generic, only : xy_output_array, ky_output_array
    

    !> String defines the type of the moment.
    character (len=*), intent(in) :: moment
    !> global s point to output
    integer, intent(in) :: ig
    integer, intent(in) :: file_count
    !FJC future: integer, intent(in) :: isg !< global species number to output
    complex :: fdisi,dens
    integer :: j,k,imod,ix,is 
    real    :: integrand  
    character(len=3) :: moment2
    character (len=6), save :: file_count_suffix

    ! Return if the point ig is not on the local processor.
    if (.not. proc_subset(0,ig,0,0,0)) return
    !FJC future: if .not. proc_subset(0,ig,0,0,is) return

    file_count_suffix=trim(int2char_zeros(file_count,6))

    !FJC future: Neater to put a loop over global species outside this function call?
    ! loop over the local species - species procs do this in parallel
    per_species_moment : do is=1,nsp

      ! start with zero slice (important if looping over 2 or more species)
      cslice_xy(:,:) = (0.,0.)
      c_xy = (0.,0.)

      ! loop over the slice points for this species
      local_slice : do imod=1,nmod ; do ix=1,nx

        ! integrate over the velocity space for this point
        dens=(0.E0,0.E0)
        velocity_space : do j=1,nmu ; do k=1,nvpar

          select case(moment)

          case('den')

            integrand = 1.E0
            moment2 = 'des'

          case('ene')

            integrand = vpgr(ls(ig),j,k,is)**2 + 2.E0*mugr(j)*bn(ix,ls(ig))
            moment2 = 'ens'

          case('pac')

            integrand = vpgr(ls(ig),j,k,is) 
            moment2 = 'pas'            

          case('p2c')

            integrand = vpgr(ls(ig),j,k,is)**2 
            moment2 = 'p2s'

          case('ppc')

            integrand = sqrt(2.E0*mugr(j)*bn(ix,ls(ig)))
            moment2 = 'pps'

          case default

            call gkw_abort('function: moment_xy, wrong moment labels') 

          end select

          ! N.B. this is probably slow!

          fdisi = get_f_from_g(imod,ix,ls(ig),j,k,is,fdis)
          dens = dens + integrand*bn(ix,ls(ig))*intvp(ls(ig),j,k,is)*intmu(j)*fdisi

        end do; end do velocity_space

        ! store the density in a temporary slice for reduction later
        c_xy(imod,ix) = dens

      end do; end do local_slice

      ! sum the slice over other processors if necessary
      call mpiallreduce_sum(c_xy(1:nmod,1:nx),cslice_xy(1:nmod,1:nx),nmod,nx,  &
         &                 COMM_VPAR_NE_MU_NE)

      ! Select one processor x slice to write
      write_file : if (proc_subset(0,ig,1,1,gsp(is))) then
        ! generate a luname based on the global species
        call xy_output_array('diagnostic/diagnos_moments', &
           & cslice_xy(1:nmod,1:nx), moment// &
           & trim(int2char_zeros(gsp(is),2)),'_'//file_count_suffix)

        if(xy_spec)then
          call ky_output_array('diagnostic/diagnos_moments', &
             & abs(cslice_xy(1:nmod,1:nx)), moment2// &
             & trim(int2char_zeros(gsp(is),2)),'_'//file_count_suffix)
        end if

      end if write_file

    end do per_species_moment

  end subroutine output_moment_xy

  !--------------------------------------------------------------------
  !> Outputs ky spectra of flux surface averaged moments.
  !--------------------------------------------------------------------
  subroutine output_momentspec_ky(fdis,moment,lun)
    use mpiinterface,     only : mpiallreduce_sum, root_processor
    !use mpicomms,         only : COMM_SP_EQ
    use grid,             only : nmod, nx, ns, nmu, nvpar, proc_subset
    use grid,             only : lsp, number_of_species
    use dist,             only : nsolc   
    use geom,             only : bn, ints
    use velocitygrid,     only : intvp,intmu,vpgr,mugr
    use matdat,           only : get_f_from_g
    use io,               only : append_chunk, xy_fmt, ascii_fmt
    use general,          only : gkw_abort
    use control,          only : io_legacy

    complex, dimension(nsolc), intent(in) :: fdis
    character (len=*), intent(in) :: moment
    integer, intent(in)  :: lun

    real :: integrand, dens, fdisi
    integer :: ipar,j,k,imod,ix,is

    moment_spec(:,:) = 0.0
    buf(:,:,:) = 0.0

    species : do is=1,number_of_species
      procset : if (proc_subset(0,0,0,0,is)) then
        mod: do imod=1,nmod
          dens= 0.0
          ! loop through the local field line points and radial wavevectors
          sx : do ipar = 1, ns
            do ix = 1, nx
              ! integrate over the velocity space for this point
              velocity_space : do j=1,nmu
                do k=1,nvpar

                  select case(moment)
                  case('den')
                    integrand = 1.E0
                  case('ene')
                    integrand = vpgr(ipar,j,k,lsp(is))**2 + &
                       & 2.E0*mugr(j)*bn(ix,ipar)
                  case('pac')
                    integrand = vpgr(ipar,j,k,lsp(is))
                  case default
                    call gkw_abort('Wrong moment called in output_momentspec_ky')
                  end select

                  ! N.B. this is probably slow!
                  fdisi = abs(get_f_from_g(imod,ix,ipar,j,k,lsp(is),fdis))
                  dens = dens + bn(ix,ipar)*intvp(ipar,j,k,lsp(is))* &
                     & intmu(j)*fdisi*integrand*ints(ipar)

                end do;
              end do velocity_space
            end do
          end do sx

          ! store the moment in a temporary slice for reduction later
          buf(imod,1,is) = dens
        end do mod
      end if procset
    end do species

    ! complete the integrals over processors, "gather" over species:
    ! sum over all processors
    call mpiallreduce_sum(buf(:,1,:),moment_spec(:,:), &
       & nmod,number_of_species)
    !SRG thinks the following should be correct, rather than the preceeding
    !line, but why do testcases pass then?! (Remove this comment
    !when you have checked that)
    ! call mpiallreduce_sum(buf(:,1,:),moment_spec(:,:), &
    !    & nmod,number_of_species, COMM_SP_EQ)

    if (root_processor) then
      if(io_legacy) then
        !call append_chunk(lun, &
        !   & (/ (moment_spec(1:nmod,is),is = 1, number_of_species) /), &
        !   & xy_fmt, ascii_fmt)

        ! issue 214#40 bug with PGI compiler here
        ! the shape( (/ (moment_spec(1:nmod,is),is = 1, number_of_species) /) ) 
        ! is not correct inside the passed routine!

        call append_chunk(lun, &
           & reshape(moment_spec, (/ nmod*number_of_species /)) , &
           & xy_fmt, ascii_fmt)

      else
        ! note, for ascii, this causes interleaving of species data in a single column
        ! therefore, io_legacy is somehow always required for ascii
        call append_chunk(lun, &
           & moment_spec(1:nmod,:), &
           & xy_fmt, ascii_fmt)
      end if
    end if

  end subroutine output_momentspec_ky


  !--------------------------------------------------------------------
  !> Calculates moments on the local domain and
  !> passes it to mpi_output_array (which uses MPI-IO and subarray
  !> datatype to write in parallel) in order to write the complete 3d
  !> moment field.
  !> 
  !> WARNING: Not yet possible in hdf5 - binary file will be produced
  !--------------------------------------------------------------------
  subroutine output_moment_3d(fdis, moment, file_count)

    use mpiinterface,     only : mpiallreduce_sum, MPI_OFFSET_KIND
    use mpiinterface,     only : mpibarrier
    use io,               only : mpi_output_array, xy_fmt, binary_fmt
    use grid,             only : nmod, nx, ns, nmu, nvpar, proc_subset
    use grid,             only : lsp, number_of_species
    use mpicomms,         only : COMM_S_NE_X_NE, COMM_VPAR_NE_MU_NE
    use dist,             only : nsolc   
    use geom,             only : bn
    use non_linear_terms, only : jind
    use global,           only : int2char_zeros
    use velocitygrid,     only : intvp,intmu,vpgr,mugr
    use matdat,           only : get_f_from_g
    use general,          only : gkw_abort
    use diagnos_generic,  only : zonal_scale_3d, four2real_2D, mphit, mrad_l

    complex, dimension(nsolc), intent(in) :: fdis
    character (len=*), intent(in) :: moment
    integer, intent(in) :: file_count

    real :: integrand
    complex :: fdisi,dens
    integer :: i,j,k,imod,ix,ipar,is
    
    character (len=13) :: luname

    character (len=3)  :: prefix
    integer (kind=MPI_OFFSET_KIND) :: idisp

    select case(moment)
    case('den')
      prefix = 'D3d'
    case('ene')
      prefix = 'E3d'
    case('pac')
      prefix = 'P3d'
    case default
      call gkw_abort('Wrong moment called in output_moment_3d')
    end select

    per_species_density : do is=1,number_of_species

      procset: if (proc_subset(0,0,0,0,is)) then

        ! loop through the local field line points 
        local_s_grid : do ipar = 1, ns

          ! start with zero slice (important if looping over 2 or more species)
          cslice_xy(:,:) = (0.,0.)
          c_xy = (0.,0.)

          ! loop over the slice points for this species
          local_slice : do imod=1,nmod
            do ix=1,nx

              ! integrate over the velocity space for this point
              dens=(0.E0,0.E0)
              velocity_space : do j=1,nmu
                do k=1,nvpar

                  select case(moment)
                  case('den')
                    integrand = 1.E0
                  case('ene')
                    integrand = vpgr(ipar,j,k,lsp(is))**2 + &
                       & 2.E0*mugr(j)*bn(ix,ipar)
                  case('pac')
                    integrand = vpgr(ipar,j,k,lsp(is))
                  case default
                    call gkw_abort('Wrong moment called in output_moment_3d')
                  end select

                  ! N.B. this is probably slow!
                  fdisi = get_f_from_g(imod,ix,ipar,j,k,lsp(is),fdis)
                  dens = dens + bn(ix,ipar)*intvp(ipar,j,k,lsp(is))* &
                     & intmu(j)*fdisi*integrand

                end do
              end do velocity_space

              ! store the density in a temporary slice for reduction later
              c_xy(imod,ix) = dens
            end do
          end do local_slice

          ! sum the slice over other processors if necessary
          call mpiallreduce_sum(c_xy(1:nmod,1:nx),cslice_xy(1:nmod,1:nx), &
             & nmod,nx, COMM_VPAR_NE_MU_NE)

          ! fill the array for the potential 
          a = (0.,0.)
          ar = 0.
          do ix = 1, nx
            do imod = 1, nmod
              a(imod,jind(ix)) = cslice_xy(imod,ix)
            end do
          end do

          !Cheat on the zonal flow appearance
          a(1,:)=zonal_scale_3d*a(1,:)   

          !Do the inverse FFT  
          call four2real_2D(ar,a,1)

          ! copy slice into write buffer
          do j=1,mrad_l
            do i=1,mphit
              out3dbuf(i,j,ipar) = ar(i,j)
            end do
          end do
        end do local_s_grid

        ! create a luname
        luname = prefix//trim(int2char_zeros(is,2))//'_'// &
           & trim(int2char_zeros(file_count,6))

        !The MPI write wrapper is general, works also without MPI
        if (proc_subset(0,0,1,1,0)) then
          ! right size of zero! 
          idisp = 0
          !call mpi_output_array(luname,out3dbuf,ns*mphit*mrad_l, &
          ! & ntype,idisp,COMM_S_NE_X_NE)
          call mpi_output_array(luname, 'diagnostic/diagnos_moments', &
             & out3dbuf, &
             & ntype, idisp, COMM_S_NE_X_NE, xy_fmt, binary_fmt)
        end if

      end if procset

    end do per_species_density

  end subroutine output_moment_3d



end module diagnos_moments
