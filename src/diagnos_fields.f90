!------------------------------------------------------------------------------
! SVN:$Id$
!>
!> This diagnostic calculates and outputs data which is closely
!> related to the electromagnetic fields, i.e. the fluctuations of
!> the electrostatic potential,the vector potential and the
!> magnetic field.
!>
!> (A) kx- and ky-spectra of phi^2 and other perturbed fields
!> (B) A spectrum suitable especially for the Rosenbluth-Hinton test
!> (C) kx- and ky-spectra of the vorticity
!> (D) Parallel structure of the fields
!> (D) The 3D fields for the whole flux tube
!>
!------------------------------------------------------------------------------
module diagnos_fields

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, final_output
  public :: output

  logical, save, public :: xy_phi
  logical, save, public :: xy_apar
  logical, save, public :: xy_bpar
  logical, save, public :: xy_vort

  !logical to control parallel_phi output; default to T
  logical, save, public :: lparallel_phi

  !logical to control parallel_apar output; default to F
  logical, save, public :: lparallel_apar

  !> logicals to select various x s diagnostic outputs
  !> (to be completed)
  logical, save, public :: xs_phi
  integer, parameter :: nmodemax = 256
  !> A list of toroidal mode numbers. The requested field is going to
  !> be output for each entry.
  integer, save, public :: imod_field(nmodemax)
  integer, save, dimension(:), allocatable :: imod_list

  !> Obsolete namelist item: Number of toroidal points to consider (if
  !> -1 outputs all)
  integer, save, public :: nmodepoints


  !> logical unit numbers for output files
  integer, save :: i_kxvort = -1, i_kyvort = -1

  !> logical unit numbers for output files (in legacy format/order/grouping)
  integer, save :: i_kyspec = -1, i_kxspec = -1
  integer, save :: i_kyspec_em = -1, i_kxspec_em = -1, i_rhtest = -1
  integer, save :: i_parphi = -1, i_parapar = -1
  

  !> Private FFT arrays.
  complex, save, allocatable :: a(:,:)
  real, save, allocatable :: ar(:,:)

  !> The fields spectra. ky_spec(nmod), kx_spec(nx), rhbuf(nx)
  !> The latter is for the Rosenbluth Hinton test
  real, save, allocatable, dimension(:) :: ky_spec, kx_spec, rhbuf

  !> (testing) buffer for 3D outputs
  real, save, allocatable :: out3dbuf(:,:,:)

  !> Buffer for MPI reductions, buffer(max(nx,nmod))
  real, save, allocatable, dimension(:) :: buffer

  !> arrays for the vorticity(nmod,nx)
  real, save, allocatable, dimension(:,:) :: vort
  real, save, allocatable, dimension(:,:) :: vortbuf
  
  !> arrays for parallel_phi phi_par(n_s_grid)
  real, save, allocatable, dimension(:) :: phi_par
  real, save, allocatable, dimension(:) :: phi_par_buf

  integer :: mphit, mrad_l, mrad_G
  integer :: mphiw3t

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    xy_phi = .false.
    xy_apar = .false.
    xy_bpar = .false.
    xy_vort = .false.
    
    lparallel_phi=.true.
    lparallel_apar=.false.
    
    xs_phi      = .false.
    imod_field = 0
 
  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(xy_phi,1)
    call mpibcast(xy_apar,1)
    call mpibcast(xy_bpar,1)
    call mpibcast(xy_vort,1)

    call mpibcast(xs_phi,1)
    call mpibcast(imod_field,nmodemax)

    call mpibcast(lparallel_phi,1)
    call mpibcast(lparallel_apar,1)

  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use control, only : nlapar, spectral_radius
    use general, only : gkw_warn, gkw_abort
    use grid,    only : nmod
   
    ! output of the Apar parallel structure only if apar run
    if ((.not. nlapar).and.lparallel_apar) then
      call gkw_warn('Output of the A_// parallel structure not possible with nlapar=.false.')
      lparallel_apar=.false.
    end if

    if (xs_phi .and. spectral_radius) then
      call gkw_abort('output_field_xs: &
         & not implemented yet for the spectral version')
    end if

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use io, only : open_real_lu, open_complex_lu, ascii_fmt, attach_metadata
    use io, only : description_key, comments_key, phys_unit_key, not_avail
    use diagnos_generic, only : attach_metadata_grid
    use components, only : finit
    use control, only : normalized, nlapar
    use control, only : flux_tube, spectral_radius, io_legacy
    use mode, only : mode_box
    use grid, only : nmod, n_x_grid, nx, n_s_grid
    use global, only : dotdat
    
    if(.not.root_processor) return

    !if (non_linear .or. mode_box) then! legacy switch
    if (mode_box) then 
      call open_real_lu('kyvort', 'diagnostic/diagnos_fields', (/ nmod /), &
         & ascii_fmt, i_kyvort)
      call open_real_lu('kxvort', 'diagnostic/diagnos_fields', (/ n_x_grid /), &
         & ascii_fmt, i_kxvort)
    end if

    if ((finit=='zonal').or.(finit=='gam')) then
      call open_complex_lu('rhtest', 'diagnostic/diagnos_fields', (/ nx /), &
         & ascii_fmt, i_rhtest)
    end if

    if (.not. normalized) then
      call open_real_lu('kyspec', 'diagnostic/diagnos_fields', (/ nmod /), &
         & ascii_fmt, i_kyspec)
      if(mode_box .and. flux_tube) then
        call attach_metadata_grid(i_kyspec, 'time', 'krho', ascii_fmt)
      else
        call attach_metadata_grid(i_kyspec, 'time', 'kzeta', ascii_fmt)
      end if
      call attach_metadata(i_kyspec, phys_unit_key, '(T_{ref}/e)^2', ascii_fmt)
      call attach_metadata(i_kyspec, description_key, &
         & 'binormal power spectrum of the electrostatic potential', ascii_fmt)
      call attach_metadata(i_kyspec, comments_key, &
         & 'Traditionally, the modulus square of the es. potential is used as &
         & a measure of the mode intensity.', ascii_fmt)
      
      call open_real_lu('kxspec', 'diagnostic/diagnos_fields', (/ n_x_grid /), &
         & ascii_fmt, i_kxspec)
      if(mode_box .and. spectral_radius) then
        call attach_metadata_grid(i_kxspec, 'time', 'kxrh', ascii_fmt)
      else
        call attach_metadata_grid(i_kxspec, 'time', not_avail, ascii_fmt)
      end if
      call attach_metadata(i_kxspec, phys_unit_key, '(T_{ref}/e)^2', ascii_fmt)
      call attach_metadata(i_kxspec, description_key, &
         & 'radial power spectrum of the electrostatic potential', ascii_fmt)
      call attach_metadata(i_kxspec, comments_key, &
         & 'Traditionally, the modulus square of the es. potential is used as &
         & a measure of the mode intensity.', ascii_fmt)

      if(nlapar) then
        call open_real_lu('kyspec_em', 'diagnostic/diagnos_fields', (/ nmod /), &
           & ascii_fmt, i_kyspec_em)
        call open_real_lu('kxspec_em', 'diagnostic/diagnos_fields', (/ n_x_grid /), &
           & ascii_fmt, i_kxspec_em)
      end if
    end if

    if (lparallel_phi) then
      call open_real_lu(dotdat('parallel_phi',io_legacy), &
         & 'diagnostic/diagnos_fields', (/ n_s_grid /), &
         & ascii_fmt, i_parphi)
    end if

    if (lparallel_apar) then
      call open_real_lu(dotdat('parallel_apar',io_legacy), &
         & 'diagnostic/diagnos_fields', (/ n_s_grid  /), &
         & ascii_fmt, i_parapar)
    end if
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use grid, only : nmod, n_x_grid, nx, n_s_grid, ns
    use diagnos_generic, only : compute_fft_sizes, any_3d_output
    use general, only : gkw_abort
    integer :: ierr
    integer :: n, i
    
    call compute_fft_sizes(nmod, n_x_grid, nx, &
       & mphit, mphiw3t, mrad_G, mrad_l)

    ! allocate the imod_field arrays:
    ! At first count all elements from the arrays no_transfer_to/from
    ! which are in the range of admissible indices.
    n = 0
    listloop: do i = 1, size(imod_field)
      if(imod_field(i) >= 1 .and. imod_field(i) <= nmod) then
        n = n + 1
      else if(imod_field(i) > nmod) then
        n = nmod
        imod_field(1) = imod_field(i)
        exit listloop
      end if
    end do listloop
    ! Now n contains the number of mode indices given by the input file.
    ! Allocate the list of requested mode numbers:
    allocate(imod_list(n),stat=ierr)
    if (ierr /= 0) then
      call gkw_abort('Could not allocate imod_list in diagnos_fields')
    end if
    ! fill this list: either will all mode numbers...
    if(imod_field(1) > nmod) then
      imod_list = (/ (i, i=1,nmod) /)
    else
      ! ... or just with a few, as given in the input namelist.
      n = 0
      do i = 1, size(imod_field)
        if(imod_field(i) >= 1 .and. imod_field(i) <= nmod) then
          n = n + 1
          imod_list(n) = imod_field(i)
        end if
      end do
    end if
    
    ! Arrays for FFT
    allocate(a(mphiw3t,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate a in diagnostic')
    endif
    allocate(ar(mphit,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate ar in diagnostic')
    endif

    allocate(vort(nmod,n_x_grid),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vort')
    allocate(vortbuf(nmod,n_x_grid),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: vortbuf')

    if (lparallel_phi.or.lparallel_apar) then
      allocate(phi_par(n_s_grid),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: phi_parallel')
      allocate(phi_par_buf(n_s_grid),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: phi_parallel_buf')
    end if

    ! array for MPI reduction
    ierr = 0 
    allocate(buffer(max(nx,nmod)), stat = ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: buffer')

    ! Arrays used when the whole three dimensional outputs selected.
    !(Not strictly needed for spc3d)
    if (any_3d_output) then
      allocate(out3dbuf(mphit,mrad_l,ns),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: out3dbuf')
    end if
  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()
    use mpiinterface, only : root_processor
    use io, only : close_lu, ascii_fmt
    use components, only : finit
    use control, only : normalized
    use control, only : nlapar
    use mode, only : mode_box

    if(allocated(a)) deallocate(a)
    if(allocated(ar)) deallocate(ar)
    if(allocated(vort)) deallocate(vort)
    if(allocated(vortbuf)) deallocate(vortbuf)
    if(allocated(phi_par)) deallocate(phi_par)
    if(allocated(phi_par_buf)) deallocate(phi_par_buf)
    if(allocated(buffer)) deallocate(buffer)
    if(allocated(out3dbuf)) deallocate(out3dbuf)

    if(.not. root_processor) return

    ! be nice and close all logical units
    if (mode_box) then 
      call close_lu(i_kyvort, ascii_fmt)
      call close_lu(i_kxvort, ascii_fmt)
    end if

    if ((finit=='zonal').or.(finit=='gam')) then
      call close_lu(i_rhtest, ascii_fmt)
    end if

    if (.not. normalized) then
      call close_lu(i_kyspec, ascii_fmt)
      call close_lu(i_kxspec, ascii_fmt)

      if(nlapar) then
        call close_lu(i_kyspec_em, ascii_fmt)
        call close_lu(i_kxspec_em, ascii_fmt)
      end if
    end if

    if (lparallel_phi) then
      call close_lu(i_parphi, ascii_fmt)
    end if

    if (lparallel_apar) then
      call close_lu(i_parapar, ascii_fmt)
    end if
  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()

  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine final_output(number)
    use diagnos_generic, only : xy_slice_ipar, xy_estep
    use global, only : int2char_zeros
    integer, intent(in) :: number
    character (len=6), save :: number_suffix
    
    number_suffix=trim(int2char_zeros(number,6))
    
    if (.not.xy_estep) then
      call calc_largestep()
      
      if (xy_apar) call output_field_xy('apa',xy_slice_ipar,number)
      if (xy_bpar) call output_field_xy('bpa',xy_slice_ipar,number)
      if (xy_phi) call output_field_xy('phi',xy_slice_ipar,number)
      if (xy_vort) call output_field_xy('vort',1,number)

      if (xs_phi) then
        call output_field_xs('phi', number)
        !call output_field_xs('apa', number) ! uncomment, if you want it
        !call output_field_xs('bpa', number)
      end if
    end if
  end subroutine final_output

  !--------------------------------------------------------------------
  !> This routine calculates the vorticity
  !> \f[
  !> \omega = \sum_{sp}\int d^3v \int ds n_{R,sp} Z_{sp} |f_{sp}|^2
  !> \f]
  !--------------------------------------------------------------------
  subroutine calc_largestep()
    use control,        only : nlapar, neoclassics, ncqtol, fluxtol
    use control,        only : spectral_radius, flux_tube, ifluxtol
    use control,        only : nlphi, nlbpar
    use grid,           only : nx, ns, nmu, nvpar, nsp, n_x_grid 
    use grid,           only : nmod, number_of_species, gsp, gx
    use dist,           only : fdisi, i_mom, i_ene
    use dist,           only : iphi, iapar, ibpar
    use geom,           only : ints, bn, signB 
    use components,     only : de, signz
    use components,     only : tearingmode, rhostar, tm_drive
    use rotation,       only : vcor
    use velocitygrid,   only : intmu, intvp
    use matdat,         only : get_f_from_g 
    use index_function, only : indx 
    use collisionop,    only : mom_conservation,ene_conservation
    use fields,         only : get_averaged_phi, get_averaged_apar
    use fields,         only : get_averaged_bpar 
    use linear_terms,   only : drift,lpoisson,lvpgrphi,lneorotsource
    use rotation,       only : cf_trap, cf_drift, vcor, coriolis
    use general,        only : gkw_is_nan
    use rho_par_switch, only : lflux_rhostar
    use mpiinterface,   only : mpiallreduce_sum
    use mpiinterface,   only : number_of_processors
    use io,             only : append_chunk, xy_fmt, ascii_fmt

    ! integers for the loop over all grid points 
    integer :: imod, ix, i, j, k, is 

    ! ! Dummy variables 
    ! complex :: dum, dumes1, dumes2, dumem1, dumem2, dumbpar1, dumbpar2
    ! complex :: dumes_rs, dumem_rs, dumbpar_rs
    ! complex :: dumem1_cos
    complex :: fdis
    ! real    :: omega_times_time
    ! ! real phi2, apa2, bpa2
    ! real drift_x, drift_y, drift_z, dumnc, dumbs
    ! integer l, imomidx

    ! The global species (ix) index is in isglb (ixg)
    integer ixg

    ! ! index for flux output array
    ! integer :: iflux

    ! Initialize to zero 
    vort = 0. 

    nmod1: do imod = 1, nmod 
      nx1: do ix = 1, nx 
        nsp1: do is = 1, nsp
          ! the actual (global) x index - blanks are left elsewhere
          ixg = gx(ix)

          ! Integral over the velocity space 
          nmu3: do j = 1, nmu
            nvpar3: do k = 1, nvpar

              ! Do the average over the flux surface
              ns3: do i = 1, ns

                ! fdis is the distribution without A_par contribution  
                fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)

                ! in the implicit scheme fdis can be NaN for intvp = 0 
                if (intvp(i,j,k,is).eq.0) fdis = 0.

                !Vorticity
                vort(imod,ixg) = vort(imod,ixg)+ signz(is)*de(ix,is)* &
                   & abs(fdis)**2 &
                   & *2.E0*bn(ix,i)*ints(i)*intvp(i,j,k,is)*intmu(j)
              end do ns3
            end do nvpar3
          end do nmu3
        end do nsp1
      end do nx1
    end do nmod1

    ! only when run on more than one processor (saves two copies)
    if (number_of_processors > 1) then

      ! FIXME This routine better should not sum a global array with a
      ! lot of zeros but gather local arrays. This should save
      ! communication and memory.
      call mpiallreduce_sum(vort,vortbuf,nmod,n_x_grid)
      vort(:,:)=vortbuf(:,:)

    end if

  end subroutine calc_largestep

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output(file_count)
    use mode, only : mode_box
    use dist, only : fdisi, nsolc
    use dist, only : get_phi, phi, get_apar, apar, get_bpar, bpar
    use diagnos_generic, only : lwrite_output1, lphi_diagnostics, xy_estep
    use diagnos_generic, only : phi3d, spc3d, apa3d, apc3d, bpa3d, bpc3d
    use diagnos_generic, only : xy_slice_ipar
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use control, only : nlapar, nlbpar, normalized
    use global, only : int2char_zeros
    use mpiinterface, only : root_processor

    integer, intent(in) :: file_count

    character (len=6), save :: file_count_suffix

    if (.not.lwrite_output1) return
    ! diagnostic modules are more portable if they fill the fields as
    ! needed.
    call get_phi(fdisi(1:nsolc),phi)
    if (nlapar) call get_apar(fdisi(1:nsolc),apar)
    if (nlbpar) call get_bpar(fdisi(1:nsolc),bpar)

    if (lparallel_phi) call output_field_parallel('phi',i_parphi)
    if (lparallel_apar) call output_field_parallel('apa',i_parapar)
    ! If you want to add the 'bpa' field output, you need to create luns
    ! and then uncomment this line
    !if (lparallel_bpar) call output_field_parallel('bpa',i_parbpar) 

    if (.not. (mode_box .and. lphi_diagnostics)) return
    
    file_count_suffix=trim(int2char_zeros(file_count,6))

    call calc_largestep

    if (xy_estep) then
      if (xy_apar) call output_field_xy('apa',xy_slice_ipar, file_count) 
      if (xy_bpar) call output_field_xy('bpa',xy_slice_ipar, file_count) 
      if (xy_phi) call output_field_xy('phi',xy_slice_ipar, file_count)
      if (xy_vort) call output_field_xy('vort',1, file_count)

      if (xs_phi) then
        call output_field_xs('phi', file_count)
        !call output_field_xs('apa', file_count) ! uncomment, if you want it
        !call output_field_xs('bpa', file_count)
      end if
    end if

    ! fields spectra
    if (.not. normalized) then
      call output_fieldspec_kykx('phi',fdisi(1:nsolc),i_kyspec,i_kxspec)
    end if

    if (nlapar .and. .not. normalized) then
      call output_fieldspec_kykx('apa',fdisi(1:nsolc),i_kyspec_em,i_kxspec_em)
    end if

    ! If you want to add the 'bpa' field output, you need to create files
    ! and luns then call
    !if (mode_box.and.nlbpar) then
    !  call output_fieldspec_kykx('bpa',fdisi(1:nsolc),i_kyspec_em2,i_kxspec_em2)
    !end if

    if(root_processor) then
    ! if(fluxes_spectra) then !legacy switch
      ! vorticity
      if (mode_box) then
        call append_chunk(i_kyvort, sum(vort, 2), xy_fmt, ascii_fmt)
        call append_chunk(i_kxvort, sum(vort, 1), xy_fmt, ascii_fmt)
      end if
    ! end if
    end if

    if (lwrite_output1) then
      ! Outputs 3D files for the whole flux tube.
      ! WARNING, data volume can be huge.
      if(phi3d.or.spc3d) call output_field_3d(fdisi(1:nsolc), &
         & 'phi',file_count)
      if(apa3d.or.apc3d) call output_field_3d(fdisi(1:nsolc), &
         & 'apa',file_count)
      if(bpa3d.or.bpc3d) call output_field_3d(fdisi(1:nsolc), &
         & 'bpa',file_count)
    end if
  end subroutine output


  !-----------------------------------------------------------------------------
  !> Generic slice output of any 3D field variable.
  !> To add a further field output add a new case.
  !> May be desirable to keep full complex number.
  !----------------------------------------------------------------------------
  subroutine output_field_xy(field,isg,file_count)

    use grid,    only : nmod, nx, ls, proc_subset
    use dist,    only : phi, apar, bpar, fdisi, get_apar, get_phi, get_bpar
    use general, only : gkw_abort
    use global,  only : int2char_zeros
    use diagnos_generic, only : zonal_scale_3d
    use diagnos_generic, only : xy_output_array, ky_output_array
    use io, only : attach_metadata
    use io, only : phys_unit_key, description_key, comments_key, not_avail
    use diagnos_generic, only : attach_metadata_grid

    !> String that defines the type of the field.
    character (len=*), intent(in) :: field
    !> Global s index of the slice to output.
    integer, intent(in) :: isg
    integer, intent(in) :: file_count
    character (len=6), save :: file_count_suffix

    ! must call from all x processors to avoid deadlock
    if (.not.proc_subset(0,isg,1,1,1)) return

    file_count_suffix=trim(int2char_zeros(file_count,6))

    select case(field)

    case('phi')

      call get_phi(fdisi,phi)

      ! output spc (modulus), including x gather
      call ky_output_array('diagnostic/diagnos_fields', &
         & abs(phi(1:nmod,1:nx,ls(isg))),'spc', &
         & file_count_suffix)
      ! ! one of the processes working here should complete the metadata
      ! if(proc_subset(1,isg,1,1,1)) then
      !   call attach_metadata_grid('spc'//file_count_suffix, &
      !      & 'diagnostic/diagnos_fields', 'krho', 'xphi', ascii_fmt)
      !   call attach_metadata('spc'//file_count_suffix, &
      !      & 'diagnostic/diagnos_fields', phys_unit_key, 'T_{ref}/e', &
      !      & ascii_fmt)
      !   call attach_metadata('spc'//file_count_suffix, &
      !      & 'diagnostic/diagnos_fields', description_key, not_avail, ascii_fmt)
      !   call attach_metadata('spc'//file_count_suffix, &
      !      & 'diagnostic/diagnos_fields', comments_key, not_avail, ascii_fmt)
      ! end if

      !Cheat on the zonal flow appearance
      phi(1,:,:)=zonal_scale_3d*phi(1,:,:)

      ! output 2d slice of phi (in real space, including x gather)
      call xy_output_array('diagnostic/diagnos_fields', &
         & phi(1:nmod,1:nx,ls(isg)),'phi', &
         & file_count_suffix)

      !reget phi, is case other routines reuse it. Is this needed ?
      call get_phi(fdisi,phi)

    case('apa')

      call get_apar(fdisi,apar)

      ! output sac (modulus), including x gather
      call ky_output_array('diagnostic/diagnos_fields', &
         & abs(apar(1:nmod,1:nx,ls(isg))),'sac', &
         & file_count_suffix) 
      ! output 2d slice of apar (in real space, including x gather)
      call xy_output_array('diagnostic/diagnos_fields', &
         & apar(1:nmod,1:nx,ls(isg)),'apa', &
         & file_count_suffix) 

    case('bpa')

      call get_bpar(fdisi,bpar)

      ! output bpc (modulus), including x gather
      call ky_output_array('diagnostic/diagnos_fields', &
         & abs(bpar(1:nmod,1:nx,ls(isg))),'bpc', &
         & file_count_suffix)   
      ! output 2d slice of bpar (in real space, including x gather)
      call xy_output_array('diagnostic/diagnos_fields', &
         & bpar(1:nmod,1:nx,ls(isg)),'bpa', &
         & file_count_suffix)

    case('vort')
      ! output vok, including x gather
      call ky_output_array('diagnostic/diagnos_fields', &
         & vort(1:nmod,1:nx),'vok',file_count_suffix)

    case default

      call gkw_abort('function: field_xy, wrong field labels') 

    end select !File version.

  end subroutine output_field_xy

  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !>
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine output_field_xs(fieldname, file_count)
    use grid,         only : nx, n_s_grid, n_x_grid, ns, nmod, proc_subset
    use dist,         only : get_phi, get_apar, get_bpar
    use dist,         only : phi, apar, bpar, fdisi
    use mpiinterface, only : gather_array
    use mpicomms,     only : COMM_S_NE, COMM_X_NE
    use mpiinterface, only : gather_array
    use io,           only : output_array, xy_fmt, binary_fmt
    use general,      only : gkw_abort
    use global,       only : int2char_zeros

    character (len=*), intent(in) :: fieldname
    integer, intent(in) :: file_count
    character (len=20) :: luname
    integer ix, is, imode
    complex, dimension(nx,ns)              :: field_xs_local
    complex, dimension(n_x_grid, n_s_grid) :: field_xs_global
    character (len=6), save :: file_count_suffix

    !> violates the letter of the law against pointers
    !> if you object you can define a new dummy field array
    !> or find a cleverer solution
    complex, pointer :: field(:,:,:)

    ! avoid some unnecessary gather-communication
    if (.not.proc_subset(0,0,1,1,1)) return

    ! fill the buffer
    select case (fieldname)
    case ('phi')
      call get_phi(fdisi,phi)
      field => phi
    case('apa') 
      call get_apar(fdisi,apar)
      field => apar
    case('bpa')
      call get_bpar(fdisi,bpar)
      field => bpar
    case default
      call gkw_abort('output_field_xs: another field is requested.')
    end select
    
    do imode = 1, size(imod_list)
      ! fill the buffer
      do ix = 1, nx
        do is = 1, ns
          field_xs_local(ix,is) = field(imod_list(imode),ix,is)
        end do
      end do

      ! parallel_s and parallel_x gather
      call gather_array(field_xs_global,n_x_grid,n_s_grid, &
         & field_xs_local,nx,ns, &
         & COMM_X_NE,COMM_S_NE,ALLGATHER=.true.)

      ! output to files
      if (proc_subset(1,1,1,1,1)) then
        ! let the IO module deal with the real/imag parts:
        file_count_suffix = trim(int2char_zeros(file_count,6))
        luname = trim(fieldname)//'_xs_n' &
           & //trim(int2char_zeros(imod_list(imode),2)) &
           & //'_t'//file_count_suffix
        call output_array(luname, 'diagnostic/diagnos_fields', &
           & field_xs_global, 'F', xy_fmt, binary_fmt)
      end if
    end do
  end subroutine output_field_xs

  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !> This routine writes the total abs()^2 of a parallel field, summed
  !> over all the modes. It is intended to be called at repeated timesteps.
  !> This can also be used as a template for time-parallel diagnostics.
  !----------------------------------------------------------------------------
  subroutine output_field_parallel(field,lun)

    use mpiinterface, only : gather_array, mpiallreduce_sum 
    use mpiinterface, only : root_processor
    use mpicomms,     only : COMM_S_NE, COMM_X_NE
    use grid,         only : ns,nmod,nx,n_s_grid, n_x_grid, proc_subset
    use dist,         only : fdisi,get_phi, get_bpar, get_apar
    use dist,         only : apar, bpar, phi
    use control,      only : non_linear
    use general,      only : gkw_abort
    use io,           only : append_chunk, xy_fmt, ascii_fmt

    character (len=*), intent(in) :: field
    integer, intent(in) :: lun
    integer :: is,imod,ix

    !> violates the letter of the law against pointers
    !> if you object you can define a new dummy field array
    !> or find a cleverer solution
    complex, pointer :: fil(:,:,:)

    ! processors not involved just return
    if (.not. proc_subset(0,0,1,1,1)) return

    select case(field)
    case('phi')
      call get_phi(fdisi,phi)
      fil => phi
    case('apa') 
      call get_apar(fdisi,apar)
      fil => apar
    case('bpa')
      call get_bpar(fdisi,bpar)
      fil => bpar
    case default
      call gkw_abort('unknown field')
    end select

    ! Sum over modes for the local s points
    phi_par_buf(:) =0.
    phi_par(:)=0.
    do is=1,ns
      do ix=1,nx
        do imod=1,nmod
          phi_par(is) = phi_par(is) + abs(fil(imod,ix,is))**2
        end do
      end do
    end do

    call mpiallreduce_sum(phi_par(1:ns),phi_par_buf(1:ns),ns,COMM_X_NE)

    ! gather all the bits of phi to the root processor (the calling processor
    ! with rank=0 in the communicator COMM_S_NE, which in this case is the
    ! global root processor).
    call gather_array(phi_par,n_s_grid,phi_par_buf,ns,COMM_S_NE)

    ! Rescale to be irrespective of number of modes
    ! For linear runs, only a single mode will dominate the potential
    ! Even if there are many modes.
    if (non_linear) phi_par(:)=phi_par(:)/(nmod*n_x_grid)

    ! Write with the root_processor.
    if(root_processor) then
      call append_chunk(lun, phi_par, xy_fmt, ascii_fmt)
    end if

  end subroutine output_field_parallel

  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !>
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine output_fieldspec_kykx(field,fdis,lun_ky,lun_kx) 

    use grid,         only : nmod, nx, n_s_grid, ls, n_x_grid, ns
    use grid,         only : proc_subset
    use dist,         only : get_phi, nsolc, get_apar, get_bpar
    use dist,         only : phi, apar, bpar
    use mpicomms,     only : COMM_S_NE_X_NE
    use control,      only : spectral_radius
    use components,   only : finit
    use fft,          only : fourcol
    use geom,         only : ints
    use mpiinterface, only : mpiallreduce_sum, gather_array, root_processor
    use mpiinterface, only : mpiallreduce_sum_inplace
    use mpicomms,     only : COMM_S_NE, COMM_X_NE, COMM_DUMMY
    use io,           only : append_chunk, xy_fmt, ascii_fmt
    use general,      only : gkw_abort
    use diagnos_generic, only : parseval_correction

    complex, intent(in) :: fdis(nsolc)  !< The distribution 
    character (len=*), intent(in) :: field
    integer, intent(in) :: lun_kx, lun_ky

    integer ::  imod, ix, is
    logical, save :: initialised = .false.
    complex, pointer :: fil(:,:,:)
    complex, allocatable, save :: ax(:,:), agx(:,:)
    integer :: ierr

    if (.not.initialised) then 

      ierr = 0 
      allocate(ky_spec(nmod), stat = ierr)
      if (ierr /= 0) call gkw_abort('could not allocate ky_spec')

      ierr = 0 
      allocate(kx_spec(n_x_grid), stat = ierr)
      if (ierr /= 0) call gkw_abort('could not allocate kx_spec')

      if (.not. spectral_radius) then
        ierr = 0 
        allocate(ax(nx,nmod), stat = ierr)
        if (ierr /= 0) call gkw_abort('could not allocate ax')

        ierr = 0 
        allocate(agx(n_x_grid,nmod), stat = ierr)
        if (ierr /= 0) call gkw_abort('could not allocate agx')
      end if

      initialised = .true. 

    end if

    ! alternatively could just pass it to the function
    select case (field)
    case ('phi')
      call get_phi(fdis,phi) 
      fil => phi
    case ('apa') 
      call get_apar(fdis,apar)
      fil => apar
    case ('bpa')
      call get_bpar(fdis,bpar)
      fil => bpar
    case default
      call gkw_abort('more fields needed')
    end select

    kx_spec(:)=0.
    ky_spec(:)=0.

    ! kyspec
    do imod = 1, nmod 
      buffer(imod) = 0.
      do is = 1, n_s_grid
        if(proc_subset(0,is,0,0,0)) then !For parallel_s
          do ix = 1, nx 
            buffer(imod) = buffer(imod) + abs(fil(imod,ix,ls(is)))**2 *ints(ls(is))
          end do
        end if
      end do
    end do

    ! PARALLEL_S or X
    call mpiallreduce_sum(buffer(1:nmod),ky_spec,nmod,COMM_S_NE_X_NE)

    if (spectral_radius) then
      ! kxspec, buffer is reused
      do ix = 1, nx 
        buffer(ix) = 0.
        do is = 1, n_s_grid 
          if(proc_subset(0,is,0,0,0)) then !For parallel_s
            do imod = 1, nmod 
              buffer(ix) = buffer(ix) + abs(fil(imod,ix,ls(is)))**2 *ints(ls(is))
            end do
          end if
        end do
      end do

      ! PARALLEL_S
      call mpiallreduce_sum(buffer(1:nx),kx_spec,nx,COMM_S_NE)

    else

      kx_spec(:) = 0
      
      do is = 1,ns
        ! get a slice
        do imod = 1, nmod
          
          do ix = 1, nx
            ax(ix, imod) = fil(imod,ix,is)
          end do
        end do

        ! gather in x direction
        call gather_array(agx, n_x_grid, nmod, &
           & ax, nx, nmod, &
           & COMM_X_NE, COMM_DUMMY)
        ! call gather_array(agx(1:n_x_grid),n_x_grid, &
        !    & ax(1:nx),nx, &
        !    & COMM_X_NE)

        ! fouriertrafo in x direction (= the first dimension of the array)
        call fourcol(agx,-1)

        ! integrate the |phi|^2 locally
        do imod = 1, nmod
          do ix = 1, n_x_grid
            kx_spec(ix) = kx_spec(ix) + abs(agx(ix, imod))**2 * &
               & ints(is) * parseval_correction(imod)
          end do
        end do
      end do
      
      ! finish off FSA by mpireduce_sum in s direction
      call mpiallreduce_sum_inplace(kx_spec,n_x_grid,COMM_S_NE)

    end if

    if (root_processor) then
      call append_chunk(lun_ky, ky_spec, xy_fmt, ascii_fmt)
      call append_chunk(lun_kx, kx_spec, xy_fmt, ascii_fmt)
      if (((finit == 'zonal').or.(finit == 'gam')) .and. field=='phi' &
         & .and. ns == n_s_grid .and. spectral_radius &
         & ) then
        ! Alternate kxspec for Rosenbluth Hinton residual benchmark,
        ! This involves a sum over the ns direction.
        call append_chunk(i_rhtest, &
           & sum(phi(1,:,:),2)/n_s_grid, &
           & xy_fmt, ascii_fmt)
      end if
    end if

  end subroutine output_fieldspec_kykx


  !----------------------------------------------------------------------
  !> Outputs the full three dimensional field in binary format
  !> Uses MPI-IO and subarray datatype to write in parallel
  !> WARNING: Not yet possible in hdf5 - binary file will be produced
  !----------------------------------------------------------------------
  subroutine output_field_3d(fdis,field,file_count)

    use io,               only : mpi_output_array, xy_fmt, binary_fmt
    use mpiinterface,     only : MPIREAL_X
    use mpiinterface,     only : mpibarrier, MPI_OFFSET_KIND
    use mpidatatypes,     only : create_subarray_datatype
    use grid,             only : nmod,nx,ns, proc_subset
    use mpicomms,         only : COMM_S_NE_X_NE
    use dist,             only : nsolc,bpar,get_bpar, apar, get_apar 
    use dist,             only : phi, get_phi
    use control,          only : spectral_radius   
    use non_linear_terms, only : jind
    use general,          only : gkw_abort
    use global,           only : int2char_zeros
    use global,           only : id_s,id_x,id_mod, lverbose
    use diagnos_generic,  only : zonal_scale_3d, bpc3d, apc3d, spc3d
    use diagnos_generic,  only : four2real_2D

    complex, dimension(nsolc), intent(in) :: fdis
    character (len=*), intent(in) :: field
    integer,intent(in) :: file_count

    integer :: i,j,imod,ix,ipar
    integer, save :: ntype, ntype2
    character (len=13) :: luname, luname2
    logical, save :: initialised = .false.
    integer (kind=MPI_OFFSET_KIND) :: idisp

    !> violates the rule against pointers, but convenient
    complex, pointer :: fil(:,:,:)

    if (.not. initialised) then
      if (lverbose) write (*,*) 'binary_field: first call - initialising' 

      if (proc_subset(0,0,1,1,1)) then
        ! create subarray MPI datatype for 3d spectral field
        call create_subarray_datatype(MPIREAL_X,ntype,id_mod,id_x,id_s)

        ! create subarray dataype for 3d real space field
        if (spectral_radius) then
          call create_subarray_datatype(MPIREAL_X,ntype2,id_mod,id_x,id_s,FFTX=mrad_l,FFTY=mphit)
        else
          call create_subarray_datatype(MPIREAL_X,ntype2,id_mod,id_x,id_s,FFTY=mphit)
        end if
      endif

      call mpibarrier()
      initialised = .true.
    else
      ! do nothing
    end if

    !Obtain field in a separate array 
    select case(field)
    case('phi')
      call get_phi(fdis,phi)
      luname = 'Spc3d'//trim(int2char_zeros(file_count,8))
      luname2 = 'Poten'//trim(int2char_zeros(file_count,8))
      fil => phi
    case('apa')
      call get_apar(fdis,apar)
      luname = 'Apc3d'//trim(int2char_zeros(file_count,8))
      luname2 = 'Apara'//trim(int2char_zeros(file_count,8))
      fil => apar
    case('bpa')
      call get_bpar(fdis,bpar) 
      luname = 'Bpc3d'//trim(int2char_zeros(file_count,8))
      luname2 = 'Bpara'//trim(int2char_zeros(file_count,8))
      fil => bpar
    case default
      call gkw_abort('Wrong field selected in binary output')
    end select

    !This can be used for all cases: The MPI write wrapper should be general
    !Write the spectral field  
    spc: if ((bpc3d.or.apc3d.or.spc3d).and.spectral_radius) then
      if (proc_subset(0,0,1,1,1)) then
        ! right size of zero! 
        idisp = 0
        ! call mpi_output_array(luname, abs(fil), ns*nmod*nx, ntype, &
        !    & idisp, COMM_S_NE_X_NE)
        call mpi_output_array(luname, 'diagnostic/diagnos_fields', &
           & abs(fil), ntype, &
           & idisp, COMM_S_NE_X_NE, xy_fmt, binary_fmt)
      end if
    end if spc

    ! loop through the local field line points 
    local_s_grid : do ipar = 1, ns

      ! fill the array for the potential 
      a = (0.,0.)
      ar = 0.
      do ix = 1, nx; do imod = 1, nmod
        a(imod,jind(ix)) = fil(imod,ix,ipar)
      end do; end do

      !Cheat on the zonal flow appearance
      a(1,:)=zonal_scale_3d*a(1,:)   

      !Do the inverse FFT
      call four2real_2D(ar,a,1)

      ! copy slice into write buffer
      do j=1,mrad_l
        do i=1,mphit
          out3dbuf(i,j,ipar) = ar(i,j)
        end do
      end do

    end do local_s_grid

    ! Write the real space field
    !This can be used for all cases: The MPI write wrapper works also without MPI
    if (proc_subset(0,0,1,1,1)) then
      ! right size of zero! 
      idisp = 0
      ! call mpi_output_array(luname2, out3dbuf, ns*mphit*mrad_l, &
      ! & ntype2, idisp, COMM_S_NE_X_NE)
      call mpi_output_array(luname2, 'diagnostic/diagnos_fields', out3dbuf, &
         & ntype2, idisp, COMM_S_NE_X_NE, xy_fmt, binary_fmt)
    end if

  end subroutine output_field_3d

end module diagnos_fields
