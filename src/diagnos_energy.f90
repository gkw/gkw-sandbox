!------------------------------------------------------------------------------
! SVN: $Id$
!> diagnos_energy.f90
!> Returns the potential energy and the time variation of kinetic energy for all 
!> the velocity contributions: gradB drifts, parallel velocity...
!> in the file gamma_contributions.dat
!> The growth rate can be computed from these outputs. For more informations:
!> A. Bottino, PhD thesis, EPFL 2938 (2004)
!> (Work in progress: electrostatic, linear, spectral)
!------------------------------------------------------------------------------
module diagnos_energy

  implicit none

  private

  logical, save, public :: lcalc_energy

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, final_output
  public :: output

  !> logical unit numbers for output files (in legacy format/order/grouping)
  integer, save :: i_energy_cons = -1

  !> number of slots in energy write array
  integer, save :: nenergy
  
  !> the variation of kinetic energy per mode : (nmod,nx,number_of_species) for the 
  !> different contributions
  real, save, allocatable, dimension(:,:,:) :: kin_par_ps,kin_gradb_ps
  real, save, allocatable, dimension(:,:,:) :: kin_betaprime_ps,kin_coriolis_ps
  real, save, allocatable, dimension(:,:,:) :: kin_centrifug_ps,kin_centpot_ps

  real, save, allocatable, dimension(:,:,:) :: kin_par_tr,kin_gradb_tr
  real, save, allocatable, dimension(:,:,:) :: kin_betaprime_tr,kin_coriolis_tr
  real, save, allocatable, dimension(:,:,:) :: kin_centrifug_tr,kin_centpot_tr

  !> the potential energy per mode : pot_en(nmod,nx,number_of_species)
  real, save, allocatable, dimension(:,:,:) :: pot_en

  real, save, allocatable, dimension(:) :: kin_tot_par_ps
  real, save, allocatable, dimension(:) :: kin_tot_gradb_ps,kin_tot_betaprime_ps
  real, save, allocatable, dimension(:) :: kin_tot_coriolis_ps,kin_tot_centrifug_ps
  real, save, allocatable, dimension(:) :: kin_tot_centpot_ps

  real, save, allocatable, dimension(:) :: kin_tot_par_tr
  real, save, allocatable, dimension(:) :: kin_tot_gradb_tr,kin_tot_betaprime_tr
  real, save, allocatable, dimension(:) :: kin_tot_coriolis_tr,kin_tot_centrifug_tr
  real, save, allocatable, dimension(:) :: kin_tot_centpot_tr

  real, save, allocatable, dimension(:) :: pot_tot_en
  real, save, allocatable, dimension(:) :: en_cons
  real, save, allocatable, dimension(:,:,:) :: energybuf(:,:,:)

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()

   lcalc_energy = .false.

  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast
    
    call mpibcast(lcalc_energy,1)

  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    nenergy=6
  
    ! Passing trapped particles (*2) + Potential energy
    nenergy = nenergy*2+1
  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use grid, only : number_of_species, n_x_grid, nmod
    use io, only : open_real_lu, ascii_fmt
    use global, only : int2char_zeros
    use control, only : io_legacy
    use mpiinterface, only : root_processor
    use global, only : dotdat
    character(len=25), dimension(number_of_species*nenergy) :: labels
    integer :: is, ienergy


    if(root_processor) then
      ! open the file to write the growth rate contributions
      if(io_legacy) then

        do is = 1, number_of_species
          ienergy = 1
          if(lcalc_energy) then

           labels(ienergy+(is-1)*nenergy) = 'pot_en_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1

           labels(ienergy+(is-1)*nenergy) = 'vpar_ps_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1

           labels(ienergy+(is-1)*nenergy) = 'vpar_tr_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1

           labels(ienergy+(is-1)*nenergy) = 'gradb_ps_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
           labels(ienergy+(is-1)*nenergy) = 'gradb_tr_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
       
           labels(ienergy+(is-1)*nenergy) = 'betaprime_ps_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
           labels(ienergy+(is-1)*nenergy) = 'betaprime_tr_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
 
           labels(ienergy+(is-1)*nenergy) = 'coriolis_ps_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
           labels(ienergy+(is-1)*nenergy) = 'coriolis_tr_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
 
           labels(ienergy+(is-1)*nenergy) = 'centrifugal_ps_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
           labels(ienergy+(is-1)*nenergy) = 'centrifugal_tr_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
 
           labels(ienergy+(is-1)*nenergy) = 'centpot_ps_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
           labels(ienergy+(is-1)*nenergy) = 'centpot_tr_species'// &
              & trim(adjustl(int2char_zeros(is,2)))
           ienergy = ienergy + 1
 
          end if
        end do
        if(lcalc_energy) then
         call open_real_lu(dotdat('gamma_contributions',io_legacy), &
            & 'diagnostic/diagnos_energy', &
            & (/ number_of_species*nenergy /), &
            & ascii_fmt, i_energy_cons, labels)
        end if
      else
        if(lcalc_energy) then
          call open_real_lu('gamma_contributions', 'diagnostic/diagnos_energy', &
             & (/ number_of_species /), ascii_fmt, i_energy_cons)
        end if
      end if
    end if

  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use grid, only : nmod,nx,number_of_species,n_s_grid,n_x_grid
    use general, only : gkw_abort
    integer :: ierr

    ierr=0

    allocate(kin_par_ps(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_par_ps')
    allocate(kin_gradb_ps(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_gradb_ps')
    allocate(kin_betaprime_ps(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_betaprime_ps')
    allocate(kin_coriolis_ps(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_coriolis_ps')
    allocate(kin_centrifug_ps(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_centrifug_ps')
    allocate(kin_centpot_ps(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_centpot_ps')

    allocate(kin_par_tr(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_par_tr')
    allocate(kin_gradb_tr(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_gradb_tr')
    allocate(kin_betaprime_tr(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_betaprime_tr')
    allocate(kin_coriolis_tr(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_coriolis_tr')
    allocate(kin_centrifug_tr(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_centrifug_tr')
    allocate(kin_centpot_tr(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_centpot_tr')

    allocate(kin_tot_par_ps(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_par_ps')
    allocate(kin_tot_gradb_ps(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_gradb_ps')
    allocate(kin_tot_betaprime_ps(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_betaprime_ps')
    allocate(kin_tot_coriolis_ps(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_coriolis_ps')
    allocate(kin_tot_centrifug_ps(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_centrifug_ps')
    allocate(kin_tot_centpot_ps(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_centpot_ps')

    allocate(kin_tot_par_tr(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_par_tr')
    allocate(kin_tot_gradb_tr(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_gradb_tr')
    allocate(kin_tot_betaprime_tr(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_betaprime_tr')
    allocate(kin_tot_coriolis_tr(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_coriolis_tr')
    allocate(kin_tot_centrifug_tr(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_centrifug_tr')
    allocate(kin_tot_centpot_tr(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: kin_tot_centpot_tr')
    
    allocate(en_cons(nenergy*number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: en_cons')
    allocate(pot_en(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pot_en')
    allocate(pot_tot_en(number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: pot_tot_en')

    allocate(energybuf(nmod,n_x_grid,number_of_species),stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: energybuf') 

  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()

  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()

  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine final_output(number)
    use diagnos_generic, only : xy_estep
    integer, intent(in) :: number

    call output_regular()

  end subroutine final_output

  !--------------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------------
  subroutine calc_kin_en()
    use grid,           only : nx, ns, nmu, nvpar, nsp, n_x_grid 
    use grid,           only : nmod, number_of_species, gsp, gx
    use dist,           only : fdisi
    use dist,           only : iphi
    use geom,           only : ints, bn, efun, ffun
    use geom,           only : jfun, metric, hfun, bmax, dfun, ifun 
    use mode,           only : krho, kxrh, ixzero, iyzero
    use components,     only : vthrat, signz, mas, veta_prime
    use components,     only : tgrid
    use velocitygrid,   only : intmu, intvp, mugr, vpgr
    use constants,      only : ci1
    use matdat,         only : get_f_from_g 
    use fields,         only : get_averaged_phi
    use linear_terms,   only : drift
    use rotation,       only : vcor
    use rotation,       only : dcfphi_dpsi, dcfphi_ds
    use mpiinterface,   only : mpiallreduce_sum, root_processor
    use mpiinterface,   only : number_of_processors
    use io,             only : append_chunk, xy_fmt, ascii_fmt
    use diagnos_generic, only : dfieldds

    ! integers for the loop over all grid points 
    integer :: imod, ix, i, j, k, is 

    ! the gyro-averaged field
    complex :: phi_ga
    ! and their deviation to s
    complex, allocatable :: dphi_gads(:)

    complex :: fdis
    real    :: ED, mask
    real drift_x, drift_y


    ! The global species (ix) index is in isglb (ixg)
    integer isglb, ixg

    ! index for flux output array
    integer :: ienergy


    if(.not. lcalc_energy) return

    ! Initialisation to zero
    kin_par_ps = 0.; kin_gradb_ps = 0.; kin_betaprime_ps = 0. 
    kin_coriolis_ps = 0.; kin_centrifug_ps = 0.; pot_en = 0.
    kin_centpot_ps = 0.
    kin_par_tr = 0.; kin_gradb_tr = 0.; kin_betaprime_tr = 0. 
    kin_coriolis_tr = 0.; kin_centrifug_tr = 0.; kin_centpot_tr = 0.
    drift_x = 0.; drift_y = 0.
    allocate(dphi_gads(ns))

    !  Calculates the potential energy and all the contributions to 
    ! time variation of kinetic energy
    nmod1: do imod = 1, nmod 
      nx1:  do ix = 1, nx 
        nsp1:    do is = 1, nsp

          ! the actual (global) species index - blanks are left elsewhere
          isglb = gsp(is)
          ! the actual (global) x index - blanks are left elsewhere
          ixg = gx(ix)

          ! Integral over the velocity space 
          nmu3: do j = 1, nmu
            call dfieldds( iphi,imod,ix,j,is,dphi_gads)
     
             nvpar3: do k = 1, nvpar

              ns3: do i = 1, ns

                ! the gyro-averaged fields 
                phi_ga  = get_averaged_phi(imod,ix,i,j,is,fdisi) 

                ! fdis is the distribution without A_par contribution  
                fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)

                ! mask for trapped passing particles
                if (sqrt(2*mugr(j)*bn(ix,i)).gt.abs(vpgr(i,j,k,is)*sqrt(bn(ix,i) &
                  &  /(bmax-bn(ix,i))))) then
                  mask= 1.
                else
                  mask= 0.
                end if

                 ! in the implicit scheme fdis can be NaN for intvp = 0 
                 if (intvp(i,j,k,is).eq.0) fdis = 0.

                 ED = vpgr(i,j,k,is)**2 + bn(ix,i)*mugr(j)

                 ! Time variation of kinetic energy due to the parallel velocity of trapped particles
                 kin_par_tr(imod,ixg,isglb)=kin_par_tr(imod,ixg,isglb)+real(fdis*conjg(dphi_gads(i))) &
                 & *ffun(ix,i)*vthrat(is)*vpgr(i,j,k,is)*bn(ix,i)* &
                 & intvp(i,j,k,is)*intmu(j)*ints(i)*mask*signz(is)

                 ! Time variation of kinetic energy due to the parallel velocity of passing particles
                 kin_par_ps(imod,ixg,isglb)=kin_par_ps(imod,ixg,isglb)+real(fdis*conjg(dphi_gads(i))) &
                 & *ffun(ix,i)*vthrat(is)*vpgr(i,j,k,is)*bn(ix,i)* &
                 & intvp(i,j,k,is)*intmu(j)*ints(i)*(-mask+1)*signz(is)
    
                 drift_x = tgrid(is)*ED*dfun(ix,i,1)
                 drift_y = tgrid(is)*ED*dfun(ix,i,2)   
                 
                 ! Time variation of kinetic energy due to the gradB drift velocity of trapped particles
                 kin_gradb_tr(imod,ixg,isglb)=kin_gradb_tr(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*mask 

                 ! Time variation of kinetic energy due to the gradB drift velocity of passing particles
                 kin_gradb_ps(imod,ixg,isglb)=kin_gradb_ps(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*(-mask+1)

                 drift_x = tgrid(is)*vpgr(i,j,k,is)**2*veta_prime(ix)*efun(ix,i,1,1)/ bn(ix,i)**2
                 drift_y = tgrid(is)*vpgr(i,j,k,is)**2*veta_prime(ix)*efun(ix,i,1,2)/ bn(ix,i)**2 
                
                 ! Time variation of kinetic energy due to the Betaprime drift velocity of trapped particles
                 kin_betaprime_tr(imod,ixg,isglb)=kin_betaprime_tr(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*mask 

                 ! Time variation of kinetic energy due to the Betaprime drift velocity of passing particles
                 kin_betaprime_ps(imod,ixg,isglb)=kin_betaprime_ps(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*(-mask+1)

                 drift_x = 2.E0*mas(is)*vthrat(is)*vpgr(i,j,k,is)*vcor*hfun(ix,i,1)
                 drift_y = 2.E0*mas(is)*vthrat(is)*vpgr(i,j,k,is)*vcor*hfun(ix,i,2) 
                 
                 ! Time variation of kinetic energy due to the coriolis drift velocity of trapped particles
                 kin_coriolis_tr(imod,ixg,isglb)=kin_coriolis_tr(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*mask 

                 ! Time variation of kinetic energy due to the coriolis drift velocity of passing particles
                 kin_coriolis_ps(imod,ixg,isglb)=kin_coriolis_ps(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*(-mask+1)
                  
                 drift_x = vcor*vcor*mas(is)*ifun(ix,i,1)
                 drift_y = vcor*vcor*mas(is)*ifun(ix,i,2)  
                 
                 ! Time variation of kinetic energy due to the centrifugal drift velocity of trapped particles
                 kin_centrifug_tr(imod,ixg,isglb)=kin_centrifug_tr(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*mask 

                 ! Time variation of kinetic energy due to the coriolis drift velocity of passing particles
                 kin_centrifug_ps(imod,ixg,isglb)=kin_centrifug_ps(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*(-mask+1)

                 drift_x = efun(ix,i,1,1)*dcfphi_dpsi(i)+efun(ix,i,3,1)*dcfphi_ds(i)
                 drift_y = efun(ix,i,1,2)*dcfphi_dpsi(i)+efun(ix,i,3,2)*dcfphi_ds(i)
                 
                 ! Time variation of kinetic energy due to the centrifugal potential drift of trapped particles
                 kin_centpot_tr(imod,ixg,isglb)=kin_centpot_tr(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*mask*signz(is) 

                 ! Time variation of kinetic energy due to the centrifugal potential drift of passing particles
                 kin_centpot_ps(imod,ixg,isglb)=kin_centpot_ps(imod,ixg,isglb)+real(fdis*conjg(phi_ga*ci1))* &
                 & (drift_x*kxrh(ix)+drift_y*krho(imod))*bn(ix,i)*intvp(i,j,k,is) &
                 & *intmu(j)*ints(i)*(-mask+1)*signz(is)

                 pot_en(imod,ixg,isglb)=pot_en(imod,ixg,isglb)+intmu(j)* & 
                 & bn(ix,i)*intvp(i,j,k,is) &
                 & *ints(i)*real(conjg(phi_ga)*fdis)*signz(is)

              end do ns3
            end do nvpar3
          end do nmu3
        end do nsp1
      end do nx1
    end do nmod1

    if(allocated(dphi_gads)) deallocate(dphi_gads)

    ! only when run on more than one processor (saves two copies)
    if (number_of_processors.gt.1) then 

      ! The 3D arrays ([k]y, k[x], species)
      ! are MPI reduced over s and velocity and "gathered" over x and species
      call energy_parallel_sum_gather(kin_par_ps)
      call energy_parallel_sum_gather(kin_par_tr)
      call energy_parallel_sum_gather(kin_gradb_ps)
      call energy_parallel_sum_gather(kin_gradb_tr)
      call energy_parallel_sum_gather(kin_betaprime_ps)
      call energy_parallel_sum_gather(kin_betaprime_tr)
      call energy_parallel_sum_gather(kin_coriolis_ps)
      call energy_parallel_sum_gather(kin_coriolis_tr)
      call energy_parallel_sum_gather(kin_centrifug_ps)
      call energy_parallel_sum_gather(kin_centrifug_tr)
      call energy_parallel_sum_gather(kin_centpot_ps)
      call energy_parallel_sum_gather(kin_centpot_tr)
      call energy_parallel_sum_gather(pot_en)

    end if

    species: do is = 1, number_of_species 
      kin_tot_par_ps(is) = 0.; kin_tot_par_tr(is)=0.;
      kin_tot_gradb_ps(is)=0.; kin_tot_gradb_tr(is)=0.
      kin_tot_betaprime_ps(is)=0.; kin_tot_betaprime_tr(is)=0.
      kin_tot_coriolis_ps(is)=0.; kin_tot_coriolis_tr(is)=0.;pot_tot_en(is) = 0.
      kin_tot_centrifug_ps(is)=0.; kin_tot_centrifug_tr(is)=0.; 
      kin_tot_centpot_ps(is)=0.; kin_tot_centpot_tr(is)=0.; 

      do imod = 1, nmod

        do ix = 1, n_x_grid

          kin_tot_par_ps(is) = kin_tot_par_ps(is) + kin_par_ps(imod,ix,is)
          kin_tot_par_tr(is) = kin_tot_par_tr(is) + kin_par_tr(imod,ix,is)
          kin_tot_gradb_ps(is) = kin_tot_gradb_ps(is) + kin_gradb_ps(imod,ix,is)
          kin_tot_gradb_tr(is) = kin_tot_gradb_tr(is) + kin_gradb_tr(imod,ix,is)
          kin_tot_betaprime_ps(is) = kin_tot_betaprime_ps(is) + kin_betaprime_ps(imod,ix,is)
          kin_tot_betaprime_tr(is) = kin_tot_betaprime_tr(is) + kin_betaprime_tr(imod,ix,is)
          kin_tot_coriolis_ps(is) = kin_tot_coriolis_ps(is) + kin_coriolis_ps(imod,ix,is)
          kin_tot_coriolis_tr(is) = kin_tot_coriolis_tr(is) + kin_coriolis_tr(imod,ix,is)
          kin_tot_centrifug_ps(is) = kin_tot_centrifug_ps(is) + kin_centrifug_ps(imod,ix,is)
          kin_tot_centrifug_tr(is) = kin_tot_centrifug_tr(is) + kin_centrifug_tr(imod,ix,is)
          kin_tot_centpot_ps(is) = kin_tot_centpot_ps(is) + kin_centpot_ps(imod,ix,is)
          kin_tot_centpot_tr(is) = kin_tot_centpot_tr(is) + kin_centpot_tr(imod,ix,is)
          pot_tot_en(is) = pot_tot_en(is) + pot_en(imod,ix,is)

        end do !n_x_grid

      end do !nmod

      ! order the required terms for output
      ienergy = 1
      if(lcalc_energy) then
       en_cons(ienergy + (is-1)*nenergy) = pot_tot_en(is)
       ienergy = ienergy + 1

       en_cons(ienergy + (is-1)*nenergy) = kin_tot_par_ps(is)
       ienergy = ienergy + 1

       en_cons(ienergy + (is-1)*nenergy) = kin_tot_par_tr(is)
       ienergy = ienergy + 1

       en_cons(ienergy + (is-1)*nenergy) = kin_tot_gradb_ps(is)
       ienergy = ienergy + 1
       en_cons(ienergy + (is-1)*nenergy) = kin_tot_gradb_tr(is)
       ienergy = ienergy + 1

       en_cons(ienergy + (is-1)*nenergy) = kin_tot_betaprime_ps(is)
       ienergy = ienergy + 1
       en_cons(ienergy + (is-1)*nenergy) = kin_tot_betaprime_tr(is)
       ienergy = ienergy + 1

       en_cons(ienergy + (is-1)*nenergy) = kin_tot_coriolis_ps(is)
       ienergy = ienergy + 1
       en_cons(ienergy + (is-1)*nenergy) = kin_tot_coriolis_tr(is)
       ienergy = ienergy + 1

       en_cons(ienergy + (is-1)*nenergy) = kin_tot_centrifug_ps(is)
       ienergy = ienergy + 1
       en_cons(ienergy + (is-1)*nenergy) = kin_tot_centrifug_tr(is)
       ienergy = ienergy + 1

       en_cons(ienergy + (is-1)*nenergy) = kin_tot_centpot_ps(is)
       ienergy = ienergy + 1
       en_cons(ienergy + (is-1)*nenergy) = kin_tot_centpot_tr(is)
       ienergy = ienergy + 1

      end if

    end do species

    contains
    !--------------------------------------------------------------------------
    !> This internal routine completes the MPI reduction of arrays over 
    !> s+velocity space and "gathers" over the species/radial grid (if required)
    !> Called by ALL procs with energy_array(nmod,n_x_grid,number_of_species)
    !--------------------------------------------------------------------------
    subroutine energy_parallel_sum_gather(energy_array)

      real, intent(inout) :: energy_array(nmod,n_x_grid,number_of_species)

      ! reduce zero-padded arrays (in species and x) over COMM_WORLD
      call mpiallreduce_sum(energy_array, energybuf, nmod, n_x_grid, number_of_species)
      energy_array(:,:,:) = energybuf(:,:,:)

    end subroutine energy_parallel_sum_gather

  end subroutine calc_kin_en
  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output_regular()
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use control, only : io_legacy

    if (lcalc_energy) then
     call calc_kin_en()

       if(io_legacy) then
         call append_chunk(i_energy_cons, en_cons, xy_fmt, ascii_fmt)
       end if
    end if

  end subroutine output_regular

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output(file_count)
    use diagnos_generic, only : lwrite_output1
    integer, intent(in) :: file_count
    if(lwrite_output1) then
      call output_regular()
    end if
  end subroutine output

end module diagnos_energy
