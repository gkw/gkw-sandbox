!------------------------------------------------------------------------------
! SVN:$Id$
!> This module coordinates all the diagnostics, the sub-modules diagnos_*
!> contain individual diagnostics, grouped mainly by related physical quantities.
!> All calculations that do not affect the solution, and data gathering required  
!> for output should appear in the diagnos_* modules.
!> Data output is handled by the io_* modules called from diagnos_* modules
!-------------------------------------------------------------------------------
module diagnostic
 
  implicit none

  private

  public :: diagnostic_read_nml, diagnostic_write_nml, diagnostic_read_last_data
  public :: diagnostic_bcast_nml, diagnostic_check_params
  public :: diagnostic_allocate, diagnostic_initial_output
  public :: diagnostic_final_output, diagnostic_naverage
  public :: diagnostic_init,diagnostic_finalize, diagnostic_pre_naverage
  public :: check_for_old_data
  
  interface diagnostic_write_nml
    module procedure diagnostic_read_nml
  end interface

  !> flush the fluxes and time etc. every nflush_ts timesteps (default is 10).
  integer, save :: nflush_ts = 0

  !> output switches which are only used in this general diagnostic module
  logical, save, public :: screen_output,lfinal_output
  
  !> file count and suffix
  integer, save :: file_count = 0

  !--------- DEPRECATED SWITCHES: -----------
  logical, save, public :: lcalc_freq
  !> output the xy outputs in binary to a single file for all times
  !> FIXME this is obsolete and should be removed
  logical, save, public :: xy_bin

contains 

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> read or write the diagnostic nml if ldiagnostic_namelist is set in control
!----------------------------------------------------------------------------
subroutine diagnostic_read_nml(ilun,io_stat,lwrite)
  use io, only : write_run_parameter
  use diagnos_generic, only : set_defaults_generic => set_default_nml_values, &
     & lwrite_output1, lphi_diagnostics, xy_estep, &
     & lisl_follow, zonal_scale_3d, &
     & spc3d, phi3d, den3d, apa3d, apc3d, ene3d, bpa3d, bpc3d, &
     & xy_fluxes, xy_fluxes_bi, xy_fluxes_p, xy_fluxes_v, xy_fluxes_fsa, &
     & xy_fluxes_em, xy_fluxes_k, &
     & xy_fluxes_bpar, xy_slice_ipar, &
     & xy_dens, xy_temp, xy_current, xy_current2, &
     & xy_spec, &
     & lradial_profile 
  use diagnos_fluxes, only : set_defaults_fluxes => set_default_nml_values, &
     & lcalc_fluxes, &
     & lpflux,leflux,lvflux, &
     & lfluxes_spectra,lfluxes_em_spectra
  use diagnos_energy, only : set_defaults_energy => set_default_nml_values, &
     & lcalc_energy
  use diagnos_fluxes_vspace, only : &
     & set_defaults_fluxes_vspace => set_default_nml_values, &
     & lfluxes_detail, lfluxes_vspace, &
     & lfluxes_vspace_phi,lfluxes_vspace_em,lfluxes_vspace_bpar
  use diagnos_energetics, only : &
     & set_defaults_energetics => set_default_nml_values, &
     & lcalc_energetics, lcalc_tot_energy
  use diagnos_corr, only : &
     & set_defaults_corr => set_default_nml_values, &
     & lcalc_corr, imod_corr
  use diagnos_fields, only : set_defaults_fields => set_default_nml_values, &
     & lparallel_phi, lparallel_apar, &
     & xy_phi,xy_apar, xy_bpar, xy_vort, &
     & xs_phi, nmodepoints, imod_field
  use diagnos_stresses, only : set_defaults_stresses => set_default_nml_values, &
        & lcalc_stresses
  use diagnos_eng, only : set_defaults_eng => set_default_nml_values, &
     & lmode_energy
  use diagnos_growth_freq, only : &
     & set_defaults_growth_freq => set_default_nml_values, &
     & lamplitudes,lgrowth_rates,lfrequencies
  use diagnos_mode_struct, only : &
     & set_defaults_mode_struct => set_default_nml_values, &
     & lparallel_output, parallel_output_timestamps, &
     & lrotate_parallel
  use diagnos_velspace, only : set_defaults_velspace => set_default_nml_values, &
     & lvelspace_output, psi_velspace,zeta_velspace, npointsvel,lfinvel
  use diagnos_rad, only : set_defaults_rad => set_default_nml_values, &
     & lrad_moment, lrad_entropy, lradial_entropy, lrad_field,  lrad_tint
  use diagnos_f, only : set_defaults_f => set_default_nml_values
  use diagnos_grid, only : set_defaults_grid => set_default_nml_values
  use diagnos_moments, only : set_defaults_moments => set_default_nml_values
  use diagnos_zfshear, only : set_defaults_zfshear => set_default_nml_values, &
     & lcalc_zfshear
  use diagnos_nonlin_transfer, only : &
     & set_defaults_nonlin_transfer => set_default_nml_values, &
     & lnonlin_transfer, nonlin_transfer_interval
  use diagnos_freq_wavelet, only : set_defaults_freq_wavelet => &
     & set_default_nml_values, lfreq_wavelet
  use diagnos_spat_wavelet, only : set_defaults_spat_wavelet => &
     & set_default_nml_values, lspat_wavelet
  use io, only : lmpi_broken_io
  ! deprecated:
  use diagnos_generic, only : lphi_diagnostics

  integer, intent(in) :: ilun
  integer, intent(out) :: io_stat
  logical, optional, intent(in) :: lwrite

  namelist /diagnostic/ &
     ! from module diagnostic:
     & nflush_ts, screen_output, lfinal_output, &
     ! from module diagnos_generic:
     & lwrite_output1, &
     & lphi_diagnostics, &
     & xy_estep, &
     & lisl_follow, zonal_scale_3d, &
     & spc3d, phi3d, den3d, apa3d, apc3d, ene3d, bpa3d, bpc3d, &
     & xy_fluxes, xy_fluxes_bi, xy_fluxes_p, xy_fluxes_v, xy_fluxes_fsa, &
     & xy_fluxes_em, xy_fluxes_k, &
     & xy_fluxes_bpar, xy_slice_ipar, &
     & xy_dens, xy_temp, xy_current, xy_current2, &
     & xy_spec, &
     & lradial_profile, &
     ! from module diagnos_fluxes:
     & lcalc_fluxes, &
     & lpflux,leflux,lvflux, &
     & lfluxes_spectra,lfluxes_em_spectra, &
     ! from module diagnos_energy:
     & lcalc_energy, &
     ! from module diagnos_fluxes_vspace:
     & lfluxes_detail, lfluxes_vspace, &
     & lfluxes_vspace_phi,lfluxes_vspace_em,lfluxes_vspace_bpar, &
     ! from module diagnos_energetics:
     & lcalc_energetics, &
     ! from module diagnos_corr:
     & lcalc_corr, imod_corr , & 
     ! from module diagnos_fields:
     & lparallel_phi, lparallel_apar, &
     & xy_phi,xy_apar, xy_bpar, xy_vort, &
     & xs_phi, nmodepoints, imod_field, &
     ! from module diagnos_moments:
     ! from module diagnos_f:
     ! from module diagnos_grid:
     ! from module diagnos_stresses:
     & lcalc_stresses, &
     ! from module diagnos_eng:
     & lmode_energy, & 
     ! from module diagnos_growth_freq:
     & lamplitudes,lgrowth_rates,lfrequencies, &
     ! from module diagnos_mode_struct:
     & lparallel_output, parallel_output_timestamps, &
     & lrotate_parallel, &
     ! from module diagnos_velspace:
     & lvelspace_output, psi_velspace,zeta_velspace, npointsvel,lfinvel, &
     ! from module diagnos_rad:
     & lrad_moment, lrad_entropy, lradial_entropy, lrad_field,  lrad_tint, &
     ! from module diagnos_zfshear:
     & lcalc_zfshear, &
     ! from module diagnos_nonlin_transfer:
     & lnonlin_transfer, nonlin_transfer_interval, &
     ! from module diagnos_freq_wavelet:
     & lfreq_wavelet, &
     ! from module diagnos_freq_wavelet:
     & lspat_wavelet, &
     ! from module io:
     & lmpi_broken_io, &
     ! obsolete or deprecated:
     & xy_bin, &
     & lcalc_freq, & 
     & lcalc_tot_energy

  if (present(lwrite)) then
    if (.not. lwrite) then
      ! set any defaults to on/true/non-zero etc (all false at declaration)

      lfinal_output = .true.
      screen_output = .true.
      lmpi_broken_io = .true.
      nflush_ts = 10

      call set_defaults_generic()
      call set_defaults_energetics()
      call set_defaults_corr()
      call set_defaults_growth_freq()
      call set_defaults_eng()
      call set_defaults_f()
      call set_defaults_fluxes_vspace()
      call set_defaults_grid()
      call set_defaults_mode_struct()
      call set_defaults_rad()
      call set_defaults_stresses()
      call set_defaults_fields()
      call set_defaults_moments()
      call set_defaults_fluxes()
      call set_defaults_velspace()
      call set_defaults_zfshear()
      call set_defaults_nonlin_transfer()
      call set_defaults_freq_wavelet()
      call set_defaults_spat_wavelet()

      ! set default values of deprecated variables and switches here
      !...
  
      ! read nml
      io_stat = 0
      read(ilun,NML=diagnostic,IOSTAT=io_stat)
    else
      ! do nothing
    end if
  else
    write(ilun,NML=diagnostic)

    ! from module diagnostic:
    call write_run_parameter('diagnostic', 'nflush_ts', nflush_ts)
    call write_run_parameter('diagnostic', 'screen_output', screen_output)
    call write_run_parameter('diagnostic', 'lfinal_output', lfinal_output) 
    ! from module diagnos_generic:
    call write_run_parameter('diagnostic', 'lwrite_output1', lwrite_output1) 
    call write_run_parameter('diagnostic', 'lphi_diagnostics', lphi_diagnostics) 
    call write_run_parameter('diagnostic', 'xy_estep', xy_estep) 
    call write_run_parameter('diagnostic', 'lisl_follow', lisl_follow)
    call write_run_parameter('diagnostic', 'zonal_scale_3d', zonal_scale_3d) 
    call write_run_parameter('diagnostic', 'spc3d', spc3d)
    call write_run_parameter('diagnostic', 'phi3d', phi3d)
    call write_run_parameter('diagnostic', 'den3d', den3d)
    call write_run_parameter('diagnostic', 'apa3d', apa3d)
    call write_run_parameter('diagnostic', 'apc3d', apc3d)
    call write_run_parameter('diagnostic', 'ene3d', ene3d)
    call write_run_parameter('diagnostic', 'bpa3d', bpa3d)
    call write_run_parameter('diagnostic', 'bpc3d', bpc3d) 
    call write_run_parameter('diagnostic', 'xy_fluxes', xy_fluxes)
    call write_run_parameter('diagnostic', 'xy_fluxes_bi', xy_fluxes_bi)
    call write_run_parameter('diagnostic', 'xy_fluxes_p', xy_fluxes_p)
    call write_run_parameter('diagnostic', 'xy_fluxes_v', xy_fluxes_v)
    call write_run_parameter('diagnostic', 'xy_fluxes_fsa', xy_fluxes_fsa) 
    call write_run_parameter('diagnostic', 'xy_fluxes_em', xy_fluxes_em)
    call write_run_parameter('diagnostic', 'xy_fluxes_k', xy_fluxes_k) 
    call write_run_parameter('diagnostic', 'xy_fluxes_bpar', xy_fluxes_bpar)
    call write_run_parameter('diagnostic', 'xy_slice_ipar', xy_slice_ipar) 
    call write_run_parameter('diagnostic', 'xy_dens', xy_dens)
    call write_run_parameter('diagnostic', 'xy_temp', xy_temp)
    call write_run_parameter('diagnostic', 'xy_current', xy_current)
    call write_run_parameter('diagnostic', 'xy_current2', xy_current2) 
    call write_run_parameter('diagnostic', 'xy_spec', xy_spec) 
    call write_run_parameter('diagnostic', 'lradial_profile', lradial_profile)
    ! from module diagnos_fluxes:
    call write_run_parameter('diagnostic', 'lcalc_fluxes', lcalc_fluxes) 
    call write_run_parameter('diagnostic', 'lpflux', lpflux)
    call write_run_parameter('diagnostic', 'leflux', leflux)
    call write_run_parameter('diagnostic', 'lvflux', lvflux) 
    call write_run_parameter('diagnostic', 'lfluxes_spectra', lfluxes_spectra)
    call write_run_parameter('diagnostic', 'lfluxes_em_spectra', &
       & lfluxes_em_spectra) 
   ! from module diagnos_energy:
    call write_run_parameter('diagnostic', 'lcalc_energy', lcalc_energy) 
    ! from module diagnos_fluxes_vspace:
    call write_run_parameter('diagnostic', 'lfluxes_detail', lfluxes_detail)
    call write_run_parameter('diagnostic', 'lfluxes_vspace', lfluxes_vspace) 
    call write_run_parameter('diagnostic', 'lfluxes_vspace_phi', &
       & lfluxes_vspace_phi)
    call write_run_parameter('diagnostic', 'lfluxes_vspace_em', &
       & lfluxes_vspace_em)
    call write_run_parameter('diagnostic', 'lfluxes_vspace_bpar', &
       & lfluxes_vspace_bpar) 
    ! from module diagnos_energetics:
    call write_run_parameter('diagnostic', 'lcalc_energetics', &
       & lcalc_energetics)
    call write_run_parameter('diagnostic', 'lcalc_tot_energy', &
       & lcalc_tot_energy) 
    ! from module diagnos_corr:
    call write_run_parameter('diagnostic', 'lcalc_corr', &
       & lcalc_corr) 
    call write_run_parameter('diagnostic', 'imod_corr', &
       & imod_corr)   
    ! from module diagnos_fields:
    call write_run_parameter('diagnostic', 'lparallel_phi', lparallel_phi)
    call write_run_parameter('diagnostic', 'lparallel_apar', lparallel_apar) 
    call write_run_parameter('diagnostic', 'xy_phi', xy_phi)
    call write_run_parameter('diagnostic', 'xy_apar', xy_apar)
    call write_run_parameter('diagnostic', 'xy_bpar', xy_bpar)
    call write_run_parameter('diagnostic', 'xy_vort', xy_vort) 
    call write_run_parameter('diagnostic', 'xs_phi', xs_phi)
    call write_run_parameter('diagnostic', 'nmodepoints', nmodepoints)
    call write_run_parameter('diagnostic', 'imod_field', imod_field) 
    ! from module diagnos_moments:
    ! from module diagnos_f:
    ! from module diagnos_grid:
    ! from module diagnos_stresses:
    call write_run_parameter('diagnostic', 'lcalc_stresses', lcalc_stresses) 
    ! from module diagnos_eng:
    call write_run_parameter('diagnostic', 'lmode_energy', lmode_energy) 
    ! from module diagnos_growth_freq:
    call write_run_parameter('diagnostic', 'lamplitudes', lamplitudes)
    call write_run_parameter('diagnostic', 'lgrowth_rates', lgrowth_rates)
    call write_run_parameter('diagnostic', 'lfrequencies', lfrequencies) 
    ! from module diagnos_mode_struct:
    call write_run_parameter('diagnostic', 'lparallel_output', &
       & lparallel_output)
    call write_run_parameter('diagnostic', 'parallel_output_timestamps', &
       & parallel_output_timestamps) 
    call write_run_parameter('diagnostic', 'lrotate_parallel', &
       & lrotate_parallel) 
    ! from module diagnos_velspace:
    call write_run_parameter('diagnostic', 'lvelspace_output', &
       & lvelspace_output)
    call write_run_parameter('diagnostic', 'psi_velspace', psi_velspace)
    call write_run_parameter('diagnostic', 'zeta_velspace', zeta_velspace)
    call write_run_parameter('diagnostic', 'npointsvel', npointsvel)
    call write_run_parameter('diagnostic', 'lfinvel', lfinvel) 
    ! from module diagnos_rad:
    call write_run_parameter('diagnostic', 'lrad_moment', lrad_moment)
    call write_run_parameter('diagnostic', 'lrad_entropy', lrad_entropy)
    call write_run_parameter('diagnostic', 'lradial_entropy', lradial_entropy)
    call write_run_parameter('diagnostic', 'lrad_field', lrad_field)
    call write_run_parameter('diagnostic', 'lrad_tint', lrad_tint) 
    ! from module diagnos_zfshear:
    call write_run_parameter('diagnostic', 'lcalc_zfshear', lcalc_zfshear) 
    ! from module diagnos_nonlin_transfer:
    call write_run_parameter('diagnostic', 'lnonlin_transfer', &
       & lnonlin_transfer)
    call write_run_parameter('diagnostic', 'nonlin_transfer_interval', &
       & nonlin_transfer_interval)
    ! from module diagnos_freq_wavelet:
    call write_run_parameter('diagnostic', 'lfreq_wavelet', &
       & lfreq_wavelet)
    ! from module diagnos_spat_wavelet:
    call write_run_parameter('diagnostic', 'lspat_wavelet', &
       & lspat_wavelet)
    ! from module io:
    call write_run_parameter('diagnostic', 'lmpi_broken_io', lmpi_broken_io) 
    ! obsolete or deprecated:
    ! ..(do not output, cause only confusion)

  end if

end subroutine diagnostic_read_nml

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> Broadcast the diagnostic namelist: every item should be communicated to
!> all processes.
!----------------------------------------------------------------------------
subroutine diagnostic_bcast_nml

  use mpiinterface, only : mpibcast
  use diagnos_generic, only : bcast_generic => bcast
  use diagnos_grid, only : bcast_grid => bcast
  use diagnos_growth_freq, only : bcast_growth_freq => bcast
  use diagnos_f, only : bcast_f => bcast
  use diagnos_moments, only : bcast_moments => bcast
  use diagnos_mode_struct, only : bcast_mode_struct => bcast
  use diagnos_fluxes, only : bcast_fluxes => bcast
  use diagnos_energy, only : bcast_energy => bcast
  use diagnos_fluxes_vspace, only : bcast_fluxes_vspace => bcast
  use diagnos_fields, only : bcast_fields => bcast
  use diagnos_velspace, only : bcast_velspace => bcast
  use diagnos_energetics, only : bcast_energetics => bcast
  use diagnos_corr, only : bcast_corr => bcast
  use diagnos_nonlin_transfer, only : bcast_nonlin_transfer => bcast
  use diagnos_eng, only : bcast_eng => bcast
  use diagnos_rad, only : bcast_rad => bcast
  use diagnos_stresses, only : bcast_stresses => bcast
  use diagnos_zfshear, only : bcast_zfshear => bcast
  use diagnos_freq_wavelet, only : bcast_freq_wavelet => bcast
  use diagnos_spat_wavelet, only : bcast_spat_wavelet => bcast

  use io, only : lmpi_broken_io

  ! broadcast items of the namelist which are defined in this module here:
  call mpibcast(nflush_ts,1)
  call mpibcast(lfinal_output,1)
  call mpibcast(screen_output,1)
  
  ! (historic) items of the diagnostic namelist which are defined in
  ! other modules but not broadcasted by those:
  call mpibcast(lmpi_broken_io,1)

  call bcast_generic

  call bcast_energetics
  call bcast_corr
  call bcast_nonlin_transfer
  call bcast_eng
  call bcast_f
  call bcast_fields
  call bcast_fluxes
  call bcast_energy
  call bcast_fluxes_vspace
  call bcast_grid
  call bcast_growth_freq
  call bcast_mode_struct
  call bcast_moments
  call bcast_rad
  call bcast_stresses
  call bcast_velspace
  call bcast_zfshear
  call bcast_freq_wavelet
  call bcast_spat_wavelet

end subroutine diagnostic_bcast_nml


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> Check the diagnostic parameters and calculate things needed before
!> allocation of memory will be done.
!>
!----------------------------------------------------------------------------
subroutine diagnostic_check_params

  use diagnos_energetics, only : check_energetics => check
  use diagnos_corr, only : check_corr => check
  use diagnos_eng, only : check_eng => check
  use diagnos_f, only : check_f => check
  use diagnos_fields, only : check_fields => check
  use diagnos_fluxes, only : check_fluxes => check
  use diagnos_energy, only : check_energy => check
  use diagnos_fluxes_vspace, only : check_fluxes_vspace => check
  use diagnos_generic, only : check_generic => check
  use diagnos_grid, only : check_grid => check
  use diagnos_growth_freq, only : check_growth_freq => check
  use diagnos_mode_struct, only : check_mode_struct => check
  use diagnos_moments, only : check_moments => check
  use diagnos_nonlin_transfer, only : check_nonlin_transfer => check
  use diagnos_rad, only : check_rad => check
  use diagnos_stresses, only : check_stresses => check
  use diagnos_velspace, only : check_velspace => check
  use diagnos_zfshear, only : check_zfshear => check
  use diagnos_freq_wavelet, only : check_freq_wavelet => check
  use diagnos_spat_wavelet, only : check_spat_wavelet => check

  call check_energetics()
  call check_corr()
  call check_eng()
  call check_f()
  call check_fields()
  call check_fluxes()
  call check_energy()
  call check_fluxes_vspace()
  call check_generic()
  call check_grid()
  call check_growth_freq()
  call check_mode_struct()
  call check_moments()
  call check_nonlin_transfer()
  call check_rad()
  call check_stresses()
  call check_velspace()
  call check_zfshear()
  call check_freq_wavelet()
  call check_spat_wavelet()
  
end subroutine diagnostic_check_params


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> Allocates the arrays of the diagnostic module that are used at runtime;
!> other diagnostics at the end of the run can manage their own memory after
!> most of the runtime memory has been deallocated.
!> Must be called after nonlinear allocate, and kgrid, and is therefore
!> called later than allocate_everything routine.
!----------------------------------------------------------------------------
subroutine diagnostic_allocate
  use diagnos_energetics, only : allocate_energetics => allocate_mem
  use diagnos_corr, only : allocate_corr => allocate_mem
  use diagnos_eng, only : allocate_eng => allocate_mem
  use diagnos_f, only : allocate_f => allocate_mem
  use diagnos_fields, only : allocate_fields => allocate_mem
  use diagnos_fluxes, only : allocate_fluxes => allocate_mem
  use diagnos_energy, only : allocate_energy => allocate_mem
  use diagnos_fluxes_vspace, only : allocate_fluxes_vspace => allocate_mem
  use diagnos_generic, only : allocate_generic => allocate_mem
  use diagnos_grid, only : allocate_grid => allocate_mem
  use diagnos_growth_freq, only : allocate_growth_freq => allocate_mem
  use diagnos_mode_struct, only : allocate_mode_struct => allocate_mem
  use diagnos_moments, only : allocate_moments => allocate_mem
  use diagnos_rad, only : allocate_rad => allocate_mem
  use diagnos_stresses, only : allocate_stresses => allocate_mem
  use diagnos_velspace, only : allocate_velspace => allocate_mem
  use diagnos_zfshear, only : allocate_zfshear => allocate_mem
  use diagnos_nonlin_transfer, only : allocate_nonlin_transfer => allocate_mem
  use diagnos_freq_wavelet, only : allocate_freq_wavelet => allocate_mem
  use diagnos_spat_wavelet, only : allocate_spat_wavelet => allocate_mem

  call allocate_generic
  
  call allocate_energetics
  call allocate_corr
  call allocate_eng
  call allocate_f
  call allocate_fields
  call allocate_fluxes
  call allocate_energy
  call allocate_fluxes_vspace
  call allocate_grid
  call allocate_growth_freq
  call allocate_mode_struct
  call allocate_moments
  call allocate_rad
  call allocate_stresses
  call allocate_velspace
  call allocate_zfshear
  call allocate_nonlin_transfer
  call allocate_freq_wavelet
  call allocate_spat_wavelet

end subroutine diagnostic_allocate

!----------------------------------------------------------------------------
!> output routine called every NAVERAGE timsteps
!----------------------------------------------------------------------------
subroutine diagnostic_naverage()

  use control, only : icomplete
  use non_linear_terms, only : entropy_radial
  use diagnos_grid, only : output_grid => output
  use diagnos_growth_freq, only : output_growth_freq => output
  use diagnos_energetics, only : output_energetics => output
  use diagnos_corr, only : output_corr => output
  use diagnos_nonlin_transfer, only : output_nonlin_transfer => output
  use diagnos_fluxes, only : output_fluxes => output
  use diagnos_energy, only : output_energy => output
  use diagnos_fluxes_vspace, only : output_fluxes_vspace => output
  use diagnos_eng, only : output_eng => output
  use diagnos_rad, only : output_rad => output
  use diagnos_stresses, only : output_stresses => output
  use diagnos_velspace, only : output_velspace => output
  use diagnos_fields, only : output_fields => output
  use diagnos_moments, only : output_moments => output
  use diagnos_f, only : output_f => output
  use diagnos_mode_struct, only : output_mode_struct => output
  use diagnos_zfshear, only : output_zfshear => output
  use diagnos_freq_wavelet, only : output_freq_wavelet => output
  use diagnos_spat_wavelet, only : output_spat_wavelet => output
  use io, only : flush_all_open_files
  use perform, only : perfon, perfoff, perfswitch, perf_measure

  integer, save :: icalls = 0
  !> counter, in order to flush files occasionally
  integer, save :: next_flush_ts = 0
  
  ! The following is used to achieve consistent numbering after restarts
  ! for diagnostics which produce a series of numbered files.
  icalls = icalls + 1
  file_count = icomplete + icalls

  ! ! FJC and SRG think we should not fill the fields here:
  ! ! diagnostic routines are more portable if they fill
  ! ! the fields as needed.
  ! call get_phi(fdisi(1:nsolc),phi)
  ! if (nlapar) call get_apar(fdisi(1:nsolc),apar)
  ! if (nlbpar) call get_bpar(fdisi(1:nsolc),bpar)
  if(perf_measure) then
    call perfon("diagnostics total",3)
    call perfon("diagnos_growth_freq",3)
  end if
  call output_growth_freq
  if(perf_measure) call perfswitch("diagnos_grid",3)
  call output_grid()
  if(perf_measure) call perfswitch("diagnos_fluxes",3)
  call output_fluxes(.false., file_count)
  if(perf_measure) call perfswitch("diagnos_energy",3)
  call output_energy(file_count)
  if(perf_measure) call perfswitch("diagnos_energetics",3)
  call output_energetics()
  if(perf_measure) call perfswitch("diagnos_corr",3)
  call output_corr()
  if(perf_measure) call perfswitch("diagnos_nonlin_transfer",3)
  call output_nonlin_transfer()
  if(perf_measure) call perfswitch("diagnos_stresses",3)
  call output_stresses()
  if(perf_measure) call perfswitch("diagnos_fields",3)
  call output_fields(file_count)
  if(perf_measure) call perfswitch("diagnos_moments",3)
  call output_moments(file_count)
  if(perf_measure) call perfswitch("diagnos_velspace",3)
  call output_velspace(file_count)
  if(perf_measure) call perfswitch("diagnos_mode_struct",3)
  call output_mode_struct
  if(perf_measure) call perfswitch("diagnos_f",3)
  call output_f(file_count)
  if(perf_measure) call perfswitch("diagnos_eng",3)
  call output_eng
  if(perf_measure) call perfswitch("diagnos_rad",3)
  call output_rad
  if(perf_measure) call perfswitch("diagnos_fluxes_vspace",3)
  call output_fluxes_vspace
  if(perf_measure) call perfswitch("diagnos_zfshear",3)
  call output_zfshear
  if(perf_measure) call perfswitch("diagnos_freq_wavelet",3)
  call output_freq_wavelet
  if(perf_measure) call perfswitch("diagnos_spat_wavelet",3)
  call output_spat_wavelet
  if(perf_measure) call perfswitch("diagnostic output rest", 3)
  
  ! all processes enter the screen_output routines, but at the lowest
  ! level only one will write to the terminal. This is done in this
  ! way to do it analogously to the logical-unit IO, and thus to
  ! simplify things.
  call write_screen_output

  ! Recompute fluxes suprema
  ! WARNING: do use any results computed in output_fluxes after this call
  call output_fluxes(.true., file_count)
  call output_energy(file_count)

  ! occasionally flush files.
  ! flush the main ts files if nflush_ts > 0
  next_flush_ts = next_flush_ts + 1 
  if (next_flush_ts == nflush_ts .and. nflush_ts > 0) then
    next_flush_ts = 0
    call flush_all_open_files
  end if
  if(perf_measure) then
    call perfoff(3)
    call perfoff(3)
  end if

end subroutine diagnostic_naverage


!----------------------------------------------------------------------------
!> Some quantities may be interesting to know right after the initialisation.
!> They can be output here.
!----------------------------------------------------------------------------
subroutine diagnostic_initial_output()
  use diagnos_grid, only : initial_output_grid => initial_output
  use diagnos_fields, only : initial_output_fields => initial_output
  use diagnos_mode_struct, only : &
     & initial_output_mode_struct => initial_output
  use diagnos_freq_wavelet, only : &
     & initial_output_freq_wavelet => initial_output
  use diagnos_rad, only : initial_output_rad => initial_output
  
  call initial_output_grid
  call initial_output_fields
  call initial_output_freq_wavelet
    
  ! make the rest dependend of the lfinal_output switch for the moment.
  if (.not. lfinal_output) return

  call initial_output_mode_struct
  call initial_output_rad

end subroutine diagnostic_initial_output


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> deal with final output after time integration has been completed 
!----------------------------------------------------------------------------
subroutine diagnostic_final_output(number)
  use diagnos_velspace, only : final_output_velspace => final_output
  use diagnos_fields, only : final_output_fields => final_output
  use diagnos_moments, only : final_output_moments => final_output
  use diagnos_fluxes, only : final_output_fluxes => final_output
  use diagnos_energy, only : final_output_energy => final_output
  use diagnos_fluxes_vspace, only : final_output_fluxes_vspace => final_output
  use diagnos_mode_struct, only : final_output_mode_struct => final_output
  use diagnos_f, only : final_output_f => final_output
  use diagnos_freq_wavelet, only : final_output_freq_wavelet => final_output
  
  !> The optional integer number can be given to produce numbered files.
  integer, optional, intent(in) :: number

  if (.not. lfinal_output) return

  if (present(number)) then
    call final_output_fluxes(number)
    call final_output_energy(number)
    call final_output_fields(number)
    call final_output_moments(number)
    call final_output_f(number)
  else
    call final_output_fluxes(file_count)
    call final_output_energy(file_count)
    call final_output_fields(file_count)
    call final_output_moments()
    call final_output_f()
  end if
  call final_output_velspace()
  call final_output_fluxes_vspace
  call final_output_mode_struct
  call final_output_freq_wavelet

end subroutine diagnostic_final_output


!----------------------------------------------------------------------------
!> perform any initialisation that needs to be done before the main time loop
!----------------------------------------------------------------------------
subroutine diagnostic_init
  use control,      only : testing
  use diagnos_generic, only : init_generic => init
  use diagnos_grid, only : init_grid => init
  use diagnos_growth_freq, only : init_growth_freq => init
  use diagnos_fluxes, only : init_fluxes => init
  use diagnos_energy, only : init_energy => init
  use diagnos_fluxes_vspace, only : init_fluxes_vspace => init
  use diagnos_fields, only : init_fields => init
  use diagnos_velspace, only : init_velspace => init
  use diagnos_energetics, only : init_energetics => init
  use diagnos_corr, only : init_corr => init
  use diagnos_rad, only : init_rad => init
  use diagnos_mode_struct, only : init_mode_struct => init
  use diagnos_moments, only : init_moments => init
  use diagnos_nonlin_transfer, only : init_nonlin_transfer => init
  use diagnos_eng, only : init_eng => init
  use diagnos_stresses, only : init_stresses => init
  use diagnos_zfshear, only : init_zfshear => init
  use diagnos_freq_wavelet, only : init_freq_wavelet => init
  use diagnos_spat_wavelet, only : init_spat_wavelet => init

  use diagnos_generic, only : lwrite_output1 
  
  ! no diagnostic files if testing MPI
  if (testing) then
    ! In order not to output any data to files, the io_format = 'none'
    ! is already set.

    ! additional flags spare us the computations of some diagnostics
    lwrite_output1 = .false.
    lfinal_output  = .false.
  end if

  call init_generic
  call init_growth_freq
  call init_grid
  call init_fluxes
  call init_energy
  call init_fluxes_vspace
  call init_fields
  call init_mode_struct
  call init_moments
  call init_nonlin_transfer
  call init_velspace
  call init_energetics
  call init_corr
  call init_eng
  call init_rad
  call init_stresses
  call init_zfshear
  call init_freq_wavelet
  call init_spat_wavelet

end subroutine diagnostic_init


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> Write output to the screen.
!----------------------------------------------------------------------------
subroutine write_screen_output
  use diagnos_growth_freq, only : screen_output_growth_freq => screen_output
  use diagnos_fluxes, only : screen_output_fluxes => screen_output
  if (screen_output) then
    call screen_output_growth_freq
    call screen_output_fluxes
  end if

end subroutine write_screen_output


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> Let diagnostics deallocate memory, close the logical units, etc.
!----------------------------------------------------------------------------
subroutine diagnostic_finalize
  use io, only : close_all_lus
  use diagnos_generic, only : finalize_generic => finalize
  use diagnos_eng, only : finalize_eng => finalize
  use diagnos_rad, only : finalize_rad => finalize
  use diagnos_energetics, only : finalize_energetics => finalize
   use diagnos_corr, only : finalize_corr => finalize
  use diagnos_growth_freq, only : finalize_growth_freq => finalize
  use diagnos_velspace, only : finalize_velspace => finalize
  use diagnos_zfshear, only : finalize_zfshear => finalize
  use diagnos_nonlin_transfer, only : finalize_nonlin_transfer => finalize
  use diagnos_freq_wavelet, only : finalize_freq_wavelet => finalize
  use diagnos_spat_wavelet, only : finalize_spat_wavelet => finalize

  call finalize_generic()
  call finalize_eng()
  call finalize_energetics()
  call finalize_corr()
  call finalize_growth_freq()
  call finalize_rad()
  call finalize_velspace()
  call finalize_zfshear()
  call finalize_nonlin_transfer()
  call finalize_freq_wavelet()
  call finalize_spat_wavelet()

  ! close all logical units, in case the diagnostics are lazy and do
  ! not do it themselves.
  call close_all_lus

end subroutine diagnostic_finalize


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> Let diagnostics read in the last chunk of data which they produced
!> in the preceeding run.
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
subroutine diagnostic_read_last_data
  use diagnos_growth_freq, only : read_last_data_growth_freq => read_last_data
  use diagnos_grid, only : read_last_data_grid => read_last_data

  ! call read_last_data_grid
  ! call read_last_data_growth_freq
  
end subroutine diagnostic_read_last_data

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> This routine checks for the existence of earlier data files.
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function check_for_old_data()
  use io, only : lu_exists, ascii_fmt
  logical :: check_for_old_data
  logical, dimension(9) :: checks

  checks(:) = .false.
  checks(1) = lu_exists('time.dat', 'diagnostic', ascii_fmt)
  checks(2) = lu_exists('fluxes.dat', 'diagnostic', ascii_fmt)
  checks(3) = lu_exists('kyspec', 'diagnostic', ascii_fmt)!legacy
  checks(3) = lu_exists('kyspec.dat', 'diagnostic', ascii_fmt)
  checks(4) = lu_exists('kxspec', 'diagnostic', ascii_fmt)!legacy
  checks(4) = lu_exists('kxspec.dat', 'diagnostic', ascii_fmt)
  checks(5) = lu_exists('eflux_spectra.dat', 'diagnostic', ascii_fmt)
  checks(6) = lu_exists('vflux_spectra.dat', 'diagnostic', ascii_fmt)
  checks(7) = lu_exists('pflux_spectra.dat', 'diagnostic', ascii_fmt)
  checks(8) = lu_exists('parallel_phi.dat', 'diagnostic', ascii_fmt)
  checks(9) = lu_exists('fluxes_nc.dat', 'diagnostic', ascii_fmt)

  check_for_old_data = any(checks)
end function check_for_old_data


!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!> some quantities need to be calculated at 2 consecutive timesteps, so this
!> routine is called the timestep before naverage, AND on naverage
!> in order to update such quantities
!> Further remarks:
!> At the moment fdis_tmp is updated in explicit_integration() just before
!> diagnostics_naverage() is called. If one ever needs fdis_tmp in one of
!> the pre_diagnostics, this must be changed.
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
subroutine diagnostic_pre_naverage(i_smallstep)
  use diagnos_growth_freq, only : calc_smallstep_growth_freq => calc_smallstep
  use diagnos_energetics, only : calc_smallstep_energetics => calc_smallstep
  use diagnos_freq_wavelet, only : calc_smallstep_freq_wavelet => calc_smallstep
  integer, intent(in) :: i_smallstep

  call calc_smallstep_growth_freq(i_smallstep)
  call calc_smallstep_energetics(i_smallstep)
  ! this one wants to be called in equidistant intervalls:
  !call calc_smallstep_freq_wavelet(i_smallstep)

end subroutine diagnostic_pre_naverage

end module diagnostic

