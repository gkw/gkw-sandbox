!------------------------------------------------------------------------------
! SVN:$Id$
!> Calculates quantities as a function along the field line for each mode.
!>
!> For each mode the following quantities are written to file
!>  sgr         the length along the field line 
!>  phi         the potential 
!>  apar        the parallel component of the vector potential 
!>  dens        the perturbed density 
!>  tpar        the perturbed parallel temperature 
!>  tperp       the perturbed perpendicular temperature 
!>  parallel_velocity       the perturbed parallel flow velocity
!>  bpar        the perturbed parallel (compressional) magnetic field
!>
!>  Note only sgr is real. The other quantities are complex.
!> 
!> This legacy output format therefore mixes fluxes, fields and moments
!> and could in future be consolidated into the more relevant modules.
!------------------------------------------------------------------------------
module diagnos_mode_struct

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, final_output
  public :: output

  
  !> switch for the parallel output 
  logical, save, public :: lparallel_output = .true.

  !> a list of timestamps can be provided in the namelist, to get more than
  !> one exemplaire of the parallel mode structure without rerunning the code
  real, save, public :: parallel_output_timestamps(512)
  
  !> switch for the rotation of the parallel output such that Re[phi(s=0)] = 1
  logical, save, public :: lrotate_parallel

  integer, dimension(7), save :: mode_struct_lus = -1
  character(len=10), dimension(7) :: mode_struct_names

  !> Private FFT arrays.
  complex, save, allocatable :: a(:,:), b(:,:)

  !> (testing) buffer for 3D outputs
  real, save, allocatable :: out3dbuf(:,:,:)

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
      lparallel_output = .true.
      parallel_output_timestamps = -1
      lrotate_parallel = .true.
  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(lparallel_output,1)
    call mpibcast(parallel_output_timestamps,512)
    call mpibcast(lrotate_parallel,1)
  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use control, only : io_legacy
    use grid, only : number_of_species, nmod, n_s_grid, n_x_grid
    use io, only : open_complex_lu, ascii_fmt, binary_fmt

    integer :: i
    
    if(root_processor) then
      
      ! initialize the list which holds the names for
      ! the three fields...
      mode_struct_names(1) = 'phi'
      mode_struct_names(2) = 'Apar'
      mode_struct_names(3) = 'Bpar'
      ! ...and any moments which shall be output.
      mode_struct_names(4) = 'dens'  ! density fluctuations
      mode_struct_names(5) = 'Tpar'  ! parallel temperature fluctuations
      mode_struct_names(6) = 'Tperp' ! parallel temperature fluct.
      mode_struct_names(7) = 'vpar' ! parallel velocity field fluct.

      if(lparallel_output) then
        if(.not.io_legacy) then
          do i = 1, size(mode_struct_names)
            if(i <= 3) then
              call open_complex_lu(mode_struct_names(i), &
                 & 'diagnostic/diagnos_mode_struct', &
                 & (/ nmod, n_s_grid, n_x_grid /), &
                 & binary_fmt, mode_struct_lus(i))
            else
              call open_complex_lu(mode_struct_names(i), &
                 & 'diagnostic/diagnos_mode_struct', &
                 & (/ number_of_species, nmod, n_s_grid, n_x_grid /), &
                 & binary_fmt, mode_struct_lus(i))
            end if
          end do
        end if
      end if

    end if
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use grid, only : ns
    use diagnos_generic, only : any_3d_output
    use diagnos_generic, only : mphit, mphiw3t, mrad_l
    use general, only : gkw_abort
    integer :: ierr

    !Private FFT arrays for diagnostics only.
    allocate(a(mphiw3t,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate a in diagnostic')
    endif

    allocate(b(mphiw3t,mrad_l), stat = ierr) 
    if (ierr.ne.0) then 
      call gkw_abort('Could not allocate b in diagnostic')
    endif

    ! Arrays used when the whole three dimensional outputs selected.
    !(Not strictly needed for spc3d)
    if (any_3d_output) then
      ! array for 3d output
      allocate(out3dbuf(mphit,mrad_l,ns),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: out3dbuf')
    end if

  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()

  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()

    call mode_struct_output_at_times()

  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine final_output(number)
    use control, only : io_legacy
    integer, intent(in), optional :: number
    
    if (lparallel_output) then
      if (present(number)) then
        if(io_legacy) then
          call parallel_output(number)
        else
          ! call parallel_output(number)
          call output_mode_structure(number)
        end if
      else
        if(io_legacy) then
          call parallel_output()
        else
          !call parallel_output()
          call output_mode_structure()
        end if
      end if
    end if
  end subroutine final_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output()

    call mode_struct_output_at_times()

  end subroutine output

  !----------------------------------------------------------------------------
  !> This routine checks if the time of the simulation has overstepped the next
  !> timestamp given in the input.dat file. Then a file is produced by
  !> parallel_output() .
  !----------------------------------------------------------------------------
  subroutine mode_struct_output_at_times()
    use control, only : time, last_largestep_time, io_legacy
    use global, only : r_tiny
    integer :: i
    if (lparallel_output) then

      do i = 1, 512
        if(parallel_output_timestamps(i) >= 0) then
          !check if one of the timestamps in the list was passed
          if (abs(time) <= r_tiny .and. &
             & abs(time - parallel_output_timestamps(i)) < r_tiny) then
            if(io_legacy) then
              call parallel_output(i)
            else
              call output_mode_structure(i)
              !call parallel_output(i)
            end if
          elseif(time >= parallel_output_timestamps(i) .and. &
             & last_largestep_time < parallel_output_timestamps(i)) then
            if(io_legacy) then
              call parallel_output(i)
            else
              call output_mode_structure(i)
              !call parallel_output(i)
            end if
          end if
        end if
      end do
    end if
  end subroutine mode_struct_output_at_times

  !----------------------------------------------------------------------------
  !>
  !----------------------------------------------------------------------------
  subroutine output_mode_structure(inumber)
    use grid,           only : nsp, nmod, nx, ns, nmu, nvpar, lsp, proc_subset
    use grid,           only : number_of_species, n_x_grid, n_s_grid
    use grid,           only : parallel_sp, parallel_s, parallel_x
    use control,        only : non_linear, flux_tube
    use dist,           only : fdisi, nsolc
    use dist,           only : get_bpar, get_phi, get_apar, get_apar_topo_order
    use dist,           only : get_bpar_topo_order, get_phi_topo_order
    use geom,           only : bn
    use mode,           only : ixzero
    use velocitygrid,   only : intmu, intvp, mugr, vpgr
    use io,             only : append_chunk, xy_fmt
    use io,             only : output_array, binary_fmt
    use global,         only : r_tiny
    use matdat,         only : get_f_from_g
    use mpiinterface,   only : mpiallreduce_maxloc, mpibcast
    use mpiinterface,   only : mpibarrier, gather_array, root_processor
    use mpiinterface,   only : processor_number
    use mpiinterface,   only : mpicomm_rank, mpireduce_sum
    use mpicomms,       only : COMM_VPAR_NE_MU_NE, COMM_VPAR_EQ_MU_EQ, COMM_S_NE_X_NE
    use general,        only : int2char
    use mpiinterface, only : mpireduce_sum, mpiallreduce_sum,  mpiallreduce_sum_inplace
    use mpiinterface, only : gather_species_posspace_cmplx, gather_posspace_cmplx
    use mpiinterface, only : proc_is_with_root_in
    integer, intent(in), optional :: inumber
    integer :: iquantity, imod, ix, i, is, k, j, maxcoord_i
    integer, dimension(2) :: maxcoord_i_ix
    complex, dimension(nsp, nmod, ns, nx) :: moment_buf
    complex, dimension(nmod, ns, nx) :: field_buf
    complex, dimension(number_of_species, &
       & nmod, n_s_grid, n_x_grid) :: moment_buf_global
    complex, dimension(nmod, n_s_grid, n_x_grid) :: field_buf_global
    real, dimension(2) :: phimax_local, phimax_global
    character(len=20) :: name
    complex :: fdis, rotate(nmod), rotate_tmp

    ! TODO allocate the global arrays only on the root process

    ! The mode structure (i.e. fields and moments) may be output more than once:
    !  * for the eigenvalue solver, there may be multiple modes found, that
    !    need to be stored
    !  * it is possible to specify a list of timestamps where the mode should
    !    be stored

    moment_buf = 0.0

    do iquantity = 1, size(mode_struct_names)
      if(root_processor) write(*,*) "output mode structure:", &
         & mode_struct_names(iquantity)

      if(iquantity <= 3) then
        ! a field is to be output.

        ! get the values of the vector potential.
        ! Note they are set to zero if nlapar = false.
        select case(mode_struct_names(iquantity))
        case('phi')
          call get_phi_topo_order(fdisi(1:nsolc),field_buf)
        case('Bpar')
          call get_bpar_topo_order(fdisi(1:nsolc),field_buf)
        case('Apar')
          call get_apar_topo_order(fdisi(1:nsolc),field_buf)
        end select

      end if

      ! if(root_processor) write(*,*) mode_struct_names(iquantity), " was copied"

      do imod = 1, nmod
        if(iquantity == 1) then
          ! then field_buf contains the electrostatic potential phi.
          if (.not.non_linear .and. lrotate_parallel) then
            ! Find the maximum of the potential.

            ! find a (single) processor on which that point
            ! of maximum potential exists
            if (flux_tube) then
              maxcoord_i = maxloc(abs(field_buf(imod,:,ixzero)),1)
              !beware using maxloc with array indices not starting at 1
              rotate(imod) = field_buf(imod,maxcoord_i,ixzero)
            else
              maxcoord_i_ix = maxloc(abs(field_buf(imod,:,:)))
              !beware using maxloc with array indices not starting at 1
              rotate(imod) = field_buf(imod,maxcoord_i_ix(1),maxcoord_i_ix(2))
            end if

            ! phimax_local(1) is the local maximum scalar data value
            ! phimax_local(2) is the local process rank
            phimax_local(1) = abs(rotate(imod))
            phimax_local(2) = real(1.0 * processor_number)
            call mpiallreduce_maxloc(phimax_local,phimax_global,1)
            rotate(imod) = phimax_global(1)

            ! Broadcast complex value of the potential (with the maximum
            ! absolute value) to all.
            ! FIXME I do not know why, but if I call the bcast with rotate(imod)
            ! then I get strange errors like 'corrupted doubly linked list'.
            !call mpibcast(rotate(imod),2,PROC=int(phimax_global(2)))
            ! Therefore bcast a scalar:
            rotate_tmp = rotate(imod)
            call mpibcast(rotate_tmp,2,PROC=int(phimax_global(2)))
            rotate(imod) = rotate_tmp
            ! phimax_global(2) is the global process rank which has the
            ! global max. data value

            ! Avoid dividing by zero
            if (abs(rotate(imod)) < r_tiny) rotate(imod) = (1.,0.)
          else
            rotate(imod)=(1.,0.)
          endif
          rotate(imod) = 1.0 / rotate(imod)
        end if

        if ((.not.non_linear).and.root_processor.and.lrotate_parallel) then
          write(*,*) 
          write(*,*) 'mode structure: normalized (rotated in complex plane) by: '
          write(*,*) rotate 
          write(*,*)
        end if

        ! Rotate all values in the complex plane relative to maximum potential
        ! defined to be (1.,0.).  Effectively, normalise out frequency.
        field_buf(imod, :, :) = field_buf(imod, :, :) * rotate(imod)
      end do

      ! if(root_processor) write(*,*) mode_struct_names(iquantity), " was rotated"

      ! If we want to calculate a moment of the distribution,
      ! then we have to compute a velocity space integral.
      if(iquantity > 3) then
        do imod = 1, nmod
          do ix = 1, nx
            do i = 1, ns
              do is = 1, nsp
                ! When using parallel species, only perform the velocity
                ! space sums when the outer loop reaches the species this
                ! process works on.

                do k = 1, nvpar
                  do j = 1, nmu
                    ! The distribution function
                    fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)

                    ! There might be gridcells where intvp = 0 ?
                    ! Still, I do not see why it is necessary to set fdis = 0
                    ! as it used to be done in the parallel_output() routine.
                    !if (intvp(i,j,k,is).eq.0.) fdis = 0.

                    select case(mode_struct_names(iquantity))
                    case('dens')
                      moment_buf(is, imod, i, ix) = moment_buf(is, imod, i, ix) + &
                         & bn(ix,i)*intvp(i,j,k,is)*intmu(j)*fdis
                    case('vpar')
                      moment_buf(is, imod, i, ix) = moment_buf(is, imod, i, ix) + &
                         & bn(ix,i)*intvp(i,j,k,is)*intmu(j)* &
                         & vpgr(i,j,k,is)* &
                         & fdis
                    case('Tpar')
                      moment_buf(is, imod, i, ix) = moment_buf(is, imod, i, ix) + &
                         & bn(ix,i)*intvp(i,j,k,is)*intmu(j)* &
                         & vpgr(i,j,k,is)**2* &
                         & fdis
                    case('Tperp')
                      moment_buf(is, imod, i, ix) = moment_buf(is, imod, i, ix) + &
                         & bn(ix,i)**2*intvp(i,j,k,is)*intmu(j)* &
                         & mugr(j)* &
                         & fdis
                    end select
                  end do ! nmu
                end do ! nvpar
              end do ! nsp
            end do ! ns
          end do ! nx

          ! Rotate all values in the complex plane relative to maximum potential
          ! defined to be (1.,0.).  Effectively, normalise out frequency.
          if (.not.non_linear) then
            if (lrotate_parallel) then
              moment_buf(:,imod,:,:) = moment_buf(:,imod,:,:) * rotate(imod)
            endif
          endif
        end do ! nmod
        ! if(root_processor) write(*,*) mode_struct_names(iquantity), " was integrated"
      end if

      if(iquantity <= 3) then
        ! reduce over processes which work on the same spatial
        ! point
        call mpiallreduce_sum_inplace(field_buf, &
           & shape(field_buf), COMM_VPAR_NE_MU_NE)
      else
        ! reduce over processes which work on the same spatial
        ! point+species;
        call mpiallreduce_sum_inplace(moment_buf, &
           & shape(moment_buf), COMM_VPAR_NE_MU_NE)
      end if

      ! if(root_processor) write(*,*) mode_struct_names(iquantity), &
      ! & " was mpi-reduced"

      ! we would like to avoid numbers in the dataset name
      !name = trim(mode_struct_names(iquantity))//trim(adjustl(int2char(inumber,3)))
      ! therefore:
      name = trim(mode_struct_names(iquantity))

      if(iquantity <= 3) then
        ! gather and output the 3D global field
        if(proc_is_with_root_in(COMM_S_NE_X_NE)) then
          ! call mpi_comm_rank(COMM_S_NE_X_NE, rank1, ierr)
          ! write (*,*) "yes, rank1=", rank1, " will gather!"

          call gather_posspace_cmplx(field_buf_global, &
             & shape(field_buf_global), &
             & field_buf,shape(field_buf), &
             & parallel_s, parallel_x)
        end if
        ! if(root_processor) write(*,*) mode_struct_names(iquantity), " was gathered"
        if (root_processor) then
          call append_chunk(mode_struct_lus(iquantity), &
             & field_buf_global, xy_fmt, binary_fmt)
        end if
      else
        ! gather and output the 4D global moment
        if(proc_is_with_root_in(COMM_VPAR_EQ_MU_EQ)) then
          ! we want to gather data from the processes which are in
          ! COMM_VPAR_EQ_MU_EQ, together with the root process. (remember
          ! that there as many different COMM_VPAR_EQ_MU_EQ communicators
          ! as there are processes in COMM_VPAR_NE_MU_NE)
          ! TODO check if it is really okay to check the communicator number
          ! like this here

          !debug output
          ! call mpicomm_rank(COMM_VPAR_EQ_MU_EQ, rank2)
          ! write(*,*) "this is rank2==",rank2, &
          !    & "and is in COMM_VPAR_EQ_MU_EQ==", COMM_VPAR_EQ_MU_EQ

          ! put debug data
          ! moment_buf = rank2
          ! field_buf = rank2

          ! At the moment, the IO is serial. The root process gathers the
          ! data from the other processes and does the IO.
          call gather_species_posspace_cmplx(moment_buf_global, &
             & shape(moment_buf_global), &
             & moment_buf,shape(moment_buf), &
             & parallel_sp, parallel_s, parallel_x)
        end if
        if (root_processor) then
          call append_chunk(mode_struct_lus(iquantity), &
             & moment_buf_global, xy_fmt, binary_fmt)
        end if
      end if
    end do ! iquantity

  end subroutine output_mode_structure


  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !>  This subroutine writes some diagnostics connected with the parallel 
  !>  mode structure. For every mode the following quantities are written
  !>  to file
  !>
  !>  sgr         the length along the field line 
  !>  phi         the potential 
  !>  apar        the parallel component of the vector potential 
  !>  dens        the perturbed density 
  !>  tpar        the perturbed parallel temperature 
  !>  tperp       the perturbed perpendicular temperature 
  !>  parallel_velocity       the perturbed parallel flow velocity
  !>  bpar        the perturbed parallel (compressional) magnetic field
  !>
  !>  Note only sgr is real. The other quantities are complex and fill 2
  !>  columns in the output file.
  !>
  !----------------------------------------------------------------------------
  subroutine parallel_output(inumber)
    use grid,           only : nmod, nx, ns, nmu, nvpar, lsp, proc_subset
    use grid,           only : number_of_species, n_x_grid, n_s_grid
    use control,        only : non_linear, flux_tube, io_legacy
    use dist,           only : fdisi, phi, apar, bpar, get_apar, nsolc
    use dist,           only : get_bpar, get_phi
    use geom,           only : sgr, bn
    use mode,           only : ixzero
    use velocitygrid,   only : intmu, intvp, mugr, vpgr
    use io,             only : open_real_lu, close_lu, append_chunk, xy_fmt, ascii_fmt
    use global,         only : r_tiny
    use matdat,         only : get_f_from_g
    use mpiinterface,   only : mpiallreduce_maxloc, mpibcast, mpiallreduce_sum
    use mpiinterface,   only : mpibarrier, gather_array, root_processor
    use mpiinterface,   only : number_of_processors, processor_number
    use mpicomms,       only : COMM_S_EQ_X_EQ, COMM_X_NE, COMM_S_NE
    use general,        only : int2char
    use global,         only : dotdat

    integer, optional, intent(in) :: inumber

    ! integers for the loop over all grid points 
    integer :: imod, ix, i, j, k, is, ihelp, il, i_s
    integer, dimension(2) :: ihelp2

    real, dimension(2) :: phimax_local,phimax_global
    ! for reduction:
    complex, dimension(4) :: arr, rarr
    complex :: phid, apad, bpad, tpar, tperp, dens, parallel_velocity
    complex :: fdis, rotate
    complex, dimension(1:nx,1:ns,9) :: local_write
    complex, dimension(1:n_x_grid,1:n_s_grid,9) :: global_write

    integer :: lun

    ! get the values of the vector potential 
    ! Note they are set to zero if nlapar = false.
    call get_apar(fdisi(1:nsolc),apar)
    call get_bpar(fdisi(1:nsolc),bpar)
    call get_phi(fdisi(1:nsolc),phi)

    ! A number can added to the logical unit name as
    !  * for the eigenvalue solver, there may be multiple modes found, that
    !    need to be stored
    !  * it is possible to specify a list of timestamps where the mode should
    !    be stored
    if (root_processor) then
      if (present(inumber)) then
        call open_real_lu(dotdat( &
           &'parallel'//trim(adjustl(int2char(inumber,5))), io_legacy), &
           & 'diagnostic/diagnos_mode_struct', (/ 15 /), &
           & ascii_fmt, lun)
      else
        call open_real_lu(dotdat('parallel',io_legacy), &
           & 'diagnostic/diagnos_mode_struct', (/ 15 /), &
           & ascii_fmt, lun)
      end if
    end if

    do i_s = 1, number_of_species
      do imod = 1, nmod

        if (.not.non_linear) then
          ! Find the maximum of the potential and with (mpi) maxloc 
          ! Find a (single) processor on which that point exists
          if (flux_tube) then
            ihelp=maxloc(abs(phi(imod,ixzero,1:ns)),1)
            ! beware using maxloc with array indices not starting at 1
            phimax_local(1)=abs(phi(imod,ixzero,ihelp))
            rotate=phi(imod,ixzero,ihelp)
          else
            ihelp2 = maxloc(abs(phi(imod,1:nx,1:ns)))
            !beware using maxloc with array indices not starting at 1
            rotate = phi(imod,ihelp2(1),ihelp2(2))
            phimax_local(1) = abs(rotate)
          end if

          ! use real(processor_number) for maxloc interface
          ! (could make a more Fortran friendly wrapper here)
          phimax_local(2)=real(1.*processor_number)
          call mpiallreduce_maxloc(phimax_local,phimax_global,1)
          ! convert back to integer for comparison
          ihelp=int(phimax_global(2))
          !Broadcast the point with maximum potential to all.
          call mpibcast(rotate,2,PROC=ihelp)
          !Avoid dividing by zero
          if (abs(rotate) < r_tiny) rotate = (1.,0.)
        else
          rotate=(1.,0.)
        endif

        do ix = 1, nx
          do i = 1, ns

            tpar  = 0.
            tperp = 0.
            dens  = 0.
            parallel_velocity = 0.

            is = lsp(i_s)
            ! When using parallel species, only perform the velocity space sums
            ! when the outer loop reaches the local species.
            if (proc_subset(0,0,0,0,i_s)) then
              do k = 1, nvpar
                do j = 1, nmu

                  ! The distribution function 
                  fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi)
                  if (intvp(i,j,k,is).eq.0.) fdis = 0.     

                  dens = dens + bn(ix,i)*intvp(i,j,k,is)*intmu(j)*fdis
                  parallel_velocity= parallel_velocity + bn(ix,i)*intvp(i,j,k,is)*intmu(j)*vpgr(i,j,k,is)*   &
                     & fdis
                  tpar = tpar + bn(ix,i)*intvp(i,j,k,is)*intmu(j)*vpgr(i,j,k,is)**2* &
                     & fdis
                  tperp= tperp + bn(ix,i)**2*intvp(i,j,k,is)*intmu(j)*mugr(j)*    &
                     & fdis

                end do
              end do
            end if
            ! reduce over points of equal s; any procs not responsible for the
            ! current species will use zero values.
            if (number_of_processors > 1) then
              arr(1) = dens ; arr(2) = tpar ; arr(3) = tperp ; arr(4) = parallel_velocity
              call mpiallreduce_sum(arr,rarr,4,COMM_S_EQ_X_EQ)
              dens = rarr(1) ; tpar = rarr(2); tperp = rarr(3) ; parallel_velocity = rarr(4)
            end if

            !Rotate all values in the complex plane relative to maximum potential
            !defined to be (1.,0.).  Effectively, normalise out frequency.
            if (.not.non_linear) then
              if (lrotate_parallel) then
                apad=apar(imod,ix,i)/rotate
                phid=phi(imod,ix,i)/rotate
                bpad=bpar(imod,ix,i)/rotate
                dens=dens/rotate
                tpar=tpar/rotate
                tperp=tperp/rotate
                parallel_velocity=parallel_velocity/rotate
              endif
            else
              apad=apar(imod,ix,i)
              phid=phi(imod,ix,i)
              bpad=bpar(imod,ix,i)
            endif

            ! Store for the columns of the file         
            local_write(ix,i,1) = sgr(i)
            local_write(ix,i,2) = phid
            local_write(ix,i,3) = apad
            local_write(ix,i,4) = dens
            local_write(ix,i,5) = tpar
            local_write(ix,i,6) = tperp
            local_write(ix,i,7) = parallel_velocity
            local_write(ix,i,8) = bpad

            ! this barrier appears to be necessary ??
            call mpibarrier()      

          end do ! ns
        end do ! nx

        ! gather (only for the root processor) for all 8 complex columns of the file
        do il = 1,8     
          call gather_array(global_write(:,:,il),n_x_grid, & 
             & n_s_grid,local_write(:,:,il),nx,ns,COMM_X_NE,COMM_S_NE)
        end do

        if (root_processor) then
          ! output
          ! real: sgr(i), complex: phid, apad, dens, tpar, tperp, parallel_velocity, bpad

          do ix = 1, n_x_grid
            do i = 1, n_s_grid
              !       write(ifile,fmt = '(15(1x,e13.5))') real(global_write(ix,i,1)), &
              !                          & (global_write(ix,i,il), il = 2,8)
              call append_chunk(lun, &
                 & (/ real(global_write(ix,i,1)), &
                 & (real(global_write(ix,i,il)), aimag(global_write(ix,i,il)), il=2,8) /), &
                 & '(15(1x,e13.5))', ascii_fmt)
            end do
          end do
        end if

      end do ! nmod
    end do  ! species

    call close_lu(lun, ascii_fmt)

    if ((.not.non_linear).and.root_processor.and.lrotate_parallel) then
      write(*,*) 
      write(*,*) 'parallel.dat: normalized (rotated in complex plane) by: '
      write(*,*) rotate 
      write(*,*)
    end if

  end subroutine parallel_output



end module diagnos_mode_struct
