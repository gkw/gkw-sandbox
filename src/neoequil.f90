!----------------------------------------------------------------------
! SVN:$Id$
!> Contains the interface with NEO and the calculation of neoclassical 
!> equilibrium components
!---------------------------------------------------------------------

module neoequil

  use global,       only : lenswitch

  implicit none

  private

  public :: neoequil_init_grids, neoequil_check_params
  public :: gradneof, neof, mid_r_point, neophi, neo_normparam

  !> The neoclassical distribution function,  neof(n_species,n_e,n_xi,n_theta) 
  real, allocatable, save :: neof(:,:,:,:,:)
  real, allocatable, save :: neophi(:,:)
  real, allocatable, save :: neophi_G(:,:)
  real, allocatable, save :: neophi_inter(:,:)
  real, allocatable, save :: fread(:)
  real, allocatable, save :: gradneof(:,:,:,:)
  real, allocatable, save :: neo_coeffs(:,:,:,:,:)
  real, allocatable, save :: neo_coeffs_inter(:,:,:,:,:)
  real, allocatable, save :: neo_equilparam(:,:)
  real, allocatable, save :: neo_normparam(:,:)

  !> The theta points used in NEO
  real, allocatable, save :: neo_theta(:), neo_eps(:)

  !> The gridsizes of the distribution function from NEO (or from GKW) 
  integer, save :: neo_n_theta, neo_n_species, neo_n_energy, neo_n_xi
  integer, save :: neo_n_radial, mid_r_point
  real   , save :: rrefoa  

  !> Length of chararacter string of filename 
  integer, parameter :: lenfile = 180

contains

subroutine neoequil_check_params
  
  use control, only : flux_tube
  use general, only : gkw_abort, gkw_warn, gkw_exit
  use global,  only : r_tiny
  use grid,    only : nperiod
  
  logical             :: neofile_exists  
  character(len = lenfile) :: neofile
 
  !Check that these, essential, files are present so that they can be read.
  neofile = "out.neo.f"
  inquire(file=neofile,EXIST=neofile_exists)
  if (.not. neofile_exists) call gkw_abort('out.neo.f: not found: '//neofile)

  neofile = "out.neo.grid"
  inquire(file=neofile,EXIST=neofile_exists)
  if (.not. neofile_exists) call gkw_abort('out.neo.grid: not found: '//neofile)
  if(.not.flux_tube) call gkw_abort('Neoclassical correction only implemented for flux tube geometry')
  if(nperiod > 1) call gkw_abort('Neoclassical correction still requires nperiod = 1')

  neofile = "out.neo.phi"
  inquire(file=neofile,EXIST=neofile_exists)
  if (.not. neofile_exists) call gkw_abort('out.neo.phi: not found: '//neofile)

  neofile = "out.neo.equil"
  inquire(file=neofile,EXIST=neofile_exists)
  if (.not. neofile_exists) call gkw_abort('out.neo.equil: not found: '//neofile)

  neofile = "out.neo.expnorm"
  inquire(file=neofile,EXIST=neofile_exists)
  if (.not. neofile_exists) call gkw_abort('out.neo.equil: not found: '//neofile)

  end subroutine neoequil_check_params

subroutine neoequil_allocate

  use grid,    only : n_s_grid, ns, parallel_s, n_vpar_grid 
  use grid,    only : n_mu_grid, number_of_species, nmu, nvpar
  use grid,    only : nsg_pt
  use general, only : gkw_abort
  use control, only : order_of_the_scheme  
  use mpiinterface, only : root_processor

  ! local parameters 
  integer ierr, nelements

  ! intialize the error parameter
  ierr=0

  nelements = neo_n_radial*neo_n_species*(neo_n_energy+1)*(neo_n_xi+1)*neo_n_theta

  allocate(fread(1:nelements),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')

  !First column is only 1:5 as this is the number of radial grid points needed
  !to calculate the radial gradients
  !At the moment the whole neof is kept on every processor 
  if (order_of_the_scheme .eq.'fourth_order') then 
    allocate(neof(1:5,1:number_of_species,-1:ns+2,1:nmu,-1:nvpar+2),stat=ierr)
    if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')
    neof(:,:,:,:,:)=0.E0
    allocate(neophi(1:5,-1:ns+2),stat=ierr)
    if (ierr /= 0) call gkw_abort('Could not allocate neophi in neoequil')
    neophi(:,:) = 0.E0
  else if (order_of_the_scheme .eq.'second_order') then 
    allocate(neof(1:5,1:number_of_species,0:ns+1,1:nmu,0:nvpar+1),stat=ierr)
    if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')
    neof(:,:,:,:,:)=0.E0
    allocate(neophi(1:5,0:ns+1),stat=ierr)
    if (ierr /= 0) call gkw_abort('Could not allocate neophi in neoequil')
    neophi(:,:) = 0.E0
  else
      call gkw_abort('velgrid_allocate: ivpar_extra; bad case of scheme order')
  endif

  allocate(gradneof(1:number_of_species,1:ns,1:nmu,1:nvpar),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')

  allocate(neophi_G(1:5,1:neo_n_theta+1),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neophi in neoequil')
  neophi(:,:) = 0.E0
 
  allocate(neophi_inter(1:5,-1:nsg_pt+2),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neophi_inter in neoequil')
  neophi_inter(:,:) = 0.E0

  allocate(neo_coeffs(1:neo_n_radial,1:neo_n_species,  &
     & 1:neo_n_energy+1,1:neo_n_xi+1,1:neo_n_theta+1),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')
  neo_coeffs(:,:,:,:,:) = 0.E0

  allocate(neo_coeffs_inter(1:neo_n_radial,1:neo_n_species,-1:nsg_pt+2,  &
     & 1:neo_n_energy+1,1:neo_n_xi+1),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')
  neo_coeffs(:,:,:,:,:) = 0.E0
  
  allocate(neo_equilparam(1:neo_n_radial,1:(7+5*neo_n_species)),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_equilparam in neoequil')
  neo_equilparam(:,:) = 0.E0

  allocate(neo_normparam(1:neo_n_radial,1:(7*neo_n_species)),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_normparam in neoequil')
  neo_normparam(:,:) = 0.E0

  if(root_processor)write(*,*)'Neo arrays allocated'

end subroutine neoequil_allocate

subroutine neoequil_deallocate

  deallocate(fread)
  deallocate(neo_coeffs)
  deallocate(neo_coeffs_inter)
  deallocate(neo_eps)
  deallocate(neophi_G)
  deallocate(neophi_inter)
  deallocate(neo_theta)

end subroutine neoequil_deallocate

subroutine gkwneo_allocate

  use grid,    only : n_s_grid, ns, parallel_s, n_vpar_grid 
  use grid,    only : n_mu_grid, number_of_species, nsg_pt
  use general, only : gkw_abort
  
  ! local parameters 
  integer ierr, nelements

  ! intialize the error parameter
  ierr=0

  nelements = n_s_grid*n_vpar_grid*n_mu_grid*number_of_species

  allocate(neof(1:5,1:number_of_species,1:n_s_grid,1:n_vpar_grid,1:n_mu_grid),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')
  neof(:,:,:,:,:)=0.E0

end subroutine gkwneo_allocate

subroutine read_gkw_neo

  use mpiinterface, only : root_processor, mpibcast
  use io,           only : get_free_file_unit
  use general, only : gkw_abort

  character(len = lenfile) :: filename2
  integer :: file_unit
  logical :: lread_input

  filename2 = "FDS.dat"
   
  inquire(FILE=filename2,EXIST=lread_input)
  if (lread_input) then
     call get_free_file_unit(file_unit)
     open(file_unit,file=filename2,FORM='formatted',STATUS='unknown')
     close(file_unit)
  endif

end subroutine read_gkw_neo

subroutine read_neo

  use mpiinterface, only : root_processor, mpibcast
  use io,           only : get_free_file_unit
  use general, only : gkw_abort, gkw_warn
  use grid,         only : nsp, number_of_species, n_s_grid

  character(len = lenfile) :: neofile, neofile2
  integer :: readint, j, freelun, ierr, nelements 
  integer :: is, i, k, jj, ix, ncol
  real    :: readdo 

  !Check that these, essential, files are present so that they can be read.
  neofile = "out.neo.f"
  neofile2 = "out.neo.grid"
  if(root_processor) write(*,*) 'Reading from NEO output files'
  
  ! Open ASCII file
  if(root_processor)then
    call get_free_file_unit(freelun)
    open(freelun, status='unknown',action='read', file=neofile2)  
    !Read the number of species
    read(freelun,*) readint
    neo_n_species = readint
  
    if(neo_n_species.ne.number_of_species)then
      write(*,*) 'Neo ', neo_n_species, ' GKW ', number_of_species 
      call gkw_warn('The number of species is not equal')
    endif      

    !Read the number of energy points
    read(freelun,*) readint
    neo_n_energy = readint
 
    !Read the number of pitch-angle points
    read(freelun,*) readint
    neo_n_xi = readint

    !Read the number of poloidal grid points
    read(freelun,*) readint
    neo_n_theta = readint
      
    if(neo_n_theta.ne.n_s_grid)then
      if(root_processor)write(*,*) 'Neo: number of theta points', neo_n_theta 
      call gkw_warn('The number of s points is not equal')
    endif   
  endif

  call mpibcast(neo_n_species,1)
  call mpibcast(neo_n_energy, 1)
  call mpibcast(neo_n_xi,1)
  call mpibcast(neo_n_theta,1)
  
  !Values of theta !The plus one is to add a +pi point (-pi is there)
  !On all processors
  allocate(neo_theta(1:neo_n_theta+1),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')
   
  if(root_processor)then
    do j=1,neo_n_theta
      read(freelun,*) readdo
      neo_theta(j) = readdo 
    end do
    neo_theta(neo_n_theta+1)=-neo_theta(1) 
  endif

  call mpibcast(neo_theta,neo_n_theta+1)

  if(root_processor)then
    !Number of radial grid points when NEO is run in profile mode 
    read(freelun,*) readint
    neo_n_radial = readint
  endif

  call mpibcast(neo_n_radial,1)

  if(neo_n_radial.ne.5)then
    if(root_processor)write(*,*) 'Neo: number of radial points', neo_n_radial 
    call gkw_warn('The number of radial points from NEO should be 5')
    if(neo_n_radial.eq.1)call gkw_warn('1 Radial grid point.  Radial gradients ignored')
  endif  
 
  allocate(neo_eps(1:neo_n_radial),stat=ierr)
  if (ierr /= 0) call gkw_abort('Could not allocate neo_theta in neoequil')
  
  if(root_processor)then
    do j=1,neo_n_radial
      read(freelun,*) readdo
      neo_eps(j) = readdo 
    end do
    close(freelun)
  endif
  call mpibcast(neo_eps,neo_n_radial)
    
  mid_r_point = (neo_n_radial+1)/2
  if(neo_n_radial.eq.1)mid_r_point=1

  if(root_processor)then
    write(*,*) 'Neo rad ,', 'Neo species ,', 'N energy ,', 'N xi ,', 'N theta'  
    write(*,*) neo_n_radial, neo_n_species, neo_n_energy, neo_n_xi, neo_n_theta
  endif

  call neoequil_allocate  
   
  nelements = neo_n_radial*neo_n_species*(neo_n_energy+1)*(neo_n_xi+1)*neo_n_theta
  
  if(root_processor)then   
    call get_free_file_unit(freelun)
    open(freelun, status='unknown',action='read', file=neofile)
    do j=1,nelements
      read(freelun,*) readdo
      fread(j) = readdo
    end do
    close(freelun)
  endif

  call mpibcast(fread,  nelements)  

  !Put the coefficients in a structure that is a little more managable.
  !Maybe this is unessesary, it can probably be done in another way
  jj=1
  do ix = 1,neo_n_radial
    do is = 1,neo_n_species
      do i = 1,neo_n_energy+1
        do j = 1,neo_n_xi+1
          do k = 1,neo_n_theta
            neo_coeffs(ix,is,i,j,k) = fread(jj)
            jj=jj+1
          enddo
        enddo
      enddo
    enddo
  enddo  
  if(root_processor)write(*,*)'Read ',jj-1,'Of ', nelements

  !Read the equil file, to find the normalising quantities
  if(root_processor)then
    call get_free_file_unit(freelun)
    neofile = "out.neo.equil"
    open(freelun, status='unknown',action='read', file=neofile)  
    ncol = 7 + 5*neo_n_species
 
    do k = 1,neo_n_radial
       read(freelun,*) (neo_equilparam(k,i),i=1,ncol)
    enddo  
  endif

  call mpibcast(neo_equilparam,  (7+5*neo_n_species)*neo_n_radial)

  !The ratio of Rref/a needed for transforming between NEO and GKW units
  rrefoa = neo_equilparam(1,5)

  !Read the equil file, to find the normalising quantities
  if(root_processor)then
    call get_free_file_unit(freelun)
    neofile = "out.neo.expnorm"
    open(freelun, status='unknown',action='read', file=neofile)  
    ncol = 7
 
    do k = 1,neo_n_radial
       read(freelun,*) (neo_normparam(k,i),i=1,ncol)
    enddo  
  endif

  call mpibcast(neo_normparam,7*neo_n_radial)

end subroutine read_neo

subroutine read_neo_phi

  use mpiinterface, only : root_processor, mpibcast
  use io,           only : get_free_file_unit
  use general,      only : gkw_abort, gkw_warn
  use grid,         only : n_s_grid

  character(len = lenfile) :: neofile
  integer :: readint, j, freelun, ierr, ncol, nrow 
  integer :: i, k
  real    :: readdo 

  !Check that these, essential, files are present so that they can be read.
  neofile = "out.neo.phi"
    
  ! Open ASCII file
  if(root_processor)then
    call get_free_file_unit(freelun)
    open(freelun, status='unknown',action='read', file=neofile)  
    nrow = 3
    ncol = (neo_n_theta+1)/nrow    
 
    do k = 1,neo_n_radial
      nrow = 3
      do j = 1,ncol
        if(j.eq.ncol)nrow=2
        read(freelun,*) (neophi_G(k,3*(j-1)+i),i=1,nrow)
        !130  format(ES16.8) 
      enddo
      neophi_G(k,neo_n_theta+1)=neophi_G(k,1)
    enddo

  endif

  call mpibcast(neophi_G, neo_n_radial*(neo_n_theta+1) )  
  
end subroutine read_neo_phi

subroutine reconstruct_f
  
  use grid,      only : nvpar, ns, nmu, nperiod, n_s_grid, nsp
  use grid,      only : n_vpar_grid, n_mu_grid, nmu, number_of_species
  use grid,      only : gmu, gvpar, gs, vpmax, mumax
  use geom,      only : bn, bn_G
  use velocitygrid,   only : vpgr, mugr
  use functions, only : legendre, leguerre, cheby  
  use io,        only : get_free_file_unit
  use components, only : signz
  use general,   only : gkw_abort, gkw_warn
  use mpiinterface, only : root_processor
  use dist,      only : fmaxwl

  real    :: zeta, ener, ener_scal, kcoef, tspec
  integer :: ne,nxi,is,i,j,k,ix,freelun,ii
  real    :: vel, normmaxwell  

  if(neo_n_species.eq.(number_of_species+1))then
    neo_n_species = number_of_species
    call gkw_warn('Assumed extra species is electrons and is ignored!')
  endif
  if(root_processor)then
    call get_free_file_unit(freelun)
    open(freelun, status='unknown',action='write', file='neof.dat')  
  endif
  do ix=1,neo_n_radial
    do is = 1,1!neo_n_species !Ignore the electrons 
      do i = -1,ns+2

        !The reconstructed distribution function (and its radial
        !gradient is stored locally on each processor, so this 
        !global s point is needed
        ii = gs(i)
        neophi(ix,i) = neophi_inter(ix,ii) 

        do j = 1,nmu
          do k = -1,nvpar+2
          
            !The corresponding pitch angle and energy value for 
            !specific GKW velocity grid point
            zeta =  vpgr(1,j,k,is)/sqrt(vpgr(1,j,k,is)**2 + 2.E0*mugr(j)*bn(1,i))
            ener =  (vpgr(1,j,k,is)**2 + 2.E0*mugr(j)*bn(1,i))
            !Rescale the reference thermal velocity
            ener = ener*(neo_normparam(3,6)/neo_normparam(ix,6))**2
           
            !Rescales the energy so that the argument of the
            !Chebyshev polynomial is 2sqrt(en/en_max) - 1
            !ener_scal = 2*sqrt(ener) - 1 !!Not used any more.

            !Here we sum over the polynomials to reconstruct f in 
            !GKWs velocity space coordinates   
            neof(ix,is,i,j,k)=0.E0
            do ne = 1,neo_n_energy+1
              do nxi = 1,neo_n_xi+1
                if(nxi.eq.1)then
                  kcoef = 0.5E0
                  ener_scal = 1.0E0
                else
                  kcoef = 1.5E0
                  ener_scal = sqrt(ener)
                endif         
                !This expansion is used in the NEO 2009 PPCF paper
                !neof(ix,is,i,j,k) = neof(ix,is,i,j,k) + neo_coeffs(ix,is,ne,nxi,i)* & 
                 !& legendre(nxi-1,zeta)*cheby(ne-1,ener_scal)
                !This one is used in the 2012 PPCF
                !write(*,*) ne-1, nxi-1, i, j, k, neo_coeffs_inter(ix,is,ii,ne,nxi)
                neof(ix,is,i,j,k) = neof(ix,is,i,j,k) + neo_coeffs_inter(ix,is,ii,ne,nxi)* & 
                 & legendre(nxi-1,zeta)*ener_scal*leguerre(ne-1,ener,kcoef)
              enddo
            enddo 
            
            !Remove the adiabatic component
            tspec = neo_equilparam(ix,7+is*2)
            
            neof(ix,is,i,j,k) = neof(ix,is,i,j,k) &
                & - signz(is)*neophi(ix,i)/tspec

            if(root_processor)write(freelun,*)neof(ix,is,i,j,k) 
                       
          enddo
        enddo
      enddo
    enddo
  enddo

  if(root_processor)close(freelun) 


end subroutine reconstruct_f

subroutine neof_renormalise

  use grid,      only : nvpar, ns, nmu, nperiod, n_s_grid, nsp
  use grid,      only : n_vpar_grid, n_mu_grid, nmu, number_of_species
  use grid,      only : gmu, gvpar, gs, vpmax, mumax
  use geom,      only : bn, bn_G
  use dist,      only : fmaxwl
  use velocitygrid,   only : vpgr, mugr
  use functions, only : legendre, leguerre, cheby  
  use io,        only : get_free_file_unit
  use components, only : signz
  use general,      only : gkw_abort, gkw_warn
  use mpiinterface, only : root_processor

  real    :: zeta, ener, ener_scal, kcoef, tspec
  integer :: ne,nxi,is,i,j,k,ix,freelun

  do ix=1,neo_n_radial
    do is = 1,neo_n_species 
      do i = -1,ns+2
        do j = 1,nmu
          do k = -1,nvpar+2
            
            !The normalisations here are due to the fact that we use 5 radial neo calculations
            !and the normalising quantaties vary radially.  It is assumed that the middle one has
            !the same values as the GKW normalisations.
            
            !Renormalise to the GKW rhostar 
            neof(ix,is,i,j,k) = sqrt(2.E0)*neof(ix,is,i,j,k)/rrefoa
            neophi(ix,i) = sqrt(2.E0)*neophi(ix,i)/rrefoa
      
            !and scale the radial phi's to their local temperature 
            neophi(ix,i) = neophi(ix,i)*neo_normparam(3,5)/neo_normparam(ix,5)

          enddo
        enddo
      enddo
    enddo
  enddo
  
end subroutine neof_renormalise

subroutine cal_gradients

  use grid,      only : nvpar, ns, nmu, nperiod, n_s_grid, nsp
  use grid,      only : n_vpar_grid, n_mu_grid, nmu, number_of_species
  use io,        only : get_free_file_unit
  use mpiinterface,  only : root_processor

  integer :: is,i,j,k,ix,freelun,ii
  real :: dum,dx

  !NEO is run in profile mode.  5 radial points are taken with the outer
  !4 used to calculate the radial gradient of the distribution function
  !and the middle one used for the value.
  if(neo_n_radial.eq.1)then
    gradneof(:,:,:,:)=0.E0
    return
  endif

  dx = neo_eps(2)-neo_eps(1)
  if(root_processor)then
    call get_free_file_unit(freelun)
    open(freelun, status='unknown',action='write', file='gradneof.dat') 
  endif
  do is = 1,nsp
    do i = 1,ns
      do j = 1,nmu
        do k = 1,nvpar
          
          !Second order finite difference radially
          dum = 8*(neof(4,is,i,j,k)-neof(2,is,i,j,k))
          dum = dum + (-neof(5,is,i,j,k)+neof(1,is,i,j,k))
          gradneof(is,i,j,k) = dum*rrefoa/(12.E0*dx)
          if(neo_n_radial.eq.1)gradneof(is,i,j,k)=0.E0
          if(root_processor)write(freelun,*)gradneof(is,i,j,k)  

          !In the parallel velocity and parallel directions
          !we use the same scheme as in the code in general  
          !These are naturally more suited to be placed in 
          !linear terms module     

        enddo
      enddo
    enddo
  enddo
  if(root_processor)close(freelun) 

end subroutine cal_gradients

!-------------------------------------------------------------------------
!> Interpolates the data from neo theta grid (which is uniform in theta)
!> onto the GKW theta grid (which is noninform in theta when shaping is used)
!-------------------------------------------------------------------------
subroutine neointerp

  use geom,   only : interpquad, pol_angle, sgr
  use grid,   only : n_s_grid, gs , nperiod, nsg_pt
  use control, only : order_of_the_scheme  
  use io,     only : get_free_file_unit
  use mpiinterface, only : root_processor

  integer :: is, ix, i, j, k, freelun
  real    :: dum(1:nsg_pt), dum2(1:nsg_pt) 
  real    :: coeff_in(1:neo_n_theta+1) 

  do k=1,n_s_grid
    dum2(k) = pol_angle(1,k)
  enddo
  if(root_processor)then
    call get_free_file_unit(freelun)
    open(freelun, status='unknown',action='write', file='neof_inter.dat')  
  endif
  do ix=1,neo_n_radial
    do is=1,neo_n_species
      do i=1,neo_n_energy+1
        do j=1,neo_n_xi+1
          
          !Must give the exact ranges as some arrays have been 
          !extended for parallelisation, or symmetry reasons.
         
          do k = 1,neo_n_theta
            coeff_in(k) = neo_coeffs(ix,is,i,j,k)
          enddo
          coeff_in(neo_n_theta+1)= neo_coeffs(ix,is,i,j,1)        
          
          call interpquad(neo_theta,coeff_in, &
             & neo_n_theta+1,nsg_pt,dum2, &
             & dum)
          
          do k =1,nsg_pt
            neo_coeffs_inter(ix,is,k,i,j)=dum(k)
            if(root_processor)write(freelun,*)dum(k)
          enddo
 
          !Ensure periodicity in the s direction (Assuming fourth order)
          neo_coeffs_inter(ix,is,nsg_pt+1,i,j)=neo_coeffs_inter(ix,is,1,i,j)
          neo_coeffs_inter(ix,is,nsg_pt+2,i,j)=neo_coeffs_inter(ix,is,2,i,j)

          neo_coeffs_inter(ix,is,0,i,j)=neo_coeffs_inter(ix,is,nsg_pt,i,j)
          neo_coeffs_inter(ix,is,-1,i,j)=neo_coeffs_inter(ix,is,nsg_pt-1,i,j)
         
        enddo
      enddo
    enddo
  enddo  
  if(root_processor)close(freelun) 

  !Then interpolate the electrostatic potential
  do ix=1,neo_n_radial

    call interpquad(neo_theta,neophi_G(ix,:), &
       & neo_n_theta+1,nsg_pt,dum2,dum)

    do k =1,nsg_pt
      neophi_inter(ix,k)=dum(k)
    enddo

    neophi_inter(ix,nsg_pt+1)=neophi_inter(ix,1)
    neophi_inter(ix,nsg_pt+2)=neophi_inter(ix,2)

    neophi_inter(ix,0)=neophi_inter(ix,nsg_pt)
    neophi_inter(ix,-1)=neophi_inter(ix,nsg_pt-1) 
  enddo
  
endsubroutine neointerp

subroutine neoequil_init_grids
 
  use mpiinterface, only : root_processor

  !Allocates and reads data from NEO output
  call read_neo
  call read_neo_phi
  !Interpolation required in the theta direction as the grids
  !do not match up
  call neointerp  
  !Reconstructs the distribution function and transforms into
  !GKW velocity space
  call reconstruct_f
  !Normalises all distribution fuction etc
  call neof_renormalise

  !Radial gradient is calculated here
  call cal_gradients 
   
  !Deallocate the arrays that are used in reading and transforming.
  call neoequil_deallocate

  if(root_processor)write(*,*) 'NEO data successfully read and transformed'

end subroutine neoequil_init_grids



end module neoequil
