!------------------------------------------------------------------------------
! SVN:$Id$
!> This diagnostic performs a wavelet analysis with respect to time
!> scales.
!------------------------------------------------------------------------------
module diagnos_freq_wavelet

  implicit none

  private

  !>==== DESCRIPTION ==================================================
  !>
  !> This diagnostic performs a wavelet analysis with respect to time
  !> scales.
  !> The underlying algorithm is very simple and the cost scale like O(N).
  !>
  !>
  !>---- FURTHER NOTES ------------------------------------------------
  !>
  !> 
  !> 
  !>
  !>---- LIMITATIONS --------------------------------------------------
  !>
  !> In the present form, the shape of the wavelet cannot be chosen, but
  !> is fixed to the half-cycle square-wave wavelet.
  !>
  !> This diagnostic produces *wrong* results, if the timestep is refined
  !> by any of the timestep estimators of GKW.
  !>
  !> This diagnostic (at least in the present form) cannot be continued
  !> without loosing information. In other words, the diagnostic data
  !> of one long run will differ from the data of two consecutive short
  !> runs - the latter have more undefined regions.
  !> Note also that the analysis/plotting script has to be capable of
  !> dealing with restarted runs in that form, if you really want that.
  !>
  !> At the moment the remaining values in avg_history are not output.
  !> Therefore, a complete reconstruction of the signal is not
  !> possible, if the signal length does not happen to be a power of
  !> two. This will be fixed.
  !>
  !>===================================================================

  public :: set_default_nml_values, init, bcast, check, allocate_mem
  public :: finalize
  public :: initial_output, final_output
  public :: calc_smallstep
  public :: output

  public :: feed_potential
  public :: wavelet_forward_trafo_stepwise
  public :: wavelet_forward_stepwise_totavg

  !> The general on/off switch for this diagnostic
  logical, save, public :: lfreq_wavelet

  !> global indices of the point where the timeseries is picked
  integer, save :: waveletpoint_iyg
  integer, save :: waveletpoint_ixg
  integer, save :: waveletpoint_isg
  integer, save :: waveletpoint_ispg

  !> The logical unit number, used to output data
  integer :: lun_freq_wavelet

  public :: wavelet_trafo_data
  type :: wavelet_trafo_data
    !> A counter for the current element index. This counter increases by one
    !> each time this diagnostic is feeded one value.
    !> (so it should equal ntime*naverage+iloop, if this diagnostic is called
    !> at every small timestep)
    integer :: ielem = 0
    
    ! unfortunately this is Fortran 2003:
    !integer, dimension(:), allocatable :: avg_history
    ! this is Fortran 95:
    real, dimension(:), pointer :: avg_history
    
    !> a buffer, to temporarily store the calculated trafo values, until
    !> they are output by IO
    integer :: nelem_trafo_buf = 0
    real, dimension(:), pointer :: trafo_buf
  end type wavelet_trafo_data

  type(wavelet_trafo_data), save :: trafo_data

  logical, save :: is_responsible_proc

  integer, parameter :: FORWARD = 1
  integer, parameter :: INVERSE = -1

contains

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()

    lfreq_wavelet = .false.

  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !> Broadcast all namelist items of this diagnostic to all processes.
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(lfreq_wavelet,1)
  end subroutine bcast

  !--------------------------------------------------------------------
  !> Check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use general, only : gkw_warn
    use control, only : normalized

    if(.not.lfreq_wavelet) return

    if(normalized) then
      call gkw_warn('The diagnos_freq_wavelet diagnostic cannot yet deal &
         & with normalization.')
      lfreq_wavelet = .false.
    end if

  end subroutine check

  !--------------------------------------------------------------------
  !> Initialize the diagnostic. This is the place to open logical
  !> units.
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use io, only : open_real_lu, ascii_fmt
    use io, only : attach_metadata
    use io, only : phys_unit_key, description_key, comments_key, not_avail
    use diagnos_generic, only : attach_metadata_grid
    use grid, only : n_x_grid, n_s_grid, nmod

    if(.not.lfreq_wavelet) return

    waveletpoint_iyg = int((nmod - 1)*2 / 2)
    waveletpoint_ixg = int(n_x_grid/2)
    waveletpoint_isg = int(n_s_grid/2)
    waveletpoint_ispg = 1
    
    if(root_processor) then
      write (*,*) 'The wavelet diagnostic picks y/x/s/sp = ', &
         & waveletpoint_iyg, waveletpoint_ixg, waveletpoint_isg, &
         & waveletpoint_ispg
    end if

    trafo_data%nelem_trafo_buf = 0
    
    if(root_processor) then
      ! open logical units
      call open_real_lu('phi_time_wavelet', &
         & 'diagnostic/diagnos_freq_wavelet', &
         & (/ 1 /), &
         & ascii_fmt, lun_freq_wavelet)
      call attach_metadata_grid(lun_freq_wavelet, &
         & 'wavelet space (see source code)', ascii_fmt)
      call attach_metadata(lun_freq_wavelet, phys_unit_key, not_avail, &
         & ascii_fmt)
         !& 'T_{ref}/e'
      call attach_metadata(lun_freq_wavelet, description_key, &
         & 'Wavelet transformation with respect to time of the &
         & electrostatic potential.', ascii_fmt)
      call attach_metadata(lun_freq_wavelet, comments_key, &
         & 'The precise structure of this dataset is described in the&
         & source code of the &
         & diagnos_freq_wavelet diagnostic.', ascii_fmt)
      call attach_metadata(lun_freq_wavelet, 'waveletpoint_iyg', &
         & waveletpoint_iyg, ascii_fmt)
      call attach_metadata(lun_freq_wavelet, 'waveletpoint_ixg', &
         & waveletpoint_ixg, ascii_fmt)
      call attach_metadata(lun_freq_wavelet, 'waveletpoint_isg', &
         & waveletpoint_isg, ascii_fmt)
      call attach_metadata(lun_freq_wavelet, 'waveletpoint_ispg', &
         & waveletpoint_ispg, ascii_fmt)
      
    end if

  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use general, only : gkw_abort
    use control, only : naverage, ntime
    use grid, only : lrx, ls, proc_subset
    integer :: i, ierr

    if(.not.lfreq_wavelet) return

    ! The process which does the computation is the one which holds a
    ! certain global iyg, ixg, isg
    is_responsible_proc = proc_subset(waveletpoint_ixg, waveletpoint_isg, &
       & 1, 1, 1)
    
    ! find the power of two which is smallereq to naverage*ntime
    i = 1
    find_pow2: do while(.true.)
      if(i*2 > naverage*ntime) then
        exit find_pow2
      else
        i = i*2
      end if
    end do find_pow2

    if(is_responsible_proc) then
      ! the process which does all the calculations, has to allocate
      !its arrays.
      allocate(trafo_data%avg_history(0:i), stat=ierr)
      !allocate(trafo_data%avg_history(0:6), stat=ierr) ! for testing purposes
      if (ierr /= 0) &
         & call gkw_abort('diagnos_freq_wavelet :: could not allocate avg_history')
      trafo_data%avg_history = 0.0
    end if

    ! in order to send the calcated data to the root process, every
    ! process needs an array. Then the data is sent via a
    ! mpireduce_sum.  (trafo_buf is actually way too large and could
    ! be made much smaller, but I have not taken the time to find the
    ! formula for the max. number of elements)
    allocate(trafo_data%trafo_buf(i*naverage), stat=ierr)
    !allocate(trafo_data%trafo_buf(6), stat=ierr) ! for testing purposes
    if (ierr /= 0) &
       & call gkw_abort('diagnos_freq_wavelet :: could not allocate trafo_buf')

  end subroutine allocate_mem


  !--------------------------------------------------------------------
  !> Clean up, deallocate, close everything.
  !--------------------------------------------------------------------
  subroutine finalize()
    use io, only : close_lu, ascii_fmt
    use mpiinterface, only : root_processor

    if(.not.lfreq_wavelet) return
    
    ! deallocate all arrays of this diagnostic
    if(associated(trafo_data%avg_history)) deallocate(trafo_data%avg_history)
    if(associated(trafo_data%trafo_buf)) deallocate(trafo_data%trafo_buf)

    if(root_processor) then
      ! be nice and close all logical units
      call close_lu(lun_freq_wavelet, ascii_fmt)
    end if
  end subroutine finalize

  !--------------------------------------------------------------------
  !> This routine is called at the beginning of each run (after
  !> restarts, too).
  !--------------------------------------------------------------------
  subroutine initial_output()
    ! use control, only : naverage, ntime
    ! use marsaglia, only : ran_kiss
    ! use io, only : output_array, xy_fmt, ascii_fmt, append_chunk
    ! use mpiinterface, only : root_processor
    ! use mpiinterface, only : mpiallreduce_sum_inplace, mpireduce_sum_inplace
    ! real, dimension(:), allocatable :: arr

    ! integer :: i,j
    ! real :: r
    ! integer, parameter :: intervall = 8

    ! if(.not.lfreq_wavelet) return
    
    ! !!!! TEST !!!!!!!!!!!!!
    ! ! note that the wavelet_trafo routine expects an array of size 2**n
    ! ! while the _trafo_stepwise mechanism does not.

    ! is_responsible_proc = root_processor
    
    ! allocate(arr(2**6))
    ! arr = 0
    ! ! generate test data
    ! i = 1
    ! do while(i + intervall <= size(arr))
    !   r = ran_kiss()
    !   arr(i:i+intervall) = r*10-5
    !   i = i + intervall
    ! end do
    ! do i = 1,size(arr)
    !   r = ran_kiss()
    !   arr(i) = arr(i) + r*1-0.5
    ! end do

    ! if(root_processor) then
    !   call output_array('test_wavelet_orig', 'diagnostic/diagnos_freq_wavelet', &
    !      & arr, 'F', xy_fmt, ascii_fmt)

    !   call wavelet_trafo(arr, FORWARD)
    !   call output_array('test_wavelet_trafo', 'diagnostic/diagnos_freq_wavelet', &
    !      & arr, 'F', xy_fmt, ascii_fmt)

    !   call wavelet_trafo(arr, INVERSE)
    !   call output_array('test_wavelet_inverse', 'diagnostic/diagnos_freq_wavelet', &
    !      & arr, 'F', xy_fmt, ascii_fmt)
    ! end if

    ! do j = 1, size(arr)
    !   if(root_processor) then
    !     call wavelet_forward_trafo_stepwise(arr(j), trafo_data)
    !   else
    !     trafo_data%trafo_buf = 0.0
    !   end if
    
    !   ! As long as IO is serial:
    !   ! broadcast the integer which tells how
    !   ! many elements were stored into the trafo_buf
    !   call mpiallreduce_sum_inplace(trafo_data%nelem_trafo_buf, 1)
    
    !   ! send the diagnostic data to root, so that root can output it
    !   call mpireduce_sum_inplace( &
    ! & trafo_data%trafo_buf(1:trafo_data%nelem_trafo_buf), q
    ! & (/ trafo_data%nelem_trafo_buf /))
    
    !   if(root_processor) then
    !     do i = 1, trafo_data%nelem_trafo_buf
    !       call append_chunk(lun_freq_wavelet, trafo_data%trafo_buf(i:i), &
    ! & xy_fmt, ascii_fmt)
    !     end do
    !   end if
    
    !   trafo_data%nelem_trafo_buf = 0
    ! end do
    
    
    ! deallocate(arr)

  end subroutine initial_output


  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine feed_potential(fdis)
    use grid, only : ls, proc_subset
    use dist, only : phi, get_phi, nsolc
    use diagnos_generic, only : inv_fft_xy_slice, r_xy
    complex, intent(in) :: fdis(nsolc)

    if(.not.lfreq_wavelet) return
    
    ! must call from all x processors to avoid deadlock
    if(proc_subset(0,waveletpoint_isg,1,1,1)) then
      call get_phi(fdis,phi)
      ! Get the real space array via the inverse FFT.
      call inv_fft_xy_slice(phi(:,:,ls(waveletpoint_isg)), r_xy)
    end if
    is_responsible_proc = proc_subset(waveletpoint_ixg,waveletpoint_isg,1,1,1)
    call feed_one_value(r_xy(waveletpoint_iyg, waveletpoint_ixg), &
       & is_responsible_proc)
    
    
  end subroutine feed_potential

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine feed_density
    use grid, only : ls, lsp, proc_subset
    use grid, only : nmod, nx, nmu, nvpar
    use dist, only : fdis => fdisi
    use velocitygrid, only : intvp,intmu,vpgr,mugr
    use geom, only : bn
    use diagnos_generic, only : inv_fft_xy_slice, c_xy, r_xy
    use matdat, only : get_f_from_g
    use mpiinterface, only : mpiallreduce_sum_inplace
    use mpicomms, only : COMM_VPAR_NE_MU_NE
    
    integer :: imod, ix, j, k
    complex :: dens, fdisi
    real :: integrand

    if(.not.lfreq_wavelet) return
    
    ! must call from all x, nvpar and mu processors
    if(proc_subset(0,waveletpoint_isg,0,0,waveletpoint_ispg)) then
      ! get a local perpendicular density slice
      ! loop over the slice points for this species
      local_slice : do imod=1,nmod
        do ix=1,nx

          ! integrate over the velocity space for this point
          dens=(0.E0,0.E0)
          velocity_space : do j=1,nmu
            do k=1,nvpar
              integrand = 1.E0

              fdisi = get_f_from_g(imod,ix,ls(waveletpoint_isg), &
                 & j,k,lsp(waveletpoint_ispg),fdis)
              dens = dens + integrand*bn(ix,ls(waveletpoint_isg)) &
                 & * intvp(ls(waveletpoint_isg),j,k,lsp(waveletpoint_ispg)) &
                 & * intmu(j)*fdisi
            end do
          end do velocity_space
          ! store the density in a temporary slice for reduction later
          c_xy(imod,ix) = dens
        end do
      end do local_slice
      ! sum the slice over other processors if necessary
      call mpiallreduce_sum_inplace(c_xy(1:nmod,1:nx),(/nmod,nx/), &
         & COMM_VPAR_NE_MU_NE)

      ! Get the real space array via the inverse FFT.
      call inv_fft_xy_slice(c_xy(:,:), r_xy)
    end if
    is_responsible_proc = proc_subset(waveletpoint_ixg,waveletpoint_isg,1,1, &
       & waveletpoint_ispg)
    call feed_one_value(r_xy(waveletpoint_iyg, waveletpoint_ixg), &
       & is_responsible_proc)
    
    
  end subroutine feed_density

  
  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine feed_one_value(val, is_responsible_proc)
    real, intent(in) :: val
    logical, intent(in) :: is_responsible_proc

    if(.not.lfreq_wavelet) return

    if(is_responsible_proc) then
      !write (*,*) "FEED FREQWAVELET:", val
      call wavelet_forward_trafo_stepwise(val, trafo_data)
    else
      trafo_data%trafo_buf = 0.0
    end if

  end subroutine feed_one_value

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine calc_smallstep(i_smallstep)
    ! use dist, only : fdisi, iphi
    ! use index_function, only : indx
    ! use grid, only : lrx, ls, proc_subset, nmod, nx
    ! use mpiinterface, only : mpiallreduce_sum_inplace
    ! use mpicomms, only : COMM_X_NE
    ! use diagnos_generic, only : parseval_correction
    integer, intent(in) :: i_smallstep
    ! integer :: imod, ix

    ! real :: val
    ! complex :: phi

    ! if(.not.lfreq_wavelet) return

    ! CODE GRAVEYARD:
    ! select case(VERSION)
    ! case (1)
    !   ! VERSION 1: Pick a certain global imod, ixg, isg
    !   ! But as the field is not in real-space representation this does
    !   ! not make much sense, probably.
    !   if(is_responsible_proc) then
    !     phi = fdisi(indx(iphi,int(waveletpoint_iyg/2), &
    !        & lrx(waveletpoint_ixg), ls(waveletpoint_isg)))
    !     val = abs(phi)
    !     ! now only that particular process has the diagnostic data.
    !   end if
    !   call feed_one_value(val, is_responsible_proc)
    ! case(2)
    !   ! VERSION 2: Integrate over the perp slice
    !   ! However, this probably averages out the interesting
    !   ! dynamics in the timeseries.
    !   phi = 0
    !   if(proc_subset(0, waveletpoint_isg, 1, 1, 1)) then
    !     ! work on processes with arbitrary ixg, but a particular isg,
    !     ! and velocityspace coordinates and species index equal to one.
    !     do imod=1,nmod
    !       do ix=1,nx
    !         phi = phi + parseval_correction(imod) &
    !            & * abs(fdisi(indx(iphi,imod, ix, ls(waveletpoint_isg))))**2 
    !       end do
    !     end do
    !     call mpiallreduce_sum_inplace(val,1,COMM_X_NE)
    !   end if

    !   ! just one of the processes which took part in the integration
    !   ! now needs to feed the wavelet trafo
    !   call feed_one_value(val, is_responsible_proc)
    ! end select

  end subroutine calc_smallstep

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output()
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use mpiinterface, only : mpiallreduce_sum_inplace, mpireduce_sum_inplace
    use mpiinterface, only : root_processor

    integer :: i

    if(.not.lfreq_wavelet) return
    
    ! As long as IO is serial:
    ! broadcast the integer which tells how
    ! many elements were stored into the trafo_buf
    call mpiallreduce_sum_inplace(trafo_data%nelem_trafo_buf, 1)

    ! send the diagnostic data to root, so that root can output it
    call mpireduce_sum_inplace( &
       & trafo_data%trafo_buf(1:trafo_data%nelem_trafo_buf), &
       & (/ trafo_data%nelem_trafo_buf /))

    if(root_processor) then
      do i = 1, trafo_data%nelem_trafo_buf
        call append_chunk(lun_freq_wavelet, trafo_data%trafo_buf(i:i), &
           & xy_fmt, ascii_fmt)
      end do
    end if

    trafo_data%nelem_trafo_buf = 0
    
  end subroutine output

  !--------------------------------------------------------------------
  !> output the total average as the very last value of the dataset.
  !--------------------------------------------------------------------
  subroutine final_output()
    use io, only : xy_fmt, ascii_fmt, append_chunk
    use mpiinterface, only : root_processor, mpireduce_sum_inplace

    real :: total_average

    if(.not.lfreq_wavelet) return
    
    if(is_responsible_proc) then
      call wavelet_forward_stepwise_totavg(trafo_data, total_average)
    else
      total_average = 0.0
    end if
    ! send the value to root
    call mpireduce_sum_inplace(total_average)

    if(root_processor) then
      !write (*,*) 'freq_wavelet: total_average =', total_average
      call append_chunk(lun_freq_wavelet, (/ total_average /), &
         & xy_fmt, ascii_fmt)
    end if
    
  end subroutine final_output

  
  !--------------------------------------------------------------------
  !> Counts the amount of twos among the prime factors of a given
  !> integer.
  !--------------------------------------------------------------------
  function get_number_of_2_divisors(n)
    integer, intent(in) :: n
    integer :: get_number_of_2_divisors
    integer :: n_

    n_ = n
    
    get_number_of_2_divisors = 0
    do while(mod(n_,2) == 0)
      n_ = n_ / 2
      get_number_of_2_divisors = get_number_of_2_divisors + 1
    end do

  end function get_number_of_2_divisors


  !--------------------------------------------------------------------
  !> Performs a forward wavelet transformation.
  !> This routine is supposed to be fed with one value after the other
  !> of the timeseries under consideration.
  !>
  !> The total array length (== the total number of calls)
  !> does *not* necessarily have to be a power of 2.
  !>
  !> At each call of this routine, as many wavelet coefficients as
  !> possible are computed and stored into trafo_buf.
  !> The number of computed elements is returned via nelem_trafo_buf.
  !>
  !> This algorithm yields a sequence of coefficients, associated to a
  !> frequency f_Ny/level  (where f_Ny is the Nyquist frequency).
  !> Note that one value is missing: The total average.
  !> The total average can be computed with a separate routine, see below.
  !> 
  !> The sequence of the integer 'level' numbers is not totally trivial, but
  !> can be computed easily with some GNU Octave/Matlab:
  !> 
  !> levels = [1];
  !> while(length(levels) < data_length)
  !>   levels = [levels levels];
  !>   levels(end+1) = levels(end) + 1;
  !> end
  !> % and then take only the first N values, if the real space representation
  !> % had N elements.
  !>
  !--------------------------------------------------------------------
  subroutine wavelet_forward_trafo_stepwise(val, trafo_dat)
    real, intent(in) :: val
    type(wavelet_trafo_data), intent(inout) :: trafo_dat

    real :: avg,diff
    integer :: level, l

    trafo_dat%ielem = trafo_dat%ielem + 1

    ! calculate the number of twos among the prime factors of elem
    level = get_number_of_2_divisors(trafo_dat%ielem)
      
    if(level == 0) then
      ! if the call counter is odd, then we just have to save the
      ! current value for the next step
      avg = val
    else
      ! the call counter is even

      do l = 1, level
        if(l == 1) then
          avg = val
        end if
        diff =  (trafo_dat%avg_history(l-1) - avg) / 2
        ! diff contains now the level l trafo value, and can be output
        
        avg =  (trafo_dat%avg_history(l-1) + avg) / 2

        ! put the value into the buffer that will be sent to root later
        trafo_dat%nelem_trafo_buf = trafo_dat%nelem_trafo_buf + 1
        trafo_dat%trafo_buf(trafo_dat%nelem_trafo_buf) = diff
      end do
    end if
    trafo_dat%avg_history(level) = avg

  end subroutine wavelet_forward_trafo_stepwise

  !----------------------------------------------------------------------------
  !> Computes the average of all values which were fed to
  !> wavelet_forward_trafo_stepwise.
  !----------------------------------------------------------------------------
  subroutine wavelet_forward_stepwise_totavg(trafo_dat, total_average)
    type(wavelet_trafo_data), intent(in) :: trafo_dat
    real, intent(out) :: total_average

    integer :: last_level, l
    
    last_level = get_number_of_2_divisors(trafo_dat%ielem)

    ! compute the total averages as a weighted average of the elements
    ! in avg_history
    do l = last_level, ubound(trafo_dat%avg_history,1)
      total_average = (2**l)*trafo_dat%avg_history(l)
    end do
    total_average = total_average / trafo_dat%ielem

  end subroutine wavelet_forward_stepwise_totavg

  !----------------------------------------------------------------------------
  !> Performs forward or inverse wavelet transformation of
  !> a whole array, in place.
  !>
  !> This routine can only deal with array sizes which are a power of 2 .
  !>
  !> The following FORTRAN routine performs wavelet decomposition and
  !> reconstruction. It has been written based on hints from Strang's
  !> article. The half-cycle square-wave wavelet requires no
  !> trigonometric functions. Wavelet basis functions are recursively
  !> computed from previous iterations. The progression in this code is
  !> from Nyquist wavelengths to DC. Each pass is half the the length of
  !> the previous pass, totaling order n computations.
  !>
  !> http://sepwww.stanford.edu/public/docs/sep65/rick2/paper_html/node2.html
  !----------------------------------------------------------------------------
  subroutine wavelet_trafo(x,direction)
    !> n is length of x, power of two
    integer, intent(in) :: direction
    real, dimension(:), intent(inout) :: x

    integer :: m, i, nelem
    
    !> a permutation buffer
    real, allocatable :: y(:)

    nelem = size(x)

    if(.not. allocated(y)) then
      allocate(y(nelem))
    end if

    if (direction == FORWARD) then
      ! forward transformation.

      ! power of 2 passes, each half as long
      m = nelem
      ! butterfly
      do while (m .gt. 1)
        do i = 1, m, 2
          y(i) = (x(i) + x(i+1)) / 2
          y(i+1) = (x(i) - x(i+1)) / 2
        enddo
        ! permutation
        m = m / 2
        do i = 1, m
          x(i) = y(i+i-1)
          x(m+i) = y(i+i)
        enddo
      end do
    elseif(direction == INVERSE) then
      ! inverse transformation.
      
      ! power of 2 passes, each twice as long
      m = 1
      do while(m .lt. nelem)
        ! permutation
        do i=1,m
          y(i+i-1) = x(i)
          y(i+i) = x(m+i)
        enddo
        m = m + m
        ! butterfly
        do i=1,m,2
          x(i) = (y(i) + y(i+1))
          x(i+1) = (y(i) - y(i+1))
        enddo
      end do
    endif

    deallocate(y)
    
  end subroutine wavelet_trafo

end module diagnos_freq_wavelet
