!-------------------------------------------------------------------------------
! SVN:$Id$
!> Module that defines and stores structures used in GKW.
!-------------------------------------------------------------------------------
module structures

  implicit none

  private

  public :: matrix_element, ghostcomm
  
  !> Element datatype
  type :: matrix_element
    integer :: imod   !< the toroidal mode number 
    integer :: ix     !< the radial grid point 
    integer :: i      !< the grid point along the field 
    integer :: j      !< the grid point in mu 
    integer :: k      !< the grid point in vpar 
    integer :: is     !< the grid point in the species
    complex :: val    !< the value of the element
    integer :: imloc  !< the desired toroidal mode number 
    integer :: ixloc  !< the desired location along the radial direction
    integer :: iloc   !< the desired location along the s-direction
    integer :: jloc   !< the desired location along the mu-direction
    integer :: kloc   !< the desired location along the vpar-direction
    integer :: isloc  !< the desired species index 
    ! The following components have default values which must 
    ! be overwritten in the prototype element before register_term 
    ! is called; matdat checks against these defaults.
    ! These numbers must NOT overlap with identifier ranges in dist.
    integer :: itype = -300
                      !< type of the iih index (ifdis,iphi,iapar,ibpar,i_mom)
    integer :: itloc = -200
                      !< type of the jjh index (ifdis,iphi,iapar,ibpar,i_mom)
    integer :: ideriv = -100
                      !< order of derivative (0,1,2,4) for timestep estimate
                      !< (NOT the order of the differential scheme)
    !> the name of the term that put the element
    character(len=64) :: term =  ''
    !> Set true once term is registered with matdat
    logical :: registered = .false.
    ! labels for special elements
    logical :: outflow = .false.  !< outflow elements for energetics diagnostic
  end type matrix_element

  !> structure for storing MPI persistent communication information
  !> this is used in exp integration for communication of fdisi ghost cells
  !> allocatable attribute is not allowed in fortan95 inside structures
  type :: ghostcomm
    integer :: nreq = 0                            !< number of requests total
    integer :: irequest = 0                        !< number of requests counter
    integer, pointer, dimension (:) :: preq        !< status returned by mpiwait
    !integer, allocatable, dimension (:,:) :: pstat !< requests labels
    logical :: running = .false.                   !< true only when running
    logical :: initialised = .false.               !< true only when initialised
  end type


  ! WHY ARE THESE HERE AND NOT IN INDEX_FUNCTION OR DIST ?
  integer, parameter :: ierr_UNDEFINED = -34266234
  integer, parameter :: ierr_OK         = 0
  integer, parameter :: ierr_BAD_ALL    = 11
  integer, parameter :: ierr_BAD_S      = 21
  integer, parameter :: ierr_BAD_VPAR   = 22
    
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

end module structures
