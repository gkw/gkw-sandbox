!------------------------------------------------------------------------------
! SVN:$Id$
!> This diagnostic performs a wavelet analysis with respect to
!> spatial scales.
!>
!> == Limitations ==
!> In the present form, the shape of the wavelet cannot be chosen, but
!> is fixed to the half-cycle square-wave wavelet.
!>
!> At the moment the remaining values in avg_history are not output.
!> Therefore, a complete reconstruction of the signal is not
!> possible, if the signal length does not happen to be a power of two.
!------------------------------------------------------------------------------
module diagnos_spat_wavelet
  use diagnos_freq_wavelet, only : wavelet_trafo_data

  implicit none

  private

  public :: set_default_nml_values, init, bcast, check, allocate_mem
  public :: finalize
  public :: initial_output, final_output
  public :: output

  !> The general on/off switch for this diagnostic
  logical, save, public :: lspat_wavelet

  !> global indices of the coordinates where the data is picked
  integer, save :: waveletpoint_iyg
  !integer, save :: waveletpoint_ixg
  integer, save :: waveletpoint_isg
  integer, save :: waveletpoint_ispg

  !> The logical unit number, used to output data
  integer :: lun_spat_wavelet

  type(wavelet_trafo_data), save :: trafo_data

contains

  !--------------------------------------------------------------------
  !> 
  !> 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    lspat_wavelet = .false.
  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !> Broadcast all namelist items of this diagnostic to all processes.
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(lspat_wavelet,1)
  end subroutine bcast

  !--------------------------------------------------------------------
  !> Check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use general, only : gkw_warn

    if(.not. lspat_wavelet) return
    
  end subroutine check

  !--------------------------------------------------------------------
  !> Initialize the diagnostic. This is the place to open logical
  !> units.
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use io, only : open_real_lu, ascii_fmt, attach_metadata
    use io, only : description_key, comments_key, phys_unit_key, not_avail
    use diagnos_generic, only : attach_metadata_grid
    use grid, only : nmod, n_s_grid

    if(.not. lspat_wavelet) return
    waveletpoint_iyg = int((nmod - 1)*2 / 2)
    !waveletpoint_ixg = int(n_x_grid/2)
    waveletpoint_isg = int(n_s_grid/2)
    waveletpoint_ispg = 1
    
    if(root_processor) then
      ! call open_real_lu('phi_spat_wavelet_y'//int2char(waveletpoint_iyg// &
      !    & '_s'//int2char(waveletpoint_isg)// &
      !    & '_sp'//int2char(waveletpoint_ispg), &
      call open_real_lu('phi_spat_wavelet', &
         & 'diagnostic/diagnos_spat_wavelet', shape(trafo_data%trafo_buf), &
         & ascii_fmt, lun_spat_wavelet)
      call attach_metadata_grid(lun_spat_wavelet, &
         & 'time', 'wavelet space (see source code)', ascii_fmt)
      ! the unit in terms of reference quantities, should be valid LaTeX:
      call attach_metadata(lun_spat_wavelet, phys_unit_key, &
         & not_avail, ascii_fmt)
      call attach_metadata(lun_spat_wavelet, description_key, &
         & 'Wavelet transformation with respect ot space of the &
         & electrostatic potential along a single radial coordinate line, as &
         & a function of time.', &
         & ascii_fmt)
      ! and some (more informal) comments there, if you want:
      call attach_metadata(lun_spat_wavelet, comments_key, &
         & 'The precise structure of this dataset is described in the&
         & source code of the &
         & diagnos_freq_wavelet diagnostic.', ascii_fmt)
      ! and you may attach even more infos:
      call attach_metadata(lun_spat_wavelet, 'waveletpoint_iyg', &
         & waveletpoint_iyg, ascii_fmt)
      ! call attach_metadata(lun_spat_wavelet, 'waveletpoint_ixg', &
      !    & waveletpoint_ixg, ascii_fmt)
      call attach_metadata(lun_spat_wavelet, 'waveletpoint_isg', &
         & waveletpoint_isg, ascii_fmt)
      call attach_metadata(lun_spat_wavelet, 'waveletpoint_ispg', &
         & waveletpoint_ispg, ascii_fmt)
    end if
    
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use general, only : gkw_abort
    use grid, only : n_x_grid
    integer :: ierr
    integer, dimension(:), allocatable :: tmp
    integer :: arrsize
    integer :: i, ones, nelem

    if(.not. lspat_wavelet) return
    ! compute the size of the buffer for the trafo in x-direction
    arrsize = n_x_grid
    i = 1
    ones = 1
    do while(ones < arrsize/2)
      i = i*2 + 1
      ones = ones * 2
    end do

    !write(*,*) "size of tmp(:) = ", i
    allocate(tmp(i), stat=ierr)
    if (ierr /= 0) &
       & call gkw_abort('diagnos_spat_wavelet :: could not allocate tmp')

    tmp(1) = 1
    nelem = 1
    do while(nelem < size(tmp,1)-1)
      do i = 1,nelem
        tmp(nelem+i) = tmp(i)
        !write(*,*) "[i,nelem] = ",i, nelem
      end do
      tmp(nelem*2+1) = tmp(nelem*2) + 1
      nelem = nelem*2+1
      !write(*,*) "nelem = ", nelem
      !write(*,*) "--- "
    end do
    !write(*,*) "tmp = ", tmp

    ! find the arrsize+1'th occurrence of a 1 in tmp.
    i = 0
    ones = 0
    do while(ones < arrsize/2+1 .and. i < size(tmp))
      i = i+1
      if(tmp(i) == 1) then
        ! increment the ones counter
        ones = ones + 1
      end if
    end do
    ! so i-1 is the size of the transform of a arrsize array.
    ! and together with the total average value, this is i values.
    !write(*,*) "i=", i, "ones=", ones

    deallocate(tmp)
    !write(*,*) "final nelem = ", nelem
    ! this means we are going to produce nelem elements when doing the
    ! trafo of the array.
    allocate(trafo_data%trafo_buf(i),stat=ierr)
    if (ierr /= 0) &
       & call gkw_abort('diagnos_spat_wavelet :: could not allocate trafo_buf')


    ! find the power of two which is smallereq than arrsize
    i = 1
    find_pow2: do while(.true.)
      if(i*2 > arrsize) then
        exit find_pow2
      else
        i = i*2
      end if
    end do find_pow2

    allocate(trafo_data%avg_history(0:i),stat=ierr)
    if (ierr /= 0) &
       & call gkw_abort('diagnos_spat_wavelet :: could not allocate xtrafo_buf')
    
  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !> Clean up, deallocate, close everything.
  !--------------------------------------------------------------------
  subroutine finalize()
    use io, only : close_lu, ascii_fmt

    if(.not. lspat_wavelet) return
    
    ! deallocate all arrays of this diagnostic
    if(associated(trafo_data%trafo_buf)) deallocate(trafo_data%trafo_buf)
    if(associated(trafo_data%avg_history)) deallocate(trafo_data%avg_history)

    ! be nice and close all logical units
    call close_lu(lun_spat_wavelet, ascii_fmt)
  end subroutine finalize

  !--------------------------------------------------------------------
  !> This routine is called at the beginning of each run (after
  !> restarts, too).
  !--------------------------------------------------------------------
  subroutine initial_output()

    if(.not. lspat_wavelet) return

  end subroutine initial_output

  !--------------------------------------------------------------------
  !> In contrast to the subroutine output(), the argument of
  !> the final_output() subroutine is optional.
  !> The number is provided if the code wants to print the result of
  !> an eigenmode calculation.
  !> If the GKW run is explicit (or implicit) then the number argument
  !> is not provided.
  !--------------------------------------------------------------------
  subroutine final_output(number)
    integer, intent(in), optional :: number

    if(.not. lspat_wavelet) return
    
    if (present(number)) then
      ! output for every eigenmode
    else
      ! one single output
    end if

  end subroutine final_output

  !--------------------------------------------------------------------
  !> The routine calc_largestep() is to be called repeatedly, after every
  !> large timestep, and here the diagnostic should calculate its
  !> quantities.
  !--------------------------------------------------------------------
  subroutine calc_largestep()
    use grid, only : ls, proc_subset, n_x_grid
    use dist, only : fdisi, phi, get_phi
    use diagnos_generic, only : inv_fft_xy_slice, r_xy
    use mpiinterface, only : mpiallreduce_sum_inplace, mpireduce_sum_inplace
    logical :: is_responsible_proc

    if(.not. lspat_wavelet) return
    
    is_responsible_proc = proc_subset(1, waveletpoint_isg, 1,1,1)

    if(.not. is_responsible_proc) then
      trafo_data%trafo_buf = 0
    end if
    
    if(proc_subset(0,waveletpoint_isg,1,1,1)) then
      call get_phi(fdisi,phi)
      ! Get the real space array via the inverse FFT.
      call inv_fft_xy_slice(phi(:,:,ls(waveletpoint_isg)), r_xy)
      ! let one of the procs work the transformation
      if(is_responsible_proc) then
        ! fill the rest of the array with the trafo values
        call wavelet_trafo_stepwise_arr(r_xy(waveletpoint_iyg, 1:n_x_grid), &
           & trafo_data, trafo_data%trafo_buf)
        
        ! and put the mean value into the last cell
        ! trafo_data%trafo_buf(size(trafo_data%trafo_buf)) = &
        !    & sum(r_xy(waveletpoint_iyg, :))/n_x_grid
      end if
    end if

    ! As long as IO is serial:
    ! send the diagnostic data to root, so that root can output it
    call mpireduce_sum_inplace( &
       & trafo_data%trafo_buf, &
       & shape(trafo_data%trafo_buf))
    
  end subroutine calc_largestep

  !--------------------------------------------------------------------
  !> Using the value-wise wavelet transformation routine of
  !> diagnos_freq_wavelet, transform an array which is known as a whole
  !--------------------------------------------------------------------
  subroutine wavelet_trafo_stepwise_arr(arr, trafo_dat, arr_trafo)
    use diagnos_freq_wavelet, only : wavelet_forward_trafo_stepwise
    use diagnos_freq_wavelet, only : wavelet_forward_stepwise_totavg
    use general, only : gkw_warn
    real, dimension(:), intent(in) :: arr
    type(wavelet_trafo_data), intent(inout) :: trafo_dat
    real, dimension(:), intent(out) :: arr_trafo
    integer :: i
    trafo_dat%nelem_trafo_buf = 0
    trafo_dat%ielem = 0
    !write (*,*) "transform array of size ", size(arr)
    do i = 1, size(arr)
      call wavelet_forward_trafo_stepwise(arr(i), trafo_dat)
      !write(*,*) "trafo...", i, trafo_dat%ielem, trafo_dat%nelem_trafo_buf
    end do
    ! and append the mean value. this should be the last cell of the
    ! buffer array then.
    trafo_dat%nelem_trafo_buf = trafo_dat%nelem_trafo_buf +1
    if(trafo_dat%nelem_trafo_buf /= size(trafo_dat%trafo_buf)) then
      !write(*,*) trafo_dat%nelem_trafo_buf, size(trafo_dat%trafo_buf)
      call gkw_warn("diagnos_spat_wavelet: This should not be the case! ERROR")
    end if
    
    call wavelet_forward_stepwise_totavg(trafo_dat, &
       & trafo_dat%trafo_buf(trafo_dat%nelem_trafo_buf))

    arr_trafo = trafo_dat%trafo_buf

  end subroutine wavelet_trafo_stepwise_arr

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine output()
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use mpiinterface, only : root_processor

    if(.not. lspat_wavelet) return

    call calc_largestep()
    
    if(root_processor) then
      call append_chunk(lun_spat_wavelet, trafo_data%trafo_buf, xy_fmt, ascii_fmt)
    end if

  end subroutine output

end module diagnos_spat_wavelet
