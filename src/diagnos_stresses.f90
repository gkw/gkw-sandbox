!------------------------------------------------------------------------------
! SVN:$Id$
!> This diagnostic calculates the Reynolds and Maxwell Stress.
!>
!> \f[
!>     m_{R,s} n_{R,s} V_Et V_Er - \tilde B_t \tilde B_r / \mu_0
!> \f]
!> where t (r) refer to the toroidal (radial) component. Perhaps
!> the poloidal component is of interest as well.
!> The diagnostic performs part of an ensemble average, by spatially
!> averaging.
!>
!> *WARNING At the moment the calculations of this diagnostic
!>  are not trustworthy, because they were never used or checked.*
!>
!>---- LIMITATIONS --------------------------------------------------
!>
!> * At the moment only components of the Reynolds Stress tensor
!>   are calculated. Maxwell stress calculation is only a stub.
!>
!------------------------------------------------------------------------------
module diagnos_stresses

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, final_output
  public :: output

  logical, save, public :: lcalc_stresses

  integer :: lun_reynolds_stress

  !Summed over species stresses
  complex :: GamR_radradS, GamR_torradS, GamR_polradS
  complex :: GamM_radradS, GamM_torradS, GamM_polradS

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    lcalc_stresses = .false.
  end subroutine set_default_nml_values

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast
    
    call mpibcast(lcalc_stresses,1)
    
  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()

    ! should it warn that it is not yet ready for use in global runs?

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use diagnos_generic, only : attach_metadata_grid
    use io, only : open_real_lu, ascii_fmt, attach_metadata
    use io, only : description_key, comments_key, phys_unit_key, not_avail
    
    if (.not.lcalc_stresses) return
    
    if(root_processor) then
      call open_real_lu('reynolds_stress', 'diagnostic/diagnos_stresses', &
         & (/ 3 /), ascii_fmt, lun_reynolds_stress)
      call attach_metadata_grid(lun_reynolds_stress, &
         & 'time', '(GamR_radradS, GamR_torradS, GamR_polradS)', ascii_fmt)
      call attach_metadata(lun_reynolds_stress, phys_unit_key, not_avail, ascii_fmt)
      call attach_metadata(lun_reynolds_stress, description_key, not_avail, ascii_fmt)
      call attach_metadata(lun_reynolds_stress, comments_key, not_avail, ascii_fmt)

    end if
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()

    if (.not.lcalc_stresses) return

  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine finalize()
    use io, only : close_lu, ascii_fmt
    use mpiinterface, only : root_processor

    if (.not.lcalc_stresses) return
    
    ! deallocate all arrays of this diagnostic
    !if(allocated(local_array_name)) deallocate(local_array_name)

    if(root_processor) then
      ! be nice and close all logical units
      call close_lu(lun_reynolds_stress, ascii_fmt)
    end if

  end subroutine finalize

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()

  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine final_output()

  end subroutine final_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output()
    use io, only : xy_fmt, ascii_fmt, append_chunk
    if (.not.lcalc_stresses) return

    call calc_largestep()

    call append_chunk(lun_reynolds_stress, &
       & real((/ GamR_radradS, GamR_torradS, GamR_polradS /)), &
       & xy_fmt, ascii_fmt)

    !Output components of the Maxwell stress - To be completed.

  end subroutine output

  !--------------------------------------------------------------------------
  !> This routine calculates the Reynolds and Maxwell stress 
  !--------------------------------------------------------------------------
  subroutine calc_largestep

    use grid,           only : nx, ns, nmu, nvpar, nsp 
    use grid,           only : nmod
    use dist,           only : fdisi
    use geom,           only : ints, efun, bn
    use mode,           only : krho, kxrh
    use components,     only : de, mas
    use velocitygrid,   only : intmu, intvp
    use matdat,         only : get_f_from_g 
    use fields,         only : get_averaged_phi
    use mpiinterface,   only : mpiallreduce_sum_inplace
    use diagnos_generic, only : parseval_correction
    !use dist, only : iphi ! for rhostar effects

    ! integers for the loop over all grid points 
    integer :: imod, ix, i, j, k, is

    !The two tensor sums
    complex :: Erad, Epol, Etor

    !Different components of the Reynolds and Maxwell stresses.
    complex :: GamR_radrad, GamR_torrad, GamR_polrad
    complex :: GamM_radrad, GamM_torrad, GamM_polrad

    complex :: fdis, phi_ga
    real    :: phi2!, apa2, bpa2

    ! An array to hold the parallel derivative of the gyroaveraged potential
    ! complex :: dphi_gads(ns)

    if (.not.lcalc_stresses) return

    ! Initialize the fluxes to zero 
    GamR_radradS = 0.
    GamR_torradS = 0.
    GamR_polradS = 0.
    GamM_radradS = 0.
    GamM_torradS = 0.
    GamM_polradS = 0.

    !Calculate the stresses.
    nsp1: do is = 1, nsp

      GamR_radrad = 0.
      GamR_torrad = 0.
      GamR_polrad = 0.
      GamM_radrad = 0.
      GamM_torrad = 0.
      GamM_polrad = 0.

      ! Spatial average.
      nmod1: do imod = 1, nmod 
        nx1:  do ix = 1, nx 

          ! Integral over the velocity space 
          nmu3: do j = 1, nmu

            ! If rhostar effects are included, calculate the parallel
            ! derivative of the gyroaveraged potential
            ! if(some_rhostar_switch) then
            !   if (nlphi) call dfieldds( iphi,imod,ix,j,is,dphi_gads)
            ! end

            nvpar3: do k = 1, nvpar

              ! Do the average over the flux surface 
              ns3: do i = 1, ns

                ! the gyro-averaged potential
                phi_ga = get_averaged_phi(imod,ix,i,j,is,fdisi) 

                ! fdis is the distribution without A_par contribution  
                fdis = get_f_from_g(imod,ix,i,j,k,is,fdisi) 
                if (intvp(i,j,k,is).eq.0) fdis = 0. 

                ! FIXME Analytically, there is a factor 2 between the
                ! ExB velocity and the tensorial form with
                ! \mathcal{E}. Where is this factor 2 in this codeblock?
                
                ! The 1 in the last index of efun indicates that this
                ! is the radial component of ExB velocity
                Erad = efun(ix,i,1,1)*kxrh(ix) + efun(ix,i,2,1)*krho(imod)
                Etor = efun(ix,i,1,2)*kxrh(ix) + efun(ix,i,2,2)*krho(imod)
                Epol = efun(ix,i,1,3)*kxrh(ix) + efun(ix,i,2,3)*krho(imod)
                ! As far as SRG understands, the third summand comes into
                ! play if finite rhostar terms are considered:
                ! if(some_rhostar_switch) then
                !   Erad = Erad + efun(ix,i,3,1)*conjg(dphi_gads(i))*rhostar
                !   Etor = Etor + efun(ix,i,3,2)*conjg(dphi_gads(i))*rhostar
                !   Epol = Epol + efun(ix,i,3,3)*conjg(dphi_gads(i))*rhostar
                !end if

                phi2 = &
                   ! real space volume element:
                   & ints(i) &
                   ! velspace volume element (The 2*pi which is
                   ! furthermore contained in the velocity-space
                   ! Jacobian is defined into intmu) :
                   & * intvp(i,j,k,is) * intmu(j) * bn(ix,i) &
                   ! The binormal integral is executed using Parseval's theorem
                   & * abs(phi_ga)**2

                ! Calculate the different components of the
                ! Reynolds Stress Tensor:
                ! \Gamma_{\psi,\psi}:
                GamR_radrad = GamR_radrad + &
                   & phi2*Erad*Erad*de(ix,is)*parseval_correction(imod)
                ! \Gamma_{\zeta,\psi}:
                ! FIXME this expression is just a blind guess by SRG!
                GamR_torrad = GamR_torrad + &
                   & phi2*Etor*Erad*de(ix,is)*parseval_correction(imod)
                ! \Gamma_{s,\psi}:
                ! FIXME this expression is just a blind guess by SRG!
                GamR_polrad = GamR_polrad + &
                   & phi2*Epol*Erad*de(ix,is)*parseval_correction(imod)

                ! Calculate the different components of the
                ! Maxwell Stress Tensor:
                ! To be completed.
              end do ns3
            end do nvpar3
          end do nmu3

        end do nx1
      end do nmod1

      ! Sum the components of the Reynolds stress over the species.
      GamR_radradS = GamR_radradS + 2.E0*mas(is)*GamR_radrad
      GamR_torradS = GamR_torradS + 2.E0*mas(is)*GamR_torrad
      GamR_polradS = GamR_polradS + 2.E0*mas(is)*GamR_polrad

    end do nsp1

    call mpiallreduce_sum_inplace(GamR_radradS,1);
    call mpiallreduce_sum_inplace(GamR_torradS,1);
    call mpiallreduce_sum_inplace(GamR_polradS,1);
    call mpiallreduce_sum_inplace(GamM_radradS,1);
    call mpiallreduce_sum_inplace(GamM_torradS,1);
    call mpiallreduce_sum_inplace(GamM_polradS,1);

  end subroutine calc_largestep


end module diagnos_stresses
