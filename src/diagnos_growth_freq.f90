!------------------------------------------------------------------------------
! SVN:$Id$
!> Calculates growth rates, amplitudes, and frequencies for both linear
!> and nonlinear runs, using the sum of the perturbed fields.  
!> 
!> Uses mode amplitudes calculated by the normalise module.
!> Calculations are both per-mode and global.  In the per-mode case, 
!> connected kx modes are identified using the mode_label array.
!> The 1D outputs (as a function of krho) give only the kx = 0 mode.
!>
!> Performs growth rate convergence checks to trigger stop in linear runs.
!------------------------------------------------------------------------------
module diagnos_growth_freq
  use control,  only : non_linear

  implicit none

  private

  public :: set_default_nml_values
  public :: init, bcast, check, finalize, allocate_mem
  public :: initial_output, final_output
  public :: output, screen_output, read_last_data
  public :: calc_smallstep

  !> Switch, to calculate the mode frequencies.
  !> output the frequencies of individual toroidal modes
  logical, save, public :: lfrequencies
  
  !> Switch to calculate and output the growth rate of the dominant
  !> mode (also called "global growth rate"), as well as the growth
  !> rates of individual modes.
  logical, save, public :: lgrowth_rates
  
  !> calculate and output the amplitudes of individual modes
  logical, save, public :: lamplitudes

  !> mode amplitudes
  real, save, allocatable, dimension(:,:) :: amplitudes
  !> mode amplitudes at previous time; used for growth rates
  real, save, allocatable, dimension(:,:) :: last_amplitudes
  !> mode amplitudes temporary array
  real, save, allocatable, dimension(:,:) :: amplitudes_tmp
  !> growth rates, per toroidal mode
  real, save, allocatable, dimension(:) :: growth_rates

  integer, parameter :: history_size = 6
  real, allocatable, save :: growth_rate_history(:,:)

  real, allocatable, save :: std_gr(:), mean_gr(:)

  !> the growth rate (or maximum growth rate) as reported in time.dat
  real, save :: growth_rate

  !> The real frequency of the mode.
  real, allocatable, save :: real_frequency(:)
  real, save :: single_real_frequency

  !> phase, last phase, for real frequency calculation
  real, save :: single_phase, single_last_phase
  real, allocatable, save :: phase(:),last_phase(:)
  complex, allocatable, save :: phase_tmp(:),phase_tmp2(:)

  !> time between phase calculations
  real, save :: delta_time_phase = 1.

  !> factors for calculating the growth of individual modes when the
  !> solution has been normalised
  real, save :: factor
  real, allocatable, save :: factors(:)

  !> sum of the amplitudes to calculate the growth rate of connected
  !> modes, as for real_frequency
  real, allocatable, save :: sum_amp(:), sum_last_amp(:)

  !> logical unit numbers of various output files (new format/order/grouping)
  integer, save :: i_growth_rate, i_real_freq

  !> logical unit numbers for output files (in legacy format/order/grouping)
  integer, save :: i_time_legacy
  integer, save :: i_growth_rates, i_amplitudes, i_frequencies

  !> 
  integer :: legacy_ncolumns
contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    
    lamplitudes = .false.
    lgrowth_rates = .false.
    lfrequencies = .false.
    
  end subroutine set_default_nml_values


  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(lgrowth_rates,1)
    call mpibcast(lfrequencies,1)
    call mpibcast(lamplitudes,1)
  end subroutine bcast

  !--------------------------------------------------------------------
  !> check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use grid, only : nmod
    use control, only : spectral_radius
    use general, only : gkw_warn
    
    if ((lgrowth_rates .or. lfrequencies) .and. &
       (nmod < 2 .and. spectral_radius)) then
      ! No need for per mode eigenvalues
      call gkw_warn('There is just a single mode, no need for per-mode' // &
         & ' growth rates and frequencies')
      lgrowth_rates = .false.
      lfrequencies = .false.
    end if

  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use io, only : open_real_lu, ascii_fmt, attach_metadata
    use diagnos_generic, only : attach_metadata_grid
    use grid, only : nmod
    use control, only : io_legacy
    use global, only : dotdat

    if(non_linear) then
      legacy_ncolumns = 1
    else
      ! besides time, write also growth rate and real frequency into
      ! the same file
      legacy_ncolumns = 3
    end if

    if(root_processor) then
      ! TODO: check if last_largestep_time is correctly in all cases
      ! (e.g. after restart)

      ! open the logical unit to write the time evolution of the
      ! growth rate.
      call open_time_lu()

      ! open the logical units for the growth rates and freqs of the
      ! individual modes.
      if (lgrowth_rates) then
        if(io_legacy) then
          call open_real_lu('growth.dat', 'diagnostic/diagnos_growth_freq', &
             & (/ nmod /), ascii_fmt, i_growth_rates)
        else
          call open_real_lu('growth_rates', 'diagnostic/diagnos_growth_freq', &
             & (/ nmod /), ascii_fmt, i_growth_rates)
          call attach_metadata_grid(i_growth_rates, 'time', 'krho', ascii_fmt)
          call attach_metadata(i_growth_rates, 'physical unit', &
             & 'v_{th,ref}/R_{ref}', ascii_fmt)
          call attach_metadata(i_growth_rates, 'description', &
             & 'Growth rates of all individual modes', ascii_fmt)
          call attach_metadata(i_growth_rates, 'comments', 'n.a.', ascii_fmt)
        end if
      end if

      if (lfrequencies) then
        call open_real_lu(dotdat('frequencies',io_legacy), &
           & 'diagnostic/diagnos_growth_freq',&
           & (/ nmod /), ascii_fmt, i_frequencies)
        call attach_metadata_grid(i_frequencies, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_frequencies, 'physical unit', 'v_{th,ref}/R_{ref}', &
           & ascii_fmt)
        call attach_metadata(i_frequencies, 'description', &
           & 'Real frequencies of the individual modes', ascii_fmt)
        call attach_metadata(i_frequencies, 'comments', 'n.a.', ascii_fmt)
      end if

      if (lamplitudes) then
        call open_real_lu(dotdat('amplitudes',io_legacy), &
           & 'diagnostic/diagnos_growth_freq', &
           & (/ nmod /), ascii_fmt, i_amplitudes)
        call attach_metadata_grid(i_amplitudes, 'time', 'krho', ascii_fmt)
        call attach_metadata(i_amplitudes, 'physical unit', 'n.a.', &
           & ascii_fmt)
        call attach_metadata(i_amplitudes, 'description', 'n.a.', ascii_fmt)
        call attach_metadata(i_amplitudes, 'comments', 'n.a.', ascii_fmt)
      end if
    end if
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine open_time_lu()
    use io, only : open_real_lu, ascii_fmt, attach_metadata
    use diagnos_generic, only : attach_metadata_grid
    use control, only : io_legacy

    if(io_legacy) then
      ! open the file to write or read the time evolution of the growth rate.
      call open_real_lu('time.dat', 'diagnostic/diagnos_growth_freq', &
         & (/ legacy_ncolumns/), ascii_fmt, &
         & i_time_legacy)
    else
      if(.not.non_linear) then
        ! the time is output by diagnos_grid
        call open_real_lu('dominant_growth_rate', &
           & 'diagnostic/diagnos_growth_freq', (/1/), &
           & ascii_fmt, i_growth_rate)
        call attach_metadata_grid(i_growth_rate, 'time', ascii_fmt)
        call attach_metadata(i_growth_rate, 'physical unit', 'v_{th,ref}/R_{ref}', &
           & ascii_fmt)
        call attach_metadata(i_growth_rate, 'description', 'n.a.', ascii_fmt)
        call attach_metadata(i_growth_rate, 'comments', 'n.a.', ascii_fmt)
        
        call open_real_lu('dominant_real_freq', &
           & 'diagnostic/diagnos_growth_freq', (/1/), &
           & ascii_fmt, i_real_freq)
        call attach_metadata_grid(i_real_freq, 'time', ascii_fmt)
        call attach_metadata(i_real_freq, 'physical unit', 'v_{th,ref}/R_{ref}', &
           & ascii_fmt)
        call attach_metadata(i_real_freq, 'description', 'n.a.', ascii_fmt)
        call attach_metadata(i_real_freq, 'comments', 'n.a.', ascii_fmt)
      end if
    end if
  end subroutine open_time_lu

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine close_time_lu()
    use io,         only : close_lu, ascii_fmt
    use control,    only : io_legacy, non_linear

    if(io_legacy) then
      call close_lu(i_time_legacy, ascii_fmt)
    else
      if(.not.non_linear) then
        ! the time is output by diagnos_grid
        call close_lu(i_growth_rate, ascii_fmt)
        call close_lu(i_real_freq, ascii_fmt)
      end if
    end if

  end subroutine close_time_lu
  
  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use general, only : gkw_abort
    use grid, only : nmod, nx
    use mode, only : nmodes
    use control, only : normalized, normalize_per_toroidal_mode
    integer :: ierr, i

    allocate(std_gr(nmodes), stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: Could not allocate std_gr')
    allocate(mean_gr(nmodes), stat=ierr)
    if (ierr /= 0) call gkw_abort('diagnostic :: Could not allocate mean_gr')

    ! arrays for mode amplitudes growth rates
    !if (lamplitudes .or. lgrowth_rates) then
      allocate(amplitudes(nmod,nx),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: amplitudes')
      allocate(last_amplitudes(nmod,nx),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: last_amplitudes')
      ! allocate the tmp array in any case because one is needed to
      ! reduce-sum the amplitudes
      allocate(amplitudes_tmp(nmod,nx),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: amplitudes_tmp')
    !end if

    ! array for growth rates
    if (lgrowth_rates) then
      allocate(growth_rates(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: growth_rates')
      allocate(sum_amp(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: sum_amp')
      allocate(sum_last_amp(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: sum_last_amp')
    end if

    ! array used to calculate growth rates in normalised runs
    if (normalized .and. normalize_per_toroidal_mode) then
      allocate(factors(nmod),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: amplitudes')
    end if

    if (nmodes == 0) call gkw_abort('allocate diagnostic after kgrid')

    ! mode frequency calculation arrays
    !if (lfrequencies) then
      allocate(real_frequency(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: real_frequency')
      allocate(phase(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: phase')
      allocate(last_phase(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: last_phase')
      allocate(phase_tmp(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: phase_tmp')
      allocate(phase_tmp2(nmodes),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: phase_tmp2')
    !end if

    ! allocate the array which holds the last few growth rates,
    ! for every toroidal mode, if requested
    allocate(growth_rate_history(nmodes, history_size), stat = ierr)
    if (ierr /= 0) call gkw_abort('could not allocate growth_rate_history')
    ! initialise the history array with something that does not trigger
    ! the convergence check
    do i = 1, history_size
      growth_rate_history(:, i) = i
    end do



  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine read_last_data()
    use control,      only : nlapar, icomplete, io_legacy
    use control,      only : time, naverage, dtim
    use mode,         only : mode_box
    use io,           only : lu_exists, ascii_fmt
    use io,           only : read_last_chunk
    use mpiinterface, only : root_processor, mpibcast
    use general,      only : gkw_warn
    integer :: nlast
    real, dimension(3) :: last_chunk

    if(io_legacy) then
      if(.not. lu_exists('time.dat', 'grid', ascii_fmt)) then
        if(root_processor) then
          write(*,*) 'file or dataset time.dat not found. Do not read last value.'
          ! Otherwise the time value set in the restart
          ! module is kept.
        end if
      else
        
        if(root_processor) then
          ! Get the last time value of the previous run from the output data.
          call open_time_lu()
          call read_last_chunk(i_time_legacy, '(3(es13.5))', &
             & last_chunk(1:legacy_ncolumns), &
             & nlast, ascii_fmt)
          call close_time_lu()
          time = last_chunk(1)

          if(icomplete /= nlast) then
            call gkw_warn('The logical unit "time(.dat)" '// &
               & 'is shorter or longer than expected.');
            write (*,*) "icomplete = ", icomplete
            write (*,*) "number of chunks in logical unit = ", nlast
          end if
        end if

        ! Broadcast the retrieved values to all processes.
        call mpibcast(time,1)
      end if
    end if

  end subroutine read_last_data

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine initial_output()

  end subroutine initial_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine final_output()

  end subroutine final_output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine calc_smallstep(i_smallstep)
    use control, only : naverage
    integer, intent(in) :: i_smallstep

    if (i_smallstep == naverage - 1) then
      call calc_phase()
    else if (i_smallstep == naverage) then
      call calc_phase()
    end if

  end subroutine calc_smallstep

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine output()
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use grid, only : nmod, nx
    use mode, only : ixzero, mode_label
    use mpiinterface, only : root_processor
    use diagnos_generic, only : lwrite_output1
    integer :: imod, ix

    !if(.not. non_linear) then
      ! calculate the mode amplitudes
      !if (lamplitudes .or. lgrowth_rates) then
      call calc_amplitudes()
      !end if

      ! calulate the growth rate
      !if(lgrowth_rates) then
      call diagnostic_growth_rate()
      call check_convergence_and_stop()
      !end if

      ! (calculate the real frequency)
      !if (lfrequencies) then
      call diagnostic_real_freq()
      !end if
    !end if

    if(lwrite_output1) then

      if (root_processor) then
        call write_time_output()

        if (lamplitudes) then
          ! Must have nmod < 512
          do ix = 1, nx
            call append_chunk(i_amplitudes, &
               & amplitudes(:, ix), xy_fmt, ascii_fmt)
          end do
        end if

        if (lgrowth_rates) then
          ! Must have nmod < 512
          call append_chunk(i_growth_rates, &
             & (/ (growth_rates(mode_label(imod,ixzero)),imod=1,nmod) /), &
             & xy_fmt, ascii_fmt)
        end if

        ! The frequencies are only outupt for the kx=0 modes
        ! and do not exactly correspond to the frequencies above
        if (lfrequencies) then
          ! Must have nmod < 512
          call append_chunk(i_frequencies, &
             & (/ (real_frequency(mode_label(imod,ixzero)),imod=1,nmod) /), &
             & xy_fmt, ascii_fmt)
        end if
      end if
    end if

  end subroutine output

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine screen_output()
    use mpiinterface, only : root_processor
    use control,   only : ntotstep, time, neoclassics
    use grid,      only : number_of_species, nmod, nx
    use mode,      only : mode_box, mode_label
    integer :: imod, ix

    ! Only root process shall write to terminal
    if(.not.root_processor) return

200 format('Time step : ',I7,' Normalised time : ',es13.5)

    write(*,200) ntotstep, time
    if(mode_box) then
      if(.not. non_linear) then
50      format('Global growth rate : ',es13.5)
        write(*,50) growth_rate
60      format('Real frequency : ',es13.5)
        write(*,60) single_real_frequency
      end if
    else
      mod : do imod = 1, nmod
        x : do ix = 1, nx
          if(.not. non_linear) then
201         format('nMode ',I3,' xMode ',I3,' Growth rate ',es13.5)
            write(*,201)imod,ix,growth_rate
202         format('nMode ',I3,' xMode ',I3,' Real frequency ',es13.5)
            write(*,202)imod,ix,real_frequency(mode_label(imod,ix))
          end if
        end do x
      end do mod
    end if

  end subroutine screen_output


  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !> Writes the given values to time.dat (legacy setting) or to three
  !> separate logical units.
  !> - For non_linear runs only the time is written.
  !> - For linear runs the frequency and the growth rate is written
  !>
  !----------------------------------------------------------------------------
  subroutine write_time_output()
    use control, only : time
    use control, only : io_legacy
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use mpiinterface, only : root_processor
    if(.not. root_processor) return

    if(io_legacy) then
      if (non_linear) then
        ! For non_linear runs write time only.
        call append_chunk(i_time_legacy, (/ time /), '(3(es13.5))', ascii_fmt)
      else
        ! write time and growth rate and real frequency
        call append_chunk(i_time_legacy, &
           & (/ time, growth_rate, single_real_frequency /), &
           & '(3(es13.5))', ascii_fmt)
      end if
    else
      if(.not. non_linear) then
        ! new style: produce three separate datasets.
        call append_chunk(i_growth_rate, (/ growth_rate /), '(1(es13.5))', &
           & ascii_fmt)
        call append_chunk(i_real_freq, (/ single_real_frequency /), &
           & '(1(es13.5))', ascii_fmt)
      end if
    end if

  end subroutine write_time_output

  !---------------------------------------------------------------------------
  !>
  !---------------------------------------------------------------------------
  subroutine calc_amplitudes
    use mpicomms,       only : COMM_X_NE, COMM_S_NE
    use mpiinterface,   only : mpiallreduce_sum
    use grid,           only : parallel_s,ns,nmod,nx
    use dist,           only : fdis => fdisi, iphi, iapar, ibpar, number_of_fields
    use index_function, only : indx
    use control,        only : nlapar, nlphi, nlbpar, normalized 
    use control,        only : spectral_radius, normalize_per_toroidal_mode
    use mode,           only : ixzero,iyzero
    use normalise,      only : fnorm1d, fnorm0d
    use control,        only : normalize_per_toroidal_mode

    logical, save :: first_time = .true.

    integer :: idx,i,j,k,is,l,imod,ix
    integer :: nfields

    integer, save, allocatable, dimension(:) :: field_id, lindx
    complex, save, allocatable, dimension(:) :: cphi, cphi_dum

    if (first_time) then

      ! construct the array for quick look up of the fields
      allocate(field_id(number_of_fields))
      nfields = 0
      if (nlphi) then
        nfields = nfields + 1
        field_id(nfields) = iphi
      end if
      if (nlapar) then
        nfields = nfields + 1
        field_id(nfields) = iapar
      end if
      if (nlbpar) then
        nfields = nfields + 1
        field_id(nfields) = ibpar
      end if
      allocate(lindx(number_of_fields*ns*nx*nmod))
      idx = 0
      lindx(:) = 0
      do k = 1, number_of_fields
        do ix = 1, nx
          do imod = 1, nmod
            do is = 1, ns
              idx=idx+1
              lindx(idx) = indx(field_id(k),imod,ix,is)
            end do
          end do
        end do
      end do
      deallocate(field_id)

      if (.not. spectral_radius) allocate(cphi(nmod))
      if (.not. spectral_radius) allocate(cphi_dum(nmod))

      ! initialise the amplitudes
      amplitudes(:,:) = 1.

      ! only run this piece of code once
      first_time = .false.

    end if

    ! keep the normalisation factor and last_amplitudes for the growth rate
    ! calculation
    if (normalized) then
      if (normalize_per_toroidal_mode) then
        do i = 1, nmod
          factors(i) = fnorm1d(i)
        end do
      else
        factor = fnorm0d
      end if
    else
      ! do nothing
    end if
    last_amplitudes = amplitudes

    ! calculate the local amplitudes
    amplitudes = 0.
    idx = 0

    if (spectral_radius) then
      do l = 1, number_of_fields; do j = 1, nx; do i = 1, nmod; do k = 1, ns
        idx=idx+1
        if (i == iyzero .and. j == ixzero) then
          ! do nothing
        else
          amplitudes(i,j) = amplitudes(i,j) + abs(fdis(lindx(idx)))**2
        end if
      end do; end do; end do; end do 
    else  !average over zero kx mode only
      cphi(:)=0.
      do l = 1, 1; do j = 1, nx; do i = 1, nmod; do k = 1, ns
        idx=idx+1
        cphi(i) = cphi(i) + fdis(lindx(idx))       
      end do; end do; end do; end do 
      call mpiallreduce_sum(cphi(:),cphi_dum(:),nmod,COMM_X_NE)

      do imod = 1, nmod ! all x values set the same for growth_rates
        amplitudes(imod,:) =  abs(cphi_dum(imod))**2
      end do

    end if

    ! reduce-sum the amplitudes if necessary
    if (parallel_s) then
      call mpiallreduce_sum(amplitudes,amplitudes_tmp,nmod,nx,COMM_S_NE)
      amplitudes = amplitudes_tmp
    end if

    amplitudes = sqrt(amplitudes)

  end subroutine calc_amplitudes

  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !>
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine calc_phase

    use mpiinterface,   only : mpiallreduce_sum
    use mpicomms,       only : COMM_S_NE_X_NE
    use constants,      only : pi
    use control,        only : time, nlapar, nlbpar, nlphi
    use grid,           only : parallel_s, parallel_x, ns, nmod, nx
    use dist,           only : fdisi,number_of_fields,iphi,iapar,ibpar
    use mode,           only : mode_label,nmodes
    use index_function, only : indx
    use general,        only : gkw_abort

    ! keep the last time this subroutine was called
    real, save :: last_time_diagn_phase = 0.
    integer, allocatable, save :: lindx(:,:,:)
    integer, allocatable  :: field_id(:)
    integer :: idx,nfields,i,j,k,is
    logical :: first_call = .true.
    integer :: ierr

    if (first_call) then

      last_phase = 0.
      single_last_phase = 0.
      phase = 0.
      single_phase = 0.
      phase_tmp = 0.
      real_frequency(:) = 0.0
      allocate(lindx(nmod,nx,number_of_fields*ns),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: lindx')
      allocate(field_id(number_of_fields),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnostic :: field_id')
      nfields = 0
      if (nlphi) then
        nfields = nfields + 1
        field_id(nfields) = iphi
      end if
      if (nlapar) then
        nfields = nfields + 1
        field_id(nfields) = iapar
      end if
      if (nlbpar) then
        nfields = nfields + 1
        field_id(nfields) = ibpar
      end if

      ! fill the array
      idx = 0
      lindx(:,:,:) = 0
      do k = 1, number_of_fields ; do is = 1, ns
        idx = idx + 1
        do j = 1, nx ; do i = 1, nmod
          lindx(i,j,idx) = indx(field_id(k),i,j,is)
        end do; end do
      end do; end do

      deallocate(field_id)
      first_call = .false.

    end if

    ! save time interval and update last_time_diagn_phase
    delta_time_phase = time - last_time_diagn_phase
    last_time_diagn_phase = time

    ! before calculating the new phase, store the old one
    last_phase  = phase
    single_last_phase = single_phase
    phase_tmp = (0.,0.)

    ! All the fields are used, calculation.
    do k = 1, number_of_fields*ns
      do j = 1, nx
        do i = 1, nmod
          phase_tmp(mode_label(i,j)) = phase_tmp(mode_label(i,j)) +            &
             & fdisi(lindx(i,j,k))
        end do
      end do
    end do

    ! sum-reduce the over the s-and x direction
    if (parallel_s .or. parallel_x) then
      call mpiallreduce_sum(phase_tmp,phase_tmp2,nmodes,COMM_S_NE_X_NE)
      phase_tmp = phase_tmp2
    end if

    ! implicit loop over modes
    phase = atan2(aimag(phase_tmp),real(phase_tmp))
    single_phase = atan2(aimag(sum(phase_tmp)),real(sum(phase_tmp)))

    ! Treat the cases of 2\pi jumps.
    ! implicit loop over modes
    where (abs(phase-last_phase) > max(pi/4.,abs(3.*real_frequency*delta_time_phase)))
      last_phase = last_phase - sign(2*pi,last_phase)
    end where
    if (abs(single_phase-single_last_phase) > max(pi/4.,                       &
       &                      abs(3.*single_real_frequency*delta_time_phase))) then
      single_last_phase = single_last_phase - sign(2*pi,single_last_phase)
    end if


  end subroutine calc_phase

  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !>
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine diagnostic_real_freq

    use global,       only : r_tiny
    use control,      only : time, last_largestep_time

    ! last_largestep_time is used for the calculation of the real frequency.

    if (abs(time-last_largestep_time) < r_tiny) then
      real_frequency = 0.
      single_real_frequency = 0.
    else
      ! implicit loop over all modes
      real_frequency = (phase - last_phase) / delta_time_phase
      single_real_frequency = (single_phase - single_last_phase) &
         & / delta_time_phase
    end if

  end subroutine diagnostic_real_freq

  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !>
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine finalize
    use io, only : close_lu, ascii_fmt

    if (allocated(std_gr)) deallocate(std_gr)
    if (allocated(mean_gr)) deallocate(mean_gr)
    
    ! deallocate the private arrays and
    ! do nothing else.
    if (allocated(growth_rate_history)) deallocate(growth_rate_history)

    if(lgrowth_rates) call close_lu(i_growth_rates, ascii_fmt)
    if(lamplitudes) call close_lu(i_amplitudes, ascii_fmt)
    if(lfrequencies) call close_lu(i_frequencies, ascii_fmt)
    call close_time_lu
 
  end subroutine finalize

  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !>
  !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  subroutine diagnostic_growth_rate()
    use global,       only : r_tiny
    use control,      only : normalized
    use control,      only : normalize_per_toroidal_mode
    use control,      only : time, last_largestep_time, spectral_radius
    use grid,         only : nx, nmod
    use normalise,    only : fnorm0d
    use mode,         only : mode_label, nmodes
    use general,      only : gkw_abort 

    integer :: i, j
    integer, save :: igrowth=0
    real, save :: fnorm0d_last=1

    ! calculate the individual growth rates if requested
    if (lgrowth_rates) then
      sum_amp = 0.
      sum_last_amp = 0.

      if (abs(time-last_largestep_time) < r_tiny) then
        ! Assume zero growth for small time changes.
        growth_rates(:) = 0.
      else 
        ! note that in nonspectral case every x has same mode label and
        ! same amplitude
        do j=1,nx
          do i=1,nmod
            sum_amp(mode_label(i,j)) = sum_amp(mode_label(i,j))                  &
               &               + amplitudes(i,j)*amplitudes(i,j)
            sum_last_amp(mode_label(i,j)) = sum_last_amp(mode_label(i,j))        &
               &               + last_amplitudes(i,j)*last_amplitudes(i,j)
          end do
        end do

        if (normalized) then
          if (normalize_per_toroidal_mode) then
            call gkw_abort('normalize_per_toroidal_mode=.true. is obsolete - do not use this option') 
          else
            ! one normalisation factor is used for every normalised mode,
            ! \attention Here are some possibilities for undefined arithmetic
            !            operations (i.e. sum_last_amp(z)=0).
            growth_rates = log(factor*sqrt(sum_amp/sum_last_amp)) / &
                 & (time-last_largestep_time)
          end if
        else
          ! not normalised - dont need to use the factors
          ! \attention Here are some possibilities for undefined arithmetic
          !            operations (i.e. sum_last_amp(z)=0).
          growth_rates = log(sqrt(sum_amp/sum_last_amp)) / &
               & (time-last_largestep_time)
        end if
      end if
    end if

    ! calculate the standard growth rate which is output to time.dat by
    ! default.  Nota Bene need to use fnorm0d here.  growth_rate is
    ! calculated regardless of whether individual growth rates are
    ! requested with lgrowth_rates.
    if (.not. normalized) then
      ! compute growth rate for a not normalised run
      if (abs(time-last_largestep_time) < r_tiny) then
        growth_rate = 0.
      else
        growth_rate = (log(fnorm0d)-log(fnorm0d_last)) / (time - last_largestep_time)
        fnorm0d_last = fnorm0d
      end if
    else
      ! compute growth rate for a normalized run
      if (abs(time-last_largestep_time) < r_tiny) then
        growth_rate = 0.
      else
        growth_rate = log(fnorm0d) / (time - last_largestep_time)
      end if
    endif

    ! fill in the array storing the growth rate time history
    igrowth = mod(igrowth+1,history_size)
    if (lgrowth_rates .and. spectral_radius) then
      growth_rate_history(:, igrowth+1) = growth_rates
    else
      ! FIXME in the nonspectral case the value of growth_rate and
      ! growth_rates differ (cf. testcase input_non_spectral.dat).
      ! As they are not consistent in that case at the moment, fall
      ! back to the global growth_rate for the convergendce check,
      ! although there are actually nmodes > 1 individual modes in
      ! and lgrowth_rates=.true. .
      growth_rate_history(:, igrowth+1) = growth_rate
    end if

    ! standard deviation of the growth rate history
    std_gr = 0.
    do i = 1, nmodes
      mean_gr(i) = sum(growth_rate_history(i,:)) / history_size
    end do

    do i = 1, nmodes
      do j = 1, history_size
        std_gr(i) = std_gr(i) + (growth_rate_history(i,j)-mean_gr(i))**2
      end do
      std_gr(i) = sqrt(std_gr(i) / history_size)
    end do

  end subroutine diagnostic_growth_rate

  subroutine check_convergence_and_stop
    use control, only : gamatol, stop_me, nan_stop, min_gr
    use control, only : max_gr, neoclassics, spectral_radius
    use control, only : time
    use mpiinterface, only : root_processor

    if (gamatol > 0. .and. .not. stop_me) then
      ! do a check for convergence on the growth rate
      stop_me = (maxval(std_gr) < gamatol) .or. stop_me
      if (stop_me .and. root_processor) then
        if(lgrowth_rates) then
          write(*,*) '**** Growth rate convergence reached for all modes: STOP ****'
        else
          write(*,*) '**** Growth rate convergence reached: STOP ****'
        end if
      end if
    end if

    if (maxval(mean_gr) > max_gr .and. time > 1.0 .and. .not. stop_me) then
      if(root_processor) then
        write(*,*) '**** max_gr: Run unstable: STOP ****'
        write(*,*)
      end if
      stop_me = .true.
      nan_stop = .true.
    end if

    if (maxval(mean_gr) < min_gr .and. maxval(std_gr) < 10.*gamatol &
       & .and. .not. neoclassics &
       & .and. .not. stop_me) then
      if(root_processor) then
        if(lgrowth_rates) then
          write(*,*) '**** min_gr: All modes are stable: STOP ****'
        else
          write(*,*) '**** min_gr: Mode is stable: STOP ****'
        end if
        write(*,*)
      end if
      stop_me = .true.
    end if

  end subroutine check_convergence_and_stop

end module diagnos_growth_freq
