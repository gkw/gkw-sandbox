!-------------------------------------------------------------------------------
! SVN:$Id$
!> Time dependent source for numerical perturbation experiments
!-------------------------------------------------------------------------------
module source_time

  implicit none 

  private 
  
  ! public routines
  public :: source_modulation, source_time_read_nml, source_time_write_nml
  public :: source_time_check_params, source_time_bcast_nml
  public :: add_source_time

  ! public variables
  public :: modulation, source_wave_number, source_time_ampl, mod_freq

  !> logical that determines if the source modulation is present
  logical, save :: modulation     

  !> source modulation wave length
  integer, save :: source_wave_number

  !> amplitude of the time dependent source
  complex, save :: source_time_ampl

  !> modulation frequency
  real, save :: mod_freq

  interface source_time_write_nml
     module procedure source_time_read_nml
  end interface

contains

!---------------------------------------------------------------------------------
!> This routine produces the matrix element for a time dependent source of the
!> form: S=amp*(v^2/v_th^2-3/2)*F_M*exp(i*(x/L-omega*t)) L=lx/source_wave_number
!> The source term is defined such that when added to the rhs of the gyrokinetic 
!> equation it does not produce particle and parallel momentum sources. 
!> For this purpose intfm (normalized velocity space integral of the Maxwellian: 
!> int(v^2/v_th^2*F_M)*dv ~ 3/2 ) is calculated and used in the matrix element 
!> instead of the 3/2 that appears in the formula above. The source term is 
!> then stored using the routine put_source_time. 
!> All parameters (length, amplitude, ecc..) have to be given in the namelist  
!> source_time. The routine is called in linear_terms.f90.  
!----------------------------------------------------------------------------------
subroutine source_modulation
  
  use grid,           only : nx, ns, nmu, nvpar, nsp, lx
  use grid,           only : parallel_vpar, parallel_mu, gx 
  use dist,           only : fmaxwl, ifdis
  use index_function, only : indx
  use matdat,         only : put_source_time
  use geom,           only : bn, dxgr
  use velocitygrid,   only : vpgr, mugr, intvp, intmu
  use components,     only : tmp, tgrid
  use general,        only : gkw_abort
  use mode,           only : iyzero
  use mpiinterface,   only : MPIALLREDUCE_SUM
  use mpicomms,       only : COMM_VPAR_NE_MU_NE
  use constants,      only : ci1, pi 

  ! real for the normalized velocity space integral of the Maxwellian
  ! the integral is calculated at each grid points (species, radial, field)
  real :: intfm, intfm1, intfm2, intfm_sum

  ! integers for the loop over all grid points
  integer :: imod, ix, i, j, k, is

  ! matrix element and amplitude of the source
  complex :: mat_elem 

  ! dummy variables
  integer :: iih 

  if (.not.modulation) return

  ! check if there is a zero mode 
  if (iyzero == 0) call gkw_abort('No zero mode for modulation') 

  do ix=1,nx; do i=1,ns; do is=1,nsp
      
    ! initialize the integral to zero
    intfm1 = 0.0
    intfm2 = 0.0 

    ! perform the integral in velocity space
    do j=1,nmu ; do k=1,nvpar

      intfm1 = intfm1 &
               & +(vpgr(i,j,k,is)**2+2.E0*bn(ix,i)*mugr(j))/(tmp(ix,is)/tgrid(is))  &
               & *fmaxwl(ix,i,j,k,is) &
               & *intvp(i,j,k,is)*intmu(j)*bn(ix,i) 

      intfm2 = intfm2 + fmaxwl(ix,i,j,k,is)*intvp(i,j,k,is)*intmu(j)*bn(ix,i) 

    end do ; end do
      
    ! MPI allreduce over COMM_VPAR_NE_MU_NE 
    if ((parallel_vpar).or.(parallel_mu)) then 
      call MPIALLREDUCE_SUM(intfm1,intfm_sum,1,COMM_VPAR_NE_MU_NE)
      intfm1 = intfm_sum
      call MPIALLREDUCE_SUM(intfm2,intfm_sum,1,COMM_VPAR_NE_MU_NE)
      intfm2 = intfm_sum
    endif 

    ! normalize the sum of the energy moment to the sum of the Maxwell such that 
    ! the particle source is exactly zero 
    intfm = intfm1 / intfm2 

    ! calculate and store the matrix element
    do j=1,nmu ; do k=1,nvpar

      ! select the zonal mode
      imod = iyzero
 
      ! index of the perturbed distribution
      iih = indx(ifdis,imod,ix,i,j,k,is) 
 
      ! matrix element 
      mat_elem = ((vpgr(i,j,k,is)**2+2.E0*bn(ix,i)*mugr(j))/(tmp(ix,is)/tgrid(is))-intfm)  &
               &  *source_time_ampl*fmaxwl(ix,i,j,k,is)*exp(2.E0*pi*ci1*source_wave_number &
               &  *(gx(ix)-1)*dxgr/lx)

      ! put the element 
      call put_source_time(iih,mat_elem)

    end do ; end do 

  end do  ; end do ; end do

end subroutine source_modulation

!------------------------------------------------------------------------------
!> This subroutine adds the time dependant source
!------------------------------------------------------------------------------
subroutine add_source_time(rhs, DPART_IN, without_dtim)
  use constants, only : ci1, c1
  use control,   only : time, dtim
  use dist,      only : nsolc
  use matdat,    only : source, source_t
  
  complex, intent(inout) :: rhs(nsolc)
  real, intent(in), optional :: DPART_IN
  logical, intent(in), optional :: without_dtim

  integer :: i
  complex :: cdum, cmodul

  if(present(without_dtim)) then
    if (without_dtim) then
      cdum=c1
    else
      cdum=dtim*c1
    end if
  else
    cdum=dtim*c1
  endif
  
  if (modulation) then

    if (present(DPART_IN)) then
      cmodul = exp(-ci1*mod_freq*(time+DPART_IN))
    else
      cmodul = exp(-ci1*mod_freq*time)
    endif

    do i = 1, nsolc 
      rhs(i) = rhs(i) + cdum*(source(i) + 0.5*( cmodul*source_t(i) + conjg(cmodul*source_t(i)) ) )
    end do

  else

    do i = 1, nsolc 
      rhs(i) = rhs(i) + cdum*source(i)
    end do

  endif
end subroutine add_source_time

!------------------------------------------------------------------------------
!> This subroutine reads (or writes) the time dependent source namelist
!------------------------------------------------------------------------------
subroutine source_time_read_nml(lun,io_stat,lwrite)
  use io, only : write_run_parameter
  use constants,      only : c1

  integer, intent(in)  :: lun
  integer, intent(out) :: io_stat

  logical, optional, intent(in) :: lwrite

  namelist /source_time/ modulation, source_wave_number, &
         & source_time_ampl, mod_freq 

  io_stat = 0
  ! read the input
  if (present(lwrite)) then
    if (.not. lwrite) then
      ! set the default values 
      modulation =         .false.
      source_wave_number = 1
      source_time_ampl =   c1
      mod_freq =           1.0      
       
      ! read namelist
      read(lun,NML=source_time,IOSTAT=io_stat) 
    else
      ! do nothing
    end if
  else
    ! write the namelist
    write(lun,NML=source_time)

    call write_run_parameter('source_time', 'modulation', modulation)
    call write_run_parameter('source_time', 'source_wave_number', source_wave_number)
    call write_run_parameter('source_time', 'source_time_ampl', source_time_ampl)
    call write_run_parameter('source_time', 'mod_freq', mod_freq)
    
  end if 

end subroutine source_time_read_nml

!------------------------------------------------------------------------------
!> bcast the time dependent source namelist params
!------------------------------------------------------------------------------
subroutine source_time_bcast_nml

  use mpiinterface, only : mpibcast 
  
  call mpibcast(modulation,         1)
  call mpibcast(source_wave_number, 1)
  call mpibcast(source_time_ampl,   1)
  call mpibcast(mod_freq,           1)
  
end subroutine source_time_bcast_nml

!------------------------------------------------------------------------------
!> put any checks that can be done before memory allocation in here
!------------------------------------------------------------------------------
subroutine source_time_check_params

  use control,    only : spectral_radius
  use general,    only : gkw_exit

  if (spectral_radius .and. modulation) then
     call gkw_exit('Must be running non spectral to run modulation')
  end if


end subroutine source_time_check_params

end module source_time
