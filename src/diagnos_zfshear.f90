!------------------------------------------------------------------------------
! SVN:$Id$
!> This diagnostic calculates the shear rate of the zonal flows.
!!    \f$ \omega = \frac{\partial}{\partial \psi} v_{E,\zeta}
!!          = \frac{\partial^2}{\partial \psi^2} \phi          \f$
!!
!! It is used for example in
!!    * Dannert and Jenko, PoP 12, 072309 (2005)
!!    * Merz and Jenko, Nucl. Fusion 50, 054005 (2010)
!!
!! Other interesting works in this context include
!!    * Hahm et al., PoP 6, No. 3, 922 (1999)
!!
!! TO DO: 
!!
!! * Write this diagnostic in a way which avoids to allocate zfshear_G
!!    on the non-root processes.
!!
!<------------------------------------------------------------------------------
module diagnos_zfshear

  implicit none

  private

  public :: set_default_nml_values, init, bcast, check, allocate_mem
  public :: finalize
  public :: initial_output, final_output
  public :: output

  !> The general on/off switch for this diagnostic
  logical, save, public :: lcalc_zfshear

  !> The logical unit number, used to output data
  integer :: i_zfshear

  complex, allocatable, dimension(:) :: zfshear
  complex, allocatable, dimension(:) :: zfshear_G

  complex :: zfshear_total

contains

  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    ! Usually the diagnostic should be switched
    ! off by default.

    lcalc_zfshear = .false.
  end subroutine set_default_nml_values

  
  !--------------------------------------------------------------------
  !> Broadcast all namelist items of this diagnostic to all processes.
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(lcalc_zfshear,1)
  end subroutine bcast

  !--------------------------------------------------------------------
  !> Check the diagnostic parameters and if the setup is compatible
  !> with this diagnostic.
  !--------------------------------------------------------------------
  subroutine check()
    use grid, only : nmod
    use general, only : gkw_warn

    if (.not.lcalc_zfshear) return
    
    ! output of the Apar parallel structure only if apar run
    if (nmod == 1) then
      call gkw_warn('Cannot use the zfshear diagnostic: There is no k_y=0 mode.')
      lcalc_zfshear=.false.
    end if
    
    !evtl. check if phi is calculated?!

  end subroutine check

  !--------------------------------------------------------------------
  !> Initialize the diagnostic. This is the place to open logical
  !> units.
  !--------------------------------------------------------------------
  subroutine init()
    use mpiinterface, only : root_processor
    use io, only : open_complex_lu, ascii_fmt, attach_metadata
    use io, only : description_key, comments_key, phys_unit_key, not_avail
    use diagnos_generic, only : attach_metadata_grid
    use grid, only : n_x_grid

    if (.not.lcalc_zfshear) return
    
    if(root_processor) then
      ! open logical units
      call open_complex_lu('zfshear', 'diagnostic/diagnos_zfshear', &
         & (/ n_x_grid /), ascii_fmt, i_zfshear)
      call attach_metadata_grid(i_zfshear, 'time', 'kxrh', ascii_fmt)
      call attach_metadata(i_zfshear, phys_unit_key, not_avail, ascii_fmt)
      call attach_metadata(i_zfshear, description_key, not_avail, ascii_fmt)
      call attach_metadata(i_zfshear, comments_key, not_avail, ascii_fmt)
      
    end if
    
  end subroutine init

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use general, only : gkw_abort
    use mpiinterface, only : root_processor
    use grid, only : nx, n_x_grid
    integer :: ierr

    if (.not.lcalc_zfshear) return
    
    allocate(zfshear(nx),stat=ierr)
    if (ierr /= 0) &
       & call gkw_abort('diagnos_zfshear :: could not allocate zfshear')
    
    if(root_processor) then
      allocate(zfshear_G(n_x_grid),stat=ierr)
    else
      ! FIXME there is a mpigather below, but nevertheless the memory
      ! has to be allocated on all processes. Hence this is not
      ! really 'distributed memory parallelism'...
      allocate(zfshear_G(n_x_grid),stat=ierr)
    end if
    if (ierr /= 0) &
       & call gkw_abort('diagnos_zfshear :: could not allocate zfshear_G')

  end subroutine allocate_mem

  !--------------------------------------------------------------------
  !> Clean up, deallocate, close everything.
  !--------------------------------------------------------------------
  subroutine finalize()
    use io, only : close_lu, ascii_fmt
    use mpiinterface, only : root_processor

    if (.not.lcalc_zfshear) return
    
    ! deallocate all arrays of this diagnostic
    if(allocated(zfshear)) deallocate(zfshear)
    if(allocated(zfshear_G)) deallocate(zfshear_G)

    if(root_processor) then
      ! be nice and close all logical units
      call close_lu(i_zfshear, ascii_fmt)
    end if
  end subroutine finalize

  !--------------------------------------------------------------------
  !> This routine is called at the beginning of each run (after
  !> restarts, too).
  !--------------------------------------------------------------------
  subroutine initial_output()

    if (.not.lcalc_zfshear) return

  end subroutine initial_output

  !--------------------------------------------------------------------
  !> In contrast to the subroutine output(), the argument of
  !> the final_output() subroutine is optional.
  !> The number is provided if the code wants to print the result of
  !> an eigenmode calculation.
  !> If the GKW run is explicit (or implicit) then the number argument
  !> is not provided.
  !--------------------------------------------------------------------
  subroutine final_output(number)
    integer, intent(in), optional :: number

    if (.not.lcalc_zfshear) return

    if (present(number)) then
      ! output for every eigenmode
    else
      ! one single output
    end if

  end subroutine final_output

  !--------------------------------------------------------------------
  !> The routine calc_largestep() is to be called repeatedly, after every
  !> large timestep, and here the diagnostic should calculate its
  !> quantities.
  !> 
  !> Splitting calculation and output is to be encouraged, particularly when
  !>  - the output is higher dimensional, and requires MPI or MPI-IO
  !>  - same data is output in different dimensional slices
  !--------------------------------------------------------------------
  subroutine calc_largestep()
    use control, only : spectral_radius
    use dist, only : fdisi, get_phi, phi, nsolc
    use grid, only : nx, n_x_grid, ns
    use mode, only : kxrh, iyzero
    use mpicomms, only : COMM_S_NE, COMM_X_NE
    use mpiinterface, only : mpiallreduce_sum_inplace, gather_array

    integer :: is, ix
    complex :: second_derivative
    real :: delta

    if (.not.lcalc_zfshear) return
    
    delta = 1
    zfshear = 0.0
    
    call get_phi(fdisi(1:nsolc),phi)

    do ix = 1, nx
      do is = 1, ns

        ! calculate the second derivative of the potential
        if(spectral_radius) then
          ! by multiplying with k_x^2
          zfshear(ix) = zfshear(ix) + phi(iyzero,ix,is) * kxrh(ix)**2
          
        else
          ! with a finite difference formula
          if(ix > 1 .and. ix < nx-1) then
            ! central difference:
            second_derivative = (phi(iyzero,ix+1,is) - 2* phi(iyzero,ix,is) &
               & + phi(iyzero,ix-1,is))/(delta**2)
          else if(ix <= 1) then
            ! forward difference:
            second_derivative = (phi(iyzero,ix+2,is) - 2* phi(iyzero,ix+1,is) &
               & + phi(iyzero,ix,is))/(delta**2)
          else if(ix >= nx-1) then
            ! backward difference:
            second_derivative = (phi(iyzero,ix-2,is) - 2* phi(iyzero,ix-1,is) &
               & + phi(iyzero,ix,is))/(delta**2)
          end if
          zfshear(ix) = zfshear(ix) + second_derivative
        end if
      end do
    end do

    ! complete the flux surface averace by an MPI reduction
    call mpiallreduce_sum_inplace(zfshear, nx, COMM_S_NE)

    !if(root_processor) zfshear_G = 0
    call gather_array(zfshear_G, n_x_grid, &
       & zfshear, nx, COMM_X_NE, .false.)
    
    ! evtl. compute total shear rate, which is a single scalar value.
    ! The total shear rate is obtained by summing over all scales.
    ! ...

  end subroutine calc_largestep

  !--------------------------------------------------------------------
  !> The routine output() should do the output to files, using the
  !> routines provided by the io module.
  !--------------------------------------------------------------------
  subroutine output()
    use io, only : append_chunk, xy_fmt, ascii_fmt
    use mpiinterface, only : root_processor

    if (.not.lcalc_zfshear) return

    call calc_largestep()

    if(root_processor) then
      call append_chunk(i_zfshear, zfshear_G, xy_fmt, ascii_fmt)
    end if
  end subroutine output


end module diagnos_zfshear
