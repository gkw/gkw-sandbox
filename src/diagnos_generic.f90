!------------------------------------------------------------------------------
! SVN:$Id$
!> This module provides switches and some utility routines which are
!> seen and used by several of the specific diagnos_* modules.
!------------------------------------------------------------------------------
module diagnos_generic

  implicit none

  private
 
  public :: set_default_nml_values
  public :: init, finalize
  public :: bcast, check, allocate_mem
  public :: compute_fft_sizes
  public :: four2real_2D
  public :: dfieldds

  public :: velocity_slice_output
  public :: ky_output_array, xy_output_array

  public :: inv_fft_xy_slice
  
  public attach_metadata_grid

  interface attach_metadata_grid
    module procedure attach_metadata_grid_1d
    module procedure attach_metadata_grid_2d
    module procedure attach_metadata_grid_3d
    module procedure attach_metadata_grid_4d
    module procedure attach_metadata_grid_1d_name
    module procedure attach_metadata_grid_2d_name
    module procedure attach_metadata_grid_3d_name
    module procedure attach_metadata_grid_4d_name
  end interface attach_metadata_grid


  !> output switches which are seen by and used in several of the specific
  !> diagnos_* modules:
  !> switch the calculation of some rather expensive diagnostics:
  logical, save, public :: lwrite_output1
  !> logical to output XY slices *every* time
  !> step, otherwise just output at the end of the run:
  logical, save, public :: xy_estep
  
  !> switch that sets whether radial fluxes and background profiles
  !> are written out
  logical, save, public :: lradial_profile
  
  !> Logical if you want to follow the magnetic island rotation around
  logical, save, public :: lisl_follow

  !> 2d diagnostics will output slices at the global s location xy_slice_ipar_
  integer, save, public :: xy_slice_ipar

  !> Logicals to select various XY diagnostic outputs
  logical, save, public :: xy_fluxes
  logical, save, public :: xy_fluxes_fsa
  logical, save, public :: xy_fluxes_k
  logical, save, public :: xy_fluxes_bi
  logical, save, public :: xy_fluxes_p
  logical, save, public :: xy_fluxes_v
  logical, save, public :: xy_fluxes_em
  logical, save, public :: xy_fluxes_bpar
  logical, save, public :: xy_dens
  logical, save, public :: xy_temp
  logical, save, public :: xy_current
  logical, save, public :: xy_current2
  !> If you want the xy data also outputted per mode rather than in 
  !> real space.  
  logical, save, public :: xy_spec

  !> logicals for 3D output
  logical, save, public :: apa3d
  logical, save, public :: spc3d
  logical, save, public :: phi3d
  logical, save, public :: apc3d
  logical, save, public :: den3d
  logical, save, public :: ene3d
  logical, save, public :: bpa3d
  logical, save, public :: bpc3d
  !> any_3d_output is the logical OR of the *3d switches above.
  logical, save, public :: any_3d_output

  !----------------------------------------------------------------
  ! Deprecated, obsolete diagnostic namelist entries:
  !----------------------------------------------------------------
  logical, save, public :: lphi_diagnostics
  ! This switch does not switch
  ! "phi diagnostics" as the name suggests, but acts as a meta
  ! switch for 2d diagnostics.
  !----------------------------------------------------------------

  !> Suppression factor for zonal flows in the 3d output
  real, save, public :: zonal_scale_3d

  !> complex slice in xy for mpi reduction
  complex, save, allocatable, dimension(:,:), public :: c_xy
  !> real slice for outputing xy
  real, save, allocatable, dimension(:,:), public :: r_xy

  !> buffer of the size of the global velocity space grid
  real, allocatable, dimension(:,:), save :: global_vspace



  !> a buffer for xy_output_array
  real, save, allocatable :: xy_output_rdum(:,:)

  !----------------------------------------------------------------
  ! other useful stuff for diagnostics
  !----------------------------------------------------------------
  integer, save, allocatable, dimension(:), public :: parseval_correction
  integer, save, public :: mphit, mphiw3t, mrad_G, mrad_l

contains

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine init()

    continue

  end subroutine init



  !--------------------------------------------------------------------
  !> Set reasonable default values for the namelist items this
  !> diagnostic provides. 
  !--------------------------------------------------------------------
  subroutine set_default_nml_values()
    use control, only : flux_tube
    use grid, only : n_s_grid
    lwrite_output1 = .true.

    lradial_profile=(.not. flux_tube)
    lphi_diagnostics = .true.

    xy_slice_ipar = n_s_grid/2  + 1
    xy_estep = .true.

    lisl_follow    = .true.

    zonal_scale_3d = 1.0

    xy_fluxes = .false.
    xy_fluxes_fsa = .false.
    xy_fluxes_k = .false.
    xy_fluxes_bi = .false.
    xy_fluxes_p  = .false.
    xy_fluxes_v  = .false.
    xy_fluxes_em = .false.
    xy_fluxes_bpar = .false.
    xy_dens = .false.
    xy_temp = .false.
    xy_current = .false.
    xy_current2 = .false.

    xy_spec = .false.

    apa3d = .false.
    spc3d = .false.
    phi3d = .false.
    apc3d = .false.
    den3d = .false.
    ene3d = .false.
    bpa3d = .false.
    bpc3d = .false.

  end subroutine set_default_nml_values


  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine bcast()
    use mpiinterface, only : mpibcast

    call mpibcast(lwrite_output1,1)

    call mpibcast(xy_slice_ipar,1)
    call mpibcast(lradial_profile, 1)

    call mpibcast(XY_fluxes,1)
    call mpibcast(XY_fluxes_fsa,1)
    call mpibcast(XY_fluxes_k,1)
    call mpibcast(XY_fluxes_bi,1)
    call mpibcast(XY_fluxes_V,1)
    call mpibcast(XY_fluxes_P,1)
    call mpibcast(XY_fluxes_em,1)
    call mpibcast(XY_fluxes_bpar,1)
    call mpibcast(XY_dens,1)
    call mpibcast(XY_temp,1)
    call mpibcast(XY_spec,1)
    call mpibcast(XY_current,1)
    call mpibcast(XY_current2,1)
    call mpibcast(apa3d,1)
    call mpibcast(bpa3d,1)
    call mpibcast(den3d,1)
    call mpibcast(ene3d,1)
    call mpibcast(spc3d,1)
    call mpibcast(apc3d,1)
    call mpibcast(bpc3d,1)
    call mpibcast(phi3d,1)
    call mpibcast(xy_estep,1)

    call mpibcast(lphi_diagnostics,1)

    call mpibcast(zonal_scale_3d,1)
    
    call mpibcast(lisl_follow,1)
  end subroutine bcast

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine check()
    use mode, only : mode_box
    use grid, only : n_s_grid
    use control, only : spectral_radius
    use general, only : gkw_warn, gkw_abort
    
    ! Select the global s-grid location at which we should make the xy slices.
    if (.not.(xy_slice_ipar >= 1 .and. xy_slice_ipar <= n_s_grid)) then
      ! default is the middle of the grid
      xy_slice_ipar = n_s_grid/2 + 1
      call gkw_warn('xy_slice_ipar out of range, will use central point')
    end if

    ! decide if there will be any 3D output, then print a warning
    any_3d_output = (apa3d .or. spc3d .or. phi3d .or. apc3d .or. den3d .or. &
       & ene3d .or. bpa3d .or. bpc3d)
    if (any_3d_output) then
      call gkw_warn('3D output data can be very large')

      if (.not. mode_box) then
        call gkw_abort('write_output: To get 3D potential output data, '// &
           & 'mode_box must also be set to true')
      end if
    end if

    if (spectral_radius .and. lradial_profile) then
      call gkw_warn('The radial profiles have no physical meaning running spectral.')
    endif


  end subroutine check

  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine allocate_mem()
    use grid, only : nmod, nx, n_x_grid
    use mode, only : iyzero
    use general, only : gkw_abort
    integer :: ierr

    call compute_fft_sizes(nmod, n_x_grid, nx, &
     & mphit, mphiw3t, mrad_G, mrad_l)
    
    allocate(parseval_correction(nmod), stat = ierr)
    if (ierr.ne.0) then
      call gkw_abort('Could not allocate parseval_correction diagnos_generic')
    endif

    ! might be allocated in vain - but if it does exist, it is a
    ! helpful buffer at several places in diagnostics.
    if(.not. allocated(r_xy)) then
      allocate(r_xy(mphit,mrad_l),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnos_generic :: r_xy')
    end if

    !FIXME how to integrate if mode_box false?
    if(nmod > 1 .and. iyzero > 0) then
      parseval_correction = 2
      ! then the first mode is the zero mode
      parseval_correction(iyzero) = 1
    else if(nmod == 1) then
      parseval_correction = 2
    else
      parseval_correction = 1
    end if


  end subroutine allocate_mem

  !****************************************************************************
  !> Generic function to output 2 grid in velocity space, handles MPI.
  !> Give it a local (real) 2D velocity space array and it outputs the
  !> global one.
  !> Call this routine with all processors.  isg and ispg are GLOBAL species 
  !> and s points for which you want to write. (if array is not dependent on
  !> these then choose 1,1 for example)
  !----------------------------------------------------------------------------
  subroutine velocity_slice_output(groupname,local,luname,ixg,isg,ispg, &
     & preference_if_mixed)
    use io,           only : output_array
    use io,           only : xy_fmt, binary_fmt, ascii_fmt
    use grid,         only : nmu,nvpar,n_vpar_grid,n_mu_grid
    use grid,         only : proc_subset
    use mpiinterface, only : gather_array
    use mpicomms,     only : COMM_VPAR_NE, COMM_MU_NE

    character (len=*), intent(in) :: groupname
    real, dimension(nvpar,nmu), intent(in) :: local
    integer, intent(in) :: isg, ispg, ixg
    character (len=*), intent(in) :: luname
    character (len=*), intent(in) :: preference_if_mixed

    if (.not.proc_subset(ixg,isg,0,0,ispg)) return

    ! allocate array to contain the full slice
    if (.not. allocated(global_vspace)) allocate(global_vspace(n_vpar_grid,n_mu_grid))

    ! gather the data
    call gather_array(global_vspace,n_vpar_grid,n_mu_grid,local,      &
       &           nvpar,nmu,COMM_VPAR_NE,COMM_MU_NE,ALLGATHER=.true.)

    ! write with 1 processor
    if (proc_subset(ixg,isg,1,1,ispg)) then
      call output_array(trim(luname), groupname, &
         & global_vspace, 'F', xy_fmt, preference_if_mixed)
    end if

  end subroutine velocity_slice_output


  !--------------------------------------------------------------------
  !>
  !--------------------------------------------------------------------
  subroutine compute_fft_sizes(nmod, n_x_grid, nx, &
     & mphit, mphiw3t, mrad_G, mrad_l)
    use control, only : spectral_radius
    use non_linear_terms, only : mphi, mphiw3, mrad
    use non_linear_terms, only : get_extended_firstdim_fft_size
    integer, intent(in) :: nmod, n_x_grid, nx
    integer, intent(out) :: mphit, mphiw3t, mrad_G, mrad_l
    
    !If the number of nmod is small the XY diagnostic lacks points in
    !the poloidal direction...this pads the ffts with
    !zeros so there are more points in that direction
    if(nmod.lt.4)then
      ! Calculate the size of the grid for the FFT
      call get_extended_firstdim_fft_size(4, mphit, mphiw3t)
    else
      mphit = mphi
      mphiw3t = mphiw3
    end if

    if (.not. spectral_radius) then
      mrad_G = n_x_grid
      mrad_l = nx ! /= mrad in nonlinear terms which contains ghost cells
    else
      mrad_l = mrad
      mrad_G = mrad
    end if

  end subroutine compute_fft_sizes

  !----------------------------------------------------------------------------
  !>
  !----------------------------------------------------------------------------
  subroutine inv_fft_xy_slice(slice_xky_in, r_xy_out)
    use components, only : tearingmode, isl_mode
    use tearingmodes, only : omega_rot
    use grid, only : nmod, nx
    use control, only : time
    use non_linear_terms, only : jind
    use general, only : gkw_abort
    
    !> the input argument is a perpendicular slice in fourier representation,
    !> so either in kxky or in xky .
    complex, dimension(nmod,nx), intent(in) :: slice_xky_in
    real, dimension(mphit,nx), intent(out) :: r_xy_out

    integer :: ix, imod, ierr

    if(.not. allocated(c_xy)) then
      allocate(c_xy(mphiw3t,mrad_l),stat=ierr)
      if (ierr /= 0) call gkw_abort('diagnos_generic :: c_xy')
    end if

    ! Get the real space array via the inverse FFT.
    c_xy = (0.,0.)
    do ix = 1, nx
      do imod = 1, nmod
        c_xy(imod,jind(ix)) = slice_xky_in(imod,ix)
      end do
    end do

    ! are we following an island?
    ! FIXME there is an 'imod' lying
    ! around here, whose value is equal to nmod. Is this looks as if
    ! it was not intended?
    if (tearingmode .and. lisl_follow) then
      c_xy = c_xy*exp(cmplx(0.,(imod-1)*omega_rot*time)/(isl_mode-1))
    end if

    !r_xy_out = 0. !overwritten anyway
    ! call the inverse FFT routine
    call four2real_2D(r_xy_out,c_xy,1)
    
  end subroutine inv_fft_xy_slice
  
  !****************************************************************************
  !> output a real array in real space given a complex array in k space
  !> including the Fourier Transform and global gather in x if needed
  !> selects correct FFT 1D or 2D for both spectral and nonspectral
  !> Call with all x procs, but only one proc in each other parallel direction
  !----------------------------------------------------------------------------
  subroutine xy_output_array(groupname, array,fileprefix,filesuffix)
    use io,               only : output_array
    use io,               only : xy_fmt, binary_fmt, ascii_fmt
    use grid,             only : nx, nmod, proc_subset
    use mpiinterface,     only : gather_array
    use mpicomms,         only : COMM_DUMMY, COMM_X_NE
    use general,          only : gkw_abort

    character (len=*), intent(in) :: groupname
    complex, dimension(nmod,nx), intent(in) :: array
    character (len=*), intent(in) :: fileprefix, filesuffix
    integer :: ierr
    
    if(.not. allocated(xy_output_rdum)) then
      allocate(xy_output_rdum(mphit,mrad_G),stat=ierr)
      if (ierr /= 0) call gkw_abort(groupname//' :: xy_output_array')
    end if

    call inv_fft_xy_slice(array, r_xy)

    !gather global array in x (allgather)
    call gather_array(xy_output_rdum(1:mphit,1:mrad_G),mphit,mrad_G, &
       & r_xy(1:mphit,1:mrad_l),mphit,mrad_l, &
       & COMM_DUMMY,COMM_X_NE,ALLGATHER=.true.)

    ! use only one x processor for output
    if(proc_subset(1,0,0,0,0)) then
      ! NOTE with the rewrite of the io, both the binary
      ! and the ascii output produced by the following line are in 'F'
      ! order. Before, the ascii output used to be in 'C' order.
      ! call output_array(fileprefix//filesuffix, groupname, &
      !    & xy_output_rdum, 'F', xy_fmt, binary_fmt)
      call output_array(fileprefix//filesuffix, groupname, &
         & xy_output_rdum, 'C', xy_fmt, binary_fmt)
    end if

  end subroutine xy_output_array

  !****************************************************************************
  !> output real array in fourier space includes the gather in x when required
  !> Produces different output for spectral (ky(kx)) and nonspectral (ky(xgr))
  !> Call with all x procs, but only one proc in each other parallel direction
  !----------------------------------------------------------------------------
  subroutine ky_output_array(groupname, array,fileprefix,filesuffix)

    use io,           only : output_array
    use io,           only : xy_fmt, binary_fmt, ascii_fmt
    use grid,         only : nmod,nx, n_x_grid, proc_subset
    use mpiinterface, only : gather_array
    use mpicomms,     only : COMM_DUMMY, COMM_X_NE

    character (len=*), intent(in) :: groupname
    character (len=*), intent(in) :: fileprefix, filesuffix
    real, intent(in) :: array(nmod,nx)
    real :: rdum(nmod,n_x_grid)

    !gather global array in x (allgather)
    call gather_array(rdum(1:nmod,1:n_x_grid),nmod,n_x_grid,       &
       & array(1:nmod,1:nx),nmod,nx,              &
       & COMM_DUMMY,COMM_X_NE,ALLGATHER=.true.) 

    ! use only one x processor for output                     
    if (.not. proc_subset(1,0,0,0,0)) return

    ! NOTE with the rewrite of the io, both the binary
    ! and the ascii output produced by the following line are in 'F'
    ! order. Before, the ascii output used to be in 'C' order.
    ! call output_array(fileprefix//filesuffix, groupname, &
    !      & rdum, 'F', xy_fmt, binary_fmt)
    call output_array(fileprefix//filesuffix, groupname, &
         & rdum, 'C', xy_fmt, binary_fmt)
  end subroutine ky_output_array



  !****************************************************************************
  !> Transforms 2D arrays in x,y from k space to real space
  !> Selects the correct transform based on spectral_radius or not
  !> Note that the input array is destroyed by the FFT.
  !----------------------------------------------------------------------------
  subroutine four2real_2D(r_out,c_in,dir)

    use control, only  : spectral_radius
    use fft,     only  : four2D_real, four1D_real

    !> complex array (input for forward)
    !complex, intent(inout)  :: c_in(mphiw3t,mrad_l)
    complex, intent(inout)  :: c_in(:,:)
    !> real_array (output for forward)
    !real,    intent(inout)  :: r_out(mphit,mrad_l)
    real,    intent(inout)  :: r_out(:,:)
    !> direction of transform (1 or -1)
    integer, intent(in)     :: dir
    integer :: ix

    if (spectral_radius) then 
      call four2D_real(r_out,c_in,dir) 
      !scale the FFT by sqrt(n), not needed, values are independant of gridsize
      !r_out=r_out/sqrt(real(mphit*mrad_G))
    else   
      !  FJC: replace with fourrow_real once working in nl_terms
      do ix = 1, size(r_out,2)!mrad_l
        call four1D_real(r_out(:,ix),c_in(:,ix),dir)
      end do
      !scale the FFT by sqrt(n), not needed, values are independant of gridsize
      !r_out=r_out/sqrt(real(mphit))
    end if

  end subroutine four2real_2D

  !--------------------------------------------------------------------------
  !> This routine calculates the s derivative of the gyroaveraged fields. it
  !> uses the differential scheme introduced in linear_terms.F90. ifield is
  !> either iphi,iapar or bpar, dfield is an complex array of the
  !> dimension i,j.
  !>
  !> The returned array contains the parallel derivative.
  !-------------------------------------------------------------------------
  subroutine dfieldds(ifield,imod,ix,j,is,dfield)
    use linear_terms, only : differential_scheme
    use matdat, only : pos_par_grid, connect_parallel, set_indx
    use structures, only : matrix_element
    use fields, only : get_averaged_apar,get_averaged_bpar,get_averaged_phi
    use dist        , only : iphi, iapar, ibpar, fdisi
    use mode        , only : parallel_phase_shift
    use geom        , only : sgr_dist
    use grid        , only : ns 
    use general,      only : gkw_abort

    integer, intent(in) :: ifield, imod,ix,j,is
    ! the s derivative of the field
    complex, intent(out):: dfield(:)
    ! required to call connect_parallel
    complex, allocatable :: field(:)
    type (matrix_element) :: elem
    integer :: ist, ipw, id, ierr, m, i, iloc, k
    logical ::ingrid
    real, dimension(5) :: w

    if(.not. allocated(field)) then
      allocate(field(ns), stat=ierr)
      if(ierr.ne.0) call gkw_abort('could not allocate field')
    end if
    
    !initialise
    dfield=0
    do i=1, ns
      select case (ifield)
      case(iphi)
        field(i)=get_averaged_phi(imod,ix,i,j,is,fdisi)
      case(iapar)
        field(i)=get_averaged_apar(imod,ix,i,j,is,fdisi)
      case(ibpar)
        field(i)=get_averaged_bpar(imod,ix,i,j,is,fdisi)
      case default
        call gkw_abort('unknown ifield in diagnos_generic::dfieldds')
      end select
    end do

    ipw = -1 
    id = + 1 

    do i=1,ns
      ! select the scheme 
      ist = pos_par_grid(imod,ix,i,k)
      call differential_scheme(ist,ipw,id,w) 
      call set_indx(elem,imod,ix,i,j,k,is)
      do m = 1, 5 

        if (w(m).ne.0.) then 

          iloc  = i + m - 3 
          elem%iloc=iloc
          call connect_parallel(elem,ingrid)
          if (ingrid) then
            dfield(i) = dfield (i) +  w(m) *field(elem%iloc) / sgr_dist
            dfield(i) = dfield (i) * parallel_phase_shift(imod,ix,i,elem%iloc)
          endif
        endif
      enddo;
    enddo
  end subroutine dfieldds

  !--------------------------------------------------------------------
  !> Clean up, deallocate, close everything.
  !--------------------------------------------------------------------
  subroutine finalize()

    if (allocated(c_xy)) deallocate(c_xy)
    if (allocated(r_xy)) deallocate(r_xy)
    if (allocated(global_vspace)) deallocate(global_vspace)

    if (allocated(xy_output_rdum)) deallocate(xy_output_rdum)

    if (allocated(parseval_correction)) deallocate(parseval_correction)
    
  end subroutine finalize

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_1d(lun, gridname1, preference_if_mixed)
    use io, only : attach_metadata
    integer, intent(in) :: lun
    character(len=*), intent(in) :: gridname1
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(lun, 'grid1', gridname1, preference_if_mixed)

  end subroutine attach_metadata_grid_1d

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_2d(lun, gridname1, gridname2, &
     & preference_if_mixed)
    use io, only : attach_metadata
    integer, intent(in) :: lun
    character(len=*), intent(in) :: gridname1, gridname2
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(lun, 'grid1', gridname1, preference_if_mixed)
    call attach_metadata(lun, 'grid2', gridname2, preference_if_mixed)

  end subroutine attach_metadata_grid_2d

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_3d(lun, gridname1, gridname2, gridname3, &
     & preference_if_mixed)
    use io, only : attach_metadata
    integer, intent(in) :: lun
    character(len=*), intent(in) :: gridname1, gridname2, gridname3
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(lun, 'grid1', gridname1, preference_if_mixed)
    call attach_metadata(lun, 'grid2', gridname2, preference_if_mixed)
    call attach_metadata(lun, 'grid3', gridname3, preference_if_mixed)

  end subroutine attach_metadata_grid_3d

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_4d(lun, &
     & gridname1, gridname2, gridname3, gridname4, &
     & preference_if_mixed)
    use io, only : attach_metadata
    integer, intent(in) :: lun
    character(len=*), intent(in) :: gridname1, gridname2, gridname3, gridname4
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(lun, 'grid1', gridname1, preference_if_mixed)
    call attach_metadata(lun, 'grid2', gridname2, preference_if_mixed)
    call attach_metadata(lun, 'grid3', gridname3, preference_if_mixed)
    call attach_metadata(lun, 'grid4', gridname4, preference_if_mixed)

  end subroutine attach_metadata_grid_4d

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_1d_name(luname, groupname, gridname1, preference_if_mixed)
    use io, only : attach_metadata
    character(len=*), intent(in) :: luname, groupname
    character(len=*), intent(in) :: gridname1
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(luname, groupname, 'grid1', gridname1, preference_if_mixed)

  end subroutine attach_metadata_grid_1d_name

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_2d_name(luname, groupname, gridname1, gridname2, &
     & preference_if_mixed)
    use io, only : attach_metadata
    character(len=*), intent(in) :: luname, groupname
    character(len=*), intent(in) :: gridname1, gridname2
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(luname, groupname, 'grid1', gridname1, preference_if_mixed)
    call attach_metadata(luname, groupname, 'grid2', gridname2, preference_if_mixed)

  end subroutine attach_metadata_grid_2d_name

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_3d_name(luname, groupname, gridname1, gridname2, gridname3, &
     & preference_if_mixed)
    use io, only : attach_metadata
    character(len=*), intent(in) :: luname, groupname
    character(len=*), intent(in) :: gridname1, gridname2, gridname3
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(luname, groupname, 'grid1', gridname1, preference_if_mixed)
    call attach_metadata(luname, groupname, 'grid2', gridname2, preference_if_mixed)
    call attach_metadata(luname, groupname, 'grid3', gridname3, preference_if_mixed)

  end subroutine attach_metadata_grid_3d_name

  !--------------------------------------------------------------------
  !> 
  !--------------------------------------------------------------------
  subroutine attach_metadata_grid_4d_name(luname, groupname, &
     & gridname1, gridname2, gridname3, gridname4, &
     & preference_if_mixed)
    use io, only : attach_metadata
    character(len=*), intent(in) :: luname, groupname
    character(len=*), intent(in) :: gridname1, gridname2, gridname3, gridname4
    character (len=*), intent(in) :: preference_if_mixed

    call attach_metadata(luname, groupname, 'grid1', gridname1, preference_if_mixed)
    call attach_metadata(luname, groupname, 'grid2', gridname2, preference_if_mixed)
    call attach_metadata(luname, groupname, 'grid3', gridname3, preference_if_mixed)
    call attach_metadata(luname, groupname, 'grid4', gridname4, preference_if_mixed)

  end subroutine attach_metadata_grid_4d_name


end module diagnos_generic
