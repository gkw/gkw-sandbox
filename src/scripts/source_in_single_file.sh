#!/bin/sh
one_file='gkw_in_one_file.F90'
rm -f $one_file
for prefix in global \
	external/specfun \
	external/marsaglia \
	fft \
	mpicomms \
	constants \
	mpidatatypes \
	mpiinterface \
	general \
	control \
	grid \
	dist \
	io \
	geom \
	mode \
	rotation \
	components \
	matdat \
	velocitygrid \
	collisionop \
	functions \
	normalise \
	non_linear_terms \
	tearingmodes \
	diagnostic \
	exp_integration \
	init \
	linear_terms \
	linart
do
	cat $prefix.*90 >> $one_file
done
