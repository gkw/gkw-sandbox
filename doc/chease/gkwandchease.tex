\documentclass[a4paper,12pt]{article}

\newcommand{\ind}[1]{\ensuremath{_{\rm{#1}}}}
\newcommand{\expo}[1]{\ensuremath{^{\rm{#1}}}}


\usepackage{amsmath}
\usepackage{amssymb}

\setlength\parindent{0in}
\oddsidemargin 0.0in
\evensidemargin 1.0in
\textwidth 6.0in
\textheight 9.0in
\headheight 0.0in
\topmargin 0.0in

% Title Page
\title{CHEASE/GKW Interface}
\author{Y. Camenen}
\date{September 11, 2008}

\begin{document}
\maketitle

%\begin{abstract}
%
%\end{abstract}

This rather technical report aims at documenting the addition of a small module in CHEASE to compute the metric elements required for GKW and of another module in GKW to read the CHEASE outputs.

%*********************************************************************
\part{Background}
%*********************************************************************
\section{CHEASE coordinates}
In CHEASE \cite{Lutjens:Comput1992, Lutjens:Comput1996}, the Grad-Shafranov equation is solved on a $\left(\sigma,\theta,\phi\right)$ grid, where $\theta$ and $\phi$ are the poloidal and toroidal
angle, respectively. The
non-orthogonal coordinate system $\left(\sigma,\theta,\phi\right)$ is related to the cylindrical coordinates system $\left(R,Z,\phi\right)$ by
\begin{equation}
\begin{aligned}
R&=\sigma\rho\ind{s}(\theta)\cos{\theta} + R\ind{c} \nonumber \\
Z&=\sigma\rho\ind{s}(\theta)\sin{\theta} + Z\ind{c} \nonumber
\end{aligned}
\end{equation}
where $0 \leq \sigma \leq 1$, $0\leq \theta \leq 2\pi$, $\left(R\ind{c},Z\ind{c}\right)$ is the center of the $\left(\sigma,\theta\right)$ grid and $\rho\ind{s}(\theta)$ is the distance from $\left(R\ind{c},Z\ind{c}\right)$ to the plasma boundary (assumed to be known). Note that $\sigma$ is not a flux label.

After the Grad-Shafranov equation has been solved, the equilibrium is mapped into flux coordinates $\left(\Psi,\chi,\phi\right)$ where
$\Psi=\frac{1}{2\pi}\int\ind{S_{pol}}\textbf{n}\cdot\textbf{B}\,{\rm d}S$ is the poloidal magnetic flux and $\chi$ is a generalized poloidal angle specified by the choice of the jacobian. Note that in
the Grad-Shafranov solver part (i.e. before the mapping), $\Psi$ is negative inside the plasma (the plasma current flows in the direction of $\nabla\phi$) and minimum on the magnetic axis. However,
for the flux coordinates, the poloidal flux is renormalized to be positive in the plasma and to increase towards the edge. This allows to have a positive radial coordinate, as usual.\\
In CHEASE, the jacobian $J_{\Psi\chi\phi}=\left[\nabla\Psi\times\nabla\chi\cdot\nabla\phi\right]^{-1}$ can have the following forms:
$$J_{\Psi\chi\phi}=C(\Psi)R\expo{NER}\left|\nabla\Psi\right|\expo{NEGP}$$
where NER and NEGP are integers specified in the input file and $C(\Psi)$ is determined by imposing that $\chi$ increases by $2\pi$ per poloidal turn. It can be shown (see Appendix A in \cite{Lutjens:Comput1992}) that:  
$$\chi(\theta)=\int_0^\theta\frac{R\sigma\rho\ind{s}^2(\tilde{\theta})}{J_{\Psi\chi\phi}\partial\Psi/\partial{\sigma}}\,{\rm d}\tilde{\theta}$$
Defining $s\ind{B}=\textrm{sign}(\bf{B}\cdot\nabla\phi)$ and $s\ind{j}=\textrm{sign}(\bf{j}\cdot\nabla\phi)$, where $\bf{j}$ is the current density, the magnetic field (axisymmetric device) is
represented as 
$$\textbf{B}=s\ind{B}F(\Psi)\nabla{\phi}+s\ind{j}\nabla\phi\times\nabla\Psi$$
with $F>0$ and $\nabla\Psi$ is directed from the magnetic axis to the edge. It leads to:
\begin{equation}
\begin{aligned}
B^\Psi&=\textbf{B}\cdot\nabla\Psi=0 \nonumber \\
B^\chi&=\textbf{B}\cdot\nabla\chi=\frac{s\ind{j}}{J_{\Psi\chi\phi}} \nonumber \\
B^\phi&=\textbf{B}\cdot\nabla\phi=s\ind{B}F\left|\nabla\phi\right|^2=\frac{s\ind{B}F}{R^2} \nonumber
\end{aligned}
\end{equation}
By convention, in CHEASE, $s\ind{B}=-1$ and $s\ind{j}=-1$ for $\nabla \phi$ oriented as in GKW, i.e. $(R,Z,\phi)$ direct. In fact, in CHEASE $\nabla \phi\expo{ch}=-\nabla \phi$ is oriented
such as to have $(R,\phi\expo{ch},Z)$ direct. However, in the code the jacobian is always defined positive and taken to be $|J_{\Psi\chi\phi\expo{ch}}|$ .


\section{GKW coordinates}
GKW uses a field aligned flux coordinate system $\left(\psi,\zeta,s\right)$ with constant jacobian on a flux surface (Hamada coordinates with third coordinate transformed). Here, $\psi$ is a flux surface label (not necessarily equal to the $\Psi$ used in CHEASE) and $-1/2\leq s\leq1/2$ for one poloidal turn ($s=0$ on the LFS midplane).

In section 2.1 of the GKW manual, it is shown that provided a toroidal coordinate system $\left(\psi,s,\phi\right)$ with $B^s$ being a flux label, it is possible to build the required field aligned flux coordinate system $\left(\psi,\zeta,s\right)$ with constant jacobian.

\section{Construction of the GKW coordinates from the CHEASE coordinates}
In CHEASE, a constant jacobian on a flux surface can be specified for the $\left(\Psi,\chi,\phi\right)$ coordinate system by setting $NER=0$ and $NEGP=0$ in the input file. With this choice, the
$\left(\Psi,\chi,\phi\right)$ coordinate system is neither straight field line, nor field aligned, however, $B^\chi=\frac{s\ind{j}}{J_{\Psi\chi\phi}}=\frac{s\ind{j}}{C(\Psi)}$ is a flux label.

From now on, $\psi=\Psi$ is chosen for the radial coordinate of the $\left(\psi,\zeta,s\right)$ GKW coordinate system.

\subsection{Transformation to straight field line coordinates}
The following transformations are applied:
\begin{equation}
\begin{aligned}
s&= \frac{\chi}{2\pi}\nonumber \\
\gamma&= \frac{\phi}{2\pi}+s\ind{B}\frac{F}{2\pi} \int_0^\chi\frac{{\rm d}\tilde{\chi}}{\textbf{B}\cdot\nabla\tilde{\chi}}\left[\left<\frac{1}{R^2} \right>
-\frac{1}{R^2(\Psi,\tilde{\chi})}\right]\nonumber
\end{aligned}
\end{equation}
leading to
\begin{equation}
\begin{aligned}
\nabla s&= \frac{\nabla\chi}{2\pi} \nonumber \\
\nabla \gamma&= \frac{\nabla\phi}{2\pi} +
s\ind{B}\frac{FJ_{\Psi\chi\phi}}{2\pi}\left[\left<\frac{1}{R^2} \right> -\frac{1}{R^2}\right]\nabla\chi + \frac{\partial \gamma}{\partial \Psi}\nabla\Psi \nonumber 
\end{aligned}
\end{equation}
and
\begin{equation}
\begin{aligned}
B^s&=\textbf{B}\cdot\nabla s=\frac{s\ind{j}}{2\pi J_{\Psi\chi\phi}} \nonumber \\
B^\gamma&=\textbf{B}\cdot\nabla\gamma=\frac{s\ind{B}F}{2\pi}\left<\frac{1}{R^2} \right> \nonumber
\end{aligned}
\end{equation}
Therefore, $B^s/B^\gamma$ is constant on a flux surface and $\left(\Psi,s,\gamma\right)$ is a straight field line coordinate system with the jacobian 
$$J_{\Psi s \gamma}=\left[\nabla\Psi\times\nabla s\cdot\nabla\gamma\right]^{-1}=4\pi^2J_{\Psi\chi\phi}>0$$ being a flux label.
In addition, the local field line pitch $B^\gamma/B^s$ is equal to its average value, the safety factor: $$q=s\ind{B}s\ind{j}FJ_{\Psi\chi\phi}\left<\frac{1}{R^2} \right>$$
The sign of the safety factor depends on the relative direction of the magnetic field and plasma current. 

\subsection{Transformation to field aligned coordinates}
The following transformation is then applied
$$\zeta=qs-\gamma$$
for which 
$$B^\zeta=\textbf{B}\cdot\nabla\zeta=qB^s-B^\gamma=0$$
%and
%$$\nabla\zeta=s\frac{\partial q}{\partial %\Psi}\nabla\Psi+\frac{q}{2\pi}\nabla\chi-\frac{\nabla\phi}{2\pi}-\frac{F}{2\pi}\frac{\nabla\chi}{\textbf{B}\cdot\nabla\chi}\left[\left<\frac{1}{R^2} \right> -\frac{1}{R^2}\right]
%- \frac{\partial \gamma}{\partial \Psi}\nabla\Psi$$
%- \frac{\nabla \Psi }{2\pi}\frac{\partial F}{\partial \Psi}\int_0^\chi\frac{{\rm d}\chi}{\textbf{B}\cdot\nabla\chi}\left[\left<\frac{1}{R^2} \right> -\frac{1}{R^2}\right]$$
The right handed coordinate system $\left(\Psi,\zeta,s\right)$ is field aligned and its jacobian is $$J_{\Psi\zeta s}=J_{\Psi s \gamma}=4\pi^2J_{\Psi\chi\phi}$$



\section{Calculation of $(\Psi,\zeta,s)$ and of the associated metric}
\subsection{Construction of $s$ and $\nabla s$}
The construction of $s$ from $\chi$ is straightforward, as well as the calculation of $\nabla s$:
\begin{align*}
s&=\frac{\chi}{2\pi}\\
\nabla s &= \frac{1}{2\pi}\nabla\chi\\
\end{align*}
By definition, $s$ increases in the same direction as the poloidal angle $\theta$.

\subsection{Construction of $\zeta$ and $\nabla \zeta$}
Remembering $q=s\ind{B}s\ind{j}FJ_{\Psi\chi\phi}\left<\frac{1}{R^2}\right>$, it can be shown that $\zeta$ simplifies to:
$$\zeta=-\frac{\phi}{2\pi}+s\ind{B}s\ind{j}\frac{F(\Psi)J_{\Psi\chi\phi}(\Psi)}{2\pi}\int_0^\chi \frac{1}{R^2(\Psi,\tilde{\chi})}\,{\rm d}\tilde{\chi}$$
The construction of $\zeta$ therefore only requires to know:
$$\,F(\Psi),\,J_{\Psi\chi\phi}(\Psi) \textrm{ and } \mathcal{A}(\Psi,\chi)=\int_0^\chi \frac{1}{R^2(\Psi,\tilde{\chi})}\,{\rm d}\tilde{\chi}$$
The last term can be calculated from the expression of $g^{33}=|\nabla\phi|^2=\frac{1}{R^2}$ given on the $(\Psi,\chi,\phi)$ grid.

For the calculation of $\nabla\zeta$, we have:
\begin{align*}
\frac{\partial\zeta}{\partial\Psi}&=s\ind{B}s\ind{j}\frac{1}{2\pi}\left[ \frac{\partial F}{\partial \Psi}J_{\Psi\chi\phi}\mathcal{A}  + F\frac{\partial J_{\Psi\chi\phi}}{\partial \Psi}\mathcal{A}
+FJ_{\Psi\chi\phi}\frac{\partial \mathcal{A}}{\partial\Psi}\right]\\ 
\frac{\partial\zeta}{\partial\chi}&=s\ind{B}s\ind{j}\frac{FJ_{\Psi\chi\phi}}{2\pi}\frac{1}{R^2}\\ 
\frac{\partial\zeta}{\partial\phi}&=-\frac{1}{2\pi}\\ 
\end{align*}
The calculation of $\nabla \gamma$ therefore requires three additional terms: 
$$\frac{\partial F}{\partial\Psi},\, \frac{\partial J_{\Psi\chi\phi}}{\partial\Psi} \textrm{ and } \frac{\partial \mathcal{A}}{\partial\Psi}$$

\subsection{Metric tensor}
For any axisymetric function $h(\Psi,\chi)$, we have:
\begin{align*}
\frac{\partial h}{\partial \zeta}&=\frac{\partial h}{\partial \Psi}\frac{\partial \Psi}{\partial \zeta}+\frac{\partial h}{\partial \chi}\frac{\partial \chi}{\partial \zeta}=0 \\
\frac{\partial h}{\partial s}&=2\pi\frac{\partial h}{\partial \chi}
\end{align*}
The metric elements are calculated in CHEASE (in the \texttt{hamada.f90} module) using:
\begin{align*}
g^{\Psi\Psi}&=g^{\Psi\Psi}\\
g^{\Psi\zeta}&=g^{\Psi\Psi}\frac{\partial\zeta}{\partial \Psi}+g^{\Psi\chi}\frac{\partial\zeta}{\partial \chi}\\
g^{\Psi s}&=\frac{g^{\Psi\chi}}{2\pi}\\
g^{\zeta\zeta}&=g^{\Psi\Psi}\left(\frac{\partial\zeta}{\partial \Psi}\right)^2+g^{\chi\chi}\left(\frac{\partial\zeta}{\partial \chi}\right)^2
	+\frac{g^{\phi\phi}}{4\pi^2}+2g^{\Psi\chi}\frac{\partial\zeta}{\partial \Psi}\frac{\partial\zeta}{\partial \chi}\\
g^{\zeta s}&=\frac{g^{\Psi\chi}}{2\pi}\frac{\partial\zeta}{\partial \Psi}+\frac{g^{\chi\chi}}{2\pi}\frac{\partial\zeta}{\partial \chi}\\
g^{s s}&=\frac{g^{\chi\chi}}{4\pi^2}\\
\end{align*}
These metric elements are an output of CHEASE with $s\ind{B}>0$ and $s\ind{j}>0$. In GKW, the radial coordinate is not $\Psi$, but $\varepsilon=\frac{1}{2R\ind{ref}}(R\ind{max}-R\ind{min)}$
and the signs of the magnetic field and the current are taken into account, leading to the following modification (done in the \texttt{geom.F90} module of GKW) of the metric elements:
\begin{align*}
g^{\varepsilon\varepsilon}_{GKW}&=\left(\frac{\partial\varepsilon}{\partial\Psi}\right)^2g^{\Psi\Psi}\\
g^{\varepsilon\zeta}_{GKW}&=s\ind{B}s\ind{j}\frac{\partial\varepsilon}{\partial\Psi}g^{\Psi\zeta}\\
g^{\varepsilon s}_{GKW}&=\frac{\partial\varepsilon}{\partial\Psi}g^{\Psi s}\\
g^{\zeta\zeta}_{GKW}&=g^{\zeta\zeta}\\
g^{\zeta s}_{GKW}&=s\ind{B}s\ind{j}g^{\zeta s}\\
g^{s s}_{GKW}&=g^{s s}\\
\end{align*}



\subsection{Other metric elements}
\subsubsection{$\mathcal{F}$}
$$\mathcal{F}=\frac{R\ind{ref}B^s}{B}=\frac{s\ind{j}R\ind{ref}}{2\pi BJ_{\Psi\chi\phi}}=\frac{2\pi s\ind{j} R\ind{ref}}{ BJ_{\Psi\zeta s}}$$
\subsubsection{$\mathcal{D}^\alpha$}
$$\mathcal{D}^\alpha=-\frac{2}{B}\mathcal{E}^{\alpha\beta}\frac{\partial B}{\partial x^\beta}$$
\subsubsection{$\mathcal{E}^{\alpha\beta}$}
Noting that $\textbf{B}$ can be written:
$$\textbf{B}=J_{\Psi\zeta s}B^s\nabla\Psi\times\nabla\zeta=4\pi^2J_{\Psi\chi\phi}B^s\nabla\Psi\times\nabla\zeta=2\pi s\ind{j}\nabla\Psi\times\nabla\zeta$$
we have:
$$\mathcal{E}^{\alpha\beta}=\frac{R\ind{ref}^2}{2BB\ind{N}}(\nabla x^\alpha\times\nabla x^\beta) \cdot \textbf{B}=s\ind{j}\frac{2\pi R\ind{ref}^2}{2BB\ind{N}}(\nabla x^\alpha\times\nabla
x^\beta)\cdot(\nabla\Psi\times\nabla\zeta)$$
$$\mathcal{E}^{\alpha\beta}=s\ind{j}\frac{\pi R\ind{ref}^2}{BB\ind{N}}(g^{\alpha\Psi}g^{\beta\zeta}-g^{\alpha\zeta}g^{\beta\Psi})$$
\subsubsection{$\mathcal{G}$}
As $\textbf{B}\cdot\nabla B=\frac{s\ind{j}}{2\pi J_{\Psi\chi\phi}}\frac{\partial B}{\partial s}$, we have:
$$\mathcal{G}=\frac{R\ind{ref}\textbf{B}\cdot\nabla B}{B^2}=\frac{1}{B}\frac{\partial B}{\partial s}\mathcal{F}$$
\subsubsection{$\mathcal{H}^\alpha$}
To calculate $\mathcal{H}^\alpha$, we need:
$$\nabla Z\cdot \nabla x^\alpha=(\nabla Z)^\alpha=\frac{\partial Z}{\partial \Psi}g^{\Psi\alpha} + \frac{\partial Z}{\partial s}g^{s\alpha} $$ 
%and
%$$|\nabla Z|^2=g_{\alpha\beta}(\nabla Z)_\alpha(\nabla Z)^\beta=\frac{1}{g^{\alpha\beta}}(\nabla Z)^\alpha(\nabla Z)^\beta$$
to obtain:
\begin{align*}
\mathcal{H}^\alpha&=\frac{R\ind{ref}^2}{B\ind{N}v\ind{thref}}\mathbf{\Omega_\perp}\cdot\nabla x^\alpha \\
\mathcal{H}^\alpha&=-s\ind{B}\frac{R\ind{ref}^2}{B\ind{N}v\ind{thref}}\frac{\Omega}{|\nabla Z|}\left( \nabla Z \cdot \nabla x^\alpha - \delta_{\alpha
s}\frac{\mathcal{F}^2}{R\ind{ref}^2}\frac{\partial Z}{\partial s}\right)\\
\end{align*}
\subsubsection{$\mathcal{I}^\alpha$}
Using again $\textbf{B}=2\pi s\ind{j}\nabla\Psi\times\nabla\zeta$, we have:
\begin{align*}
\mathcal{I}^\alpha&=\frac{R\ind{ref}^2\Omega^2}{v\ind{thref}^2B\ind{N}}\frac{R}{B}(\nabla x^\alpha\times\nabla R)\cdot\mathbf{B}\\
\mathcal{I}^\alpha&=s\ind{j}\frac{R\ind{ref}^2\Omega^2}{v\ind{thref}^2B\ind{N}}\frac{2\pi R}{B}\left( g^{\Psi\alpha}\nabla R\cdot\nabla\zeta - g^{\zeta\alpha}\nabla R\cdot\nabla\psi \right)\\
\end{align*}
which can be written as a function of $\mathcal{E}^{\alpha\beta}$:
$$\mathcal{I}^\alpha=\frac{2R\Omega^2}{v\ind{thref}^2}\mathcal{E}^{\alpha\beta}\frac{\partial R}{\partial x^\beta}$$

%*********************************************************************
\part{CHEASE modification}
%*********************************************************************
\section{New quantities}
\subsection{$\beta_{\Psi \chi}\left(\theta=0\right)$}
In CHEASE, the non orthogonality $\beta_{\Psi \chi}$ is defined as 
$$\beta_{\Psi \chi}=\frac{\nabla\Psi\cdot\nabla\chi}{\left|\nabla \Psi \right|^2}$$
The quantity BCHIN, calculated in \texttt{surface.f90} on the CHIN mesh, corresponds to $$\beta_{\Psi \chi}-\beta_{\Psi \chi}\left(\theta=0\right)$$
Even if usually small, $\beta_{\Psi \chi}\left(\theta=0\right)$ is not necessarily zero and has to be calculated. \\
As the line $\theta=0$ is a horizontal line, we have $\nabla\chi\cdot\nabla R|_{\theta=0}=0$, and hence:
$$\beta_{\Psi \chi}\left(\theta=0\right)=\frac{1}{|\nabla \Psi|^2}|\nabla \chi|\frac{\partial{\Psi}}{\partial{Z}}$$
Moreover, we have:
$$\frac{1}{J_{\Psi\chi\phi}^2}=\det\left(g^{ij}\right)=\frac{|\nabla \Psi|^2}{R^2}\left(|\nabla \chi|^2 - \beta_{\Psi \chi}^2 |\nabla \Psi|^2\right)$$
From which we can deduce:
$$\beta_{\Psi \chi}\left(\theta=0\right)=\frac{R}{J_{\Psi\chi\phi}|\nabla\Psi|^3}\frac{\partial{\Psi}}{\partial{Z}}\frac{1}{\sqrt{1-\frac{1}{|\nabla\Psi|^2}\left(\frac{\partial{\Psi}}{\partial Z}\right)^2}}$$
The calculation of $\beta_{\Psi \chi}\left(\theta=0\right)$, variable ZBETCHI0, has been added in \texttt{chipsimetrics.f90}.

\subsection{Partial derivatives of $R$ and $Z$}
From $\nabla R\cdot \nabla \Psi=\frac{\partial \Psi}{\partial R}$ and $\nabla R\cdot \nabla \Psi= \frac{\partial R}{\partial \Psi}|\nabla \Psi|^2 + \frac{\partial R}{\partial \chi}|\nabla \Psi|^2 \beta_{\Psi \chi}$, we can write:
$$\frac{\partial R}{\partial \Psi}= \frac{1}{|\nabla \Psi|^2}\left( \frac{\partial \Psi}{\partial R} - \frac{\partial R}{\partial \chi}|\nabla \Psi|^2 \beta_{\Psi \chi}\right)$$
Then, $\frac{\partial R}{\partial \chi}$ can be calculated from:
$$\frac{\partial R}{\partial \chi}=\frac{\partial \sigma}{\partial \chi}\rho_s\cos{\theta}-\sigma\rho_s\frac{\partial \theta}{\partial \chi}\sin{\theta} +\sigma  \frac{\partial \rho_s}{\partial\theta} \frac{\partial\theta}{\partial\chi}\cos{\theta}$$
and
\begin{align*}
\frac{\partial\sigma}{\partial\chi}&=-\frac{1}{|A|}\frac{\partial\Psi}{\partial\theta}\\
\frac{\partial\theta}{\partial\chi}&=\frac{1}{|A|}\frac{\partial\Psi}{\partial\sigma}\\
\end{align*}
with 
$$|A|=\frac{\partial \Psi}{\partial \sigma}\frac{\partial\chi}{\partial\theta} - \frac{\partial\Psi}{\partial\theta}\frac{\partial\chi}{\partial\sigma}= \frac{R\sigma\rho_s^2}{J_{\Psi\chi\phi}}$$
So finally,
$$\frac{\partial R}{\partial \Psi}=\frac{1}{|\nabla \Psi|^2}\frac{\partial \Psi}{\partial R}-\frac{1}{|A|}\beta_{\Psi \chi}\left( - \frac{\partial \Psi}{\partial \theta}\rho_s\cos{\theta}-\sigma\rho_s\frac{\partial \Psi}{\partial \sigma}\sin{\theta} +\sigma  \frac{\partial \rho_s}{\partial\theta} \frac{\partial\Psi}{\partial\sigma}\cos{\theta}\right)$$
A similar calculation leads to $\frac{\partial Z}{\partial \Psi}$ and $\frac{\partial Z}{\partial \chi}$. All these quantities are computed in \texttt{chipsimetrics.f90} and named ZDRDPSI, ZDRDCHI, ZDZDPSI, ZDZDCHI, respectively.

\subsection{$\mathcal{A}$ and $\partial\mathcal{A}/\partial\Psi$}
The integrals
$$\mathcal{A}(\Psi,\chi)=\int_0^\chi \frac{1}{R^2}\,{\rm d}\tilde{\chi}$$
and 
$$\frac{\partial\mathcal{A}}{\partial \Psi}(\Psi,\chi)=-2\int_0^\chi \frac{1}{R^3}\frac{\partial R}{\partial \Psi}\,{\rm d}\tilde{\chi}$$
are calculated in \texttt{surface.f90} (variables AH and AHPR) using the expression of $\frac{\partial R}{\partial \Psi}$ derived above and performing gaussian integration on the $\Psi=C\expo{te}$ surfaces. A correction (variable AHPRCOR) is applied to $\frac{\partial\mathcal{A}}{\partial \Psi}$ in \texttt{chipsimetrics.f90} to take into account equilibria with non-zero $\beta_{\Psi \chi}\left(\theta=0\right)$.

\section{Output file}
The output file containing the metric elements for the $(\Psi,\zeta,s)$ coordinate system is named \texttt{hamada.dat} and is created in \texttt{hamada.f90}. All quantities are given in MKSA units. The output file content is described below.

\textbf{Scalars}\\
NPSI: number of points for the radial grid\\
NS: number of points for the poloidal grid (specified with NCHI in the CHEASE namelist)\\
R0EXP: reference major radius used in CHEASE, generally taken to be close to the magnetic axis radius for faster convergence (but the actual position is not necessarily known beforehand)\\
B0EXP: reference magnetic field in CHEASE defined as the magnetic field at R0EXP\\
Raxis: magnetic axis radius as calculated by CHEASE
\vspace{0.5cm}

\textbf{Grids}\\
PSI: $\Psi$, the poloidal flux, zero at the magnetic axis, increasing towards the edge. The $\Psi$ grid given in output is equidistant in $\sqrt{\Psi}$ (except if mesh densification is used). The
first point is NOT on the magnetic axis (it is at $\sqrt{\Psi}=0.5*\sqrt{\Psi\ind{\max}}/\mathrm{NPSI}$, while on the magnetic axis $\sqrt{\Psi}=0$). The steps of the grid are
$\sqrt{\Psi\ind{\max}}/\mathrm{NPSI}$, except for the last one wich is half of it to have the last point on the last closed flux surface.\\
S: $s$, as defined in GKW. The grid is equidistant and for a given flux surface $s=0$ is on the low field side, at the height of the magnetic axis. 
\vspace{0.5cm}

\textbf{1D arrays}\\
Rgeom: major radius of the flux surface centre, defined as $(R\ind{max}+R\ind{min})/2$\\
amin: minor radius of the flux surface, defined as $(R\ind{max}-R\ind{min})/2$\\
damindpsi: $\partial \mathrm{amin}/\partial \Psi$\\
d2amindpsi2: $\partial^2 \mathrm{amin}/\partial \Psi^2$\\
bmax: maximum of the magnetic field strength on the flux surface\\
bmin: minimum of the magnetic field strength on the flux surface\\
q: safety factor\\
dqdpsi: $\partial q /\partial \Psi$\\
dqdpsi\_chk: value of $\partial \zeta / \partial \Psi$ at $s=1$ (equal to $\partial q /\partial \Psi$)\\
p: plasma pressure\\
dpdpsi: $\partial p /\partial \Psi$\\
jac: $J_{\Psi\zeta s}$ the jacobian of the $(\Psi,\zeta,s)$ coordinates\\
djacdpsi: $\partial J_{\Psi\zeta s} /\partial \Psi$\\
f: $F=RB\ind{t} = R^2|\mathbf{B}\cdot\nabla\phi|$ (always positive by convention)\\
dfdpsi: $\partial F /\partial \Psi$\\

\textbf{2D arrays}\\
g11: $\nabla \Psi \cdot \nabla \Psi$\\
g12: $\nabla \Psi \cdot \nabla \zeta$\\
g13: $\nabla \Psi \cdot \nabla s$\\
g22: $\nabla \zeta \cdot \nabla \zeta$\\
g23: $\nabla \zeta \cdot \nabla s$\\
g33: $\nabla s \cdot \nabla s$\\
B: the norm of the magnetic field\\
dBdpsi: $\partial B/\partial \Psi$\\
dBds: $\partial B/\partial s$\\
R: major radius of the $(\Psi,s)$ grid points (which describe flux surfaces) \\
Z: height of the $(\Psi,s)$ grid points (which describe flux surfaces) \\
dRdpsi: $\partial R/\partial \Psi$\\
dRds: $\partial R/\partial s$
dZdpsi: $\partial Z/\partial \Psi$\\
dZds: $\partial Z/\partial s$\\
theta: the poloidal angle, zero at $s=0$, increasing anticlockwise and measured from the magnetic axis\\
Ah: $\mathcal{A}$ \\
dAhdpsi: $\partial \mathcal{A}/\partial \Psi$\\ 
dzetadpsi: $\partial \zeta/\partial \Psi$\\ 
dzetadchi: $\partial \zeta/\partial \chi$\\ 

\section{Tests}
The matlab file used to perform the checks can be found in \texttt{/home/camenen/chease/tests/} on \texttt{hal}.
\subsection{Consistency checks}
The following checks have been performed using the outputs stored in \texttt{hamada.dat}
\begin{itemize}
 \item The output LCFS is the same as the input LCFS
 \item The output $FF'$ is the same as the input $FF'$
 \item The partial derivatives computed in matlab on the output grid are quasi-identical (excpet close to the magnetic axis) to the one computed in CHEASE for the following quantities: $q$, $p$, $F$, $J_{\Psi\zeta s}$, $R$, $Z$, $B$ and $\mathcal{A}$.\\
 There is a significant disagreement ($10$ to $20\%$) for the derivatives of $R$, $Z$ and $B$ at $s$ values where the derivatives are almost zero.
 \item $$\frac{1}{J_{\Psi\zeta s}^2}=det\left( g^{ij}\right)_{i,j=\Psi,\zeta,s}$$
 \item $|\nabla R|^2=1$ and $|\nabla Z|^2=1$ 
 \item $B^2=4\pi^2\left[ g^{\Psi\Psi}g^{\zeta\zeta} - (g^{\Psi\zeta})^2\right]$ (validation of $\frac{\partial \zeta}{\partial \chi}$)
 \item $q=F J_{\Psi \chi \phi}\left< \frac{1}{R^2}\right>$, using $\left< \frac{1}{R^2}\right>=\frac{1}{2\pi}\mathcal{A}_{\chi=2\pi}$
 \item The cartesian coordinates $(x,y,z)$ are related to the cylindrical coordinates $(R,Z,\phi)$ by:
\begin{align*}
x&=R\cos\phi\\
y&=Z\sin\phi\\
z&=Z\\
\end{align*}
with $$\phi = 2\pi\left( - \zeta + FJ_{\psi\chi\phi}\int_0^s\frac{1}{R^2}\,\rm{d}s\right)$$
Using the definition $g_{ij}=\frac{\partial x^k}{\partial\xi ^i}\frac{\partial x^k}{\partial\xi ^j}$ with $(x^1,x^2,x^3)=(x,y,z)$ and $(\xi^1,\xi^2,\xi^3)=(\Psi,\zeta,s)$ all the relationships $g^{ik}g_{kj}=\delta_{ij}$ have been checked (validation of $\frac{\partial \zeta}{\partial \psi}$, $\frac{\partial \zeta}{\partial \chi}$ and strong consistency check). Note that:
\begin{align*}
&\left[\frac{\partial \phi}{\partial\psi}\right]_{\zeta, s}=2\pi\left[\frac{\partial \zeta}{\partial\psi}\right]_{\chi, \phi}\\
&\left[\frac{\partial \phi}{\partial s}\right]_{\Psi, \zeta}=4\pi^2\left[\frac{\partial \zeta}{\partial\chi}\right]_{\psi, \phi}\\
&\left[\frac{\partial \phi}{\partial\zeta}\right]_{\psi, s}=-2\pi\\
\end{align*}

\end{itemize}

\subsection{Comparison with LIUQE and Psi-toolbox}
CHEASE is run with the $FF'$ and $p'$ values from LIUQE. The Psi-toolbox is used to compute the metric of the LIUQE equilibrium.
\begin{itemize}
 \item The flux surface description $(R,Z)$ is similar for CHEASE and LIUQE (small difference for the position of the magnetic axis).
 \item For the two codes, $\left| \nabla \Psi\right|$, $\left<\left| \nabla \Psi\right|\right>$, $\left<\frac{1}{R^2}\right>$ and $\mathcal{A}$ are similar (less than $2\%$ difference).
 \item $J_{\Psi \zeta s}^{CHEASE}=\frac{\partial \mathcal{V}}{\partial \Psi}^{LIUQE}$
\end{itemize}

\subsection{Comparison with the low aspect-ratio shifted circular flux surfaces approximation}
CHEASE is run with a circular LCFS and the metric elements are compared to the analytical formulae obtained in the low aspect-ratio approximation for circular flux surfaces. The agreement is relatively good, especially in the plasma center, as expected.

\subsection{Tilted plasmas and $\beta_{\Psi \chi}\left(\theta=0\right)$}
To check the calculation of $\beta_{\Psi \chi}\left(\theta=0\right)$, the input LCFS used for CHEASE has been rotated by $\pi/8$ to have a finite value of $\beta_{\Psi \chi}\left(\theta=0\right)$. The value of $\frac{\partial R}{\partial {\Psi}}$ obtained in CHEASE is then compared to the one directly calculated in matlab using the values of $R$ 
on the $(\Psi, \chi)$ grid. The comparison shows that if the finite $\beta_{\Psi \chi}\left(\theta=0\right)$ correction is not included, there is a significant discrepancy between the values of $\frac{\partial R}{\partial {\Psi}}$ calculated in CHEASE and matlab, whereas the agreement is perfect when the correction is included.
Note that $\beta_{\Psi \chi}\left(\theta=0\right)$ is also involved in the calculation of $g^{\Psi\chi}$ and has a strong impact on this parameter.


\section{List of modified files}
The following files have been modified: \texttt{cotrol.f90}, \texttt{chipsimetrics.f90}, \texttt{g\_2.f90}, \texttt{globals.f90}, \texttt{Makefile}, \texttt{mappin.f90}, \texttt{ogyropsi.f90}, \texttt{stepon.f90}, \texttt{surface.f90}, \texttt{write\_ogyropsi.f90} and \texttt{write\_ogyropsi\_hdf5.f90} \\
The following file has been created:
\texttt{hamada.f90}

%*********************************************************************
\part{GKW modification}
%*********************************************************************
Most of the modifications have been done in the subroutine \texttt{geom\_init\_grids} of the module \texttt{geom.f90} where the calculation of the metric elements and related quantities from the chease output file has been implemented. 
\section{Namelist GEOM}
The namelist GEOM has been extended to include the following variables:
\begin{itemize}
 \item GEOM\_TYPE\\
It is used to specify whether the metric elements and related quantities are computed using the s-alpha model or using the output of an MHD equilibrium solver. At present, two options are implemented: 's-alpha' and 'chease'.
 \item EQFILE\\
It contains the path and name of the chease output file from which the MHD equilibrium related quantities are read when GEOM\_TYPE has the value 'chease'
 \item SIGNB\\
It contains the sign of $\bf{B}\cdot\nabla\phi$
 \item SIGNJ\\
It contains the sign of $\bf{j}\cdot\nabla\phi$, where $\bf{j}$ is the plasma current density.

\end{itemize}
With the 'chease' option, the value of EPS specified in the namelist GEOM is used to find the corresponding $q$ and $\hat{s}$ from the chease output file (i.e. the Q and SHAT given in the namelist are not used). Note that EPS is defined as follow:
$$\mathrm{EPS}=\frac{r}{R\ind{ref}}=\frac{R\ind{max}-R\ind{min}}{2R\ind{axis}}$$
To be consistent, this definition of EPS must be used to calculate the normalised gradients given in the SPECIES namelist
Note that in chease, the radial coordinate is $\Psi$, not $\epsilon$, which means that a factor $\frac{\partial \epsilon}{\partial \Psi}$ has to be added in various places to the quantities loaded from the chease output file.

\section{Grids}
The chease outputs are given on a $(\Psi,s)$ grid. To ensure that every point of the GKW $s$-grid corresponds to a point of the CHEASE $s$-grid, the following conditions are needed:
\begin{itemize}
 \item NS must be odd
 \item $\frac{\mathrm{NS}}{2\mathrm{NPERIOD}-1}$ has to be an integer
 \item $\mathrm{NCHI}=2k\frac{\mathrm{NS}}{2\mathrm{NPERIOD}-1}$ with $k$ being an non-zero integer (NCHI is used in the chease input file to specify the number of points for the $s$-grid)
\end{itemize}
These conditions are checked in the subroutine \texttt{geom\_init\_grids} of the module \texttt{geom.f90}

\section{$\beta$ and $\beta'$}
The BETA parameter specified in the namelist SPCGENERAL is used in the Amp\`ere law and has the following definition:
$$\mathrm{BETA}=\frac{2\mu\ind{0}n\ind{ref}T\ind{ref}}{B\ind{ref}^2}$$
For later use, the variable BETA\_REAL has been created and is loaded from the chease output file. It is defined as:
$$\mathrm{BETA\_REAL}=\frac{2\mu\ind{0}p}{B\ind{ref}^2}$$
where $p$ is the total ``MHD'' plasma pressure (i.e. the pressure solution of the Grad-Shafranov equation) and $B\ind{ref}$ is taken to be $B\ind{axis}$, the magnetic field strength at the magnetic axis position. \\
The value of $\beta'$ is needed to calculate part of the curvature drift, with
$$\beta'=\frac{2\mu\ind{0}}{B^2}\frac{\partial p}{\partial \epsilon}$$
From the chease output, the variable 
$$\mathrm{BETA\_PRIME\_REAL}=\frac{2\mu\ind{0}}{B\ind{axis}^2}\frac{\partial p}{\partial \epsilon}$$
is loaded and $\mathrm{BETA\_PRIME}=\mathrm{BETA\_PRIME\_REAL}$ is then used in \texttt{components.f90}. So we have:
$$\beta'=\mathrm{BETA\_PRIME}\frac{B\ind{axis}^2}{B^2}$$

\section{Output file}
The subroutine \texttt{write\_geom} has been added in the \texttt{diagnostics.f90} module. It writes an output file named \texttt{geom.dat} and containing the quantities related to the MHD equilibrium.

\section{Tests}
\begin{itemize}
 \item Check that $\mathcal{E}^{\epsilon\zeta}=\frac{R\ind{ref}^2B\ind{ref}}{4\pi}\frac{\partial\epsilon}{\partial \Psi}$.
\end{itemize}


\bibliographystyle{plain}
\bibliography{gkwandchease}

\end{document}
