\documentclass[a4paper,10pt]{article}
\usepackage{amssymb}
\usepackage{graphicx}
\graphicspath{{./fig/}}
\usepackage{fullpage}
\usepackage{hyperref} 

\hypersetup{
bookmarks,
plainpages=false,
colorlinks=true,
pdfborder={0 0 0 0},
linkcolor=blue,
citecolor=blue,
urlcolor=blue,
pdfstartview={FitH},
bookmarksopen=false,
unicode
}

\newcommand{\doc}[1]{\href{http://code.google.com/p/gkw/source/browse/trunk/doc/#1}{#1}}
\newcommand{\src}[1]{\href{http://code.google.com/p/gkw/source/browse/trunk/src/#1}{#1}}
\newcommand{\scripts}[1]{\href{http://code.google.com/p/gkw/source/browse/trunk/scripts/#1}{#1}}
\newcommand{\matlab}[1]{\href{http://code.google.com/p/gkw/source/browse/trunk/matlab/#1}{#1}}

\setlength{\textheight}{24cm}

%opening
\title{GKW for Beginners}
\author{F.J. Casson}
\date{11 April 2008.  Last Revised 08 Feb 2011.}
\setlength{\parindent}{0pt}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract} 

This introductory document aims to describe the very basic things a newcomer needs to know in trying to understand or use GKW. These are the things that are (mostly!) obvious to anyone who has worked with the code on any serious level.  To anyone who has worked with another gyrokinetic code many of these things will also be obvious, but there are practicalities specific to GKW.  Therefore this document has to be written and improved by newcomers still in the early stages of learning GKW.  This is still a work in progress, as such you are encouraged to add to and improve it.  

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\tableofcontents
\setlength{\parskip}{8pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{What does the code do?}

The code solves the gyrokinetic Vlasov equation for the evolution of plasma particle distribution functions self consistently with the Maxwell equations in a toroidally symmetric geometry. One velocity dimension of the general Vlasov equation is removed by averaging over the small scale cyclotron (gyro) motion of a charged particle in a magnetic field. Thus the equations are 5 dimensional (3 space, 2 velocity) with the two directions of velocity $v_\perp$ and $v_\parallel$.  Together, the gyrokinetic approximation and flux-tube geometry allow us to simulate microscale tokamak plasma turbulence.

The code can model multiple plasma species, including non linear terms, kinetic electrons, particle collisions, a rotating plasma and arbitrary flux surface geometry (through interface with the CHEASE code). The equations are time integrated forwards using a choice of implicit or explicit methods, but only the explicit schemes parallelise.

The gyrokinetic ordering uses length scale assumptions:
\begin{equation}
R \nabla_\parallel  \approx 1 \qquad 
R \nabla_\perp \approx 1/ \rho_*
\end{equation}
where $\rho_*=\rho_i/R$ . These orderings mean that the model can resolve structures perpendicular to the magnetic field on the scale of the Larmor radius $\rho_i$, whilst perpendicular structures will have a length scale the order of the system (tokamak) size $R$. This ordering is chosen to describe drift wave structures.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where is the code?}

In the SVN repository at googlecode.  Non-members may check out a read-only working copy anonymously over HTTP.

\texttt{svn checkout http://gkw.googlecode.com/svn/trunk/ gkw-read-only}.

If you have not used a content management system before then briefly read: Chapters 1 and 2 of the SVN book (\href{http://svnbook.red-bean.com/en/1.4/index.html}{http://svnbook.red-bean.com/en/1.4/index.html}).  If you have used CVS, SVN is similar but newer and has a couple of differences. Googlecode has a graphical user interface which makes it easy to use many of the features of SVN.

When you check out the code the following folders will be created:
\begin{itemize}
 \item \texttt{/src} contains all the source code for GKW.
 \item \texttt{/doc} contains documentation files. \texttt{/doc/input} contains some sample input files.
 \item \texttt{/config} contains per host build configuration makefiles.
 \item \texttt{/tests} contains a set of test cases we use for testing changes made to the code.
 \item \texttt{/matlab} contains matlab scripts for preparing input files and interpreting output data.
 \item \texttt{/scripts} contains bash scripts that (amongst other things) can test if the code is working as it should or submit multiple jobs.
 \item \texttt{/libs} contains additional libraries that are needed and also provided with gkw for easier compilation.
\end{itemize}

Briefly, the most useful svn commands you will need are:
\begin{verbatim}
 svn status -u -q                   (check for updates and local changes)
 svn up                             (perform update)
 svn diff filename.f90              (see local changes of file.f90)
 svn diff | kompare - -o            (see all local changes, pretty)
 svn log -v --limit 10              (read the latest log messages)
\end{verbatim}
But read the SVN manual for more details.

You will find it useful to define a \texttt{GKW\_HOME} environment variable which is the path to your GKW folder, as this variable is used in some scripts. Adding the following to your \texttt{.bashrc} file will make these things automatic:

\begin{verbatim}
   export GKW_HOME="/path/to/gkw/"
   export PATH="$PATH:${GKW_HOME}/scripts"
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{How do I compile from source?}

The code should compile with default options on your machine by running \texttt{gmake} in the top level GKW directory. \texttt{gmake} will create:
\begin{itemize}
\item \texttt{/obj} folder for all the object files (these can be deleted later)
\item \texttt{/run} folder which will contain the compiled executable. 
\end{itemize}

On many shared machines, GKW makefiles are already configured.  
When you build for the first time on a new machine, a template configuration file will be created for your machine in \textit{./config/machine}.  
The basic configuration will not contain MPI or FFTW (needed for parallel or nonlinear runs).
You can configure FFTW and MPI and other compile options specific to your machine by editing the file \textit{default.mk} in this folder. 
\texttt{gmake clean} will remove all the object files if you want to do a completely fresh compile.

If you need to run nonspectral, implicit or global, before compiling gkw you should first compile the additional libraries in ./libs using the script ./libs/compile\_umf, and configure the GKw config file to link against these, more details on this are in the main manual.  These libraries are not changed by the developers of gkw, so compiling them needs to be done only once.\\

\section{How do I do a run?}

You need to create a file named \texttt{input.dat} in the same directory as the executable.  This file is the input deck containing all the switches which control how the code runs.  There are some sample input files in \texttt{/doc/input}. Also in the \texttt{/doc} folder is an \doc{input.dat.sample} file which should describe all the input variables. Many of the input switches are optional and default values will be used if they are not provided.  The \doc{input.dat.minimum} file shows the variables that \textit{must} be provided.

The code is quite sensitive to bad formatting or typos in \texttt{input.dat}, in which case you will hit a read error as soon as you run the code.  Most of the time if you input a set of incompatible switches the code will tell you and throw you out.

Two key output files created are \texttt{time.dat} which records the (linear) mode growth rate against time, and 
\texttt{fluxes.dat} which records particle fluxes against time.  More details on the other output files are in the main manual.

A few salient points about the input file (many more in the full manual):

\subsection{Species}
There is no default species data, hence you must provide some. The species you input must satisfy quasi-neutrality. This requires you to set ratios of densities and charge appropriately such that the sum over all species $s$ is zero:
\begin{equation}
 \sum_s z_s \cdot dens_s = 0
\end{equation}
\begin{equation}
 \sum_s z_s \cdot dens_s \cdot rln_s= 0
\end{equation}
You can input multiple ion species but only one negatively charged species (electrons) is allowed.  Note that the number of species read in is controlled by the number of species parameter in the 'gridsize' namelist and hence it is possible that later species data you specify will not be read in if this is set incorrectly.  The \texttt{number\_of\_species} input variable does not include adiabatic electrons but the species data for these will still be read if this the case.

\subsection{Timestep and resolution}
The timestep required for stability can vary greatly depending on input parameters or choice of scheme.  In particular kinetic electrons require a timestep $10^{-2}$ smaller.  The question of necessary resolution needed is even harder to answer, in fact it is one of the controversial questions in gyrokinetics.  To a large extent it depends on which physics problem you want to study.  Use the sample provided \doc{input} files to get you started.

\subsection{Parallelisation}
To run parallel (MPI) jobs you need to use \texttt{mpiexec} in an environment where the computer you want to run on knows what MPI is (i.e. it has an mpi controller configured and running). 

The code will parallelise first over the $\mu$ grid and (non-adiabatic) species, and then over the $v_{\parallel}$ and $s$ grid points.  The code will select what it considers to be the most appropriate parallelisation given your input, but this can be overridden.  Nevertheless it is sensible to have in mind a parallelisation setup when you set the gridsize, as the user controls the number of processors the code will be run on and should choose a number that fits with the gridsize. 

\subsection{Modes}
The spacing and location of the modes is determined by the `modes' namelist.   For linear runs, set \texttt{mode\_box=.false., NX=1 and NMOD=1}.  For nonlinear runs, there is a spreadsheet \doc{mode\_box\_calculator.ods} in the documentation folder which should help you determine the mode spacing.

\subsection{Solver Methods}
Safest option:
\begin{verbatim}
METHOD='EXP'
METH=2
\end{verbatim}

\subsection{Batch jobs}
There are also scripts for doing multiple runs. The script \scripts{gkwnlin}  uses a directory structure with batch input files all put in an input folder (with different names), and output files will all be put in relevant folders.  The scripts submit jobs to a queue in the portable batch system (PBS).  If you haven't used PBS before, read a little about how it works.

Typical workflow for a batch job is the following:
\begin{itemize}
\item Compile the GKW executable for the machine you are on. 
\item Run \texttt{gkwnlin -p project\_name} to create a new project directory.
\item Run matlab script \matlab{create\_gkwscan} to make a scan of the given input file and put these in the input folder.  Or if you only need to make a few input files, you can do this manually.
\item Run \texttt{gkwnlin} from the input folder to submit multiple input files to the PBS queue.  Use something like \texttt{gkwnlin -np 8 -h 3 *}.  gkwnlin will take the most recent executable from \texttt{\$GKW\_HOME/run} to run with all the input files you give. 
\item Run \texttt{qstat} to the status of your jobs in the queue and see if they are running yet.
\item After the first loop has completed you should look in the output files ./runs/*/out, to check the job is running ok.
To stop an individual job use \texttt{qdel}, or do \texttt{touch gkw.stop} in its run folder for a clean stop.
\item Go get coffee.

\end{itemize}

These scripts are self documenting on the -options syntax they require.  Type \texttt{gkwnlin} for help, or in matlab \texttt{help create\_gkwscan}.  Note that \texttt{create\_gkwscan} works best if you keep your batch projects runs in \texttt{~/runs/gkw/}; or else reconfigure the matlab script \texttt{gkwpath}.

\section{Geometry and Domain}

\begin{figure}[hb!]
 \begin{center}
 \includegraphics[scale=0.3]{flux_tube.eps}
\caption{Sketch of the flux tube domain with $q=2$, $\epsilon=r/R=0.29$ and $\hat{s}=1$ showing the GKW coordinate directions.} 
 \label{fig.domain}
\end{center}
\end{figure}

\begin{figure}[ht!]
 \begin{center}
 \includegraphics[scale=0.3]{shear.eps}
\caption{Superposition of two perpendicular cross sections of the flux tube separated by one polodial circuit.  The flux tube is sheared as it follows the magnetic shear.  This picture hopes to make clear that each $k_\zeta$ mode must connect to a higher one for a periodic boundary in $s$.  Namely, the difference between connected modes is $\Delta k_\psi = k_\zeta {\partial q \over \partial \psi}$. }
 \label{fig.shear}
\end{center}
\end{figure}

The coordinates used are field aligned Hamada coordinates $(s, \psi, \zeta)$,  in which magnetic field lines are straightened. In these coordinates, the components of the magnetic field are flux functions and $B^\psi = B^\zeta = 0$ so that ${\bf B} \cdot \nabla = B^s {\partial \over \partial s}$.  Whilst $s(\theta,\phi)$ is a coordinate along the field line, $\psi(r)$ is a radial coordinate which describes the flux surfaces and $\zeta(\theta,\phi)$ is a (mostly poloidal) coordinate which lies within a flux surface.  The purpose of these coordinates is to allow separation of the different length scales of the parallel and perpendicular plasma dynamics.

GKW is written with general tensors so that in principle the code can deal with any toroidally symmetric 
equilibrium. This means that all the equations are expressed with the use of general tensors, and 
the metric elements. Currently  the geometry dependent quantities are calculated in the code only for the $\hat s - \alpha$ equilibrium. In addition, GKW can read CHEASE output for arbitrary flux surface configuration. CHEASE takes an input of the shape of the last closed flux surface, a boundary pressure, and a current profile. From these inputs, CHEASE solves the Grad-Shafranov equation to find an equilibria flux surface configurations which it outputs as the poloidal flux. See the documentation file \texttt{gkwandchease.tex} for more information.

GKW is a local gyrokinetic code modelling a flux tube domain, and is spectral (with periodic boundaries) in both directions perpendicular to a field line.  The flux tube domain is bounded by four magnetic field lines, two within each of two adjacent flux surfaces (Fig \ref{fig.domain}). As the domain progresses helically round the torus it is sheared with the magnetic shear.  When one poloidal circuit is completed the flux tube can be connected to itself, but the shear requires the $k_\psi$ modes to be be connected (Fig. \ref{fig.shear}) at the $s$ boundary.

In GKW, computation in the perpendicular directions is done in the wavevector domain for computational efficiency, whilst a finite difference scheme is used in the parallel directions.  In the general formulation, this pseudo-spectral method is more general than assuming an eikonal form for the solution. In our ordering of terms we keep only first order terms in $\rho_*=\rho/R \ll 1$, which makes the two approaches equivalent.  The transformation to field aligned coordinates is mathematically equivalent to the ballooning transform of the eikonal form in this case. The periodicity constraints equate to the coupling of the $k_\psi$ modes described above.  Since the flux tube is sheared, the consequences of magnetic shear is to couple the modes.  For further details see the section \textit{local limit} in \textit{GKW How and Why}.

\section{What is under the bonnet?}

The code is written in Fortran 95 and uses MPI for (optional) parallel implementation.  The code is split into separate Fortran modules in the \texttt{src} folder.  The program runs from \texttt{linart.F90} and then uses the modules as called from there, each of which can recursively call other modules. 
The automatically generated reference dictionary for the code is at \href{http://www.gkw.org.uk/doxygen}{http://www.gkw.org.uk/doxygen} and is a useful way to explore the code.   The file \texttt{program\_flow} in the documentation folder shows the code progression (though it is a little out of date).
If you want to familiarise yourself with inner details of the code, we suggest that after reading the manual, you look at the source files 
\src{linart.f90}, \src{init.f90}, \src{mode.f90}, \src{linear\_terms.F90} and \src{exp\_integration.F90}, in that order.

\begin{itemize}
\item Modules *.F90 are put through the preprocessor mainly for processing \#if blocks for MPI.
\item Modules *.f90 are not put through the preprocessor.
\end{itemize}

\section{Philosophy behind the coding}

\begin{itemize}
 \item The code is designed to make initialisation procedures easy to read.  As such they may not be the most computationally efficient ways of doing things. Only the computationally expensive parts of the code are optimised, other parts are designed to be readable.

 \item The code has a lot built in error reporting with the idea that if you break something, or if something is currently not working, it will tell you.  In addition, the script \scripts{gkw\_run\_tests} will run the code against a set of test cases and report if any changes made have altered the physics output of the code.

 \item The code uses global variables for most things.  This is a compromise with simplicity and readability by reducing the need for functions with many arguments passed to them. 
To avoid confusing problems often associated with global variables the \texttt{use control,only : number of processors} and data hiding within modules limits the use of variables declared in other modules to those explicitly named.

\end{itemize}

\section{Checking in changes to the code}

If you are going to contribute changes to the code, you will need a google account and your username will need to be added to the list of authorised users.
Once this is done, you may check out a authenticated working copy, \texttt{svn checkout https://gkw.googlecode.com/svn/trunk/ gkw --username my.username}.  Full details are at \href{http://code.google.com/p/gkw/source/checkout}{http://code.google.com/p/gkw/source/checkout} when you are signed in with your google account.  From your authenticated working copy, you may make changes that can be checked in.  

Think carefully before you check something in as it will be visible for all the world to see, for ever.  Do not check any code that is not open source.  The repository should only be used for plain text files (and possibly some figures).  Before you check in any changes to the source, you should run the test script \texttt{gkw\_run\_tests} to check you have not broken the code.  This script runs the code under a number of scenarios and checks if the physics output has changed relative to a previously generated reference output.

When checking in (\texttt{svn ci}) you should specify a log message, for which a text editor will open.  This message is stored in the SVN log but will also be emailed to all the GKW developers.   The default text editor can be changed by putting (for example) \texttt{export EDITOR=kwrite} in your \texttt{.bashrc}.

GKW has two mailing lists which you can request to join.
\begin{itemize}
\item \href{http://groups.google.com/group/gkw-updates}{http://groups.google.com/group/gkw-updates}
\item \href{http://groups.google.com/group/gkw-issues}{http://groups.google.com/group/gkw-issues}
\end{itemize}


\section{Where can I get more help?}

The following are other useful GKW resources.

\begin{itemize}
\item The GKW manual at \href{http://www.gkw.org.uk/tikiwiki/Manual}{http://www.gkw.org.uk/tikiwiki/Manual} has a detailed description of the gyrokinetic framework as is implemented in GKW covering the equations, co-ordinate system, and normalisations of the code.  It also includes further details on running the code, input and output files, and the benchmarks.

\item A full listing of all the input variables is provided in \doc{input.dat.sample}

\item An automatically generated reference dictionary for the code is at 
\href{http://www.gkw.org.uk/doxygen}{http://www.gkw.org.uk/doxygen}

\item Browse some of the papers in which GKW has been used to get an idea of what it does.

\href{http://www.gkw.org.uk/tikiwiki/publications}{http://www.gkw.org.uk/tikiwiki/publications}

\item If you find a problem with code, please submit it to the issue tracker: 

\href{http://code.google.com/p/gkw/issues/list}{http://code.google.com/p/gkw/issues/list}

\item Arthur Peeters obviously knows the answer to most GKW questions, but other people working with the code may know the answer to simple questions, just ask.  

Please see \href{http://code.google.com/p/gkw/wiki/Support}{http://code.google.com/p/gkw/wiki/Support}.

\end{itemize}

\end{document}
