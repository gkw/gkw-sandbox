!nmp = 1
!The line above indicates the number of processors this input file is intended to be run on. 
!It only read by humans or by scripts, not by gkw.

!This example input file should be a comprehensive list of possible inputs, some experimental or obsolete.
!Default values are used if no value is provided.
!Some values have no default (or a default which fails) and must be provided to run (see other minimum sample).

!---------------------------------------------------------------------------------------------------------------------
 &CONTROL                                !This namelist is read in control.f90
 order_of_the_scheme = 'fourth_order'        !Scheme is "fourth_order" or "second_order" in space (default='fourth_order')
 parallel_boundary_conditions = 'zero'        !Can be "zero" or "zero_deriv" for 4th order                   (default = 'zero')
 NON_LINEAR = .false.                        !Include the non linear terms, computationally expensive fft        (default = F)
 zonal_adiabatic = .false.,                !If zonal flows corrections included for adiabiatic electrons        (default = F)
 METHOD = 'EXP',                !Time integration "EXP" for explicit (others not supported)                (default = 'EXP')
 METH   = 2,                        !Choice of algorithm for METHOD.
                                  !For EXP 1 = modified mid point, 2 = 4th order RK, 3 = 3rd order scheme (default = 2)
 DTIM   = 1.5e-3                         !Time step size (normalized)                                      (default = 0.005)
 NTIME  = 350,                        !Number of iterations of NAVERAGE.                        (default = 0 will not run)
 NAVERAGE = 100,                !No of timesteps between re-normalisation, data output         (default = 0 will not run)
                                !NTIME * NAVERAGE = total number of timesteps
 nlapar = .false.,                 !True = keep the electromagnetic potential. false = electrostatic        (default = F)
 nlbpar = .false.,                 !True = keep the electromagnetic compression.                            (default = F)
 nlphi = .true.                        !True if the electrostatic potential is kept in the equations                (default = T)
 neoclassics=.false.                !True if the neoclassical terms are kept (broken)                        (default = F)
 collisions = .false.                !Turn on the collision operator                                         (default = F)
 disp_par = 0.3                        !(Hyper) Dissipation coefficient for parallel derivatives.                 (default=0.2)
 disp_vp  = 0.0                        !(Hyper) Dissipation coefficient for parallel velocity space                (default=0.2)
 disp_x   = 0.0                        !(Hyper) dissipation coeffcient in perpendicular x direction                (default=0.0)
 disp_y   = 0.0                        !(Hyper) dissipation coeffcient in perpendicular y direction                (default=0.0)
 lverbose     = .false.         !Switch on verbose code output if it exists anywhere in the code        (default = F)
 vp_trap = 0                      !Integer that determines how the parallel velocity grid is set up.         (default = 0)
                                !If = 0  parallel velocity grid is uniform and constant along the field line 
                                !If = 1  parallel velocity follows the trapping condition (developmental)
 normalized =.true.                !If the distribution function is renormalised every naverage timesteps         (default = T)
 irun = 0.                      !Run number - use for optional sequential numbering of restart files.   (default = 0)
                                !If 0, FDS will be read and overwritten If 1, FDS will be read and FD1 will be written 
                                !If 2, FD1 will be read and FD2 will be written, etc (up to 99) 
 testing = .false.,                !Alows modules to be called in non standard order (obsolete)                 (default = F)
 silent = .false.,                !Reduce screen output (currently only matrix compression)                  (default = F)
 matrix_format = 'complex'        !Only complex is supported (obsolete)                            (default = 'complex')
 nl_dtim_est = .true.            !Use the nonlinear timestep adjustor, slow in big parallel job          (default = T)
                                 !If false, the code will be forced to abort if the timestep is too large.
 dt_min = 0.                     !the code will stop if dtim gets smaller than this value                (default = 0.)
 max_sec = 1                 !Internal program kill time (set lower than walltime)                  (default =-1)
/
!---------------------------------------------------------------------------------------------------------------------
 &GRIDSIZE                        !Namelist read in grid.f90
                                !Default grids sizes of 0 will not run, hence these must be provided.
 NX = 1,                           !Number of radial wave vectors - currently needs to be an odd number for fft
 N_s_grid = 105,                 !Number of grid points along the field line
 NPERIOD = 3,                         !Integer that specifies the length of the field line,
                                !The field line makes 2*nperiod - 1 poloidal turns. For nonlinear, nperiod must be 1. 
                                !Note that for nperiod = 1 and mode_box=true the kx modes are connected 
                                !For other nperiod /= 1 the solution is set to zero at the end of the field line.
 N_mu_grid   = 8,                 !Total number of magnetic moment grid points 
 N_vpar_grid = 64,                 !Number of grid points for parallel velocity (formerly nvpar, which still works)
 NMOD = 1,                        !Total number of binormal modes - do not interact for non-linear runs
 number_of_species = 2                 !Number of species. Note: do not count the adiabatic species 
 vpmax = 3                    !maximum normalized value of the parallel velocity grid            (default = 3.)
 mumax = 4.5                    !maximum normalized value of the mu velocity grid                  (default = 4.5)
 /
!---------------------------------------------------------------------------------------------------------------------
 &MODE                                !Namelist read in mode.f90
 CHIN    = 0.,                         !Poloidal shift of the ballooning transform                        (default = 0)
 KTHRHO  = 1.000000e-01!Poloidal wave vector(s) if mode_box=.false.                        (default = 0 fails)
                                !Only first nmod are used. Does nothing for mode_box=.true.  
                                !For ITG The most unstable mode has kthro ~ 0.5
 mode_box = .false.,                 !Determines if there is a 2D grid of ky,kx. if true use nperiod = 1    (default = F)
                                !If nperiod = 1 and mode box = .true. the kx modes will be coupled.
 krhomax = 1.0,                        !For mode_box, the maximum k_theta rho_i (ky) used                 (default = 0 fails)
                                !nmod>1 modes are equally spaced from 0 to to krhomax
                                !k_perp is evaluated on the low field side of the outboard midplane
                                !rho is evaluted at the magnetic field axis, theta=pi/2
                                !Note that other codes may normalise the thermal velocity differently
                                !Which can result in a factor sqrt(2) greater in gkw k_theta vectors
 ikxspace = 7,                         !Determines the spacing between the different kx modes                (default = 0 fails)
                                !Use to control relative kxmax (radial resolution) and kxmin (lx)
                                !See mode_box_calculator.ods spreadsheet for detail
                                !This integer is called 'p' in the manual, section local limit.
 /
!---------------------------------------------------------------------------------------------------------------------
 &GEOM                                 !Namelist read in geom.F90
 SHAT = 0.1642,                        !Magnetic shear        (not used with geom_type chease)                (default = 1.23e4)
 Q    = 2.0747,                         !Safety factor        (not used with geom_type chease)                (default = 1.23e4)
 EPS  = 0.1498                        !minor radial coordinate / major radius                                (default = 1.23e4)
 GEOM_TYPE = 'chease'          !Switch for the metric: 's-alpha' or 'circ' or 'chease'                        (default = 's-alpha')
 EQFILE='./hamada.GeomComp.dat'          !file containing the chease output, only need for 'chease' mode
 SIGNB = 1,                        !Sign of B.grad_phi, the toroidal component of the magnetic field (default = 1)
 SIGNJ = 1                        !Sign of j.grad_phi, the toroidal component of the plasma current (default = 1)
 EPS_TYPE = 1                    !> Radial coordinate used to specify the chosen FS (chease only)
                                !> 1=eps, 2=rho_psi                                             (default=1)
/
!---------------------------------------------------------------------------------------------------------------------
 &SPCGENERAL                        !This namelist read in components.f90
 beta = 0.000,                        !Plasma beta (not used if nlapar = .false.)                        (default = 0.00)
 adiabatic_electrons = .false.   !Adiabatic electrons (alternative is kinetic electrons)                (default = .false.)
                                !Note that kinetic electrons require a smaller timestep for stability
 amp_init = 1e-3                !Initial amplitude of distribution function                        (default = 1e-3)
 finit = "noise"                !Perturbed distribution function is initialised                 (default = 'cosine')
                                !Allowed are "noise","cosine" (default), "gauss" or "zonal"
                                !'line' (of gaussians), 'island' 'sgauss' (velocity space)
                                !'kxzero' and 'kyzero' for an individual mode only, for testing
                                !Magnetic islands are experimental and under development.
 wstar = 10.0                   !The normalised magnetic island width, used with finit = 'island'  (default = 0.E0)
 Ls    = 2.0                    !In the tearing mode initialisation, Ls is the mode damping length (default = 2.E0)
 isl_rot_freq = 0.1             !The rotation frequency of the magnetic island perturbation        (default = 0.0)
 /
!---------------------------------------------------------------------------------------------------------------------
 !The species must satify quasi-neutrality.
 !This requires you to set ratios of densities and charge appropriately
 !Note that the if number_of_species above is less than the number in the list below, later ones will not be read in
 !If number_of_species is greater than the number in the in the list below, the program will crash.
 !There are no default values for most of the parameters.
 !If there is an adiabatic species, it is the negatively charged species. 

 &SPECIES                         !This namelist read in components.f90
 MASS  = 1.0,                         !Species mass in terms of deuterium mass (=2.013 AMU)
 Z     = 1.0,                         !Species charge. If negative, assumed to be the electrons.
 TEMP  = 1.,                         !Temperature of species scaled by reference temperature - see CPC paper
 DENS  = 1.,                        !Density of species scaled by reference density - see CPC paper
 rlt   = 9,                        !Temperature gradient R/LT
 rln   = 3,                         !Density gradient R/Ln
 uprim = 0.0,                        !Gradient of the toroidal velocity R^2 grad omega / v_thref, toroidal shear flow
 background = 'maxwell'                !Used to define the background distribution (developmental)        (default = 'maxwell')
                                !Can be `maxwell' or 'alpha'(experimental)
 param = 0                        !Only Used if background=`alpha' (experimental)                        (default = 0)
 / 
 &SPECIES 
 MASS  = 2.7777e-4,                 !Yes, these are the electrons
 Z     = -1.0,                         !Only one species may have negative charge. No muons or antiprotons please.
 TEMP  = 1.0, 
 dens  = 1.0,
 rlt   = 9,
 rln   = 3., 
 uprim = 0.0,
 /
!---------------------------------------------------------------------------------------------------------------------
 &ROTATION                        !Optional Namelist read in rotation.F90
 VCOR = 0.0                        !Rotation of the plasma vcor =  Rref Omega / vthref = Omega_N        (default = 0.0)
 / 
!---------------------------------------------------------------------------------------------------------------------
 &COLLISIONS                        !Optional namelist only used if collisions = .true.  Read in collisionop.f90
 rref = 1                        !reference major radius used in collision operator                     (default=1.0) 
 tref = 1                        !reference temperature in units of kev used for the collision operator (default=1.0)
 nref = 1                        !reference density in units 10^19 m^-3 used for the collision operator (default=1.0)
 pitch_angle = .true.           !Switches for the collsion terms, this one is pitch angle scattering   (default = T)
 en_scatter = .true.            !Energy scattering term                                                (default = T)
 friction_coll = .true.         !Collisional friction term                                             (default = T)
 mom_conservation = .false.     !Use the correction to conserve momentum                               (default = T)
 mass_conserve = .true.,        !Forces zero flux boundary on velocity space so no out flow of mass    (default = T)
 freq_override=.false.          !Manually specify ii collision frequency to override ref values above. (default = F)
 coll_freq=6.515E-5             !Ion-ion collision frequency used if freq_override=.true.              (default=0.0)
 /
!---------------------------------------------------------------------------------------------------------------------
 &LINEAR_TERMS_SWITCHES                !Optional namelist read in linear_terms.F90 to switch on /off individual linear terms. 
 lampere = .true.               !Allows time evolution of vector potential                      (default = .true.)
 lpoisson = .true.              !Allows time evolution of electro-static potential              (default = .true.)
 lvpar_grad_df = .true.         !Parallel streaming                                             (default = .true.)
 lvdgradf = .true.              !Effect of drifts on g                                          (default = .true.)
 ltrapdf = .true.               !Particle trapping term                                         (default = .true.)
 lve_grad_fm = .true.           !Source terms                                                   (default = .true.)
 lvd_grad_phi_fm = .true.       !Source terms                                                   (default = .true.)
 lvpgrphi = .true.              !Particle acceleration via E fields                             (default = .true.)
 lg2f_correction = .true.       !Adds electromagnetic correction to f                           (default = .true.)
 lpoisson_zf = .true.           !Zonal flow term in poissons equation                           (default = .true.)
 lneoclassical = .true.         !Neoclassical effects (also require neoclassics in control)     (default = .true.)
 /
