\chapter{\label{ch:eiv}Eigenvalue solver}

%\fancyfoot[L]{$ $Date$ $}
%\fancyfoot[R]{$ $Revision$ $}

Here we give some information on the usage of the eigenvalue solver, implemented
in GKW, as well as some technical details. 
Further information can be found in the presentation of R. Buchholz on the GKW webpages under Talks.

As the system size is to big for a direct (numerical) solution, projection
methods are used.
For a brief introduction, see the {\sc slepc} manual \cite{SLE12} and references therein.

%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------

\section{\label{sec:eivusage}Usage}
First, GKW must be compiled with {\sc slepc/petsc}, see \ref{subsubsec:slepc}
for details.

The easiest way to create an input file for the eigenvalue solver, is the following:
First, set up an input file for use with the exponential time
integration scheme.
As second step then replace \name{METHOD = 'EXP'} with \name{METHOD='EIV'}
in the \name{control} namelist, and  set \name{METH=1} and \name{NAVERAGE=1} (also \name{METH=2} and/or \name{NAVERAGE$>$1} work, but these are at least not faster and usually slower).

The third and final step is to set the parameters for the eigenvalue solver
itself. This is done using the optional namelist \name{eiv_integration}, 
an example of which is given below.
One has to select \name{which_eigenvalues} should be searched for, 
for a full list of the options, see the sample input file.
In addition, you can choose between two methods for extracting eigenvalues,
\code{type_extraction = 'harmonic'}, and \code{type_extraction = 'ritz'}.
The target values  for \code{growthrate} and \code{freq}) are always 
used if \code{type_extraction = 'harmonic'}.  They are also used
if you choose \code{type_extraction = 'ritz'} and \code{which_eigenvalues =  11}
and \code{comparison_routine = 2} ('TARGET_COMPLEX').
% also used for TARGET_REAL / TARGET_IMAGINARY ?
Becuase the harmonic extraction makes a kind of shift-and-invert
transformation, values near the target value have the largest
magnitude, and so target values must always be provided.

Unfortunately it is difficult to say in advance what settings are optimal,
or will guarantee to find the physical eigenmodes of the system.  Suggested
starting points for the eigenvalue settings are given below. 
One rule seems to be valid in general:
If you use harmonic extraction and search for the mode with largest
eigenvalue, you should keep the target frequency \name{freq=0.0}.  
If you don't find an eigenvalue, increase the target \name{growthrate}, 
while if you just find stable high frequency eigenmodes, decrease it.

To share some more details of our experiences, fig. \ref{fig:eivharmonicgrowth}--\ref{fig:eivtargetsa} 
show a scan for two set of parameters.
Figures \ref{fig:eivharmonicgrowth}--\ref{fig:eivharmonicncv} show a scan
for harmonic extraction, in which the eigenvalues with largest real part where
searched. The first one has \name{freq=0}, while \name{growthrate} was varied,
while it is the other way round for the second figure.
The last one shows for two points from the previous scans, a scan over the
size of the subspace (\name{nr\_column\_vec}).
\begin{figure}[htp!]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{HarmonicGrowth}
    \caption{\label{fig:eivharmonicgrowth} Scan over growth rate with
    frequency set to zero. Dashed lines depict negative values of
    \name{growthrate}. Normalization is done with the maximum number of
    iterations for the first/second eigenvalue/total number of iterations,
    respectively.}
  \end{center}
\end{figure}
\begin{figure}[htp!]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{HarmonicFreq}
    \caption{\label{fig:eivharmonicfreq} Scan over frequency with growth rate
      set to zero. Dashed lines depict negative values of \name{freq}. The
      point at $10^{-7}$ actually had zero frequency. Normalization is as for
      fig. \ref{fig:eivharmonicgrowth}. The dashed-dotted line depicts the
      minimum value for the total iterations of fig. \ref{fig:eivharmonicgrowth}
      (only counting points where both unstable eigenmodes are found).}
  \end{center}
\end{figure}
\begin{figure}[htp!]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{HarmonicNcv}
    \caption{\label{fig:eivharmonicncv} Scan over size of subspace, for one
    point of each of the other two scans. First case: \name{growthrate = 0.0},
    freq = 0.45, searching for 5 eigenpairs (default value: 20). Second case: growthrate = 2.50E-3,
    freq = 0.0, searching for 2 eigenpairs (default value: 17).}
  \end{center}
\end{figure}

The input parameters for fig. \ref{fig:eivtargets1}--\ref{fig:eivtargetsa} are
based on the \File{STD_linear_ITG} and \File{simple_TEM} input files found in
\File{doc/input}, with $R/L_{T_i} = 6$, which is near the ITG-TEM transition.
Two unstable eigenmodes are therefore expected.
The fixed parameters for the eigenvalue solver were
\begin{verbatim}
max_iterations = 30000
tolerance = 1.0e-6
type_solver = 'krylovschur'
type_extraction = 'harmonic'
number_eigenvalues = 3
nr_column_vec      = 20
mat_vec_routine    = 1
comparison_routine = 2
which_eigenvalues =  1    ! largest magnitude
\end{verbatim}
We are searching for three eigenvalues for the case that there are more
unstable ones than expected.
Scans were made over the target \name{growthrate} and \name{freq} for the harmonic extraction.
\begin{figure}[htp!]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{Targets}
    \caption{\label{fig:eivtargets1} Scan for harmonic extraction and
      searching for eigenvalue with largest magnitude over the target values.
      Plus signs refer to actual found eigenvalues, crosses to 'conjugated'
      eigenvalues, stars to the projections on the growth rate axis. Color
      of the dots refers to the number of found eigenvalues. Please note, that
      a value of 1(or more) requires that the fastest growing eigenmode was found and
      a value of 2(or more) requires that both fastest growing eigenmodes where found.}
  \end{center}
\end{figure}
\begin{figure}[htp!]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{Targets2}
    \caption{\label{fig:eivtargets2} Scan for harmonic extraction and
      searching for eigenvalue with largest magnitude over the target values.
      Here is scanned around the second eigenvalue.
      For the meaning of the symbols please refer to the caption of fig.
      \ref{fig:eivtargets1}.}
  \end{center}
\end{figure}
\begin{figure}
  \begin{center}[htp!]
    \includegraphics[width=0.8\textwidth]{TargetsA}
    \caption{\label{fig:eivtargetsa} Scan for harmonic extraction and
      searching for eigenvalue with largest magnitude over the target values.
      Scan along \name{freq = 0}, some of the other two scans can also be
      seen.
      For the meaning of the symbols please refer to the caption of fig.
      \ref{fig:eivtargets1}.}
  \end{center}
\end{figure}
Some remarks on the speed: If you just want to find the fastest growing
eigenmode, there is not much variation. About 900-1000 iterations were
needed, if these mode was found. Only for the violet points is a large
difference, here 6000-8000 iterations needed to be performed.
If you are also interested in the second eigenpair, then the situation is
quite samilar. This is found usually 100-150 iterations after the first one in
the case of fig. \ref{fig:eivtargets1}, while for fig. \ref{fig:eivtargets2}
the difference is quite often 0 or 1. The third (stable) eigenvalue then takes about
16-18 times longer.  This could be avoided by setting a suitable value for max_iterations.\\
An exception to this is the range $growthrate=0.75-0.9$ with $freq=0$, the third
eigenvalue was already found after three times the iterations needed for the
second eigenvalue.

The robustness and the speed of the solver are very dependant on the initial condition
vector provided.  We found \name{finit="cosine4"} to be a good choice, but if one is
doing a parameter scan, it can also be useful to restart with the output of the previous
run.  The batch launcher \scripts{gkwnlin} provides a facility (\name{-restart_chain}) for doing 
this.

The output, except for the screen output is only written if the solver finishes,
which means either it finds the requested number of eigenpairs or it hits the
maximum number of iterations.
The output differs in some ways from the output obtained with \name{METHOD = 'EXP'}.
The main difference is that timestep is replaced by mode in files with time
dependent output. For example, the first line in \File{fluxes.dat} will contain
the fluxes determined for the first found eigenmode, the second line those for
the second eigenmode, and so on.
Second, there is an output file \File{eigenvalues.dat} that contains the growth
rate and frequency determined by {\sc slepc} (first column acts as an index).
Also you will notice that not a single \File{parallel.dat} or restart file is produced,
but one for each found eigenmode.

If some eigenvalues are found, you should check that these belong to a
reasonable, physical mode. If
the eigenvalues given by the diagnostics (found in \File{time.dat}) and the one
determined by slepc (found in \File{eigenvalues.dat}) differ this is a sign
(not a necessary one)
that the result is unphysical, but the easiest possibility is to plot the mode eigenfunction.
Note also that some of the eigenvalues found could be physical while others
are not.  Also a warning regarding parallelization: different parallel decompositions 
 might result in different eigenmodes being found, in particular for stable eigenmodes.

%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------

\section{\label{sec:eivtechdetails}Technical details}

The eigenvalue solver in {\sc gkw} is mainly a interface for a library that does
this job. So far the library {\sc slepc} (which relies on {\sc petsc}) is used (and
there are currently no plans to add support for/change support to other libraries).

Most parameters of the \name{eiv_integration} namelist act as wrapper for the
corresponding parameter in {\sc slepc}.
Via switches the call to the corresponding routine with the wanted argument is done.

Note that no matrix is formed by {\sc slepc}, as the so called matrix-free approach
is used. This means instead of a matrix we just define the action of the matrix
on a vector via a function, provided to {\sc slepc}.
%This approach is used as the actual matrix is not available.
Depending on the settings in the input file, either \code{exp_integration} 
(\name{mat_vec_routine = 1}) is used
to determine the result of the matrix vector multiplication or just the subroutine
\code{calculate_rhs} (\name{mat_vec_routine = 2}).

%-----------------------------------------------------------------------------

\subsection{\label{subsec:eivproblem}The actual eigenvalue problem}
In general an eigenvalue problem is formulated as
\be
  M x = \lambda x, \label{eq:eivgen}
\ee
where $A$ is the operator of the problem and $\lambda$ is the (complex)
eigenvalue.
The \code{exp_integration} part, solves
\be
  f_{t+1} = f_{t} + dt A f_{t} + dt B \phi
\ee
\be
  0 = C f_{t} + D \phi
\ee
with the state vector $f$, four matrices $A$, $B$, $C$ and $D$ and $\phi$
represents all the fields that may be present in the simulation.
Note that \code{fdisi} = (f, phi).
It principle it is possible to solve the last equation for $\phi$ and insert
it into the former to get
\be
  f_{t+1} = f{t} + dt A f_{t} - dt B D^{-1} C f_{t}.
\ee
The disadvantage of this approach would be, that the inversion of the matrix $D$
and the matrix matrix multiplication most probably would destroy the sparsity
structure of the complete operator. As is clear, this would make the determination
of the eigenvalues much more expensive.
For this reason the matrix is not explicitly formed.
If the subroutine \code{calculate_rhs} is used instead, then the $f_{t}$ term
vanishes, but otherwise the computation remains unchanged, thus also in this
case a shell matrix is used.

%-----------------------------------------------------------------------------

\subsection{\label{subsec:eivtrafoeivgf}Transformation between eigenvalue and growth rate/frequency}
The transformation between the eigenvalue computed by SLEPC and the growth
rate and frequency from the code can be derived as follows.
Recall again the general form of the eigenvalue problem \eqref{eq:eivgen}, in
comparison to this, the eigenvalue problem in GKW is
\be
  l f =  (1 + dt N)^n f
\ee
where $N$ sums up the parts from A and B and $n$ is \code{naverage}.
Identifying $M$ and $(1 + dt N)^n$ results in
\be
  \lambda = (1 + dt N) = l
\ee
\be
  \ln \lambda = n \ln l
\ee

\be
  \frac{\ln \lambda}{n dt} = \frac{\ln l}{n dt}
\ee
The right hand side of these equation is the same relation as between the
change in the norm and the growth rate in the code. We take this as reason to
propose this as general transformation between the eigenvalue determined by slepc and
the growth rate/frequency got by gkw (another reason is it works).

\newcommand{\pder}[2]{\frac{\partial#1}{\partial#2}}
The transformation for the tolerance $a$ is
\bee
	a_{gkw} & = & \sqrt{\left( \pder{\lambda_{gkw}}{\lambda_{slepc}} |\lambda_{slepc}|a_{slepc}\right)^2} \\
	& = & \sqrt{\left( \pder{(\log(\lambda_{slepc})/(n_{average} \Delta t))}{\lambda_{slepc}} |\lambda_{slepc}|a_{slepc}\right)^2} \\
	& = & \sqrt{\left( \frac{1}{n_{average} \Delta t} a_{slepc}\right)^2} \\
	& = & \frac{a_{slepc}}{n_{average} \Delta t}
\eee


%-----------------------------------------------------------------------------

\subsection{\label{subsec:eivuserroutines}User defined routines}
There are two points at which SLEPC uses subroutines provided by GKW.
Number one is -- as should be clear -- the matrix vector product.
As the already existing capabilities of gkw should be used, these first copy
the input vector into \variable{fdisi}, the necessary operations are
performed and then the resulting new state is copied to the output vector.
``Necessary operations'' is so far either using \code{explicit_integration} or
\code{calculate_rhs}, both from the \code{exp_integration} module.

Ordering the eigenvalues, is the second place where a (slepc-)user defined
routine is used. These ordering functions determine which part of the
spectrum is wanted. Besides of some predefined orderings, slepc also offers
the possibility to let us decide, by telling which of two eigenvalues we like
the most.

\section{\label{sec:eivfaq}FAQ}

\paragraph{What is a good starting point for the parameters?}

For the Ritz extraction (here for an electromagnetic case with kinetic electrons), 
to find the two most unstable modes you could try
\begin{verbatim}
&CONTROL
 fac_dtim_est=0.5, naverage= 1, method= 'EIV', meth= 1,
 read_file = .true., irun = 2   ! to restart from previous dominant mode
 ! using gkwnlin -restart_chain
 /
&eiv_integration  ! using Ritz extraction method, the default
 max_iterations = 90000 ! avoid looking for stable eigenmodes
 tolerance=4.0e-4
 number_eigenvalues=2
 which_eigenvalues=3    ! LARGEST REAL (most unstable modes)
 luse_initial_value = .true.
 /
&SPCGENERAL
 finit="cosine4"   ! or restart from previous run in parameter scan
 /
\end{verbatim}
It appears that the timestep cannot go above that required for explicit time integration
but that setting a very small timestep makes the convergence take longer.
Setting the tolerance too small can also cause convergence to take longer, and can even prevent
the correct modes from being found.


For the harmonic extraction, you can try the following.
\begin{verbatim}
 &CONTROL
 fac_dtim_est=0.5; naverage= 1, method= 'EIV', meth= 1,
 /
 &eiv_integration
 max_iterations = 50000
 tolerance = 1.0e-4
 type_solver = 'krylovschur'
 type_extraction = 'harmonic'
 number_eigenvalues = 2
 nr_column_vec      = 20
 mat_vec_routine    = 1
 comparison_routine = 2
 which_eigenvalues  = 11 ! look for eigenvalues near the target
 growthrate =  0.5000 
 freq       =  0.0000
 luse_initial_value = .true.
 /
 &SPCGENERAL
 finit="cosine4"
 /
 \end{verbatim}
If you increase \code{number_eigenvalues} you may also have to increase
\code{nr_column_vec}.

\paragraph{Does \code{nr_column_vec = 0} (letting 'PETSC_DECIDE') work?}

From the scan done over the size of the subspace (see fig. \ref{fig:eivharmonicncv}
we would expect, that it should work well for at least \code{number_eigenvalues = 1-2},
while for bigger values it might be not optimal.\\
Experience shows that even for \code{number_eigenvalues = 15} eigenvalues have
been found.

%\paragraph{How Do I find the number of iterations at which an eigenmode was found?}
%If you have searched only for one eigenmode you can find this information in
%the screen output. For more modes you have to set \code{CONTROL::lverbose = .true.},
%then the iteration along with the eigenvalue is written to the screen.
% FJC Is this correct? I can't see a screen output for iterations per eigenmode 
% anywhere in the current code.

% \paragraph{Is there a way to output eigenmodes as they are found, instead
% of waiting until the end or the maximum number of iterations ?}

% \paragraph{Doesn't the optimal nr_colum_vec increase with the problem size ?}
